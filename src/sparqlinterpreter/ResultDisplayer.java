package sparqlinterpreter;

import patternmatching.What;
import triple.ItemList;

public class ResultDisplayer {
    
    ItemList<What> variableList;

    public ResultDisplayer() {
        this.variableList = new ItemList<>();
    }
    
    public void addVariable(What variable){
        this.variableList.add(variable);
    }
    
}
