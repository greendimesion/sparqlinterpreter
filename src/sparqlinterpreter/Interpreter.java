package sparqlinterpreter;

import patternmatching.Query;
import patternmatching.QueryOperand;
import patternmatching.Term;
import patternmatching.Variable;
import patternmatching.What;
import sparqltree.SparqlSemanticTree;

public class Interpreter {

    private SparqlSemanticTree semanticTree;
    private PatternBuilder patternBuilder;
    private ResultDisplayer resultDisplayer;
    private int index;

    public Interpreter() {
        this.semanticTree = null;
        this.patternBuilder = null;
        this.resultDisplayer = null;
        this.index = 0;
    }

    public void executeQuery(String sparqlQuery) {
        this.semanticTree = new SparqlSemanticTree(sparqlQuery);
        this.patternBuilder = new PatternBuilder();
        this.resultDisplayer = new ResultDisplayer();
        analizequery();
    }

    private void analizequery() {
        String token = this.semanticTree.getValue(this.index);
        this.index++;
        switch (token) {
            case "SELECT":
                analizeSelectClause();
                break;
            default:
        }
    }

    private void analizeSelectClause() {
        String token = this.semanticTree.getValue(this.index);
        while (semanticTree.isVariable(index)) {
            this.resultDisplayer.addVariable(new What(token));
            this.index++;
        }
        analizeWhereClause();
    }

    private void analizeWhereClause() {
        if (semanticTree.getValue(index).equals("WHERE"))this.index++;
        this.index++;
        analizeGroupGraphPattern();
    }

    private void analizeGroupGraphPattern() {
        while (!(semanticTree.getValue(index).equals("}"))) {
            if (isTripletBlock()) createQuery();
            if (semanticTree.isFunction(index))functionCall();
            if (semanticTree.getValue(index).equals(".")) this.index++;
        }
    }

    private boolean isTripletBlock() {
        return semanticTree.isTripletMember(index) || (semanticTree.isVariable(index));
    }

    private void functionCall() {
        String token = this.semanticTree.getValue(this.index);
        this.index++;
        switch (token) {
            case "FILTER":
                
                break;
            case "MINUS":
                
                break;
            case "OPTIONAL":
                
                break;
            default:
        }
    }
    
    private Query createQuery() {
        QueryOperand subject = getQueryOperand();
        QueryOperand predicate = getQueryOperand();
        QueryOperand object = getQueryOperand();
        return new Query(subject, predicate, object);
    }

    private QueryOperand getQueryOperand() {
        if (this.semanticTree.isVariable(this.index)) {
            return new Variable(this.semanticTree.getValue(this.index++));
        }
        return new Term(this.semanticTree.getValue(this.index++));
    }
}
