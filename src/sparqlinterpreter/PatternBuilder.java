package sparqlinterpreter;

import patternmatching.Constant;
import patternmatching.Pattern;
import patternmatching.PatternOperand;
import patternmatching.Query;
import patternmatching.QueryOperand;
import patternmatching.Term;
import patternmatching.Variable;
import patternmatching.What;
import triple.ItemList;

public class PatternBuilder {
       
    ItemList<Pattern> patternList;

    public PatternBuilder() {
        this.patternList = new ItemList<>();
    }
    
    public void addPattern(Pattern pattern){
        patternList.add(pattern);
    }

    public ItemList<Pattern> getPatternList() {
        return patternList;
    }
    
    public void createPattern(Query query){
        patternList.add(queryToPattern(query));
    }

    private Pattern queryToPattern(Query query) {
        return new Pattern(toPatternOperand(query.getSubject()), toPatternOperand(query.getPredicate()), toPatternOperand(query.getObject()));
    }
    
    private PatternOperand toPatternOperand(QueryOperand queryOperand) {
        if (queryOperand instanceof Variable)return new What(((Variable)queryOperand).getName());
        return new Constant(Long.parseLong(((Term)queryOperand).getText()));
    }
    
    
}
