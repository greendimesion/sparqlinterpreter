package results;

import triple.ItemList;

public class ResultsTable {
    
    private final ItemList<ResultColumn> columns;

    public ResultsTable() {
        this.columns = new ItemList<>();
    }

    public void addVariable(String variableName, ResultType type) {
        this.columns.add(new ResultColumn(variableName, type));
    }
    
    public void setResults(String variableName, ItemList<Long> result){
        getColumn(variableName).addResults(result);
    }
    
    public ResultType getType(String variableName){
        return getColumn(variableName).getType();
    }
    
    public ItemList<Long> getResults(String variableName){
        return getColumn(variableName).getRows();
    }
    
    private ResultColumn getColumn(String variableName){
        for (int i = 0; i < this.columns.size(); i++) {
            if (this.columns.get(i).getHeader().equals(variableName))
                return this.columns.get(i);
        }
        return null;
    }
}
