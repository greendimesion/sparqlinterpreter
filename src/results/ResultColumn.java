package results;

import triple.ItemList;

public class ResultColumn {
    
    private final String header;
    private final ResultType type;
    private final ItemList<Long> rows;

    public ResultColumn(String header, ResultType type) {
        this.header = header;
        this.type = type;
        this.rows = new ItemList<>();
    }

    public String getHeader() {
        return header;
    }

    public ResultType getType() {
        return type;
    }

    public void addResults(ItemList<Long> result) {
        this.rows.addAll(result);
    }

    public ItemList<Long> getRows() {
        return rows;
    }
    
}
