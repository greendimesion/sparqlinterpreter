package patternmatching;

public class Pattern  {
    private final PatternOperand subject;
    private final PatternOperand predicate;
    private final PatternOperand object;

    public Pattern(PatternOperand subject, PatternOperand predicate, PatternOperand object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public PatternOperand getSubject() {
        return subject;
    }

    public PatternOperand getPredicate() {
        return predicate;
    }

    public PatternOperand getObject() {
        return object;
    }
    
    public int getOrder() {
        return getOrder(subject) + getOrder(predicate) + getOrder(object);
    }
    
    private static int getOrder(PatternOperand operand) {
        return (operand instanceof What) ? 1 : 0;
    }
     
    
}
