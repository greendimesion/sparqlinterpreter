package patternmatching;

public class Term extends QueryOperand{
    
    private final String term;

    public Term(String term) {
        this.term = term;
    }
    
    public String getText(){
        return this.term;
    }
    
}
