package patternmatching;

public class Query {
    
    private final QueryOperand subject;
    private final QueryOperand predicate;
    private final QueryOperand object;

    public Query(QueryOperand subject, QueryOperand predicate, QueryOperand object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public QueryOperand getSubject() {
        return subject;
    }

    public QueryOperand getPredicate() {
        return predicate;
    }

    public QueryOperand getObject() {
        return object;
    }
    
    
}
