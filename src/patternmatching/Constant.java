package patternmatching;

public class Constant extends PatternOperand {
    
    private final long id;

    public Constant(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
    
}
