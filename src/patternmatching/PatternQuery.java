package patternmatching;

import triple.Triple;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class PatternQuery extends Thread {

    private final Pattern pattern;
    private final ArrayList<Listener> listenerList;
    private final HashMap<String, ConstantSet> sets;
    private boolean result;
    

    public PatternQuery(Pattern pattern) {
        this.pattern = pattern;
        this.listenerList = new ArrayList<>();
        this.sets = new HashMap<>();
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void addListener(Listener listener) {
        listenerList.add(listener);
    }

    @Override
    public void run() {
        execute();
        notifiyListeners();
    }

    public boolean getResult() {
        return result;
    }

    public ConstantSet getSet(String name) {
        return sets.get(name);
    }

    protected abstract boolean checkPattern();

    protected abstract ConstantSet getSubjects();

    protected abstract ConstantSet getPredicates();

    protected abstract ConstantSet getObjects();

    private void execute() {
        switch (pattern.getOrder()) {
            case 0:
                this.result = checkPattern();
                break;
            case 1:
                if (isWhat(pattern.getSubject())) {
                    What what = (What) pattern.getSubject();
                    createSet(what);
                    sets.put(what.getName(), getSubjects());
                    if (sets.get(what.getName()).size() > 0) {
                        this.result = true;
                        return;
                    }
                } else if (isWhat(pattern.getPredicate())) {
                    What what = (What) pattern.getPredicate();
                    createSet(what);
                    sets.put(what.getName(), getPredicates());
                    if (sets.get(what.getName()).size() > 0) {
                        this.result = true;
                        return;
                    }
                } else if (isWhat(pattern.getObject())) {
                    What what = (What) pattern.getObject();
                    createSet(what);
                    sets.put(what.getName(), getObjects());
                    if (sets.get(what.getName()).size() > 0) {
                        this.result = true;
                        return;
                    }
                }
                this.result = false;
                break;
            default:
                this.result = false;
        }
    }

    private void notifiyListeners() {
        for (Listener listener : listenerList)
            listener.finished(this);
    }

    private void createSet(What what) {
        this.sets.put(what.getName(), new ConstantSet());
    }

    protected Triple getPatternTriple() {
        return new Triple(getPatternSubject(), (int) getPatternPredicate(), getPatternObject());
    }

    protected long getPatternSubject() {
        return getConstantId(pattern.getSubject());
    }

    protected long getPatternPredicate() {
        return getConstantId(pattern.getPredicate());
    }

    protected long getPatternObject() {
        return getConstantId(pattern.getObject());
    }

    private static long getConstantId(PatternOperand operand) {
        return (operand instanceof Constant) ? ((Constant) operand).getId() : -1;
    }

    private static boolean isWhat(PatternOperand operand) {
        return operand instanceof What;
    }
    
    public interface Listener {

        public void finished(PatternQuery patternQuery);
    }

}