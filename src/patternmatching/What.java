package patternmatching;

public class What extends PatternOperand{
    
    private final String name;

    public What(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}

