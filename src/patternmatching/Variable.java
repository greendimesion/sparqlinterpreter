package patternmatching;

public class Variable extends QueryOperand{

    private final String name;

    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
