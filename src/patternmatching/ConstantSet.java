package patternmatching;

import java.util.ArrayList;

public class ConstantSet extends PatternOperand {
    
    private final ArrayList<Long> items;

    public ConstantSet(long... ids) {
        this.items = new ArrayList<>();
    }
    
    public int size() {
        return items.size();
    }
    
    public long get(int index) {
        return items.get(index);
    }
      
    public void add(long... idList) {
        for (long id : idList) this.items.add(id);
    }
    
    public void add(ArrayList<Long> set) {
        this.items.addAll(set);
    }
    
}
