
package triple;

import java.util.Iterator;

public class ItemList <T> implements Iterable <T> {
    private static final int EXTENSION = 1024;
    private T[] list;
    private int size;

    public ItemList(){
        this.list = (T[]) new Object[EXTENSION];
        this.size = 0;
    }

    public T get(int index) {
        if (index>= list.length) return null;
        return list[index];
    }

    public int size(){
        return size;
    }
    
    public void add(T item) {
        if (size == list.length) expand();
        list[size++] = item;
    }
    
    public void addAll(ItemList<T> itemList){
        for (T item: itemList) 
            add(item);
    }
    
    private void expand() {
        T[] extension = (T[]) new Object[this.list.length + EXTENSION];
        System.arraycopy(this.list, 0, extension, 0, this.list.length); 
        this.list = extension;
    }
    
    private void removeByIndex(int index) {
        for (int i = 0, j = 0; i < size(); i++) {
            if (i == index) continue;
            this.list[j++] = this.list[i];
        }
        this.size--;
    }
    
    public void remove(int index) {
        removeByIndex(index);
    }

    @Override
    public Iterator <T> iterator() {
        return new ItemListIterator<>();
    }

    private class ItemListIterator<O> implements Iterator<T> {
        
        private int index;

        public ItemListIterator() {
            this.index = 0;
        }

        @Override
        public boolean hasNext() {
            return (index < size);
        }

        @Override
        public T next() {
            return get(index++);
        }

        @Override
        public void remove() {
            removeByIndex(index);
        }

        
    }
    
}
