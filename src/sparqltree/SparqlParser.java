// $ANTLR 3.0.1 C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g 2013-10-25 12:50:59
package sparqltree;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class SparqlParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "IRI_REF", "PNAME_NS", "INTEGER", "NIL", "VAR1", "VAR2", "LANGTAG", "DECIMAL", "DOUBLE", "INTEGER_POSITIVE", "DECIMAL_POSITIVE", "DOUBLE_POSITIVE", "INTEGER_NEGATIVE", "DECIMAL_NEGATIVE", "DOUBLE_NEGATIVE", "STRING_LITERAL1", "STRING_LITERAL2", "PNAME_LN", "BLANK_NODE_LABEL", "ANON", "PN_CHARS", "PN_PREFIX", "PN_LOCAL", "VARNAME", "PN_CHARS_BASE", "DIGIT", "EXPONENT", "ECHAR", "STRING_LITERAL_LONG1", "STRING_LITERAL_LONG2", "WS", "PN_CHARS_U", "'BASE'", "'PREFIX'", "'SELECT'", "'DISTINCT'", "'REDUCED'", "'('", "'AS'", "')'", "'*'", "'CONSTRUCT'", "'WHERE'", "'{'", "'}'", "'DESCRIBE'", "'ASK'", "'FROM'", "'NAMED'", "'GROUP'", "'BY'", "'HAVING'", "'ORDER'", "'ASC'", "'DESC'", "'LIMIT'", "'OFFSET'", "'VALUES'", "'.'", "'OPTIONAL'", "'GRAPH'", "'SERVICE'", "'SILENT'", "'BIND'", "'UNDEF'", "'MINUS'", "'UNION'", "'FILTER'", "','", "';'", "'a'", "'|'", "'/'", "'^'", "'?'", "'+'", "'!'", "'['", "']'", "'||'", "'&&'", "'='", "'!='", "'<'", "'>'", "'<='", "'>='", "'-'", "'STR'", "'LANG'", "'LANGMATCHES'", "'DATATYPE'", "'BOUND'", "'IRI'", "'URI'", "'BNODE'", "'RAND'", "'ABS'", "'CEIL'", "'FLOOR'", "'ROUND'", "'CONCAT'", "'STRLEN'", "'UCASE'", "'LCASE'", "'ENCODE_FOR_URI'", "'CONTAINS'", "'STRSTARTS'", "'STRENDS'", "'STRBEFORE'", "'STRAFTER'", "'YEAR'", "'MONTH'", "'DAY'", "'HOURS'", "'MINUTES'", "'SECONDS'", "'TIMEZONE'", "'TZ'", "'NOW'", "'UUID'", "'STRUUID'", "'MD5'", "'SHA1'", "'SHA256'", "'SHA384'", "'SHA512'", "'COALESCE'", "'IF'", "'STRLANG'", "'STRDT'", "'sameTerm'", "'isIRI'", "'isURI'", "'isBLANK'", "'isLITERAL'", "'isNUMERIC'", "'REGEX'", "'SUBSTR'", "'REPLACE'", "'EXISTS'", "'NOT'", "'COUNT'", "'SUM'", "'MIN'", "'MAX'", "'AVG'", "'SAMPLE'", "'GROUP_CONCAT'", "'SEPARATOR'", "'^^'", "'true'", "'false'"
    };
    public static final int INTEGER=6;
    public static final int IRI_REF=4;
    public static final int EXPONENT=30;
    public static final int INTEGER_NEGATIVE=16;
    public static final int BLANK_NODE_LABEL=22;
    public static final int PN_LOCAL=26;
    public static final int PNAME_NS=5;
    public static final int PNAME_LN=21;
    public static final int EOF=-1;
    public static final int ECHAR=31;
    public static final int VARNAME=27;
    public static final int ANON=23;
    public static final int WS=34;
    public static final int STRING_LITERAL_LONG2=33;
    public static final int PN_CHARS_BASE=28;
    public static final int NIL=7;
    public static final int DECIMAL=11;
    public static final int VAR1=8;
    public static final int STRING_LITERAL_LONG1=32;
    public static final int VAR2=9;
    public static final int DECIMAL_NEGATIVE=17;
    public static final int PN_PREFIX=25;
    public static final int INTEGER_POSITIVE=13;
    public static final int DECIMAL_POSITIVE=14;
    public static final int STRING_LITERAL2=20;
    public static final int STRING_LITERAL1=19;
    public static final int PN_CHARS=24;
    public static final int DOUBLE=12;
    public static final int PN_CHARS_U=35;
    public static final int DIGIT=29;
    public static final int DOUBLE_NEGATIVE=18;
    public static final int DOUBLE_POSITIVE=15;
    public static final int LANGTAG=10;

        public SparqlParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g"; }


    public static class query_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start query
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:13:1: query : prologue ( selectQuery | constructQuery | describeQuery | askQuery ) valuesClause ;
    public final query_return query() throws RecognitionException {
        query_return retval = new query_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        prologue_return prologue1 = null;

        selectQuery_return selectQuery2 = null;

        constructQuery_return constructQuery3 = null;

        describeQuery_return describeQuery4 = null;

        askQuery_return askQuery5 = null;

        valuesClause_return valuesClause6 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:5: ( prologue ( selectQuery | constructQuery | describeQuery | askQuery ) valuesClause )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:7: prologue ( selectQuery | constructQuery | describeQuery | askQuery ) valuesClause
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_prologue_in_query38);
            prologue1=prologue();
            _fsp--;

            adaptor.addChild(root_0, prologue1.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:16: ( selectQuery | constructQuery | describeQuery | askQuery )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 38:
                {
                alt1=1;
                }
                break;
            case 45:
                {
                alt1=2;
                }
                break;
            case 49:
                {
                alt1=3;
                }
                break;
            case 50:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("14:16: ( selectQuery | constructQuery | describeQuery | askQuery )", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:18: selectQuery
                    {
                    pushFollow(FOLLOW_selectQuery_in_query42);
                    selectQuery2=selectQuery();
                    _fsp--;

                    adaptor.addChild(root_0, selectQuery2.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:32: constructQuery
                    {
                    pushFollow(FOLLOW_constructQuery_in_query46);
                    constructQuery3=constructQuery();
                    _fsp--;

                    adaptor.addChild(root_0, constructQuery3.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:49: describeQuery
                    {
                    pushFollow(FOLLOW_describeQuery_in_query50);
                    describeQuery4=describeQuery();
                    _fsp--;

                    adaptor.addChild(root_0, describeQuery4.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:65: askQuery
                    {
                    pushFollow(FOLLOW_askQuery_in_query54);
                    askQuery5=askQuery();
                    _fsp--;

                    adaptor.addChild(root_0, askQuery5.getTree());

                    }
                    break;

            }

            pushFollow(FOLLOW_valuesClause_in_query58);
            valuesClause6=valuesClause();
            _fsp--;

            adaptor.addChild(root_0, valuesClause6.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end query

    public static class prologue_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prologue
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:17:1: prologue : ( baseDecl | prefixDecl )* ;
    public final prologue_return prologue() throws RecognitionException {
        prologue_return retval = new prologue_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        baseDecl_return baseDecl7 = null;

        prefixDecl_return prefixDecl8 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:5: ( ( baseDecl | prefixDecl )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:7: ( baseDecl | prefixDecl )*
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:7: ( baseDecl | prefixDecl )*
            loop2:
            do {
                int alt2=3;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==36) ) {
                    alt2=1;
                }
                else if ( (LA2_0==37) ) {
                    alt2=2;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:9: baseDecl
            	    {
            	    pushFollow(FOLLOW_baseDecl_in_prologue75);
            	    baseDecl7=baseDecl();
            	    _fsp--;

            	    adaptor.addChild(root_0, baseDecl7.getTree());

            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:20: prefixDecl
            	    {
            	    pushFollow(FOLLOW_prefixDecl_in_prologue79);
            	    prefixDecl8=prefixDecl();
            	    _fsp--;

            	    adaptor.addChild(root_0, prefixDecl8.getTree());

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end prologue

    public static class baseDecl_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start baseDecl
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:21:1: baseDecl : 'BASE' IRI_REF ;
    public final baseDecl_return baseDecl() throws RecognitionException {
        baseDecl_return retval = new baseDecl_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal9=null;
        Token IRI_REF10=null;

        Object string_literal9_tree=null;
        Object IRI_REF10_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:22:5: ( 'BASE' IRI_REF )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:22:7: 'BASE' IRI_REF
            {
            root_0 = (Object)adaptor.nil();

            string_literal9=(Token)input.LT(1);
            match(input,36,FOLLOW_36_in_baseDecl97); 
            string_literal9_tree = (Object)adaptor.create(string_literal9);
            adaptor.addChild(root_0, string_literal9_tree);

            IRI_REF10=(Token)input.LT(1);
            match(input,IRI_REF,FOLLOW_IRI_REF_in_baseDecl99); 
            IRI_REF10_tree = (Object)adaptor.create(IRI_REF10);
            adaptor.addChild(root_0, IRI_REF10_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end baseDecl

    public static class prefixDecl_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prefixDecl
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:25:1: prefixDecl : 'PREFIX' PNAME_NS IRI_REF ;
    public final prefixDecl_return prefixDecl() throws RecognitionException {
        prefixDecl_return retval = new prefixDecl_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal11=null;
        Token PNAME_NS12=null;
        Token IRI_REF13=null;

        Object string_literal11_tree=null;
        Object PNAME_NS12_tree=null;
        Object IRI_REF13_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:26:5: ( 'PREFIX' PNAME_NS IRI_REF )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:26:7: 'PREFIX' PNAME_NS IRI_REF
            {
            root_0 = (Object)adaptor.nil();

            string_literal11=(Token)input.LT(1);
            match(input,37,FOLLOW_37_in_prefixDecl114); 
            string_literal11_tree = (Object)adaptor.create(string_literal11);
            adaptor.addChild(root_0, string_literal11_tree);

            PNAME_NS12=(Token)input.LT(1);
            match(input,PNAME_NS,FOLLOW_PNAME_NS_in_prefixDecl116); 
            PNAME_NS12_tree = (Object)adaptor.create(PNAME_NS12);
            adaptor.addChild(root_0, PNAME_NS12_tree);

            IRI_REF13=(Token)input.LT(1);
            match(input,IRI_REF,FOLLOW_IRI_REF_in_prefixDecl118); 
            IRI_REF13_tree = (Object)adaptor.create(IRI_REF13);
            adaptor.addChild(root_0, IRI_REF13_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end prefixDecl

    public static class selectQuery_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start selectQuery
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:29:1: selectQuery : selectClause ( datasetClause )* whereClause solutionModifier ;
    public final selectQuery_return selectQuery() throws RecognitionException {
        selectQuery_return retval = new selectQuery_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        selectClause_return selectClause14 = null;

        datasetClause_return datasetClause15 = null;

        whereClause_return whereClause16 = null;

        solutionModifier_return solutionModifier17 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:30:5: ( selectClause ( datasetClause )* whereClause solutionModifier )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:30:7: selectClause ( datasetClause )* whereClause solutionModifier
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_selectClause_in_selectQuery136);
            selectClause14=selectClause();
            _fsp--;

            adaptor.addChild(root_0, selectClause14.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:30:20: ( datasetClause )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==51) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:30:20: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_selectQuery138);
            	    datasetClause15=datasetClause();
            	    _fsp--;

            	    adaptor.addChild(root_0, datasetClause15.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            pushFollow(FOLLOW_whereClause_in_selectQuery141);
            whereClause16=whereClause();
            _fsp--;

            adaptor.addChild(root_0, whereClause16.getTree());
            pushFollow(FOLLOW_solutionModifier_in_selectQuery143);
            solutionModifier17=solutionModifier();
            _fsp--;

            adaptor.addChild(root_0, solutionModifier17.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end selectQuery

    public static class subSelect_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start subSelect
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:33:1: subSelect : selectClause whereClause solutionModifier valuesClause ;
    public final subSelect_return subSelect() throws RecognitionException {
        subSelect_return retval = new subSelect_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        selectClause_return selectClause18 = null;

        whereClause_return whereClause19 = null;

        solutionModifier_return solutionModifier20 = null;

        valuesClause_return valuesClause21 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:34:5: ( selectClause whereClause solutionModifier valuesClause )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:34:7: selectClause whereClause solutionModifier valuesClause
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_selectClause_in_subSelect161);
            selectClause18=selectClause();
            _fsp--;

            adaptor.addChild(root_0, selectClause18.getTree());
            pushFollow(FOLLOW_whereClause_in_subSelect163);
            whereClause19=whereClause();
            _fsp--;

            adaptor.addChild(root_0, whereClause19.getTree());
            pushFollow(FOLLOW_solutionModifier_in_subSelect165);
            solutionModifier20=solutionModifier();
            _fsp--;

            adaptor.addChild(root_0, solutionModifier20.getTree());
            pushFollow(FOLLOW_valuesClause_in_subSelect167);
            valuesClause21=valuesClause();
            _fsp--;

            adaptor.addChild(root_0, valuesClause21.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end subSelect

    public static class selectClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start selectClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:37:1: selectClause : 'SELECT' ( 'DISTINCT' | 'REDUCED' )? ( ( var | ( '(' expression 'AS' var ')' ) )+ | '*' ) ;
    public final selectClause_return selectClause() throws RecognitionException {
        selectClause_return retval = new selectClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal22=null;
        Token set23=null;
        Token char_literal25=null;
        Token string_literal27=null;
        Token char_literal29=null;
        Token char_literal30=null;
        var_return var24 = null;

        expression_return expression26 = null;

        var_return var28 = null;


        Object string_literal22_tree=null;
        Object set23_tree=null;
        Object char_literal25_tree=null;
        Object string_literal27_tree=null;
        Object char_literal29_tree=null;
        Object char_literal30_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:5: ( 'SELECT' ( 'DISTINCT' | 'REDUCED' )? ( ( var | ( '(' expression 'AS' var ')' ) )+ | '*' ) )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:7: 'SELECT' ( 'DISTINCT' | 'REDUCED' )? ( ( var | ( '(' expression 'AS' var ')' ) )+ | '*' )
            {
            root_0 = (Object)adaptor.nil();

            string_literal22=(Token)input.LT(1);
            match(input,38,FOLLOW_38_in_selectClause182); 
            string_literal22_tree = (Object)adaptor.create(string_literal22);
            adaptor.addChild(root_0, string_literal22_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:16: ( 'DISTINCT' | 'REDUCED' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=39 && LA4_0<=40)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
                    {
                    set23=(Token)input.LT(1);
                    if ( (input.LA(1)>=39 && input.LA(1)<=40) ) {
                        input.consume();
                        adaptor.addChild(root_0, adaptor.create(set23));
                        errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse =
                            new MismatchedSetException(null,input);
                        recoverFromMismatchedSet(input,mse,FOLLOW_set_in_selectClause184);    throw mse;
                    }


                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:44: ( ( var | ( '(' expression 'AS' var ')' ) )+ | '*' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=VAR1 && LA6_0<=VAR2)||LA6_0==41) ) {
                alt6=1;
            }
            else if ( (LA6_0==44) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("38:44: ( ( var | ( '(' expression 'AS' var ')' ) )+ | '*' )", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:46: ( var | ( '(' expression 'AS' var ')' ) )+
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:46: ( var | ( '(' expression 'AS' var ')' ) )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=3;
                        int LA5_0 = input.LA(1);

                        if ( ((LA5_0>=VAR1 && LA5_0<=VAR2)) ) {
                            alt5=1;
                        }
                        else if ( (LA5_0==41) ) {
                            alt5=2;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:48: var
                    	    {
                    	    pushFollow(FOLLOW_var_in_selectClause199);
                    	    var24=var();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, var24.getTree());

                    	    }
                    	    break;
                    	case 2 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:54: ( '(' expression 'AS' var ')' )
                    	    {
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:54: ( '(' expression 'AS' var ')' )
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:56: '(' expression 'AS' var ')'
                    	    {
                    	    char_literal25=(Token)input.LT(1);
                    	    match(input,41,FOLLOW_41_in_selectClause205); 
                    	    char_literal25_tree = (Object)adaptor.create(char_literal25);
                    	    adaptor.addChild(root_0, char_literal25_tree);

                    	    pushFollow(FOLLOW_expression_in_selectClause207);
                    	    expression26=expression();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, expression26.getTree());
                    	    string_literal27=(Token)input.LT(1);
                    	    match(input,42,FOLLOW_42_in_selectClause209); 
                    	    string_literal27_tree = (Object)adaptor.create(string_literal27);
                    	    adaptor.addChild(root_0, string_literal27_tree);

                    	    pushFollow(FOLLOW_var_in_selectClause211);
                    	    var28=var();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, var28.getTree());
                    	    char_literal29=(Token)input.LT(1);
                    	    match(input,43,FOLLOW_43_in_selectClause213); 
                    	    char_literal29_tree = (Object)adaptor.create(char_literal29);
                    	    adaptor.addChild(root_0, char_literal29_tree);


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:91: '*'
                    {
                    char_literal30=(Token)input.LT(1);
                    match(input,44,FOLLOW_44_in_selectClause222); 
                    char_literal30_tree = (Object)adaptor.create(char_literal30);
                    adaptor.addChild(root_0, char_literal30_tree);


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end selectClause

    public static class constructQuery_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start constructQuery
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:41:1: constructQuery : 'CONSTRUCT' ( constructTemplate ( datasetClause )* whereClause solutionModifier | ( datasetClause )* 'WHERE' '{' ( triplesTemplate )? '}' solutionModifier ) ;
    public final constructQuery_return constructQuery() throws RecognitionException {
        constructQuery_return retval = new constructQuery_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal31=null;
        Token string_literal37=null;
        Token char_literal38=null;
        Token char_literal40=null;
        constructTemplate_return constructTemplate32 = null;

        datasetClause_return datasetClause33 = null;

        whereClause_return whereClause34 = null;

        solutionModifier_return solutionModifier35 = null;

        datasetClause_return datasetClause36 = null;

        triplesTemplate_return triplesTemplate39 = null;

        solutionModifier_return solutionModifier41 = null;


        Object string_literal31_tree=null;
        Object string_literal37_tree=null;
        Object char_literal38_tree=null;
        Object char_literal40_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:5: ( 'CONSTRUCT' ( constructTemplate ( datasetClause )* whereClause solutionModifier | ( datasetClause )* 'WHERE' '{' ( triplesTemplate )? '}' solutionModifier ) )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:7: 'CONSTRUCT' ( constructTemplate ( datasetClause )* whereClause solutionModifier | ( datasetClause )* 'WHERE' '{' ( triplesTemplate )? '}' solutionModifier )
            {
            root_0 = (Object)adaptor.nil();

            string_literal31=(Token)input.LT(1);
            match(input,45,FOLLOW_45_in_constructQuery239); 
            string_literal31_tree = (Object)adaptor.create(string_literal31);
            adaptor.addChild(root_0, string_literal31_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:19: ( constructTemplate ( datasetClause )* whereClause solutionModifier | ( datasetClause )* 'WHERE' '{' ( triplesTemplate )? '}' solutionModifier )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==47) ) {
                alt10=1;
            }
            else if ( (LA10_0==46||LA10_0==51) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("42:19: ( constructTemplate ( datasetClause )* whereClause solutionModifier | ( datasetClause )* 'WHERE' '{' ( triplesTemplate )? '}' solutionModifier )", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:21: constructTemplate ( datasetClause )* whereClause solutionModifier
                    {
                    pushFollow(FOLLOW_constructTemplate_in_constructQuery243);
                    constructTemplate32=constructTemplate();
                    _fsp--;

                    adaptor.addChild(root_0, constructTemplate32.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:39: ( datasetClause )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==51) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:39: datasetClause
                    	    {
                    	    pushFollow(FOLLOW_datasetClause_in_constructQuery245);
                    	    datasetClause33=datasetClause();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, datasetClause33.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    pushFollow(FOLLOW_whereClause_in_constructQuery248);
                    whereClause34=whereClause();
                    _fsp--;

                    adaptor.addChild(root_0, whereClause34.getTree());
                    pushFollow(FOLLOW_solutionModifier_in_constructQuery250);
                    solutionModifier35=solutionModifier();
                    _fsp--;

                    adaptor.addChild(root_0, solutionModifier35.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:85: ( datasetClause )* 'WHERE' '{' ( triplesTemplate )? '}' solutionModifier
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:85: ( datasetClause )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==51) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:85: datasetClause
                    	    {
                    	    pushFollow(FOLLOW_datasetClause_in_constructQuery254);
                    	    datasetClause36=datasetClause();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, datasetClause36.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    string_literal37=(Token)input.LT(1);
                    match(input,46,FOLLOW_46_in_constructQuery257); 
                    string_literal37_tree = (Object)adaptor.create(string_literal37);
                    adaptor.addChild(root_0, string_literal37_tree);

                    char_literal38=(Token)input.LT(1);
                    match(input,47,FOLLOW_47_in_constructQuery259); 
                    char_literal38_tree = (Object)adaptor.create(char_literal38);
                    adaptor.addChild(root_0, char_literal38_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:112: ( triplesTemplate )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( ((LA9_0>=IRI_REF && LA9_0<=VAR2)||(LA9_0>=DECIMAL && LA9_0<=ANON)||LA9_0==41||LA9_0==81||(LA9_0>=155 && LA9_0<=156)) ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:112: triplesTemplate
                            {
                            pushFollow(FOLLOW_triplesTemplate_in_constructQuery261);
                            triplesTemplate39=triplesTemplate();
                            _fsp--;

                            adaptor.addChild(root_0, triplesTemplate39.getTree());

                            }
                            break;

                    }

                    char_literal40=(Token)input.LT(1);
                    match(input,48,FOLLOW_48_in_constructQuery264); 
                    char_literal40_tree = (Object)adaptor.create(char_literal40);
                    adaptor.addChild(root_0, char_literal40_tree);

                    pushFollow(FOLLOW_solutionModifier_in_constructQuery266);
                    solutionModifier41=solutionModifier();
                    _fsp--;

                    adaptor.addChild(root_0, solutionModifier41.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end constructQuery

    public static class describeQuery_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start describeQuery
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:45:1: describeQuery : 'DESCRIBE' ( ( varOrIRIref )+ | '*' ) ( datasetClause )* ( whereClause )? solutionModifier ;
    public final describeQuery_return describeQuery() throws RecognitionException {
        describeQuery_return retval = new describeQuery_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal42=null;
        Token char_literal44=null;
        varOrIRIref_return varOrIRIref43 = null;

        datasetClause_return datasetClause45 = null;

        whereClause_return whereClause46 = null;

        solutionModifier_return solutionModifier47 = null;


        Object string_literal42_tree=null;
        Object char_literal44_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:5: ( 'DESCRIBE' ( ( varOrIRIref )+ | '*' ) ( datasetClause )* ( whereClause )? solutionModifier )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:7: 'DESCRIBE' ( ( varOrIRIref )+ | '*' ) ( datasetClause )* ( whereClause )? solutionModifier
            {
            root_0 = (Object)adaptor.nil();

            string_literal42=(Token)input.LT(1);
            match(input,49,FOLLOW_49_in_describeQuery283); 
            string_literal42_tree = (Object)adaptor.create(string_literal42);
            adaptor.addChild(root_0, string_literal42_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:18: ( ( varOrIRIref )+ | '*' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=IRI_REF && LA12_0<=PNAME_NS)||(LA12_0>=VAR1 && LA12_0<=VAR2)||LA12_0==PNAME_LN) ) {
                alt12=1;
            }
            else if ( (LA12_0==44) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("46:18: ( ( varOrIRIref )+ | '*' )", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:20: ( varOrIRIref )+
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:20: ( varOrIRIref )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( ((LA11_0>=IRI_REF && LA11_0<=PNAME_NS)||(LA11_0>=VAR1 && LA11_0<=VAR2)||LA11_0==PNAME_LN) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:20: varOrIRIref
                    	    {
                    	    pushFollow(FOLLOW_varOrIRIref_in_describeQuery287);
                    	    varOrIRIref43=varOrIRIref();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, varOrIRIref43.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:35: '*'
                    {
                    char_literal44=(Token)input.LT(1);
                    match(input,44,FOLLOW_44_in_describeQuery292); 
                    char_literal44_tree = (Object)adaptor.create(char_literal44);
                    adaptor.addChild(root_0, char_literal44_tree);


                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:41: ( datasetClause )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==51) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:41: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_describeQuery296);
            	    datasetClause45=datasetClause();
            	    _fsp--;

            	    adaptor.addChild(root_0, datasetClause45.getTree());

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:56: ( whereClause )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=46 && LA14_0<=47)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:56: whereClause
                    {
                    pushFollow(FOLLOW_whereClause_in_describeQuery299);
                    whereClause46=whereClause();
                    _fsp--;

                    adaptor.addChild(root_0, whereClause46.getTree());

                    }
                    break;

            }

            pushFollow(FOLLOW_solutionModifier_in_describeQuery302);
            solutionModifier47=solutionModifier();
            _fsp--;

            adaptor.addChild(root_0, solutionModifier47.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end describeQuery

    public static class askQuery_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start askQuery
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:49:1: askQuery : 'ASK' ( datasetClause )* whereClause solutionModifier ;
    public final askQuery_return askQuery() throws RecognitionException {
        askQuery_return retval = new askQuery_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal48=null;
        datasetClause_return datasetClause49 = null;

        whereClause_return whereClause50 = null;

        solutionModifier_return solutionModifier51 = null;


        Object string_literal48_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:50:5: ( 'ASK' ( datasetClause )* whereClause solutionModifier )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:50:7: 'ASK' ( datasetClause )* whereClause solutionModifier
            {
            root_0 = (Object)adaptor.nil();

            string_literal48=(Token)input.LT(1);
            match(input,50,FOLLOW_50_in_askQuery317); 
            string_literal48_tree = (Object)adaptor.create(string_literal48);
            adaptor.addChild(root_0, string_literal48_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:50:13: ( datasetClause )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==51) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:50:13: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_askQuery319);
            	    datasetClause49=datasetClause();
            	    _fsp--;

            	    adaptor.addChild(root_0, datasetClause49.getTree());

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            pushFollow(FOLLOW_whereClause_in_askQuery322);
            whereClause50=whereClause();
            _fsp--;

            adaptor.addChild(root_0, whereClause50.getTree());
            pushFollow(FOLLOW_solutionModifier_in_askQuery324);
            solutionModifier51=solutionModifier();
            _fsp--;

            adaptor.addChild(root_0, solutionModifier51.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end askQuery

    public static class datasetClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start datasetClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:53:1: datasetClause : 'FROM' ( defaultGraphClause | namedGraphClause ) ;
    public final datasetClause_return datasetClause() throws RecognitionException {
        datasetClause_return retval = new datasetClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal52=null;
        defaultGraphClause_return defaultGraphClause53 = null;

        namedGraphClause_return namedGraphClause54 = null;


        Object string_literal52_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:5: ( 'FROM' ( defaultGraphClause | namedGraphClause ) )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:7: 'FROM' ( defaultGraphClause | namedGraphClause )
            {
            root_0 = (Object)adaptor.nil();

            string_literal52=(Token)input.LT(1);
            match(input,51,FOLLOW_51_in_datasetClause339); 
            string_literal52_tree = (Object)adaptor.create(string_literal52);
            adaptor.addChild(root_0, string_literal52_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:14: ( defaultGraphClause | namedGraphClause )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=IRI_REF && LA16_0<=PNAME_NS)||LA16_0==PNAME_LN) ) {
                alt16=1;
            }
            else if ( (LA16_0==52) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("54:14: ( defaultGraphClause | namedGraphClause )", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:16: defaultGraphClause
                    {
                    pushFollow(FOLLOW_defaultGraphClause_in_datasetClause343);
                    defaultGraphClause53=defaultGraphClause();
                    _fsp--;

                    adaptor.addChild(root_0, defaultGraphClause53.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:37: namedGraphClause
                    {
                    pushFollow(FOLLOW_namedGraphClause_in_datasetClause347);
                    namedGraphClause54=namedGraphClause();
                    _fsp--;

                    adaptor.addChild(root_0, namedGraphClause54.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end datasetClause

    public static class defaultGraphClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start defaultGraphClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:57:1: defaultGraphClause : sourceSelector ;
    public final defaultGraphClause_return defaultGraphClause() throws RecognitionException {
        defaultGraphClause_return retval = new defaultGraphClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        sourceSelector_return sourceSelector55 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:58:5: ( sourceSelector )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:58:7: sourceSelector
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_sourceSelector_in_defaultGraphClause364);
            sourceSelector55=sourceSelector();
            _fsp--;

            adaptor.addChild(root_0, sourceSelector55.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end defaultGraphClause

    public static class namedGraphClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start namedGraphClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:61:1: namedGraphClause : 'NAMED' sourceSelector ;
    public final namedGraphClause_return namedGraphClause() throws RecognitionException {
        namedGraphClause_return retval = new namedGraphClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal56=null;
        sourceSelector_return sourceSelector57 = null;


        Object string_literal56_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:62:5: ( 'NAMED' sourceSelector )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:62:7: 'NAMED' sourceSelector
            {
            root_0 = (Object)adaptor.nil();

            string_literal56=(Token)input.LT(1);
            match(input,52,FOLLOW_52_in_namedGraphClause382); 
            string_literal56_tree = (Object)adaptor.create(string_literal56);
            adaptor.addChild(root_0, string_literal56_tree);

            pushFollow(FOLLOW_sourceSelector_in_namedGraphClause384);
            sourceSelector57=sourceSelector();
            _fsp--;

            adaptor.addChild(root_0, sourceSelector57.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end namedGraphClause

    public static class sourceSelector_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start sourceSelector
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:65:1: sourceSelector : iriRef ;
    public final sourceSelector_return sourceSelector() throws RecognitionException {
        sourceSelector_return retval = new sourceSelector_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        iriRef_return iriRef58 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:66:5: ( iriRef )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:66:7: iriRef
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_iriRef_in_sourceSelector402);
            iriRef58=iriRef();
            _fsp--;

            adaptor.addChild(root_0, iriRef58.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end sourceSelector

    public static class whereClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start whereClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:69:1: whereClause : ( 'WHERE' )? groupGraphPattern ;
    public final whereClause_return whereClause() throws RecognitionException {
        whereClause_return retval = new whereClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal59=null;
        groupGraphPattern_return groupGraphPattern60 = null;


        Object string_literal59_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:70:5: ( ( 'WHERE' )? groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:70:7: ( 'WHERE' )? groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:70:7: ( 'WHERE' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==46) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:70:7: 'WHERE'
                    {
                    string_literal59=(Token)input.LT(1);
                    match(input,46,FOLLOW_46_in_whereClause420); 
                    string_literal59_tree = (Object)adaptor.create(string_literal59);
                    adaptor.addChild(root_0, string_literal59_tree);


                    }
                    break;

            }

            pushFollow(FOLLOW_groupGraphPattern_in_whereClause423);
            groupGraphPattern60=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern60.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end whereClause

    public static class solutionModifier_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start solutionModifier
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:73:1: solutionModifier : ( groupClause )? ( havingClause )? ( orderClause )? ( limitOffsetClauses )? ;
    public final solutionModifier_return solutionModifier() throws RecognitionException {
        solutionModifier_return retval = new solutionModifier_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        groupClause_return groupClause61 = null;

        havingClause_return havingClause62 = null;

        orderClause_return orderClause63 = null;

        limitOffsetClauses_return limitOffsetClauses64 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:5: ( ( groupClause )? ( havingClause )? ( orderClause )? ( limitOffsetClauses )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:7: ( groupClause )? ( havingClause )? ( orderClause )? ( limitOffsetClauses )?
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:7: ( groupClause )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==53) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:7: groupClause
                    {
                    pushFollow(FOLLOW_groupClause_in_solutionModifier438);
                    groupClause61=groupClause();
                    _fsp--;

                    adaptor.addChild(root_0, groupClause61.getTree());

                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:20: ( havingClause )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==55) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:20: havingClause
                    {
                    pushFollow(FOLLOW_havingClause_in_solutionModifier441);
                    havingClause62=havingClause();
                    _fsp--;

                    adaptor.addChild(root_0, havingClause62.getTree());

                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:34: ( orderClause )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==56) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:34: orderClause
                    {
                    pushFollow(FOLLOW_orderClause_in_solutionModifier444);
                    orderClause63=orderClause();
                    _fsp--;

                    adaptor.addChild(root_0, orderClause63.getTree());

                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:47: ( limitOffsetClauses )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=59 && LA21_0<=60)) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:47: limitOffsetClauses
                    {
                    pushFollow(FOLLOW_limitOffsetClauses_in_solutionModifier447);
                    limitOffsetClauses64=limitOffsetClauses();
                    _fsp--;

                    adaptor.addChild(root_0, limitOffsetClauses64.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end solutionModifier

    public static class groupClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start groupClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:77:1: groupClause : 'GROUP' 'BY' ( groupCondition )+ ;
    public final groupClause_return groupClause() throws RecognitionException {
        groupClause_return retval = new groupClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal65=null;
        Token string_literal66=null;
        groupCondition_return groupCondition67 = null;


        Object string_literal65_tree=null;
        Object string_literal66_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:78:5: ( 'GROUP' 'BY' ( groupCondition )+ )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:78:7: 'GROUP' 'BY' ( groupCondition )+
            {
            root_0 = (Object)adaptor.nil();

            string_literal65=(Token)input.LT(1);
            match(input,53,FOLLOW_53_in_groupClause463); 
            string_literal65_tree = (Object)adaptor.create(string_literal65);
            adaptor.addChild(root_0, string_literal65_tree);

            string_literal66=(Token)input.LT(1);
            match(input,54,FOLLOW_54_in_groupClause465); 
            string_literal66_tree = (Object)adaptor.create(string_literal66);
            adaptor.addChild(root_0, string_literal66_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:78:20: ( groupCondition )+
            int cnt22=0;
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>=IRI_REF && LA22_0<=PNAME_NS)||(LA22_0>=VAR1 && LA22_0<=VAR2)||LA22_0==PNAME_LN||LA22_0==41||(LA22_0>=92 && LA22_0<=152)) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:78:20: groupCondition
            	    {
            	    pushFollow(FOLLOW_groupCondition_in_groupClause467);
            	    groupCondition67=groupCondition();
            	    _fsp--;

            	    adaptor.addChild(root_0, groupCondition67.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt22 >= 1 ) break loop22;
                        EarlyExitException eee =
                            new EarlyExitException(22, input);
                        throw eee;
                }
                cnt22++;
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end groupClause

    public static class groupCondition_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start groupCondition
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:81:1: groupCondition : ( builtInCall | functionCall | '(' expression ( 'AS' var )? ')' | var );
    public final groupCondition_return groupCondition() throws RecognitionException {
        groupCondition_return retval = new groupCondition_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal70=null;
        Token string_literal72=null;
        Token char_literal74=null;
        builtInCall_return builtInCall68 = null;

        functionCall_return functionCall69 = null;

        expression_return expression71 = null;

        var_return var73 = null;

        var_return var75 = null;


        Object char_literal70_tree=null;
        Object string_literal72_tree=null;
        Object char_literal74_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:5: ( builtInCall | functionCall | '(' expression ( 'AS' var )? ')' | var )
            int alt24=4;
            switch ( input.LA(1) ) {
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
                {
                alt24=1;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt24=2;
                }
                break;
            case 41:
                {
                alt24=3;
                }
                break;
            case VAR1:
            case VAR2:
                {
                alt24=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("81:1: groupCondition : ( builtInCall | functionCall | '(' expression ( 'AS' var )? ')' | var );", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:7: builtInCall
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_builtInCall_in_groupCondition483);
                    builtInCall68=builtInCall();
                    _fsp--;

                    adaptor.addChild(root_0, builtInCall68.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:21: functionCall
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_functionCall_in_groupCondition487);
                    functionCall69=functionCall();
                    _fsp--;

                    adaptor.addChild(root_0, functionCall69.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:36: '(' expression ( 'AS' var )? ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal70=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_groupCondition491); 
                    char_literal70_tree = (Object)adaptor.create(char_literal70);
                    adaptor.addChild(root_0, char_literal70_tree);

                    pushFollow(FOLLOW_expression_in_groupCondition493);
                    expression71=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression71.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:51: ( 'AS' var )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==42) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:53: 'AS' var
                            {
                            string_literal72=(Token)input.LT(1);
                            match(input,42,FOLLOW_42_in_groupCondition497); 
                            string_literal72_tree = (Object)adaptor.create(string_literal72);
                            adaptor.addChild(root_0, string_literal72_tree);

                            pushFollow(FOLLOW_var_in_groupCondition499);
                            var73=var();
                            _fsp--;

                            adaptor.addChild(root_0, var73.getTree());

                            }
                            break;

                    }

                    char_literal74=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_groupCondition504); 
                    char_literal74_tree = (Object)adaptor.create(char_literal74);
                    adaptor.addChild(root_0, char_literal74_tree);


                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:71: var
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_var_in_groupCondition508);
                    var75=var();
                    _fsp--;

                    adaptor.addChild(root_0, var75.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end groupCondition

    public static class havingClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start havingClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:85:1: havingClause : 'HAVING' ( havingCondition )+ ;
    public final havingClause_return havingClause() throws RecognitionException {
        havingClause_return retval = new havingClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal76=null;
        havingCondition_return havingCondition77 = null;


        Object string_literal76_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:86:5: ( 'HAVING' ( havingCondition )+ )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:86:7: 'HAVING' ( havingCondition )+
            {
            root_0 = (Object)adaptor.nil();

            string_literal76=(Token)input.LT(1);
            match(input,55,FOLLOW_55_in_havingClause523); 
            string_literal76_tree = (Object)adaptor.create(string_literal76);
            adaptor.addChild(root_0, string_literal76_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:86:16: ( havingCondition )+
            int cnt25=0;
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>=IRI_REF && LA25_0<=PNAME_NS)||LA25_0==PNAME_LN||LA25_0==41||(LA25_0>=92 && LA25_0<=152)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:86:16: havingCondition
            	    {
            	    pushFollow(FOLLOW_havingCondition_in_havingClause525);
            	    havingCondition77=havingCondition();
            	    _fsp--;

            	    adaptor.addChild(root_0, havingCondition77.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt25 >= 1 ) break loop25;
                        EarlyExitException eee =
                            new EarlyExitException(25, input);
                        throw eee;
                }
                cnt25++;
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end havingClause

    public static class havingCondition_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start havingCondition
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:89:1: havingCondition : constraint ;
    public final havingCondition_return havingCondition() throws RecognitionException {
        havingCondition_return retval = new havingCondition_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        constraint_return constraint78 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:90:5: ( constraint )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:90:7: constraint
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_constraint_in_havingCondition541);
            constraint78=constraint();
            _fsp--;

            adaptor.addChild(root_0, constraint78.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end havingCondition

    public static class orderClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start orderClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:93:1: orderClause : 'ORDER' 'BY' ( orderCondition )+ ;
    public final orderClause_return orderClause() throws RecognitionException {
        orderClause_return retval = new orderClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal79=null;
        Token string_literal80=null;
        orderCondition_return orderCondition81 = null;


        Object string_literal79_tree=null;
        Object string_literal80_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:94:5: ( 'ORDER' 'BY' ( orderCondition )+ )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:94:7: 'ORDER' 'BY' ( orderCondition )+
            {
            root_0 = (Object)adaptor.nil();

            string_literal79=(Token)input.LT(1);
            match(input,56,FOLLOW_56_in_orderClause556); 
            string_literal79_tree = (Object)adaptor.create(string_literal79);
            adaptor.addChild(root_0, string_literal79_tree);

            string_literal80=(Token)input.LT(1);
            match(input,54,FOLLOW_54_in_orderClause558); 
            string_literal80_tree = (Object)adaptor.create(string_literal80);
            adaptor.addChild(root_0, string_literal80_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:94:20: ( orderCondition )+
            int cnt26=0;
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=IRI_REF && LA26_0<=PNAME_NS)||(LA26_0>=VAR1 && LA26_0<=VAR2)||LA26_0==PNAME_LN||LA26_0==41||(LA26_0>=57 && LA26_0<=58)||(LA26_0>=92 && LA26_0<=152)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:94:20: orderCondition
            	    {
            	    pushFollow(FOLLOW_orderCondition_in_orderClause560);
            	    orderCondition81=orderCondition();
            	    _fsp--;

            	    adaptor.addChild(root_0, orderCondition81.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt26 >= 1 ) break loop26;
                        EarlyExitException eee =
                            new EarlyExitException(26, input);
                        throw eee;
                }
                cnt26++;
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end orderClause

    public static class orderCondition_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start orderCondition
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:97:1: orderCondition : ( ( ( 'ASC' | 'DESC' ) brackettedExpression ) | ( constraint | var ) );
    public final orderCondition_return orderCondition() throws RecognitionException {
        orderCondition_return retval = new orderCondition_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set82=null;
        brackettedExpression_return brackettedExpression83 = null;

        constraint_return constraint84 = null;

        var_return var85 = null;


        Object set82_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:5: ( ( ( 'ASC' | 'DESC' ) brackettedExpression ) | ( constraint | var ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( ((LA28_0>=57 && LA28_0<=58)) ) {
                alt28=1;
            }
            else if ( ((LA28_0>=IRI_REF && LA28_0<=PNAME_NS)||(LA28_0>=VAR1 && LA28_0<=VAR2)||LA28_0==PNAME_LN||LA28_0==41||(LA28_0>=92 && LA28_0<=152)) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("97:1: orderCondition : ( ( ( 'ASC' | 'DESC' ) brackettedExpression ) | ( constraint | var ) );", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:7: ( ( 'ASC' | 'DESC' ) brackettedExpression )
                    {
                    root_0 = (Object)adaptor.nil();

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:7: ( ( 'ASC' | 'DESC' ) brackettedExpression )
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:9: ( 'ASC' | 'DESC' ) brackettedExpression
                    {
                    set82=(Token)input.LT(1);
                    if ( (input.LA(1)>=57 && input.LA(1)<=58) ) {
                        input.consume();
                        adaptor.addChild(root_0, adaptor.create(set82));
                        errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse =
                            new MismatchedSetException(null,input);
                        recoverFromMismatchedSet(input,mse,FOLLOW_set_in_orderCondition578);    throw mse;
                    }

                    pushFollow(FOLLOW_brackettedExpression_in_orderCondition588);
                    brackettedExpression83=brackettedExpression();
                    _fsp--;

                    adaptor.addChild(root_0, brackettedExpression83.getTree());

                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:53: ( constraint | var )
                    {
                    root_0 = (Object)adaptor.nil();

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:53: ( constraint | var )
                    int alt27=2;
                    int LA27_0 = input.LA(1);

                    if ( ((LA27_0>=IRI_REF && LA27_0<=PNAME_NS)||LA27_0==PNAME_LN||LA27_0==41||(LA27_0>=92 && LA27_0<=152)) ) {
                        alt27=1;
                    }
                    else if ( ((LA27_0>=VAR1 && LA27_0<=VAR2)) ) {
                        alt27=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("98:53: ( constraint | var )", 27, 0, input);

                        throw nvae;
                    }
                    switch (alt27) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:55: constraint
                            {
                            pushFollow(FOLLOW_constraint_in_orderCondition596);
                            constraint84=constraint();
                            _fsp--;

                            adaptor.addChild(root_0, constraint84.getTree());

                            }
                            break;
                        case 2 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:68: var
                            {
                            pushFollow(FOLLOW_var_in_orderCondition600);
                            var85=var();
                            _fsp--;

                            adaptor.addChild(root_0, var85.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end orderCondition

    public static class limitOffsetClauses_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start limitOffsetClauses
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:101:1: limitOffsetClauses : ( limitClause ( offsetClause )? | offsetClause ( limitClause )? );
    public final limitOffsetClauses_return limitOffsetClauses() throws RecognitionException {
        limitOffsetClauses_return retval = new limitOffsetClauses_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        limitClause_return limitClause86 = null;

        offsetClause_return offsetClause87 = null;

        offsetClause_return offsetClause88 = null;

        limitClause_return limitClause89 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:2: ( limitClause ( offsetClause )? | offsetClause ( limitClause )? )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==59) ) {
                alt31=1;
            }
            else if ( (LA31_0==60) ) {
                alt31=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("101:1: limitOffsetClauses : ( limitClause ( offsetClause )? | offsetClause ( limitClause )? );", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:4: limitClause ( offsetClause )?
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_limitClause_in_limitOffsetClauses614);
                    limitClause86=limitClause();
                    _fsp--;

                    adaptor.addChild(root_0, limitClause86.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:16: ( offsetClause )?
                    int alt29=2;
                    int LA29_0 = input.LA(1);

                    if ( (LA29_0==60) ) {
                        alt29=1;
                    }
                    switch (alt29) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:16: offsetClause
                            {
                            pushFollow(FOLLOW_offsetClause_in_limitOffsetClauses616);
                            offsetClause87=offsetClause();
                            _fsp--;

                            adaptor.addChild(root_0, offsetClause87.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:32: offsetClause ( limitClause )?
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_offsetClause_in_limitOffsetClauses621);
                    offsetClause88=offsetClause();
                    _fsp--;

                    adaptor.addChild(root_0, offsetClause88.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:45: ( limitClause )?
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0==59) ) {
                        alt30=1;
                    }
                    switch (alt30) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:45: limitClause
                            {
                            pushFollow(FOLLOW_limitClause_in_limitOffsetClauses623);
                            limitClause89=limitClause();
                            _fsp--;

                            adaptor.addChild(root_0, limitClause89.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end limitOffsetClauses

    public static class limitClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start limitClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:105:1: limitClause : 'LIMIT' INTEGER ;
    public final limitClause_return limitClause() throws RecognitionException {
        limitClause_return retval = new limitClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal90=null;
        Token INTEGER91=null;

        Object string_literal90_tree=null;
        Object INTEGER91_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:106:5: ( 'LIMIT' INTEGER )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:106:7: 'LIMIT' INTEGER
            {
            root_0 = (Object)adaptor.nil();

            string_literal90=(Token)input.LT(1);
            match(input,59,FOLLOW_59_in_limitClause639); 
            string_literal90_tree = (Object)adaptor.create(string_literal90);
            adaptor.addChild(root_0, string_literal90_tree);

            INTEGER91=(Token)input.LT(1);
            match(input,INTEGER,FOLLOW_INTEGER_in_limitClause641); 
            INTEGER91_tree = (Object)adaptor.create(INTEGER91);
            adaptor.addChild(root_0, INTEGER91_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end limitClause

    public static class offsetClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start offsetClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:109:1: offsetClause : 'OFFSET' INTEGER ;
    public final offsetClause_return offsetClause() throws RecognitionException {
        offsetClause_return retval = new offsetClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal92=null;
        Token INTEGER93=null;

        Object string_literal92_tree=null;
        Object INTEGER93_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:110:5: ( 'OFFSET' INTEGER )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:110:7: 'OFFSET' INTEGER
            {
            root_0 = (Object)adaptor.nil();

            string_literal92=(Token)input.LT(1);
            match(input,60,FOLLOW_60_in_offsetClause656); 
            string_literal92_tree = (Object)adaptor.create(string_literal92);
            adaptor.addChild(root_0, string_literal92_tree);

            INTEGER93=(Token)input.LT(1);
            match(input,INTEGER,FOLLOW_INTEGER_in_offsetClause658); 
            INTEGER93_tree = (Object)adaptor.create(INTEGER93);
            adaptor.addChild(root_0, INTEGER93_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end offsetClause

    public static class valuesClause_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start valuesClause
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:113:1: valuesClause : ( 'VALUES' dataBlock )? ;
    public final valuesClause_return valuesClause() throws RecognitionException {
        valuesClause_return retval = new valuesClause_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal94=null;
        dataBlock_return dataBlock95 = null;


        Object string_literal94_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:114:2: ( ( 'VALUES' dataBlock )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:114:4: ( 'VALUES' dataBlock )?
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:114:4: ( 'VALUES' dataBlock )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==61) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:114:6: 'VALUES' dataBlock
                    {
                    string_literal94=(Token)input.LT(1);
                    match(input,61,FOLLOW_61_in_valuesClause675); 
                    string_literal94_tree = (Object)adaptor.create(string_literal94);
                    adaptor.addChild(root_0, string_literal94_tree);

                    pushFollow(FOLLOW_dataBlock_in_valuesClause677);
                    dataBlock95=dataBlock();
                    _fsp--;

                    adaptor.addChild(root_0, dataBlock95.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end valuesClause

    public static class triplesTemplate_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start triplesTemplate
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:117:1: triplesTemplate : triplesSameSubject ( '.' ( triplesTemplate )? )? ;
    public final triplesTemplate_return triplesTemplate() throws RecognitionException {
        triplesTemplate_return retval = new triplesTemplate_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal97=null;
        triplesSameSubject_return triplesSameSubject96 = null;

        triplesTemplate_return triplesTemplate98 = null;


        Object char_literal97_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:5: ( triplesSameSubject ( '.' ( triplesTemplate )? )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:7: triplesSameSubject ( '.' ( triplesTemplate )? )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_triplesSameSubject_in_triplesTemplate695);
            triplesSameSubject96=triplesSameSubject();
            _fsp--;

            adaptor.addChild(root_0, triplesSameSubject96.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:26: ( '.' ( triplesTemplate )? )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==62) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:28: '.' ( triplesTemplate )?
                    {
                    char_literal97=(Token)input.LT(1);
                    match(input,62,FOLLOW_62_in_triplesTemplate699); 
                    char_literal97_tree = (Object)adaptor.create(char_literal97);
                    adaptor.addChild(root_0, char_literal97_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:32: ( triplesTemplate )?
                    int alt33=2;
                    int LA33_0 = input.LA(1);

                    if ( ((LA33_0>=IRI_REF && LA33_0<=VAR2)||(LA33_0>=DECIMAL && LA33_0<=ANON)||LA33_0==41||LA33_0==81||(LA33_0>=155 && LA33_0<=156)) ) {
                        alt33=1;
                    }
                    switch (alt33) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:32: triplesTemplate
                            {
                            pushFollow(FOLLOW_triplesTemplate_in_triplesTemplate701);
                            triplesTemplate98=triplesTemplate();
                            _fsp--;

                            adaptor.addChild(root_0, triplesTemplate98.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end triplesTemplate

    public static class groupGraphPattern_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start groupGraphPattern
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:121:1: groupGraphPattern : '{' ( subSelect | groupGraphPatternSub ) '}' ;
    public final groupGraphPattern_return groupGraphPattern() throws RecognitionException {
        groupGraphPattern_return retval = new groupGraphPattern_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal99=null;
        Token char_literal102=null;
        subSelect_return subSelect100 = null;

        groupGraphPatternSub_return groupGraphPatternSub101 = null;


        Object char_literal99_tree=null;
        Object char_literal102_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:5: ( '{' ( subSelect | groupGraphPatternSub ) '}' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:7: '{' ( subSelect | groupGraphPatternSub ) '}'
            {
            root_0 = (Object)adaptor.nil();

            char_literal99=(Token)input.LT(1);
            match(input,47,FOLLOW_47_in_groupGraphPattern720); 
            char_literal99_tree = (Object)adaptor.create(char_literal99);
            adaptor.addChild(root_0, char_literal99_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:11: ( subSelect | groupGraphPatternSub )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==38) ) {
                alt35=1;
            }
            else if ( ((LA35_0>=IRI_REF && LA35_0<=VAR2)||(LA35_0>=DECIMAL && LA35_0<=ANON)||LA35_0==41||(LA35_0>=47 && LA35_0<=48)||LA35_0==61||(LA35_0>=63 && LA35_0<=65)||LA35_0==67||LA35_0==69||LA35_0==71||LA35_0==81||(LA35_0>=155 && LA35_0<=156)) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("122:11: ( subSelect | groupGraphPatternSub )", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:13: subSelect
                    {
                    pushFollow(FOLLOW_subSelect_in_groupGraphPattern724);
                    subSelect100=subSelect();
                    _fsp--;

                    adaptor.addChild(root_0, subSelect100.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:25: groupGraphPatternSub
                    {
                    pushFollow(FOLLOW_groupGraphPatternSub_in_groupGraphPattern728);
                    groupGraphPatternSub101=groupGraphPatternSub();
                    _fsp--;

                    adaptor.addChild(root_0, groupGraphPatternSub101.getTree());

                    }
                    break;

            }

            char_literal102=(Token)input.LT(1);
            match(input,48,FOLLOW_48_in_groupGraphPattern732); 
            char_literal102_tree = (Object)adaptor.create(char_literal102);
            adaptor.addChild(root_0, char_literal102_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end groupGraphPattern

    public static class groupGraphPatternSub_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start groupGraphPatternSub
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:125:1: groupGraphPatternSub : ( triplesBlock )? ( graphPatternNotTriples ( '.' )? ( triplesBlock )? )* ;
    public final groupGraphPatternSub_return groupGraphPatternSub() throws RecognitionException {
        groupGraphPatternSub_return retval = new groupGraphPatternSub_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal105=null;
        triplesBlock_return triplesBlock103 = null;

        graphPatternNotTriples_return graphPatternNotTriples104 = null;

        triplesBlock_return triplesBlock106 = null;


        Object char_literal105_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:2: ( ( triplesBlock )? ( graphPatternNotTriples ( '.' )? ( triplesBlock )? )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:4: ( triplesBlock )? ( graphPatternNotTriples ( '.' )? ( triplesBlock )? )*
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:4: ( triplesBlock )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( ((LA36_0>=IRI_REF && LA36_0<=VAR2)||(LA36_0>=DECIMAL && LA36_0<=ANON)||LA36_0==41||LA36_0==81||(LA36_0>=155 && LA36_0<=156)) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:4: triplesBlock
                    {
                    pushFollow(FOLLOW_triplesBlock_in_groupGraphPatternSub744);
                    triplesBlock103=triplesBlock();
                    _fsp--;

                    adaptor.addChild(root_0, triplesBlock103.getTree());

                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:18: ( graphPatternNotTriples ( '.' )? ( triplesBlock )? )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==47||LA39_0==61||(LA39_0>=63 && LA39_0<=65)||LA39_0==67||LA39_0==69||LA39_0==71) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:20: graphPatternNotTriples ( '.' )? ( triplesBlock )?
            	    {
            	    pushFollow(FOLLOW_graphPatternNotTriples_in_groupGraphPatternSub749);
            	    graphPatternNotTriples104=graphPatternNotTriples();
            	    _fsp--;

            	    adaptor.addChild(root_0, graphPatternNotTriples104.getTree());
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:43: ( '.' )?
            	    int alt37=2;
            	    int LA37_0 = input.LA(1);

            	    if ( (LA37_0==62) ) {
            	        alt37=1;
            	    }
            	    switch (alt37) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:43: '.'
            	            {
            	            char_literal105=(Token)input.LT(1);
            	            match(input,62,FOLLOW_62_in_groupGraphPatternSub751); 
            	            char_literal105_tree = (Object)adaptor.create(char_literal105);
            	            adaptor.addChild(root_0, char_literal105_tree);


            	            }
            	            break;

            	    }

            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:48: ( triplesBlock )?
            	    int alt38=2;
            	    int LA38_0 = input.LA(1);

            	    if ( ((LA38_0>=IRI_REF && LA38_0<=VAR2)||(LA38_0>=DECIMAL && LA38_0<=ANON)||LA38_0==41||LA38_0==81||(LA38_0>=155 && LA38_0<=156)) ) {
            	        alt38=1;
            	    }
            	    switch (alt38) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:126:48: triplesBlock
            	            {
            	            pushFollow(FOLLOW_triplesBlock_in_groupGraphPatternSub754);
            	            triplesBlock106=triplesBlock();
            	            _fsp--;

            	            adaptor.addChild(root_0, triplesBlock106.getTree());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end groupGraphPatternSub

    public static class triplesBlock_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start triplesBlock
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:129:1: triplesBlock : triplesSameSubjectPath ( '.' ( triplesBlock )? )? ;
    public final triplesBlock_return triplesBlock() throws RecognitionException {
        triplesBlock_return retval = new triplesBlock_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal108=null;
        triplesSameSubjectPath_return triplesSameSubjectPath107 = null;

        triplesBlock_return triplesBlock109 = null;


        Object char_literal108_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:130:5: ( triplesSameSubjectPath ( '.' ( triplesBlock )? )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:130:7: triplesSameSubjectPath ( '.' ( triplesBlock )? )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_triplesSameSubjectPath_in_triplesBlock773);
            triplesSameSubjectPath107=triplesSameSubjectPath();
            _fsp--;

            adaptor.addChild(root_0, triplesSameSubjectPath107.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:130:30: ( '.' ( triplesBlock )? )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==62) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:130:32: '.' ( triplesBlock )?
                    {
                    char_literal108=(Token)input.LT(1);
                    match(input,62,FOLLOW_62_in_triplesBlock777); 
                    char_literal108_tree = (Object)adaptor.create(char_literal108);
                    adaptor.addChild(root_0, char_literal108_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:130:36: ( triplesBlock )?
                    int alt40=2;
                    int LA40_0 = input.LA(1);

                    if ( ((LA40_0>=IRI_REF && LA40_0<=VAR2)||(LA40_0>=DECIMAL && LA40_0<=ANON)||LA40_0==41||LA40_0==81||(LA40_0>=155 && LA40_0<=156)) ) {
                        alt40=1;
                    }
                    switch (alt40) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:130:36: triplesBlock
                            {
                            pushFollow(FOLLOW_triplesBlock_in_triplesBlock779);
                            triplesBlock109=triplesBlock();
                            _fsp--;

                            adaptor.addChild(root_0, triplesBlock109.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end triplesBlock

    public static class graphPatternNotTriples_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start graphPatternNotTriples
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:133:1: graphPatternNotTriples : ( groupOrUnionGraphPattern | optionalGraphPattern | minusGraphPattern | graphGraphPattern | serviceGraphPattern | filter | bind | inlineData );
    public final graphPatternNotTriples_return graphPatternNotTriples() throws RecognitionException {
        graphPatternNotTriples_return retval = new graphPatternNotTriples_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        groupOrUnionGraphPattern_return groupOrUnionGraphPattern110 = null;

        optionalGraphPattern_return optionalGraphPattern111 = null;

        minusGraphPattern_return minusGraphPattern112 = null;

        graphGraphPattern_return graphGraphPattern113 = null;

        serviceGraphPattern_return serviceGraphPattern114 = null;

        filter_return filter115 = null;

        bind_return bind116 = null;

        inlineData_return inlineData117 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:2: ( groupOrUnionGraphPattern | optionalGraphPattern | minusGraphPattern | graphGraphPattern | serviceGraphPattern | filter | bind | inlineData )
            int alt42=8;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt42=1;
                }
                break;
            case 63:
                {
                alt42=2;
                }
                break;
            case 69:
                {
                alt42=3;
                }
                break;
            case 64:
                {
                alt42=4;
                }
                break;
            case 65:
                {
                alt42=5;
                }
                break;
            case 71:
                {
                alt42=6;
                }
                break;
            case 67:
                {
                alt42=7;
                }
                break;
            case 61:
                {
                alt42=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("133:1: graphPatternNotTriples : ( groupOrUnionGraphPattern | optionalGraphPattern | minusGraphPattern | graphGraphPattern | serviceGraphPattern | filter | bind | inlineData );", 42, 0, input);

                throw nvae;
            }

            switch (alt42) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:4: groupOrUnionGraphPattern
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_groupOrUnionGraphPattern_in_graphPatternNotTriples795);
                    groupOrUnionGraphPattern110=groupOrUnionGraphPattern();
                    _fsp--;

                    adaptor.addChild(root_0, groupOrUnionGraphPattern110.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:31: optionalGraphPattern
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_optionalGraphPattern_in_graphPatternNotTriples799);
                    optionalGraphPattern111=optionalGraphPattern();
                    _fsp--;

                    adaptor.addChild(root_0, optionalGraphPattern111.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:54: minusGraphPattern
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_minusGraphPattern_in_graphPatternNotTriples803);
                    minusGraphPattern112=minusGraphPattern();
                    _fsp--;

                    adaptor.addChild(root_0, minusGraphPattern112.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:74: graphGraphPattern
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_graphGraphPattern_in_graphPatternNotTriples807);
                    graphGraphPattern113=graphGraphPattern();
                    _fsp--;

                    adaptor.addChild(root_0, graphGraphPattern113.getTree());

                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:94: serviceGraphPattern
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_serviceGraphPattern_in_graphPatternNotTriples811);
                    serviceGraphPattern114=serviceGraphPattern();
                    _fsp--;

                    adaptor.addChild(root_0, serviceGraphPattern114.getTree());

                    }
                    break;
                case 6 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:116: filter
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_filter_in_graphPatternNotTriples815);
                    filter115=filter();
                    _fsp--;

                    adaptor.addChild(root_0, filter115.getTree());

                    }
                    break;
                case 7 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:125: bind
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_bind_in_graphPatternNotTriples819);
                    bind116=bind();
                    _fsp--;

                    adaptor.addChild(root_0, bind116.getTree());

                    }
                    break;
                case 8 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:134:132: inlineData
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_inlineData_in_graphPatternNotTriples823);
                    inlineData117=inlineData();
                    _fsp--;

                    adaptor.addChild(root_0, inlineData117.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end graphPatternNotTriples

    public static class optionalGraphPattern_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start optionalGraphPattern
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:137:1: optionalGraphPattern : 'OPTIONAL' groupGraphPattern ;
    public final optionalGraphPattern_return optionalGraphPattern() throws RecognitionException {
        optionalGraphPattern_return retval = new optionalGraphPattern_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal118=null;
        groupGraphPattern_return groupGraphPattern119 = null;


        Object string_literal118_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:138:5: ( 'OPTIONAL' groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:138:7: 'OPTIONAL' groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            string_literal118=(Token)input.LT(1);
            match(input,63,FOLLOW_63_in_optionalGraphPattern838); 
            string_literal118_tree = (Object)adaptor.create(string_literal118);
            adaptor.addChild(root_0, string_literal118_tree);

            pushFollow(FOLLOW_groupGraphPattern_in_optionalGraphPattern840);
            groupGraphPattern119=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern119.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end optionalGraphPattern

    public static class graphGraphPattern_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start graphGraphPattern
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:141:1: graphGraphPattern : 'GRAPH' varOrIRIref groupGraphPattern ;
    public final graphGraphPattern_return graphGraphPattern() throws RecognitionException {
        graphGraphPattern_return retval = new graphGraphPattern_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal120=null;
        varOrIRIref_return varOrIRIref121 = null;

        groupGraphPattern_return groupGraphPattern122 = null;


        Object string_literal120_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:142:5: ( 'GRAPH' varOrIRIref groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:142:7: 'GRAPH' varOrIRIref groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            string_literal120=(Token)input.LT(1);
            match(input,64,FOLLOW_64_in_graphGraphPattern855); 
            string_literal120_tree = (Object)adaptor.create(string_literal120);
            adaptor.addChild(root_0, string_literal120_tree);

            pushFollow(FOLLOW_varOrIRIref_in_graphGraphPattern857);
            varOrIRIref121=varOrIRIref();
            _fsp--;

            adaptor.addChild(root_0, varOrIRIref121.getTree());
            pushFollow(FOLLOW_groupGraphPattern_in_graphGraphPattern859);
            groupGraphPattern122=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern122.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end graphGraphPattern

    public static class serviceGraphPattern_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start serviceGraphPattern
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:145:1: serviceGraphPattern : 'SERVICE' ( 'SILENT' )? varOrIRIref groupGraphPattern ;
    public final serviceGraphPattern_return serviceGraphPattern() throws RecognitionException {
        serviceGraphPattern_return retval = new serviceGraphPattern_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal123=null;
        Token string_literal124=null;
        varOrIRIref_return varOrIRIref125 = null;

        groupGraphPattern_return groupGraphPattern126 = null;


        Object string_literal123_tree=null;
        Object string_literal124_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:146:2: ( 'SERVICE' ( 'SILENT' )? varOrIRIref groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:146:4: 'SERVICE' ( 'SILENT' )? varOrIRIref groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            string_literal123=(Token)input.LT(1);
            match(input,65,FOLLOW_65_in_serviceGraphPattern871); 
            string_literal123_tree = (Object)adaptor.create(string_literal123);
            adaptor.addChild(root_0, string_literal123_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:146:14: ( 'SILENT' )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==66) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:146:14: 'SILENT'
                    {
                    string_literal124=(Token)input.LT(1);
                    match(input,66,FOLLOW_66_in_serviceGraphPattern873); 
                    string_literal124_tree = (Object)adaptor.create(string_literal124);
                    adaptor.addChild(root_0, string_literal124_tree);


                    }
                    break;

            }

            pushFollow(FOLLOW_varOrIRIref_in_serviceGraphPattern876);
            varOrIRIref125=varOrIRIref();
            _fsp--;

            adaptor.addChild(root_0, varOrIRIref125.getTree());
            pushFollow(FOLLOW_groupGraphPattern_in_serviceGraphPattern878);
            groupGraphPattern126=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern126.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end serviceGraphPattern

    public static class bind_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start bind
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:149:1: bind : 'BIND' '(' expression 'AS' var ')' ;
    public final bind_return bind() throws RecognitionException {
        bind_return retval = new bind_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal127=null;
        Token char_literal128=null;
        Token string_literal130=null;
        Token char_literal132=null;
        expression_return expression129 = null;

        var_return var131 = null;


        Object string_literal127_tree=null;
        Object char_literal128_tree=null;
        Object string_literal130_tree=null;
        Object char_literal132_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:150:5: ( 'BIND' '(' expression 'AS' var ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:150:7: 'BIND' '(' expression 'AS' var ')'
            {
            root_0 = (Object)adaptor.nil();

            string_literal127=(Token)input.LT(1);
            match(input,67,FOLLOW_67_in_bind893); 
            string_literal127_tree = (Object)adaptor.create(string_literal127);
            adaptor.addChild(root_0, string_literal127_tree);

            char_literal128=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_bind895); 
            char_literal128_tree = (Object)adaptor.create(char_literal128);
            adaptor.addChild(root_0, char_literal128_tree);

            pushFollow(FOLLOW_expression_in_bind897);
            expression129=expression();
            _fsp--;

            adaptor.addChild(root_0, expression129.getTree());
            string_literal130=(Token)input.LT(1);
            match(input,42,FOLLOW_42_in_bind899); 
            string_literal130_tree = (Object)adaptor.create(string_literal130);
            adaptor.addChild(root_0, string_literal130_tree);

            pushFollow(FOLLOW_var_in_bind901);
            var131=var();
            _fsp--;

            adaptor.addChild(root_0, var131.getTree());
            char_literal132=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_bind903); 
            char_literal132_tree = (Object)adaptor.create(char_literal132);
            adaptor.addChild(root_0, char_literal132_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end bind

    public static class inlineData_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start inlineData
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:153:1: inlineData : 'VALUES' dataBlock ;
    public final inlineData_return inlineData() throws RecognitionException {
        inlineData_return retval = new inlineData_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal133=null;
        dataBlock_return dataBlock134 = null;


        Object string_literal133_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:154:2: ( 'VALUES' dataBlock )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:154:4: 'VALUES' dataBlock
            {
            root_0 = (Object)adaptor.nil();

            string_literal133=(Token)input.LT(1);
            match(input,61,FOLLOW_61_in_inlineData915); 
            string_literal133_tree = (Object)adaptor.create(string_literal133);
            adaptor.addChild(root_0, string_literal133_tree);

            pushFollow(FOLLOW_dataBlock_in_inlineData917);
            dataBlock134=dataBlock();
            _fsp--;

            adaptor.addChild(root_0, dataBlock134.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end inlineData

    public static class dataBlock_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start dataBlock
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:157:1: dataBlock : ( inlineDataOneVar | inlineDataFull );
    public final dataBlock_return dataBlock() throws RecognitionException {
        dataBlock_return retval = new dataBlock_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        inlineDataOneVar_return inlineDataOneVar135 = null;

        inlineDataFull_return inlineDataFull136 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:158:5: ( inlineDataOneVar | inlineDataFull )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( ((LA44_0>=VAR1 && LA44_0<=VAR2)) ) {
                alt44=1;
            }
            else if ( (LA44_0==NIL||LA44_0==41) ) {
                alt44=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("157:1: dataBlock : ( inlineDataOneVar | inlineDataFull );", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:158:7: inlineDataOneVar
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_inlineDataOneVar_in_dataBlock932);
                    inlineDataOneVar135=inlineDataOneVar();
                    _fsp--;

                    adaptor.addChild(root_0, inlineDataOneVar135.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:158:26: inlineDataFull
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_inlineDataFull_in_dataBlock936);
                    inlineDataFull136=inlineDataFull();
                    _fsp--;

                    adaptor.addChild(root_0, inlineDataFull136.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end dataBlock

    public static class inlineDataOneVar_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start inlineDataOneVar
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:161:1: inlineDataOneVar : var '{' ( dataBlockValue )* '}' ;
    public final inlineDataOneVar_return inlineDataOneVar() throws RecognitionException {
        inlineDataOneVar_return retval = new inlineDataOneVar_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal138=null;
        Token char_literal140=null;
        var_return var137 = null;

        dataBlockValue_return dataBlockValue139 = null;


        Object char_literal138_tree=null;
        Object char_literal140_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:162:5: ( var '{' ( dataBlockValue )* '}' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:162:7: var '{' ( dataBlockValue )* '}'
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_var_in_inlineDataOneVar951);
            var137=var();
            _fsp--;

            adaptor.addChild(root_0, var137.getTree());
            char_literal138=(Token)input.LT(1);
            match(input,47,FOLLOW_47_in_inlineDataOneVar953); 
            char_literal138_tree = (Object)adaptor.create(char_literal138);
            adaptor.addChild(root_0, char_literal138_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:162:15: ( dataBlockValue )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( ((LA45_0>=IRI_REF && LA45_0<=INTEGER)||(LA45_0>=DECIMAL && LA45_0<=PNAME_LN)||LA45_0==68||(LA45_0>=155 && LA45_0<=156)) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:162:15: dataBlockValue
            	    {
            	    pushFollow(FOLLOW_dataBlockValue_in_inlineDataOneVar955);
            	    dataBlockValue139=dataBlockValue();
            	    _fsp--;

            	    adaptor.addChild(root_0, dataBlockValue139.getTree());

            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);

            char_literal140=(Token)input.LT(1);
            match(input,48,FOLLOW_48_in_inlineDataOneVar958); 
            char_literal140_tree = (Object)adaptor.create(char_literal140);
            adaptor.addChild(root_0, char_literal140_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end inlineDataOneVar

    public static class inlineDataFull_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start inlineDataFull
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:165:1: inlineDataFull : ( NIL | '(' ( var )* ')' ) '{' ( '(' ( dataBlockValue )* ')' | NIL )* '}' ;
    public final inlineDataFull_return inlineDataFull() throws RecognitionException {
        inlineDataFull_return retval = new inlineDataFull_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token NIL141=null;
        Token char_literal142=null;
        Token char_literal144=null;
        Token char_literal145=null;
        Token char_literal146=null;
        Token char_literal148=null;
        Token NIL149=null;
        Token char_literal150=null;
        var_return var143 = null;

        dataBlockValue_return dataBlockValue147 = null;


        Object NIL141_tree=null;
        Object char_literal142_tree=null;
        Object char_literal144_tree=null;
        Object char_literal145_tree=null;
        Object char_literal146_tree=null;
        Object char_literal148_tree=null;
        Object NIL149_tree=null;
        Object char_literal150_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:2: ( ( NIL | '(' ( var )* ')' ) '{' ( '(' ( dataBlockValue )* ')' | NIL )* '}' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:4: ( NIL | '(' ( var )* ')' ) '{' ( '(' ( dataBlockValue )* ')' | NIL )* '}'
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:4: ( NIL | '(' ( var )* ')' )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==NIL) ) {
                alt47=1;
            }
            else if ( (LA47_0==41) ) {
                alt47=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("166:4: ( NIL | '(' ( var )* ')' )", 47, 0, input);

                throw nvae;
            }
            switch (alt47) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:6: NIL
                    {
                    NIL141=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_inlineDataFull972); 
                    NIL141_tree = (Object)adaptor.create(NIL141);
                    adaptor.addChild(root_0, NIL141_tree);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:12: '(' ( var )* ')'
                    {
                    char_literal142=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_inlineDataFull976); 
                    char_literal142_tree = (Object)adaptor.create(char_literal142);
                    adaptor.addChild(root_0, char_literal142_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:16: ( var )*
                    loop46:
                    do {
                        int alt46=2;
                        int LA46_0 = input.LA(1);

                        if ( ((LA46_0>=VAR1 && LA46_0<=VAR2)) ) {
                            alt46=1;
                        }


                        switch (alt46) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:16: var
                    	    {
                    	    pushFollow(FOLLOW_var_in_inlineDataFull978);
                    	    var143=var();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, var143.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop46;
                        }
                    } while (true);

                    char_literal144=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_inlineDataFull981); 
                    char_literal144_tree = (Object)adaptor.create(char_literal144);
                    adaptor.addChild(root_0, char_literal144_tree);


                    }
                    break;

            }

            char_literal145=(Token)input.LT(1);
            match(input,47,FOLLOW_47_in_inlineDataFull985); 
            char_literal145_tree = (Object)adaptor.create(char_literal145);
            adaptor.addChild(root_0, char_literal145_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:31: ( '(' ( dataBlockValue )* ')' | NIL )*
            loop49:
            do {
                int alt49=3;
                int LA49_0 = input.LA(1);

                if ( (LA49_0==41) ) {
                    alt49=1;
                }
                else if ( (LA49_0==NIL) ) {
                    alt49=2;
                }


                switch (alt49) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:33: '(' ( dataBlockValue )* ')'
            	    {
            	    char_literal146=(Token)input.LT(1);
            	    match(input,41,FOLLOW_41_in_inlineDataFull989); 
            	    char_literal146_tree = (Object)adaptor.create(char_literal146);
            	    adaptor.addChild(root_0, char_literal146_tree);

            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:37: ( dataBlockValue )*
            	    loop48:
            	    do {
            	        int alt48=2;
            	        int LA48_0 = input.LA(1);

            	        if ( ((LA48_0>=IRI_REF && LA48_0<=INTEGER)||(LA48_0>=DECIMAL && LA48_0<=PNAME_LN)||LA48_0==68||(LA48_0>=155 && LA48_0<=156)) ) {
            	            alt48=1;
            	        }


            	        switch (alt48) {
            	    	case 1 :
            	    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:37: dataBlockValue
            	    	    {
            	    	    pushFollow(FOLLOW_dataBlockValue_in_inlineDataFull991);
            	    	    dataBlockValue147=dataBlockValue();
            	    	    _fsp--;

            	    	    adaptor.addChild(root_0, dataBlockValue147.getTree());

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop48;
            	        }
            	    } while (true);

            	    char_literal148=(Token)input.LT(1);
            	    match(input,43,FOLLOW_43_in_inlineDataFull994); 
            	    char_literal148_tree = (Object)adaptor.create(char_literal148);
            	    adaptor.addChild(root_0, char_literal148_tree);


            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:166:59: NIL
            	    {
            	    NIL149=(Token)input.LT(1);
            	    match(input,NIL,FOLLOW_NIL_in_inlineDataFull998); 
            	    NIL149_tree = (Object)adaptor.create(NIL149);
            	    adaptor.addChild(root_0, NIL149_tree);


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);

            char_literal150=(Token)input.LT(1);
            match(input,48,FOLLOW_48_in_inlineDataFull1003); 
            char_literal150_tree = (Object)adaptor.create(char_literal150);
            adaptor.addChild(root_0, char_literal150_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end inlineDataFull

    public static class dataBlockValue_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start dataBlockValue
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:169:1: dataBlockValue : ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | 'UNDEF' );
    public final dataBlockValue_return dataBlockValue() throws RecognitionException {
        dataBlockValue_return retval = new dataBlockValue_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal155=null;
        iriRef_return iriRef151 = null;

        rdfLiteral_return rdfLiteral152 = null;

        numericLiteral_return numericLiteral153 = null;

        booleanLiteral_return booleanLiteral154 = null;


        Object string_literal155_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:170:2: ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | 'UNDEF' )
            int alt50=5;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt50=1;
                }
                break;
            case STRING_LITERAL1:
            case STRING_LITERAL2:
                {
                alt50=2;
                }
                break;
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt50=3;
                }
                break;
            case 155:
            case 156:
                {
                alt50=4;
                }
                break;
            case 68:
                {
                alt50=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("169:1: dataBlockValue : ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | 'UNDEF' );", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:170:4: iriRef
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_iriRef_in_dataBlockValue1015);
                    iriRef151=iriRef();
                    _fsp--;

                    adaptor.addChild(root_0, iriRef151.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:170:13: rdfLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_rdfLiteral_in_dataBlockValue1019);
                    rdfLiteral152=rdfLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, rdfLiteral152.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:170:26: numericLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_numericLiteral_in_dataBlockValue1023);
                    numericLiteral153=numericLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, numericLiteral153.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:170:43: booleanLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_booleanLiteral_in_dataBlockValue1027);
                    booleanLiteral154=booleanLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, booleanLiteral154.getTree());

                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:170:60: 'UNDEF'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal155=(Token)input.LT(1);
                    match(input,68,FOLLOW_68_in_dataBlockValue1031); 
                    string_literal155_tree = (Object)adaptor.create(string_literal155);
                    adaptor.addChild(root_0, string_literal155_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end dataBlockValue

    public static class minusGraphPattern_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start minusGraphPattern
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:173:1: minusGraphPattern : 'MINUS' groupGraphPattern ;
    public final minusGraphPattern_return minusGraphPattern() throws RecognitionException {
        minusGraphPattern_return retval = new minusGraphPattern_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal156=null;
        groupGraphPattern_return groupGraphPattern157 = null;


        Object string_literal156_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:174:5: ( 'MINUS' groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:174:7: 'MINUS' groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            string_literal156=(Token)input.LT(1);
            match(input,69,FOLLOW_69_in_minusGraphPattern1046); 
            string_literal156_tree = (Object)adaptor.create(string_literal156);
            adaptor.addChild(root_0, string_literal156_tree);

            pushFollow(FOLLOW_groupGraphPattern_in_minusGraphPattern1048);
            groupGraphPattern157=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern157.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end minusGraphPattern

    public static class groupOrUnionGraphPattern_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start groupOrUnionGraphPattern
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:177:1: groupOrUnionGraphPattern : groupGraphPattern ( 'UNION' groupGraphPattern )* ;
    public final groupOrUnionGraphPattern_return groupOrUnionGraphPattern() throws RecognitionException {
        groupOrUnionGraphPattern_return retval = new groupOrUnionGraphPattern_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal159=null;
        groupGraphPattern_return groupGraphPattern158 = null;

        groupGraphPattern_return groupGraphPattern160 = null;


        Object string_literal159_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:178:5: ( groupGraphPattern ( 'UNION' groupGraphPattern )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:178:7: groupGraphPattern ( 'UNION' groupGraphPattern )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern1063);
            groupGraphPattern158=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern158.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:178:25: ( 'UNION' groupGraphPattern )*
            loop51:
            do {
                int alt51=2;
                int LA51_0 = input.LA(1);

                if ( (LA51_0==70) ) {
                    alt51=1;
                }


                switch (alt51) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:178:27: 'UNION' groupGraphPattern
            	    {
            	    string_literal159=(Token)input.LT(1);
            	    match(input,70,FOLLOW_70_in_groupOrUnionGraphPattern1067); 
            	    string_literal159_tree = (Object)adaptor.create(string_literal159);
            	    adaptor.addChild(root_0, string_literal159_tree);

            	    pushFollow(FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern1069);
            	    groupGraphPattern160=groupGraphPattern();
            	    _fsp--;

            	    adaptor.addChild(root_0, groupGraphPattern160.getTree());

            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end groupOrUnionGraphPattern

    public static class filter_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start filter
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:181:1: filter : 'FILTER' constraint ;
    public final filter_return filter() throws RecognitionException {
        filter_return retval = new filter_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal161=null;
        constraint_return constraint162 = null;


        Object string_literal161_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:182:5: ( 'FILTER' constraint )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:182:7: 'FILTER' constraint
            {
            root_0 = (Object)adaptor.nil();

            string_literal161=(Token)input.LT(1);
            match(input,71,FOLLOW_71_in_filter1087); 
            string_literal161_tree = (Object)adaptor.create(string_literal161);
            adaptor.addChild(root_0, string_literal161_tree);

            pushFollow(FOLLOW_constraint_in_filter1089);
            constraint162=constraint();
            _fsp--;

            adaptor.addChild(root_0, constraint162.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end filter

    public static class constraint_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start constraint
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:185:1: constraint : ( brackettedExpression | builtInCall | functionCall );
    public final constraint_return constraint() throws RecognitionException {
        constraint_return retval = new constraint_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        brackettedExpression_return brackettedExpression163 = null;

        builtInCall_return builtInCall164 = null;

        functionCall_return functionCall165 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:186:2: ( brackettedExpression | builtInCall | functionCall )
            int alt52=3;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt52=1;
                }
                break;
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
                {
                alt52=2;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt52=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("185:1: constraint : ( brackettedExpression | builtInCall | functionCall );", 52, 0, input);

                throw nvae;
            }

            switch (alt52) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:186:4: brackettedExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_brackettedExpression_in_constraint1101);
                    brackettedExpression163=brackettedExpression();
                    _fsp--;

                    adaptor.addChild(root_0, brackettedExpression163.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:186:27: builtInCall
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_builtInCall_in_constraint1105);
                    builtInCall164=builtInCall();
                    _fsp--;

                    adaptor.addChild(root_0, builtInCall164.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:186:41: functionCall
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_functionCall_in_constraint1109);
                    functionCall165=functionCall();
                    _fsp--;

                    adaptor.addChild(root_0, functionCall165.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end constraint

    public static class functionCall_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start functionCall
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:189:1: functionCall : iriRef argList ;
    public final functionCall_return functionCall() throws RecognitionException {
        functionCall_return retval = new functionCall_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        iriRef_return iriRef166 = null;

        argList_return argList167 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:190:5: ( iriRef argList )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:190:7: iriRef argList
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_iriRef_in_functionCall1124);
            iriRef166=iriRef();
            _fsp--;

            adaptor.addChild(root_0, iriRef166.getTree());
            pushFollow(FOLLOW_argList_in_functionCall1126);
            argList167=argList();
            _fsp--;

            adaptor.addChild(root_0, argList167.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end functionCall

    public static class argList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start argList
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:193:1: argList : ( NIL | '(' ( 'DISTINCT' )? expression ( ',' expression )* ')' );
    public final argList_return argList() throws RecognitionException {
        argList_return retval = new argList_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token NIL168=null;
        Token char_literal169=null;
        Token string_literal170=null;
        Token char_literal172=null;
        Token char_literal174=null;
        expression_return expression171 = null;

        expression_return expression173 = null;


        Object NIL168_tree=null;
        Object char_literal169_tree=null;
        Object string_literal170_tree=null;
        Object char_literal172_tree=null;
        Object char_literal174_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:5: ( NIL | '(' ( 'DISTINCT' )? expression ( ',' expression )* ')' )
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==NIL) ) {
                alt55=1;
            }
            else if ( (LA55_0==41) ) {
                alt55=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("193:1: argList : ( NIL | '(' ( 'DISTINCT' )? expression ( ',' expression )* ')' );", 55, 0, input);

                throw nvae;
            }
            switch (alt55) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:7: NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    NIL168=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_argList1141); 
                    NIL168_tree = (Object)adaptor.create(NIL168);
                    adaptor.addChild(root_0, NIL168_tree);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:13: '(' ( 'DISTINCT' )? expression ( ',' expression )* ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal169=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_argList1145); 
                    char_literal169_tree = (Object)adaptor.create(char_literal169);
                    adaptor.addChild(root_0, char_literal169_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:17: ( 'DISTINCT' )?
                    int alt53=2;
                    int LA53_0 = input.LA(1);

                    if ( (LA53_0==39) ) {
                        alt53=1;
                    }
                    switch (alt53) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:17: 'DISTINCT'
                            {
                            string_literal170=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_argList1147); 
                            string_literal170_tree = (Object)adaptor.create(string_literal170);
                            adaptor.addChild(root_0, string_literal170_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_argList1150);
                    expression171=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression171.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:40: ( ',' expression )*
                    loop54:
                    do {
                        int alt54=2;
                        int LA54_0 = input.LA(1);

                        if ( (LA54_0==72) ) {
                            alt54=1;
                        }


                        switch (alt54) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:194:42: ',' expression
                    	    {
                    	    char_literal172=(Token)input.LT(1);
                    	    match(input,72,FOLLOW_72_in_argList1154); 
                    	    char_literal172_tree = (Object)adaptor.create(char_literal172);
                    	    adaptor.addChild(root_0, char_literal172_tree);

                    	    pushFollow(FOLLOW_expression_in_argList1156);
                    	    expression173=expression();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, expression173.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop54;
                        }
                    } while (true);

                    char_literal174=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_argList1161); 
                    char_literal174_tree = (Object)adaptor.create(char_literal174);
                    adaptor.addChild(root_0, char_literal174_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end argList

    public static class expressionList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start expressionList
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:197:1: expressionList : ( NIL | '(' expression ( ',' expression )* ')' );
    public final expressionList_return expressionList() throws RecognitionException {
        expressionList_return retval = new expressionList_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token NIL175=null;
        Token char_literal176=null;
        Token char_literal178=null;
        Token char_literal180=null;
        expression_return expression177 = null;

        expression_return expression179 = null;


        Object NIL175_tree=null;
        Object char_literal176_tree=null;
        Object char_literal178_tree=null;
        Object char_literal180_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:198:5: ( NIL | '(' expression ( ',' expression )* ')' )
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==NIL) ) {
                alt57=1;
            }
            else if ( (LA57_0==41) ) {
                alt57=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("197:1: expressionList : ( NIL | '(' expression ( ',' expression )* ')' );", 57, 0, input);

                throw nvae;
            }
            switch (alt57) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:198:7: NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    NIL175=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_expressionList1176); 
                    NIL175_tree = (Object)adaptor.create(NIL175);
                    adaptor.addChild(root_0, NIL175_tree);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:198:13: '(' expression ( ',' expression )* ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal176=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_expressionList1180); 
                    char_literal176_tree = (Object)adaptor.create(char_literal176);
                    adaptor.addChild(root_0, char_literal176_tree);

                    pushFollow(FOLLOW_expression_in_expressionList1182);
                    expression177=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression177.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:198:28: ( ',' expression )*
                    loop56:
                    do {
                        int alt56=2;
                        int LA56_0 = input.LA(1);

                        if ( (LA56_0==72) ) {
                            alt56=1;
                        }


                        switch (alt56) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:198:30: ',' expression
                    	    {
                    	    char_literal178=(Token)input.LT(1);
                    	    match(input,72,FOLLOW_72_in_expressionList1186); 
                    	    char_literal178_tree = (Object)adaptor.create(char_literal178);
                    	    adaptor.addChild(root_0, char_literal178_tree);

                    	    pushFollow(FOLLOW_expression_in_expressionList1188);
                    	    expression179=expression();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, expression179.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop56;
                        }
                    } while (true);

                    char_literal180=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_expressionList1193); 
                    char_literal180_tree = (Object)adaptor.create(char_literal180);
                    adaptor.addChild(root_0, char_literal180_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end expressionList

    public static class constructTemplate_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start constructTemplate
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:201:1: constructTemplate : '{' ( constructTriples )? '}' ;
    public final constructTemplate_return constructTemplate() throws RecognitionException {
        constructTemplate_return retval = new constructTemplate_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal181=null;
        Token char_literal183=null;
        constructTriples_return constructTriples182 = null;


        Object char_literal181_tree=null;
        Object char_literal183_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:202:5: ( '{' ( constructTriples )? '}' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:202:7: '{' ( constructTriples )? '}'
            {
            root_0 = (Object)adaptor.nil();

            char_literal181=(Token)input.LT(1);
            match(input,47,FOLLOW_47_in_constructTemplate1208); 
            char_literal181_tree = (Object)adaptor.create(char_literal181);
            adaptor.addChild(root_0, char_literal181_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:202:11: ( constructTriples )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( ((LA58_0>=IRI_REF && LA58_0<=VAR2)||(LA58_0>=DECIMAL && LA58_0<=ANON)||LA58_0==41||LA58_0==81||(LA58_0>=155 && LA58_0<=156)) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:202:11: constructTriples
                    {
                    pushFollow(FOLLOW_constructTriples_in_constructTemplate1210);
                    constructTriples182=constructTriples();
                    _fsp--;

                    adaptor.addChild(root_0, constructTriples182.getTree());

                    }
                    break;

            }

            char_literal183=(Token)input.LT(1);
            match(input,48,FOLLOW_48_in_constructTemplate1213); 
            char_literal183_tree = (Object)adaptor.create(char_literal183);
            adaptor.addChild(root_0, char_literal183_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end constructTemplate

    public static class constructTriples_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start constructTriples
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:205:1: constructTriples : triplesSameSubject ( '.' ( constructTriples )? )? ;
    public final constructTriples_return constructTriples() throws RecognitionException {
        constructTriples_return retval = new constructTriples_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal185=null;
        triplesSameSubject_return triplesSameSubject184 = null;

        constructTriples_return constructTriples186 = null;


        Object char_literal185_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:206:5: ( triplesSameSubject ( '.' ( constructTriples )? )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:206:7: triplesSameSubject ( '.' ( constructTriples )? )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_triplesSameSubject_in_constructTriples1228);
            triplesSameSubject184=triplesSameSubject();
            _fsp--;

            adaptor.addChild(root_0, triplesSameSubject184.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:206:26: ( '.' ( constructTriples )? )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==62) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:206:28: '.' ( constructTriples )?
                    {
                    char_literal185=(Token)input.LT(1);
                    match(input,62,FOLLOW_62_in_constructTriples1232); 
                    char_literal185_tree = (Object)adaptor.create(char_literal185);
                    adaptor.addChild(root_0, char_literal185_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:206:32: ( constructTriples )?
                    int alt59=2;
                    int LA59_0 = input.LA(1);

                    if ( ((LA59_0>=IRI_REF && LA59_0<=VAR2)||(LA59_0>=DECIMAL && LA59_0<=ANON)||LA59_0==41||LA59_0==81||(LA59_0>=155 && LA59_0<=156)) ) {
                        alt59=1;
                    }
                    switch (alt59) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:206:32: constructTriples
                            {
                            pushFollow(FOLLOW_constructTriples_in_constructTriples1234);
                            constructTriples186=constructTriples();
                            _fsp--;

                            adaptor.addChild(root_0, constructTriples186.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end constructTriples

    public static class triplesSameSubject_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start triplesSameSubject
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:209:1: triplesSameSubject : ( varOrTerm propertyListNotEmpty | triplesNode propertyList );
    public final triplesSameSubject_return triplesSameSubject() throws RecognitionException {
        triplesSameSubject_return retval = new triplesSameSubject_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        varOrTerm_return varOrTerm187 = null;

        propertyListNotEmpty_return propertyListNotEmpty188 = null;

        triplesNode_return triplesNode189 = null;

        propertyList_return propertyList190 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:210:5: ( varOrTerm propertyListNotEmpty | triplesNode propertyList )
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( ((LA61_0>=IRI_REF && LA61_0<=VAR2)||(LA61_0>=DECIMAL && LA61_0<=ANON)||(LA61_0>=155 && LA61_0<=156)) ) {
                alt61=1;
            }
            else if ( (LA61_0==41||LA61_0==81) ) {
                alt61=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("209:1: triplesSameSubject : ( varOrTerm propertyListNotEmpty | triplesNode propertyList );", 61, 0, input);

                throw nvae;
            }
            switch (alt61) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:210:7: varOrTerm propertyListNotEmpty
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_varOrTerm_in_triplesSameSubject1253);
                    varOrTerm187=varOrTerm();
                    _fsp--;

                    adaptor.addChild(root_0, varOrTerm187.getTree());
                    pushFollow(FOLLOW_propertyListNotEmpty_in_triplesSameSubject1255);
                    propertyListNotEmpty188=propertyListNotEmpty();
                    _fsp--;

                    adaptor.addChild(root_0, propertyListNotEmpty188.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:210:40: triplesNode propertyList
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_triplesNode_in_triplesSameSubject1259);
                    triplesNode189=triplesNode();
                    _fsp--;

                    adaptor.addChild(root_0, triplesNode189.getTree());
                    pushFollow(FOLLOW_propertyList_in_triplesSameSubject1261);
                    propertyList190=propertyList();
                    _fsp--;

                    adaptor.addChild(root_0, propertyList190.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end triplesSameSubject

    public static class propertyList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start propertyList
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:213:1: propertyList : ( propertyListNotEmpty )? ;
    public final propertyList_return propertyList() throws RecognitionException {
        propertyList_return retval = new propertyList_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        propertyListNotEmpty_return propertyListNotEmpty191 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:214:5: ( ( propertyListNotEmpty )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:214:7: ( propertyListNotEmpty )?
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:214:7: ( propertyListNotEmpty )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( ((LA62_0>=IRI_REF && LA62_0<=PNAME_NS)||(LA62_0>=VAR1 && LA62_0<=VAR2)||LA62_0==PNAME_LN||LA62_0==74) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:214:7: propertyListNotEmpty
                    {
                    pushFollow(FOLLOW_propertyListNotEmpty_in_propertyList1276);
                    propertyListNotEmpty191=propertyListNotEmpty();
                    _fsp--;

                    adaptor.addChild(root_0, propertyListNotEmpty191.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end propertyList

    public static class propertyListNotEmpty_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start propertyListNotEmpty
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:217:1: propertyListNotEmpty : verb objectList ( ';' ( verb objectList )? )* ;
    public final propertyListNotEmpty_return propertyListNotEmpty() throws RecognitionException {
        propertyListNotEmpty_return retval = new propertyListNotEmpty_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal194=null;
        verb_return verb192 = null;

        objectList_return objectList193 = null;

        verb_return verb195 = null;

        objectList_return objectList196 = null;


        Object char_literal194_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:218:2: ( verb objectList ( ';' ( verb objectList )? )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:218:4: verb objectList ( ';' ( verb objectList )? )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_verb_in_propertyListNotEmpty1289);
            verb192=verb();
            _fsp--;

            adaptor.addChild(root_0, verb192.getTree());
            pushFollow(FOLLOW_objectList_in_propertyListNotEmpty1291);
            objectList193=objectList();
            _fsp--;

            adaptor.addChild(root_0, objectList193.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:218:20: ( ';' ( verb objectList )? )*
            loop64:
            do {
                int alt64=2;
                int LA64_0 = input.LA(1);

                if ( (LA64_0==73) ) {
                    alt64=1;
                }


                switch (alt64) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:218:22: ';' ( verb objectList )?
            	    {
            	    char_literal194=(Token)input.LT(1);
            	    match(input,73,FOLLOW_73_in_propertyListNotEmpty1295); 
            	    char_literal194_tree = (Object)adaptor.create(char_literal194);
            	    adaptor.addChild(root_0, char_literal194_tree);

            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:218:26: ( verb objectList )?
            	    int alt63=2;
            	    int LA63_0 = input.LA(1);

            	    if ( ((LA63_0>=IRI_REF && LA63_0<=PNAME_NS)||(LA63_0>=VAR1 && LA63_0<=VAR2)||LA63_0==PNAME_LN||LA63_0==74) ) {
            	        alt63=1;
            	    }
            	    switch (alt63) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:218:28: verb objectList
            	            {
            	            pushFollow(FOLLOW_verb_in_propertyListNotEmpty1299);
            	            verb195=verb();
            	            _fsp--;

            	            adaptor.addChild(root_0, verb195.getTree());
            	            pushFollow(FOLLOW_objectList_in_propertyListNotEmpty1301);
            	            objectList196=objectList();
            	            _fsp--;

            	            adaptor.addChild(root_0, objectList196.getTree());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop64;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end propertyListNotEmpty

    public static class verb_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start verb
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:221:1: verb : ( varOrIRIref | 'a' );
    public final verb_return verb() throws RecognitionException {
        verb_return retval = new verb_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal198=null;
        varOrIRIref_return varOrIRIref197 = null;


        Object char_literal198_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:222:2: ( varOrIRIref | 'a' )
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( ((LA65_0>=IRI_REF && LA65_0<=PNAME_NS)||(LA65_0>=VAR1 && LA65_0<=VAR2)||LA65_0==PNAME_LN) ) {
                alt65=1;
            }
            else if ( (LA65_0==74) ) {
                alt65=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("221:1: verb : ( varOrIRIref | 'a' );", 65, 0, input);

                throw nvae;
            }
            switch (alt65) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:222:4: varOrIRIref
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_varOrIRIref_in_verb1319);
                    varOrIRIref197=varOrIRIref();
                    _fsp--;

                    adaptor.addChild(root_0, varOrIRIref197.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:222:18: 'a'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal198=(Token)input.LT(1);
                    match(input,74,FOLLOW_74_in_verb1323); 
                    char_literal198_tree = (Object)adaptor.create(char_literal198);
                    adaptor.addChild(root_0, char_literal198_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end verb

    public static class objectList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start objectList
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:225:1: objectList : object ( ',' object )* ;
    public final objectList_return objectList() throws RecognitionException {
        objectList_return retval = new objectList_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal200=null;
        object_return object199 = null;

        object_return object201 = null;


        Object char_literal200_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:226:2: ( object ( ',' object )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:226:4: object ( ',' object )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_object_in_objectList1335);
            object199=object();
            _fsp--;

            adaptor.addChild(root_0, object199.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:226:11: ( ',' object )*
            loop66:
            do {
                int alt66=2;
                int LA66_0 = input.LA(1);

                if ( (LA66_0==72) ) {
                    alt66=1;
                }


                switch (alt66) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:226:13: ',' object
            	    {
            	    char_literal200=(Token)input.LT(1);
            	    match(input,72,FOLLOW_72_in_objectList1339); 
            	    char_literal200_tree = (Object)adaptor.create(char_literal200);
            	    adaptor.addChild(root_0, char_literal200_tree);

            	    pushFollow(FOLLOW_object_in_objectList1341);
            	    object201=object();
            	    _fsp--;

            	    adaptor.addChild(root_0, object201.getTree());

            	    }
            	    break;

            	default :
            	    break loop66;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end objectList

    public static class object_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start object
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:229:1: object : graphNode ;
    public final object_return object() throws RecognitionException {
        object_return retval = new object_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        graphNode_return graphNode202 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:230:5: ( graphNode )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:230:7: graphNode
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_graphNode_in_object1359);
            graphNode202=graphNode();
            _fsp--;

            adaptor.addChild(root_0, graphNode202.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end object

    public static class triplesSameSubjectPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start triplesSameSubjectPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:233:1: triplesSameSubjectPath : ( varOrTerm propertyListPathNotEmpty | triplesNodePath propertyListPath );
    public final triplesSameSubjectPath_return triplesSameSubjectPath() throws RecognitionException {
        triplesSameSubjectPath_return retval = new triplesSameSubjectPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        varOrTerm_return varOrTerm203 = null;

        propertyListPathNotEmpty_return propertyListPathNotEmpty204 = null;

        triplesNodePath_return triplesNodePath205 = null;

        propertyListPath_return propertyListPath206 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:234:2: ( varOrTerm propertyListPathNotEmpty | triplesNodePath propertyListPath )
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( ((LA67_0>=IRI_REF && LA67_0<=VAR2)||(LA67_0>=DECIMAL && LA67_0<=ANON)||(LA67_0>=155 && LA67_0<=156)) ) {
                alt67=1;
            }
            else if ( (LA67_0==41||LA67_0==81) ) {
                alt67=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("233:1: triplesSameSubjectPath : ( varOrTerm propertyListPathNotEmpty | triplesNodePath propertyListPath );", 67, 0, input);

                throw nvae;
            }
            switch (alt67) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:234:4: varOrTerm propertyListPathNotEmpty
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_varOrTerm_in_triplesSameSubjectPath1371);
                    varOrTerm203=varOrTerm();
                    _fsp--;

                    adaptor.addChild(root_0, varOrTerm203.getTree());
                    pushFollow(FOLLOW_propertyListPathNotEmpty_in_triplesSameSubjectPath1373);
                    propertyListPathNotEmpty204=propertyListPathNotEmpty();
                    _fsp--;

                    adaptor.addChild(root_0, propertyListPathNotEmpty204.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:234:41: triplesNodePath propertyListPath
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_triplesNodePath_in_triplesSameSubjectPath1377);
                    triplesNodePath205=triplesNodePath();
                    _fsp--;

                    adaptor.addChild(root_0, triplesNodePath205.getTree());
                    pushFollow(FOLLOW_propertyListPath_in_triplesSameSubjectPath1379);
                    propertyListPath206=propertyListPath();
                    _fsp--;

                    adaptor.addChild(root_0, propertyListPath206.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end triplesSameSubjectPath

    public static class propertyListPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start propertyListPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:237:1: propertyListPath : ( propertyListPathNotEmpty )? ;
    public final propertyListPath_return propertyListPath() throws RecognitionException {
        propertyListPath_return retval = new propertyListPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        propertyListPathNotEmpty_return propertyListPathNotEmpty207 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:238:2: ( ( propertyListPathNotEmpty )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:238:4: ( propertyListPathNotEmpty )?
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:238:4: ( propertyListPathNotEmpty )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( ((LA68_0>=IRI_REF && LA68_0<=PNAME_NS)||(LA68_0>=VAR1 && LA68_0<=VAR2)||LA68_0==PNAME_LN||LA68_0==41||LA68_0==74||LA68_0==77||LA68_0==80) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:238:4: propertyListPathNotEmpty
                    {
                    pushFollow(FOLLOW_propertyListPathNotEmpty_in_propertyListPath1391);
                    propertyListPathNotEmpty207=propertyListPathNotEmpty();
                    _fsp--;

                    adaptor.addChild(root_0, propertyListPathNotEmpty207.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end propertyListPath

    public static class propertyListPathNotEmpty_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start propertyListPathNotEmpty
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:241:1: propertyListPathNotEmpty : ( verbPath | verbSimple ) objectListPath ( ';' ( ( verbPath | verbSimple ) objectList )? )* ;
    public final propertyListPathNotEmpty_return propertyListPathNotEmpty() throws RecognitionException {
        propertyListPathNotEmpty_return retval = new propertyListPathNotEmpty_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal211=null;
        verbPath_return verbPath208 = null;

        verbSimple_return verbSimple209 = null;

        objectListPath_return objectListPath210 = null;

        verbPath_return verbPath212 = null;

        verbSimple_return verbSimple213 = null;

        objectList_return objectList214 = null;


        Object char_literal211_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:5: ( ( verbPath | verbSimple ) objectListPath ( ';' ( ( verbPath | verbSimple ) objectList )? )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:7: ( verbPath | verbSimple ) objectListPath ( ';' ( ( verbPath | verbSimple ) objectList )? )*
            {
            root_0 = (Object)adaptor.nil();

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:7: ( verbPath | verbSimple )
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( ((LA69_0>=IRI_REF && LA69_0<=PNAME_NS)||LA69_0==PNAME_LN||LA69_0==41||LA69_0==74||LA69_0==77||LA69_0==80) ) {
                alt69=1;
            }
            else if ( ((LA69_0>=VAR1 && LA69_0<=VAR2)) ) {
                alt69=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("242:7: ( verbPath | verbSimple )", 69, 0, input);

                throw nvae;
            }
            switch (alt69) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:9: verbPath
                    {
                    pushFollow(FOLLOW_verbPath_in_propertyListPathNotEmpty1409);
                    verbPath208=verbPath();
                    _fsp--;

                    adaptor.addChild(root_0, verbPath208.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:20: verbSimple
                    {
                    pushFollow(FOLLOW_verbSimple_in_propertyListPathNotEmpty1413);
                    verbSimple209=verbSimple();
                    _fsp--;

                    adaptor.addChild(root_0, verbSimple209.getTree());

                    }
                    break;

            }

            pushFollow(FOLLOW_objectListPath_in_propertyListPathNotEmpty1417);
            objectListPath210=objectListPath();
            _fsp--;

            adaptor.addChild(root_0, objectListPath210.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:48: ( ';' ( ( verbPath | verbSimple ) objectList )? )*
            loop72:
            do {
                int alt72=2;
                int LA72_0 = input.LA(1);

                if ( (LA72_0==73) ) {
                    alt72=1;
                }


                switch (alt72) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:50: ';' ( ( verbPath | verbSimple ) objectList )?
            	    {
            	    char_literal211=(Token)input.LT(1);
            	    match(input,73,FOLLOW_73_in_propertyListPathNotEmpty1421); 
            	    char_literal211_tree = (Object)adaptor.create(char_literal211);
            	    adaptor.addChild(root_0, char_literal211_tree);

            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:54: ( ( verbPath | verbSimple ) objectList )?
            	    int alt71=2;
            	    int LA71_0 = input.LA(1);

            	    if ( ((LA71_0>=IRI_REF && LA71_0<=PNAME_NS)||(LA71_0>=VAR1 && LA71_0<=VAR2)||LA71_0==PNAME_LN||LA71_0==41||LA71_0==74||LA71_0==77||LA71_0==80) ) {
            	        alt71=1;
            	    }
            	    switch (alt71) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:56: ( verbPath | verbSimple ) objectList
            	            {
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:56: ( verbPath | verbSimple )
            	            int alt70=2;
            	            int LA70_0 = input.LA(1);

            	            if ( ((LA70_0>=IRI_REF && LA70_0<=PNAME_NS)||LA70_0==PNAME_LN||LA70_0==41||LA70_0==74||LA70_0==77||LA70_0==80) ) {
            	                alt70=1;
            	            }
            	            else if ( ((LA70_0>=VAR1 && LA70_0<=VAR2)) ) {
            	                alt70=2;
            	            }
            	            else {
            	                NoViableAltException nvae =
            	                    new NoViableAltException("242:56: ( verbPath | verbSimple )", 70, 0, input);

            	                throw nvae;
            	            }
            	            switch (alt70) {
            	                case 1 :
            	                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:58: verbPath
            	                    {
            	                    pushFollow(FOLLOW_verbPath_in_propertyListPathNotEmpty1427);
            	                    verbPath212=verbPath();
            	                    _fsp--;

            	                    adaptor.addChild(root_0, verbPath212.getTree());

            	                    }
            	                    break;
            	                case 2 :
            	                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:242:69: verbSimple
            	                    {
            	                    pushFollow(FOLLOW_verbSimple_in_propertyListPathNotEmpty1431);
            	                    verbSimple213=verbSimple();
            	                    _fsp--;

            	                    adaptor.addChild(root_0, verbSimple213.getTree());

            	                    }
            	                    break;

            	            }

            	            pushFollow(FOLLOW_objectList_in_propertyListPathNotEmpty1435);
            	            objectList214=objectList();
            	            _fsp--;

            	            adaptor.addChild(root_0, objectList214.getTree());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop72;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end propertyListPathNotEmpty

    public static class verbPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start verbPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:245:1: verbPath : path ;
    public final verbPath_return verbPath() throws RecognitionException {
        verbPath_return retval = new verbPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        path_return path215 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:246:5: ( path )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:246:7: path
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_path_in_verbPath1456);
            path215=path();
            _fsp--;

            adaptor.addChild(root_0, path215.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end verbPath

    public static class verbSimple_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start verbSimple
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:249:1: verbSimple : var ;
    public final verbSimple_return verbSimple() throws RecognitionException {
        verbSimple_return retval = new verbSimple_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        var_return var216 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:250:5: ( var )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:250:7: var
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_var_in_verbSimple1471);
            var216=var();
            _fsp--;

            adaptor.addChild(root_0, var216.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end verbSimple

    public static class objectListPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start objectListPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:253:1: objectListPath : objectPath ( ',' objectPath )* ;
    public final objectListPath_return objectListPath() throws RecognitionException {
        objectListPath_return retval = new objectListPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal218=null;
        objectPath_return objectPath217 = null;

        objectPath_return objectPath219 = null;


        Object char_literal218_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:254:5: ( objectPath ( ',' objectPath )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:254:7: objectPath ( ',' objectPath )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_objectPath_in_objectListPath1486);
            objectPath217=objectPath();
            _fsp--;

            adaptor.addChild(root_0, objectPath217.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:254:18: ( ',' objectPath )*
            loop73:
            do {
                int alt73=2;
                int LA73_0 = input.LA(1);

                if ( (LA73_0==72) ) {
                    alt73=1;
                }


                switch (alt73) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:254:20: ',' objectPath
            	    {
            	    char_literal218=(Token)input.LT(1);
            	    match(input,72,FOLLOW_72_in_objectListPath1490); 
            	    char_literal218_tree = (Object)adaptor.create(char_literal218);
            	    adaptor.addChild(root_0, char_literal218_tree);

            	    pushFollow(FOLLOW_objectPath_in_objectListPath1492);
            	    objectPath219=objectPath();
            	    _fsp--;

            	    adaptor.addChild(root_0, objectPath219.getTree());

            	    }
            	    break;

            	default :
            	    break loop73;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end objectListPath

    public static class objectPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start objectPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:257:1: objectPath : graphNodePath ;
    public final objectPath_return objectPath() throws RecognitionException {
        objectPath_return retval = new objectPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        graphNodePath_return graphNodePath220 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:258:5: ( graphNodePath )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:258:7: graphNodePath
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_graphNodePath_in_objectPath1510);
            graphNodePath220=graphNodePath();
            _fsp--;

            adaptor.addChild(root_0, graphNodePath220.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end objectPath

    public static class path_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start path
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:261:1: path : pathAlternative ;
    public final path_return path() throws RecognitionException {
        path_return retval = new path_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        pathAlternative_return pathAlternative221 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:262:5: ( pathAlternative )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:262:7: pathAlternative
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_pathAlternative_in_path1525);
            pathAlternative221=pathAlternative();
            _fsp--;

            adaptor.addChild(root_0, pathAlternative221.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end path

    public static class pathAlternative_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathAlternative
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:265:1: pathAlternative : pathSequence ( '|' pathSequence )* ;
    public final pathAlternative_return pathAlternative() throws RecognitionException {
        pathAlternative_return retval = new pathAlternative_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal223=null;
        pathSequence_return pathSequence222 = null;

        pathSequence_return pathSequence224 = null;


        Object char_literal223_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:266:5: ( pathSequence ( '|' pathSequence )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:266:7: pathSequence ( '|' pathSequence )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_pathSequence_in_pathAlternative1540);
            pathSequence222=pathSequence();
            _fsp--;

            adaptor.addChild(root_0, pathSequence222.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:266:20: ( '|' pathSequence )*
            loop74:
            do {
                int alt74=2;
                int LA74_0 = input.LA(1);

                if ( (LA74_0==75) ) {
                    alt74=1;
                }


                switch (alt74) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:266:22: '|' pathSequence
            	    {
            	    char_literal223=(Token)input.LT(1);
            	    match(input,75,FOLLOW_75_in_pathAlternative1544); 
            	    char_literal223_tree = (Object)adaptor.create(char_literal223);
            	    adaptor.addChild(root_0, char_literal223_tree);

            	    pushFollow(FOLLOW_pathSequence_in_pathAlternative1546);
            	    pathSequence224=pathSequence();
            	    _fsp--;

            	    adaptor.addChild(root_0, pathSequence224.getTree());

            	    }
            	    break;

            	default :
            	    break loop74;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathAlternative

    public static class pathSequence_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathSequence
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:269:1: pathSequence : pathEltOrInverse ( '/' pathEltOrInverse )* ;
    public final pathSequence_return pathSequence() throws RecognitionException {
        pathSequence_return retval = new pathSequence_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal226=null;
        pathEltOrInverse_return pathEltOrInverse225 = null;

        pathEltOrInverse_return pathEltOrInverse227 = null;


        Object char_literal226_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:270:5: ( pathEltOrInverse ( '/' pathEltOrInverse )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:270:7: pathEltOrInverse ( '/' pathEltOrInverse )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_pathEltOrInverse_in_pathSequence1563);
            pathEltOrInverse225=pathEltOrInverse();
            _fsp--;

            adaptor.addChild(root_0, pathEltOrInverse225.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:270:24: ( '/' pathEltOrInverse )*
            loop75:
            do {
                int alt75=2;
                int LA75_0 = input.LA(1);

                if ( (LA75_0==76) ) {
                    alt75=1;
                }


                switch (alt75) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:270:26: '/' pathEltOrInverse
            	    {
            	    char_literal226=(Token)input.LT(1);
            	    match(input,76,FOLLOW_76_in_pathSequence1567); 
            	    char_literal226_tree = (Object)adaptor.create(char_literal226);
            	    adaptor.addChild(root_0, char_literal226_tree);

            	    pushFollow(FOLLOW_pathEltOrInverse_in_pathSequence1569);
            	    pathEltOrInverse227=pathEltOrInverse();
            	    _fsp--;

            	    adaptor.addChild(root_0, pathEltOrInverse227.getTree());

            	    }
            	    break;

            	default :
            	    break loop75;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathSequence

    public static class pathElt_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathElt
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:273:1: pathElt : pathPrimary ( pathMod )? ;
    public final pathElt_return pathElt() throws RecognitionException {
        pathElt_return retval = new pathElt_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        pathPrimary_return pathPrimary228 = null;

        pathMod_return pathMod229 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:274:5: ( pathPrimary ( pathMod )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:274:7: pathPrimary ( pathMod )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_pathPrimary_in_pathElt1587);
            pathPrimary228=pathPrimary();
            _fsp--;

            adaptor.addChild(root_0, pathPrimary228.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:274:19: ( pathMod )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==44||(LA76_0>=78 && LA76_0<=79)) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:274:19: pathMod
                    {
                    pushFollow(FOLLOW_pathMod_in_pathElt1589);
                    pathMod229=pathMod();
                    _fsp--;

                    adaptor.addChild(root_0, pathMod229.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathElt

    public static class pathEltOrInverse_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathEltOrInverse
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:277:1: pathEltOrInverse : ( pathElt | '^' pathElt );
    public final pathEltOrInverse_return pathEltOrInverse() throws RecognitionException {
        pathEltOrInverse_return retval = new pathEltOrInverse_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal231=null;
        pathElt_return pathElt230 = null;

        pathElt_return pathElt232 = null;


        Object char_literal231_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:278:5: ( pathElt | '^' pathElt )
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( ((LA77_0>=IRI_REF && LA77_0<=PNAME_NS)||LA77_0==PNAME_LN||LA77_0==41||LA77_0==74||LA77_0==80) ) {
                alt77=1;
            }
            else if ( (LA77_0==77) ) {
                alt77=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("277:1: pathEltOrInverse : ( pathElt | '^' pathElt );", 77, 0, input);

                throw nvae;
            }
            switch (alt77) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:278:7: pathElt
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_pathElt_in_pathEltOrInverse1605);
                    pathElt230=pathElt();
                    _fsp--;

                    adaptor.addChild(root_0, pathElt230.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:278:17: '^' pathElt
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal231=(Token)input.LT(1);
                    match(input,77,FOLLOW_77_in_pathEltOrInverse1609); 
                    char_literal231_tree = (Object)adaptor.create(char_literal231);
                    adaptor.addChild(root_0, char_literal231_tree);

                    pushFollow(FOLLOW_pathElt_in_pathEltOrInverse1611);
                    pathElt232=pathElt();
                    _fsp--;

                    adaptor.addChild(root_0, pathElt232.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathEltOrInverse

    public static class pathMod_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathMod
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:281:1: pathMod : ( '?' | '*' | '+' );
    public final pathMod_return pathMod() throws RecognitionException {
        pathMod_return retval = new pathMod_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set233=null;

        Object set233_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:282:5: ( '?' | '*' | '+' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set233=(Token)input.LT(1);
            if ( input.LA(1)==44||(input.LA(1)>=78 && input.LA(1)<=79) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set233));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_pathMod0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathMod

    public static class pathPrimary_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathPrimary
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:285:1: pathPrimary : ( iriRef | 'a' | '!' pathNegatedPropertySet | '(' path ')' );
    public final pathPrimary_return pathPrimary() throws RecognitionException {
        pathPrimary_return retval = new pathPrimary_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal235=null;
        Token char_literal236=null;
        Token char_literal238=null;
        Token char_literal240=null;
        iriRef_return iriRef234 = null;

        pathNegatedPropertySet_return pathNegatedPropertySet237 = null;

        path_return path239 = null;


        Object char_literal235_tree=null;
        Object char_literal236_tree=null;
        Object char_literal238_tree=null;
        Object char_literal240_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:286:5: ( iriRef | 'a' | '!' pathNegatedPropertySet | '(' path ')' )
            int alt78=4;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt78=1;
                }
                break;
            case 74:
                {
                alt78=2;
                }
                break;
            case 80:
                {
                alt78=3;
                }
                break;
            case 41:
                {
                alt78=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("285:1: pathPrimary : ( iriRef | 'a' | '!' pathNegatedPropertySet | '(' path ')' );", 78, 0, input);

                throw nvae;
            }

            switch (alt78) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:286:7: iriRef
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_iriRef_in_pathPrimary1649);
                    iriRef234=iriRef();
                    _fsp--;

                    adaptor.addChild(root_0, iriRef234.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:286:16: 'a'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal235=(Token)input.LT(1);
                    match(input,74,FOLLOW_74_in_pathPrimary1653); 
                    char_literal235_tree = (Object)adaptor.create(char_literal235);
                    adaptor.addChild(root_0, char_literal235_tree);


                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:286:22: '!' pathNegatedPropertySet
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal236=(Token)input.LT(1);
                    match(input,80,FOLLOW_80_in_pathPrimary1657); 
                    char_literal236_tree = (Object)adaptor.create(char_literal236);
                    adaptor.addChild(root_0, char_literal236_tree);

                    pushFollow(FOLLOW_pathNegatedPropertySet_in_pathPrimary1659);
                    pathNegatedPropertySet237=pathNegatedPropertySet();
                    _fsp--;

                    adaptor.addChild(root_0, pathNegatedPropertySet237.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:286:51: '(' path ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal238=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_pathPrimary1663); 
                    char_literal238_tree = (Object)adaptor.create(char_literal238);
                    adaptor.addChild(root_0, char_literal238_tree);

                    pushFollow(FOLLOW_path_in_pathPrimary1665);
                    path239=path();
                    _fsp--;

                    adaptor.addChild(root_0, path239.getTree());
                    char_literal240=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_pathPrimary1667); 
                    char_literal240_tree = (Object)adaptor.create(char_literal240);
                    adaptor.addChild(root_0, char_literal240_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathPrimary

    public static class pathNegatedPropertySet_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathNegatedPropertySet
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:289:1: pathNegatedPropertySet : ( pathOneInPropertySet | '(' ( pathOneInPropertySet ( '|' pathOneInPropertySet )* )? ')' );
    public final pathNegatedPropertySet_return pathNegatedPropertySet() throws RecognitionException {
        pathNegatedPropertySet_return retval = new pathNegatedPropertySet_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal242=null;
        Token char_literal244=null;
        Token char_literal246=null;
        pathOneInPropertySet_return pathOneInPropertySet241 = null;

        pathOneInPropertySet_return pathOneInPropertySet243 = null;

        pathOneInPropertySet_return pathOneInPropertySet245 = null;


        Object char_literal242_tree=null;
        Object char_literal244_tree=null;
        Object char_literal246_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:5: ( pathOneInPropertySet | '(' ( pathOneInPropertySet ( '|' pathOneInPropertySet )* )? ')' )
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( ((LA81_0>=IRI_REF && LA81_0<=PNAME_NS)||LA81_0==PNAME_LN||LA81_0==74||LA81_0==77) ) {
                alt81=1;
            }
            else if ( (LA81_0==41) ) {
                alt81=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("289:1: pathNegatedPropertySet : ( pathOneInPropertySet | '(' ( pathOneInPropertySet ( '|' pathOneInPropertySet )* )? ')' );", 81, 0, input);

                throw nvae;
            }
            switch (alt81) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:7: pathOneInPropertySet
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_pathOneInPropertySet_in_pathNegatedPropertySet1682);
                    pathOneInPropertySet241=pathOneInPropertySet();
                    _fsp--;

                    adaptor.addChild(root_0, pathOneInPropertySet241.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:30: '(' ( pathOneInPropertySet ( '|' pathOneInPropertySet )* )? ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal242=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_pathNegatedPropertySet1686); 
                    char_literal242_tree = (Object)adaptor.create(char_literal242);
                    adaptor.addChild(root_0, char_literal242_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:34: ( pathOneInPropertySet ( '|' pathOneInPropertySet )* )?
                    int alt80=2;
                    int LA80_0 = input.LA(1);

                    if ( ((LA80_0>=IRI_REF && LA80_0<=PNAME_NS)||LA80_0==PNAME_LN||LA80_0==74||LA80_0==77) ) {
                        alt80=1;
                    }
                    switch (alt80) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:36: pathOneInPropertySet ( '|' pathOneInPropertySet )*
                            {
                            pushFollow(FOLLOW_pathOneInPropertySet_in_pathNegatedPropertySet1690);
                            pathOneInPropertySet243=pathOneInPropertySet();
                            _fsp--;

                            adaptor.addChild(root_0, pathOneInPropertySet243.getTree());
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:57: ( '|' pathOneInPropertySet )*
                            loop79:
                            do {
                                int alt79=2;
                                int LA79_0 = input.LA(1);

                                if ( (LA79_0==75) ) {
                                    alt79=1;
                                }


                                switch (alt79) {
                            	case 1 :
                            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:290:59: '|' pathOneInPropertySet
                            	    {
                            	    char_literal244=(Token)input.LT(1);
                            	    match(input,75,FOLLOW_75_in_pathNegatedPropertySet1694); 
                            	    char_literal244_tree = (Object)adaptor.create(char_literal244);
                            	    adaptor.addChild(root_0, char_literal244_tree);

                            	    pushFollow(FOLLOW_pathOneInPropertySet_in_pathNegatedPropertySet1696);
                            	    pathOneInPropertySet245=pathOneInPropertySet();
                            	    _fsp--;

                            	    adaptor.addChild(root_0, pathOneInPropertySet245.getTree());

                            	    }
                            	    break;

                            	default :
                            	    break loop79;
                                }
                            } while (true);


                            }
                            break;

                    }

                    char_literal246=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_pathNegatedPropertySet1704); 
                    char_literal246_tree = (Object)adaptor.create(char_literal246);
                    adaptor.addChild(root_0, char_literal246_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathNegatedPropertySet

    public static class pathOneInPropertySet_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start pathOneInPropertySet
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:293:1: pathOneInPropertySet : ( iriRef | 'a' | '^' ( iriRef | 'a' ) );
    public final pathOneInPropertySet_return pathOneInPropertySet() throws RecognitionException {
        pathOneInPropertySet_return retval = new pathOneInPropertySet_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal248=null;
        Token char_literal249=null;
        Token char_literal251=null;
        iriRef_return iriRef247 = null;

        iriRef_return iriRef250 = null;


        Object char_literal248_tree=null;
        Object char_literal249_tree=null;
        Object char_literal251_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:2: ( iriRef | 'a' | '^' ( iriRef | 'a' ) )
            int alt83=3;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt83=1;
                }
                break;
            case 74:
                {
                alt83=2;
                }
                break;
            case 77:
                {
                alt83=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("293:1: pathOneInPropertySet : ( iriRef | 'a' | '^' ( iriRef | 'a' ) );", 83, 0, input);

                throw nvae;
            }

            switch (alt83) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:4: iriRef
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_iriRef_in_pathOneInPropertySet1716);
                    iriRef247=iriRef();
                    _fsp--;

                    adaptor.addChild(root_0, iriRef247.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:13: 'a'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal248=(Token)input.LT(1);
                    match(input,74,FOLLOW_74_in_pathOneInPropertySet1720); 
                    char_literal248_tree = (Object)adaptor.create(char_literal248);
                    adaptor.addChild(root_0, char_literal248_tree);


                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:19: '^' ( iriRef | 'a' )
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal249=(Token)input.LT(1);
                    match(input,77,FOLLOW_77_in_pathOneInPropertySet1724); 
                    char_literal249_tree = (Object)adaptor.create(char_literal249);
                    adaptor.addChild(root_0, char_literal249_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:23: ( iriRef | 'a' )
                    int alt82=2;
                    int LA82_0 = input.LA(1);

                    if ( ((LA82_0>=IRI_REF && LA82_0<=PNAME_NS)||LA82_0==PNAME_LN) ) {
                        alt82=1;
                    }
                    else if ( (LA82_0==74) ) {
                        alt82=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("294:23: ( iriRef | 'a' )", 82, 0, input);

                        throw nvae;
                    }
                    switch (alt82) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:25: iriRef
                            {
                            pushFollow(FOLLOW_iriRef_in_pathOneInPropertySet1728);
                            iriRef250=iriRef();
                            _fsp--;

                            adaptor.addChild(root_0, iriRef250.getTree());

                            }
                            break;
                        case 2 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:294:34: 'a'
                            {
                            char_literal251=(Token)input.LT(1);
                            match(input,74,FOLLOW_74_in_pathOneInPropertySet1732); 
                            char_literal251_tree = (Object)adaptor.create(char_literal251);
                            adaptor.addChild(root_0, char_literal251_tree);


                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end pathOneInPropertySet

    public static class triplesNode_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start triplesNode
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:297:1: triplesNode : ( collection | blankNodePropertyList );
    public final triplesNode_return triplesNode() throws RecognitionException {
        triplesNode_return retval = new triplesNode_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        collection_return collection252 = null;

        blankNodePropertyList_return blankNodePropertyList253 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:298:5: ( collection | blankNodePropertyList )
            int alt84=2;
            int LA84_0 = input.LA(1);

            if ( (LA84_0==41) ) {
                alt84=1;
            }
            else if ( (LA84_0==81) ) {
                alt84=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("297:1: triplesNode : ( collection | blankNodePropertyList );", 84, 0, input);

                throw nvae;
            }
            switch (alt84) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:298:7: collection
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_collection_in_triplesNode1749);
                    collection252=collection();
                    _fsp--;

                    adaptor.addChild(root_0, collection252.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:298:20: blankNodePropertyList
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_blankNodePropertyList_in_triplesNode1753);
                    blankNodePropertyList253=blankNodePropertyList();
                    _fsp--;

                    adaptor.addChild(root_0, blankNodePropertyList253.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end triplesNode

    public static class blankNodePropertyList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start blankNodePropertyList
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:301:1: blankNodePropertyList : '[' propertyListNotEmpty ']' ;
    public final blankNodePropertyList_return blankNodePropertyList() throws RecognitionException {
        blankNodePropertyList_return retval = new blankNodePropertyList_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal254=null;
        Token char_literal256=null;
        propertyListNotEmpty_return propertyListNotEmpty255 = null;


        Object char_literal254_tree=null;
        Object char_literal256_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:302:5: ( '[' propertyListNotEmpty ']' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:302:7: '[' propertyListNotEmpty ']'
            {
            root_0 = (Object)adaptor.nil();

            char_literal254=(Token)input.LT(1);
            match(input,81,FOLLOW_81_in_blankNodePropertyList1768); 
            char_literal254_tree = (Object)adaptor.create(char_literal254);
            adaptor.addChild(root_0, char_literal254_tree);

            pushFollow(FOLLOW_propertyListNotEmpty_in_blankNodePropertyList1770);
            propertyListNotEmpty255=propertyListNotEmpty();
            _fsp--;

            adaptor.addChild(root_0, propertyListNotEmpty255.getTree());
            char_literal256=(Token)input.LT(1);
            match(input,82,FOLLOW_82_in_blankNodePropertyList1772); 
            char_literal256_tree = (Object)adaptor.create(char_literal256);
            adaptor.addChild(root_0, char_literal256_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end blankNodePropertyList

    public static class triplesNodePath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start triplesNodePath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:305:1: triplesNodePath : ( collectionPath | blankNodePropertyListPath );
    public final triplesNodePath_return triplesNodePath() throws RecognitionException {
        triplesNodePath_return retval = new triplesNodePath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        collectionPath_return collectionPath257 = null;

        blankNodePropertyListPath_return blankNodePropertyListPath258 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:306:5: ( collectionPath | blankNodePropertyListPath )
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==41) ) {
                alt85=1;
            }
            else if ( (LA85_0==81) ) {
                alt85=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("305:1: triplesNodePath : ( collectionPath | blankNodePropertyListPath );", 85, 0, input);

                throw nvae;
            }
            switch (alt85) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:306:7: collectionPath
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_collectionPath_in_triplesNodePath1787);
                    collectionPath257=collectionPath();
                    _fsp--;

                    adaptor.addChild(root_0, collectionPath257.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:306:24: blankNodePropertyListPath
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_blankNodePropertyListPath_in_triplesNodePath1791);
                    blankNodePropertyListPath258=blankNodePropertyListPath();
                    _fsp--;

                    adaptor.addChild(root_0, blankNodePropertyListPath258.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end triplesNodePath

    public static class blankNodePropertyListPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start blankNodePropertyListPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:309:1: blankNodePropertyListPath : '[' propertyListPathNotEmpty ']' ;
    public final blankNodePropertyListPath_return blankNodePropertyListPath() throws RecognitionException {
        blankNodePropertyListPath_return retval = new blankNodePropertyListPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal259=null;
        Token char_literal261=null;
        propertyListPathNotEmpty_return propertyListPathNotEmpty260 = null;


        Object char_literal259_tree=null;
        Object char_literal261_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:310:2: ( '[' propertyListPathNotEmpty ']' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:310:4: '[' propertyListPathNotEmpty ']'
            {
            root_0 = (Object)adaptor.nil();

            char_literal259=(Token)input.LT(1);
            match(input,81,FOLLOW_81_in_blankNodePropertyListPath1803); 
            char_literal259_tree = (Object)adaptor.create(char_literal259);
            adaptor.addChild(root_0, char_literal259_tree);

            pushFollow(FOLLOW_propertyListPathNotEmpty_in_blankNodePropertyListPath1805);
            propertyListPathNotEmpty260=propertyListPathNotEmpty();
            _fsp--;

            adaptor.addChild(root_0, propertyListPathNotEmpty260.getTree());
            char_literal261=(Token)input.LT(1);
            match(input,82,FOLLOW_82_in_blankNodePropertyListPath1807); 
            char_literal261_tree = (Object)adaptor.create(char_literal261);
            adaptor.addChild(root_0, char_literal261_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end blankNodePropertyListPath

    public static class collection_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start collection
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:313:1: collection : '(' ( graphNode )+ ')' ;
    public final collection_return collection() throws RecognitionException {
        collection_return retval = new collection_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal262=null;
        Token char_literal264=null;
        graphNode_return graphNode263 = null;


        Object char_literal262_tree=null;
        Object char_literal264_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:314:5: ( '(' ( graphNode )+ ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:314:7: '(' ( graphNode )+ ')'
            {
            root_0 = (Object)adaptor.nil();

            char_literal262=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_collection1822); 
            char_literal262_tree = (Object)adaptor.create(char_literal262);
            adaptor.addChild(root_0, char_literal262_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:314:11: ( graphNode )+
            int cnt86=0;
            loop86:
            do {
                int alt86=2;
                int LA86_0 = input.LA(1);

                if ( ((LA86_0>=IRI_REF && LA86_0<=VAR2)||(LA86_0>=DECIMAL && LA86_0<=ANON)||LA86_0==41||LA86_0==81||(LA86_0>=155 && LA86_0<=156)) ) {
                    alt86=1;
                }


                switch (alt86) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:314:11: graphNode
            	    {
            	    pushFollow(FOLLOW_graphNode_in_collection1824);
            	    graphNode263=graphNode();
            	    _fsp--;

            	    adaptor.addChild(root_0, graphNode263.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt86 >= 1 ) break loop86;
                        EarlyExitException eee =
                            new EarlyExitException(86, input);
                        throw eee;
                }
                cnt86++;
            } while (true);

            char_literal264=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_collection1827); 
            char_literal264_tree = (Object)adaptor.create(char_literal264);
            adaptor.addChild(root_0, char_literal264_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end collection

    public static class collectionPath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start collectionPath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:317:1: collectionPath : '(' ( graphNodePath )+ ')' ;
    public final collectionPath_return collectionPath() throws RecognitionException {
        collectionPath_return retval = new collectionPath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal265=null;
        Token char_literal267=null;
        graphNodePath_return graphNodePath266 = null;


        Object char_literal265_tree=null;
        Object char_literal267_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:318:5: ( '(' ( graphNodePath )+ ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:318:7: '(' ( graphNodePath )+ ')'
            {
            root_0 = (Object)adaptor.nil();

            char_literal265=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_collectionPath1842); 
            char_literal265_tree = (Object)adaptor.create(char_literal265);
            adaptor.addChild(root_0, char_literal265_tree);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:318:11: ( graphNodePath )+
            int cnt87=0;
            loop87:
            do {
                int alt87=2;
                int LA87_0 = input.LA(1);

                if ( ((LA87_0>=IRI_REF && LA87_0<=VAR2)||(LA87_0>=DECIMAL && LA87_0<=ANON)||LA87_0==41||LA87_0==81||(LA87_0>=155 && LA87_0<=156)) ) {
                    alt87=1;
                }


                switch (alt87) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:318:11: graphNodePath
            	    {
            	    pushFollow(FOLLOW_graphNodePath_in_collectionPath1844);
            	    graphNodePath266=graphNodePath();
            	    _fsp--;

            	    adaptor.addChild(root_0, graphNodePath266.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt87 >= 1 ) break loop87;
                        EarlyExitException eee =
                            new EarlyExitException(87, input);
                        throw eee;
                }
                cnt87++;
            } while (true);

            char_literal267=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_collectionPath1847); 
            char_literal267_tree = (Object)adaptor.create(char_literal267);
            adaptor.addChild(root_0, char_literal267_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end collectionPath

    public static class graphNode_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start graphNode
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:321:1: graphNode : ( varOrTerm | triplesNode );
    public final graphNode_return graphNode() throws RecognitionException {
        graphNode_return retval = new graphNode_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        varOrTerm_return varOrTerm268 = null;

        triplesNode_return triplesNode269 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:322:5: ( varOrTerm | triplesNode )
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( ((LA88_0>=IRI_REF && LA88_0<=VAR2)||(LA88_0>=DECIMAL && LA88_0<=ANON)||(LA88_0>=155 && LA88_0<=156)) ) {
                alt88=1;
            }
            else if ( (LA88_0==41||LA88_0==81) ) {
                alt88=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("321:1: graphNode : ( varOrTerm | triplesNode );", 88, 0, input);

                throw nvae;
            }
            switch (alt88) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:322:7: varOrTerm
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_varOrTerm_in_graphNode1862);
                    varOrTerm268=varOrTerm();
                    _fsp--;

                    adaptor.addChild(root_0, varOrTerm268.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:322:19: triplesNode
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_triplesNode_in_graphNode1866);
                    triplesNode269=triplesNode();
                    _fsp--;

                    adaptor.addChild(root_0, triplesNode269.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end graphNode

    public static class graphNodePath_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start graphNodePath
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:325:1: graphNodePath : ( varOrTerm | triplesNodePath );
    public final graphNodePath_return graphNodePath() throws RecognitionException {
        graphNodePath_return retval = new graphNodePath_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        varOrTerm_return varOrTerm270 = null;

        triplesNodePath_return triplesNodePath271 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:326:5: ( varOrTerm | triplesNodePath )
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( ((LA89_0>=IRI_REF && LA89_0<=VAR2)||(LA89_0>=DECIMAL && LA89_0<=ANON)||(LA89_0>=155 && LA89_0<=156)) ) {
                alt89=1;
            }
            else if ( (LA89_0==41||LA89_0==81) ) {
                alt89=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("325:1: graphNodePath : ( varOrTerm | triplesNodePath );", 89, 0, input);

                throw nvae;
            }
            switch (alt89) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:326:7: varOrTerm
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_varOrTerm_in_graphNodePath1881);
                    varOrTerm270=varOrTerm();
                    _fsp--;

                    adaptor.addChild(root_0, varOrTerm270.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:326:19: triplesNodePath
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_triplesNodePath_in_graphNodePath1885);
                    triplesNodePath271=triplesNodePath();
                    _fsp--;

                    adaptor.addChild(root_0, triplesNodePath271.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end graphNodePath

    public static class varOrTerm_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start varOrTerm
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:329:1: varOrTerm : ( var | graphTerm );
    public final varOrTerm_return varOrTerm() throws RecognitionException {
        varOrTerm_return retval = new varOrTerm_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        var_return var272 = null;

        graphTerm_return graphTerm273 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:330:5: ( var | graphTerm )
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( ((LA90_0>=VAR1 && LA90_0<=VAR2)) ) {
                alt90=1;
            }
            else if ( ((LA90_0>=IRI_REF && LA90_0<=NIL)||(LA90_0>=DECIMAL && LA90_0<=ANON)||(LA90_0>=155 && LA90_0<=156)) ) {
                alt90=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("329:1: varOrTerm : ( var | graphTerm );", 90, 0, input);

                throw nvae;
            }
            switch (alt90) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:330:7: var
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_var_in_varOrTerm1900);
                    var272=var();
                    _fsp--;

                    adaptor.addChild(root_0, var272.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:331:7: graphTerm
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_graphTerm_in_varOrTerm1908);
                    graphTerm273=graphTerm();
                    _fsp--;

                    adaptor.addChild(root_0, graphTerm273.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end varOrTerm

    public static class varOrIRIref_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start varOrIRIref
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:334:1: varOrIRIref : ( var | iriRef );
    public final varOrIRIref_return varOrIRIref() throws RecognitionException {
        varOrIRIref_return retval = new varOrIRIref_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        var_return var274 = null;

        iriRef_return iriRef275 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:335:5: ( var | iriRef )
            int alt91=2;
            int LA91_0 = input.LA(1);

            if ( ((LA91_0>=VAR1 && LA91_0<=VAR2)) ) {
                alt91=1;
            }
            else if ( ((LA91_0>=IRI_REF && LA91_0<=PNAME_NS)||LA91_0==PNAME_LN) ) {
                alt91=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("334:1: varOrIRIref : ( var | iriRef );", 91, 0, input);

                throw nvae;
            }
            switch (alt91) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:335:7: var
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_var_in_varOrIRIref1926);
                    var274=var();
                    _fsp--;

                    adaptor.addChild(root_0, var274.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:335:13: iriRef
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_iriRef_in_varOrIRIref1930);
                    iriRef275=iriRef();
                    _fsp--;

                    adaptor.addChild(root_0, iriRef275.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end varOrIRIref

    public static class var_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start var
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:338:1: var : ( VAR1 | VAR2 );
    public final var_return var() throws RecognitionException {
        var_return retval = new var_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set276=null;

        Object set276_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:339:5: ( VAR1 | VAR2 )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set276=(Token)input.LT(1);
            if ( (input.LA(1)>=VAR1 && input.LA(1)<=VAR2) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set276));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_var0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end var

    public static class graphTerm_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start graphTerm
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:343:1: graphTerm : ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | blankNode | NIL );
    public final graphTerm_return graphTerm() throws RecognitionException {
        graphTerm_return retval = new graphTerm_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token NIL282=null;
        iriRef_return iriRef277 = null;

        rdfLiteral_return rdfLiteral278 = null;

        numericLiteral_return numericLiteral279 = null;

        booleanLiteral_return booleanLiteral280 = null;

        blankNode_return blankNode281 = null;


        Object NIL282_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:344:5: ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | blankNode | NIL )
            int alt92=6;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt92=1;
                }
                break;
            case STRING_LITERAL1:
            case STRING_LITERAL2:
                {
                alt92=2;
                }
                break;
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt92=3;
                }
                break;
            case 155:
            case 156:
                {
                alt92=4;
                }
                break;
            case BLANK_NODE_LABEL:
            case ANON:
                {
                alt92=5;
                }
                break;
            case NIL:
                {
                alt92=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("343:1: graphTerm : ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | blankNode | NIL );", 92, 0, input);

                throw nvae;
            }

            switch (alt92) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:344:7: iriRef
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_iriRef_in_graphTerm1974);
                    iriRef277=iriRef();
                    _fsp--;

                    adaptor.addChild(root_0, iriRef277.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:345:7: rdfLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_rdfLiteral_in_graphTerm1982);
                    rdfLiteral278=rdfLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, rdfLiteral278.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:346:7: numericLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_numericLiteral_in_graphTerm1990);
                    numericLiteral279=numericLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, numericLiteral279.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:347:7: booleanLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_booleanLiteral_in_graphTerm1998);
                    booleanLiteral280=booleanLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, booleanLiteral280.getTree());

                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:348:7: blankNode
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_blankNode_in_graphTerm2006);
                    blankNode281=blankNode();
                    _fsp--;

                    adaptor.addChild(root_0, blankNode281.getTree());

                    }
                    break;
                case 6 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:349:7: NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    NIL282=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_graphTerm2014); 
                    NIL282_tree = (Object)adaptor.create(NIL282);
                    adaptor.addChild(root_0, NIL282_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end graphTerm

    public static class expression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start expression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:352:1: expression : conditionalOrExpression ;
    public final expression_return expression() throws RecognitionException {
        expression_return retval = new expression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        conditionalOrExpression_return conditionalOrExpression283 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:353:5: ( conditionalOrExpression )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:353:7: conditionalOrExpression
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_conditionalOrExpression_in_expression2032);
            conditionalOrExpression283=conditionalOrExpression();
            _fsp--;

            adaptor.addChild(root_0, conditionalOrExpression283.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end expression

    public static class conditionalOrExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start conditionalOrExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:356:1: conditionalOrExpression : conditionalAndExpression ( '||' conditionalAndExpression )* ;
    public final conditionalOrExpression_return conditionalOrExpression() throws RecognitionException {
        conditionalOrExpression_return retval = new conditionalOrExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal285=null;
        conditionalAndExpression_return conditionalAndExpression284 = null;

        conditionalAndExpression_return conditionalAndExpression286 = null;


        Object string_literal285_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:357:5: ( conditionalAndExpression ( '||' conditionalAndExpression )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:357:7: conditionalAndExpression ( '||' conditionalAndExpression )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_conditionalAndExpression_in_conditionalOrExpression2050);
            conditionalAndExpression284=conditionalAndExpression();
            _fsp--;

            adaptor.addChild(root_0, conditionalAndExpression284.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:357:32: ( '||' conditionalAndExpression )*
            loop93:
            do {
                int alt93=2;
                int LA93_0 = input.LA(1);

                if ( (LA93_0==83) ) {
                    alt93=1;
                }


                switch (alt93) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:357:34: '||' conditionalAndExpression
            	    {
            	    string_literal285=(Token)input.LT(1);
            	    match(input,83,FOLLOW_83_in_conditionalOrExpression2054); 
            	    string_literal285_tree = (Object)adaptor.create(string_literal285);
            	    adaptor.addChild(root_0, string_literal285_tree);

            	    pushFollow(FOLLOW_conditionalAndExpression_in_conditionalOrExpression2056);
            	    conditionalAndExpression286=conditionalAndExpression();
            	    _fsp--;

            	    adaptor.addChild(root_0, conditionalAndExpression286.getTree());

            	    }
            	    break;

            	default :
            	    break loop93;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end conditionalOrExpression

    public static class conditionalAndExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start conditionalAndExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:360:1: conditionalAndExpression : valueLogical ( '&&' valueLogical )* ;
    public final conditionalAndExpression_return conditionalAndExpression() throws RecognitionException {
        conditionalAndExpression_return retval = new conditionalAndExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal288=null;
        valueLogical_return valueLogical287 = null;

        valueLogical_return valueLogical289 = null;


        Object string_literal288_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:361:5: ( valueLogical ( '&&' valueLogical )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:361:7: valueLogical ( '&&' valueLogical )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_valueLogical_in_conditionalAndExpression2076);
            valueLogical287=valueLogical();
            _fsp--;

            adaptor.addChild(root_0, valueLogical287.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:361:20: ( '&&' valueLogical )*
            loop94:
            do {
                int alt94=2;
                int LA94_0 = input.LA(1);

                if ( (LA94_0==84) ) {
                    alt94=1;
                }


                switch (alt94) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:361:22: '&&' valueLogical
            	    {
            	    string_literal288=(Token)input.LT(1);
            	    match(input,84,FOLLOW_84_in_conditionalAndExpression2080); 
            	    string_literal288_tree = (Object)adaptor.create(string_literal288);
            	    adaptor.addChild(root_0, string_literal288_tree);

            	    pushFollow(FOLLOW_valueLogical_in_conditionalAndExpression2082);
            	    valueLogical289=valueLogical();
            	    _fsp--;

            	    adaptor.addChild(root_0, valueLogical289.getTree());

            	    }
            	    break;

            	default :
            	    break loop94;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end conditionalAndExpression

    public static class valueLogical_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start valueLogical
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:364:1: valueLogical : relationalExpression ;
    public final valueLogical_return valueLogical() throws RecognitionException {
        valueLogical_return retval = new valueLogical_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        relationalExpression_return relationalExpression290 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:365:5: ( relationalExpression )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:365:7: relationalExpression
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_relationalExpression_in_valueLogical2102);
            relationalExpression290=relationalExpression();
            _fsp--;

            adaptor.addChild(root_0, relationalExpression290.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end valueLogical

    public static class relationalExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start relationalExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:368:1: relationalExpression : numericExpression ( '=' numericExpression | '!=' numericExpression | '<' numericExpression | '>' numericExpression | '<=' numericExpression | '>=' numericExpression )? ;
    public final relationalExpression_return relationalExpression() throws RecognitionException {
        relationalExpression_return retval = new relationalExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal292=null;
        Token string_literal294=null;
        Token char_literal296=null;
        Token char_literal298=null;
        Token string_literal300=null;
        Token string_literal302=null;
        numericExpression_return numericExpression291 = null;

        numericExpression_return numericExpression293 = null;

        numericExpression_return numericExpression295 = null;

        numericExpression_return numericExpression297 = null;

        numericExpression_return numericExpression299 = null;

        numericExpression_return numericExpression301 = null;

        numericExpression_return numericExpression303 = null;


        Object char_literal292_tree=null;
        Object string_literal294_tree=null;
        Object char_literal296_tree=null;
        Object char_literal298_tree=null;
        Object string_literal300_tree=null;
        Object string_literal302_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:5: ( numericExpression ( '=' numericExpression | '!=' numericExpression | '<' numericExpression | '>' numericExpression | '<=' numericExpression | '>=' numericExpression )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:7: numericExpression ( '=' numericExpression | '!=' numericExpression | '<' numericExpression | '>' numericExpression | '<=' numericExpression | '>=' numericExpression )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_numericExpression_in_relationalExpression2119);
            numericExpression291=numericExpression();
            _fsp--;

            adaptor.addChild(root_0, numericExpression291.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:25: ( '=' numericExpression | '!=' numericExpression | '<' numericExpression | '>' numericExpression | '<=' numericExpression | '>=' numericExpression )?
            int alt95=7;
            switch ( input.LA(1) ) {
                case 85:
                    {
                    alt95=1;
                    }
                    break;
                case 86:
                    {
                    alt95=2;
                    }
                    break;
                case 87:
                    {
                    alt95=3;
                    }
                    break;
                case 88:
                    {
                    alt95=4;
                    }
                    break;
                case 89:
                    {
                    alt95=5;
                    }
                    break;
                case 90:
                    {
                    alt95=6;
                    }
                    break;
            }

            switch (alt95) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:27: '=' numericExpression
                    {
                    char_literal292=(Token)input.LT(1);
                    match(input,85,FOLLOW_85_in_relationalExpression2123); 
                    char_literal292_tree = (Object)adaptor.create(char_literal292);
                    adaptor.addChild(root_0, char_literal292_tree);

                    pushFollow(FOLLOW_numericExpression_in_relationalExpression2125);
                    numericExpression293=numericExpression();
                    _fsp--;

                    adaptor.addChild(root_0, numericExpression293.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:51: '!=' numericExpression
                    {
                    string_literal294=(Token)input.LT(1);
                    match(input,86,FOLLOW_86_in_relationalExpression2129); 
                    string_literal294_tree = (Object)adaptor.create(string_literal294);
                    adaptor.addChild(root_0, string_literal294_tree);

                    pushFollow(FOLLOW_numericExpression_in_relationalExpression2131);
                    numericExpression295=numericExpression();
                    _fsp--;

                    adaptor.addChild(root_0, numericExpression295.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:76: '<' numericExpression
                    {
                    char_literal296=(Token)input.LT(1);
                    match(input,87,FOLLOW_87_in_relationalExpression2135); 
                    char_literal296_tree = (Object)adaptor.create(char_literal296);
                    adaptor.addChild(root_0, char_literal296_tree);

                    pushFollow(FOLLOW_numericExpression_in_relationalExpression2137);
                    numericExpression297=numericExpression();
                    _fsp--;

                    adaptor.addChild(root_0, numericExpression297.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:100: '>' numericExpression
                    {
                    char_literal298=(Token)input.LT(1);
                    match(input,88,FOLLOW_88_in_relationalExpression2141); 
                    char_literal298_tree = (Object)adaptor.create(char_literal298);
                    adaptor.addChild(root_0, char_literal298_tree);

                    pushFollow(FOLLOW_numericExpression_in_relationalExpression2143);
                    numericExpression299=numericExpression();
                    _fsp--;

                    adaptor.addChild(root_0, numericExpression299.getTree());

                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:124: '<=' numericExpression
                    {
                    string_literal300=(Token)input.LT(1);
                    match(input,89,FOLLOW_89_in_relationalExpression2147); 
                    string_literal300_tree = (Object)adaptor.create(string_literal300);
                    adaptor.addChild(root_0, string_literal300_tree);

                    pushFollow(FOLLOW_numericExpression_in_relationalExpression2149);
                    numericExpression301=numericExpression();
                    _fsp--;

                    adaptor.addChild(root_0, numericExpression301.getTree());

                    }
                    break;
                case 6 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:369:149: '>=' numericExpression
                    {
                    string_literal302=(Token)input.LT(1);
                    match(input,90,FOLLOW_90_in_relationalExpression2153); 
                    string_literal302_tree = (Object)adaptor.create(string_literal302);
                    adaptor.addChild(root_0, string_literal302_tree);

                    pushFollow(FOLLOW_numericExpression_in_relationalExpression2155);
                    numericExpression303=numericExpression();
                    _fsp--;

                    adaptor.addChild(root_0, numericExpression303.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end relationalExpression

    public static class numericExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start numericExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:372:1: numericExpression : additiveExpression ;
    public final numericExpression_return numericExpression() throws RecognitionException {
        numericExpression_return retval = new numericExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        additiveExpression_return additiveExpression304 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:373:5: ( additiveExpression )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:373:7: additiveExpression
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_additiveExpression_in_numericExpression2175);
            additiveExpression304=additiveExpression();
            _fsp--;

            adaptor.addChild(root_0, additiveExpression304.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end numericExpression

    public static class additiveExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start additiveExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:376:1: additiveExpression : multiplicativeExpression ( '+' multiplicativeExpression | '-' multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )* ;
    public final additiveExpression_return additiveExpression() throws RecognitionException {
        additiveExpression_return retval = new additiveExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal306=null;
        Token char_literal308=null;
        multiplicativeExpression_return multiplicativeExpression305 = null;

        multiplicativeExpression_return multiplicativeExpression307 = null;

        multiplicativeExpression_return multiplicativeExpression309 = null;

        numericLiteralPositive_return numericLiteralPositive310 = null;

        numericLiteralNegative_return numericLiteralNegative311 = null;


        Object char_literal306_tree=null;
        Object char_literal308_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:5: ( multiplicativeExpression ( '+' multiplicativeExpression | '-' multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:7: multiplicativeExpression ( '+' multiplicativeExpression | '-' multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression2192);
            multiplicativeExpression305=multiplicativeExpression();
            _fsp--;

            adaptor.addChild(root_0, multiplicativeExpression305.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:32: ( '+' multiplicativeExpression | '-' multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )*
            loop96:
            do {
                int alt96=5;
                switch ( input.LA(1) ) {
                case 79:
                    {
                    alt96=1;
                    }
                    break;
                case 91:
                    {
                    alt96=2;
                    }
                    break;
                case INTEGER_POSITIVE:
                case DECIMAL_POSITIVE:
                case DOUBLE_POSITIVE:
                    {
                    alt96=3;
                    }
                    break;
                case INTEGER_NEGATIVE:
                case DECIMAL_NEGATIVE:
                case DOUBLE_NEGATIVE:
                    {
                    alt96=4;
                    }
                    break;

                }

                switch (alt96) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:34: '+' multiplicativeExpression
            	    {
            	    char_literal306=(Token)input.LT(1);
            	    match(input,79,FOLLOW_79_in_additiveExpression2196); 
            	    char_literal306_tree = (Object)adaptor.create(char_literal306);
            	    adaptor.addChild(root_0, char_literal306_tree);

            	    pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression2198);
            	    multiplicativeExpression307=multiplicativeExpression();
            	    _fsp--;

            	    adaptor.addChild(root_0, multiplicativeExpression307.getTree());

            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:65: '-' multiplicativeExpression
            	    {
            	    char_literal308=(Token)input.LT(1);
            	    match(input,91,FOLLOW_91_in_additiveExpression2202); 
            	    char_literal308_tree = (Object)adaptor.create(char_literal308);
            	    adaptor.addChild(root_0, char_literal308_tree);

            	    pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression2204);
            	    multiplicativeExpression309=multiplicativeExpression();
            	    _fsp--;

            	    adaptor.addChild(root_0, multiplicativeExpression309.getTree());

            	    }
            	    break;
            	case 3 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:96: numericLiteralPositive
            	    {
            	    pushFollow(FOLLOW_numericLiteralPositive_in_additiveExpression2208);
            	    numericLiteralPositive310=numericLiteralPositive();
            	    _fsp--;

            	    adaptor.addChild(root_0, numericLiteralPositive310.getTree());

            	    }
            	    break;
            	case 4 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:377:121: numericLiteralNegative
            	    {
            	    pushFollow(FOLLOW_numericLiteralNegative_in_additiveExpression2212);
            	    numericLiteralNegative311=numericLiteralNegative();
            	    _fsp--;

            	    adaptor.addChild(root_0, numericLiteralNegative311.getTree());

            	    }
            	    break;

            	default :
            	    break loop96;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end additiveExpression

    public static class multiplicativeExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start multiplicativeExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:380:1: multiplicativeExpression : unaryExpression ( '*' unaryExpression | '/' unaryExpression )* ;
    public final multiplicativeExpression_return multiplicativeExpression() throws RecognitionException {
        multiplicativeExpression_return retval = new multiplicativeExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal313=null;
        Token char_literal315=null;
        unaryExpression_return unaryExpression312 = null;

        unaryExpression_return unaryExpression314 = null;

        unaryExpression_return unaryExpression316 = null;


        Object char_literal313_tree=null;
        Object char_literal315_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:381:5: ( unaryExpression ( '*' unaryExpression | '/' unaryExpression )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:381:7: unaryExpression ( '*' unaryExpression | '/' unaryExpression )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression2232);
            unaryExpression312=unaryExpression();
            _fsp--;

            adaptor.addChild(root_0, unaryExpression312.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:381:23: ( '*' unaryExpression | '/' unaryExpression )*
            loop97:
            do {
                int alt97=3;
                int LA97_0 = input.LA(1);

                if ( (LA97_0==44) ) {
                    alt97=1;
                }
                else if ( (LA97_0==76) ) {
                    alt97=2;
                }


                switch (alt97) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:381:25: '*' unaryExpression
            	    {
            	    char_literal313=(Token)input.LT(1);
            	    match(input,44,FOLLOW_44_in_multiplicativeExpression2236); 
            	    char_literal313_tree = (Object)adaptor.create(char_literal313);
            	    adaptor.addChild(root_0, char_literal313_tree);

            	    pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression2238);
            	    unaryExpression314=unaryExpression();
            	    _fsp--;

            	    adaptor.addChild(root_0, unaryExpression314.getTree());

            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:381:47: '/' unaryExpression
            	    {
            	    char_literal315=(Token)input.LT(1);
            	    match(input,76,FOLLOW_76_in_multiplicativeExpression2242); 
            	    char_literal315_tree = (Object)adaptor.create(char_literal315);
            	    adaptor.addChild(root_0, char_literal315_tree);

            	    pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression2244);
            	    unaryExpression316=unaryExpression();
            	    _fsp--;

            	    adaptor.addChild(root_0, unaryExpression316.getTree());

            	    }
            	    break;

            	default :
            	    break loop97;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end multiplicativeExpression

    public static class unaryExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start unaryExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:384:1: unaryExpression : ( '!' primaryExpression | '+' primaryExpression | '-' primaryExpression | primaryExpression );
    public final unaryExpression_return unaryExpression() throws RecognitionException {
        unaryExpression_return retval = new unaryExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal317=null;
        Token char_literal319=null;
        Token char_literal321=null;
        primaryExpression_return primaryExpression318 = null;

        primaryExpression_return primaryExpression320 = null;

        primaryExpression_return primaryExpression322 = null;

        primaryExpression_return primaryExpression323 = null;


        Object char_literal317_tree=null;
        Object char_literal319_tree=null;
        Object char_literal321_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:385:5: ( '!' primaryExpression | '+' primaryExpression | '-' primaryExpression | primaryExpression )
            int alt98=4;
            switch ( input.LA(1) ) {
            case 80:
                {
                alt98=1;
                }
                break;
            case 79:
                {
                alt98=2;
                }
                break;
            case 91:
                {
                alt98=3;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case INTEGER:
            case VAR1:
            case VAR2:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
            case STRING_LITERAL1:
            case STRING_LITERAL2:
            case PNAME_LN:
            case 41:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
            case 155:
            case 156:
                {
                alt98=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("384:1: unaryExpression : ( '!' primaryExpression | '+' primaryExpression | '-' primaryExpression | primaryExpression );", 98, 0, input);

                throw nvae;
            }

            switch (alt98) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:385:8: '!' primaryExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal317=(Token)input.LT(1);
                    match(input,80,FOLLOW_80_in_unaryExpression2265); 
                    char_literal317_tree = (Object)adaptor.create(char_literal317);
                    adaptor.addChild(root_0, char_literal317_tree);

                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression2267);
                    primaryExpression318=primaryExpression();
                    _fsp--;

                    adaptor.addChild(root_0, primaryExpression318.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:386:7: '+' primaryExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal319=(Token)input.LT(1);
                    match(input,79,FOLLOW_79_in_unaryExpression2275); 
                    char_literal319_tree = (Object)adaptor.create(char_literal319);
                    adaptor.addChild(root_0, char_literal319_tree);

                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression2277);
                    primaryExpression320=primaryExpression();
                    _fsp--;

                    adaptor.addChild(root_0, primaryExpression320.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:387:7: '-' primaryExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal321=(Token)input.LT(1);
                    match(input,91,FOLLOW_91_in_unaryExpression2285); 
                    char_literal321_tree = (Object)adaptor.create(char_literal321);
                    adaptor.addChild(root_0, char_literal321_tree);

                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression2287);
                    primaryExpression322=primaryExpression();
                    _fsp--;

                    adaptor.addChild(root_0, primaryExpression322.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:388:7: primaryExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression2295);
                    primaryExpression323=primaryExpression();
                    _fsp--;

                    adaptor.addChild(root_0, primaryExpression323.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end unaryExpression

    public static class primaryExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start primaryExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:391:1: primaryExpression : ( brackettedExpression | builtInCall | iriRefOrFunction | rdfLiteral | numericLiteral | booleanLiteral | var );
    public final primaryExpression_return primaryExpression() throws RecognitionException {
        primaryExpression_return retval = new primaryExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        brackettedExpression_return brackettedExpression324 = null;

        builtInCall_return builtInCall325 = null;

        iriRefOrFunction_return iriRefOrFunction326 = null;

        rdfLiteral_return rdfLiteral327 = null;

        numericLiteral_return numericLiteral328 = null;

        booleanLiteral_return booleanLiteral329 = null;

        var_return var330 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:5: ( brackettedExpression | builtInCall | iriRefOrFunction | rdfLiteral | numericLiteral | booleanLiteral | var )
            int alt99=7;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt99=1;
                }
                break;
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
                {
                alt99=2;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt99=3;
                }
                break;
            case STRING_LITERAL1:
            case STRING_LITERAL2:
                {
                alt99=4;
                }
                break;
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt99=5;
                }
                break;
            case 155:
            case 156:
                {
                alt99=6;
                }
                break;
            case VAR1:
            case VAR2:
                {
                alt99=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("391:1: primaryExpression : ( brackettedExpression | builtInCall | iriRefOrFunction | rdfLiteral | numericLiteral | booleanLiteral | var );", 99, 0, input);

                throw nvae;
            }

            switch (alt99) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:7: brackettedExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_brackettedExpression_in_primaryExpression2312);
                    brackettedExpression324=brackettedExpression();
                    _fsp--;

                    adaptor.addChild(root_0, brackettedExpression324.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:30: builtInCall
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_builtInCall_in_primaryExpression2316);
                    builtInCall325=builtInCall();
                    _fsp--;

                    adaptor.addChild(root_0, builtInCall325.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:44: iriRefOrFunction
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_iriRefOrFunction_in_primaryExpression2320);
                    iriRefOrFunction326=iriRefOrFunction();
                    _fsp--;

                    adaptor.addChild(root_0, iriRefOrFunction326.getTree());

                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:63: rdfLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_rdfLiteral_in_primaryExpression2324);
                    rdfLiteral327=rdfLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, rdfLiteral327.getTree());

                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:76: numericLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_numericLiteral_in_primaryExpression2328);
                    numericLiteral328=numericLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, numericLiteral328.getTree());

                    }
                    break;
                case 6 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:93: booleanLiteral
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_booleanLiteral_in_primaryExpression2332);
                    booleanLiteral329=booleanLiteral();
                    _fsp--;

                    adaptor.addChild(root_0, booleanLiteral329.getTree());

                    }
                    break;
                case 7 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:392:110: var
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_var_in_primaryExpression2336);
                    var330=var();
                    _fsp--;

                    adaptor.addChild(root_0, var330.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end primaryExpression

    public static class brackettedExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start brackettedExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:395:1: brackettedExpression : '(' expression ')' ;
    public final brackettedExpression_return brackettedExpression() throws RecognitionException {
        brackettedExpression_return retval = new brackettedExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal331=null;
        Token char_literal333=null;
        expression_return expression332 = null;


        Object char_literal331_tree=null;
        Object char_literal333_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:396:5: ( '(' expression ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:396:7: '(' expression ')'
            {
            root_0 = (Object)adaptor.nil();

            char_literal331=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_brackettedExpression2353); 
            char_literal331_tree = (Object)adaptor.create(char_literal331);
            adaptor.addChild(root_0, char_literal331_tree);

            pushFollow(FOLLOW_expression_in_brackettedExpression2355);
            expression332=expression();
            _fsp--;

            adaptor.addChild(root_0, expression332.getTree());
            char_literal333=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_brackettedExpression2357); 
            char_literal333_tree = (Object)adaptor.create(char_literal333);
            adaptor.addChild(root_0, char_literal333_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end brackettedExpression

    public static class builtInCall_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start builtInCall
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:399:1: builtInCall : ( aggregate | 'STR' '(' expression ')' | 'LANG' '(' expression ')' | 'LANGMATCHES' '(' expression ',' expression ')' | 'DATATYPE' '(' expression ')' | 'BOUND' '(' var ')' | 'IRI' '(' expression ')' | 'URI' '(' expression ')' | 'BNODE' ( '(' expression ')' | NIL ) | 'RAND' NIL | 'ABS' '(' expression ')' | 'CEIL' '(' expression ')' | 'FLOOR' '(' expression ')' | 'ROUND' '(' expression ')' | 'CONCAT' expressionList | subStringExpression | 'STRLEN' '(' expression ')' | strReplaceExpression | 'UCASE' '(' expression ')' | 'LCASE' '(' expression ')' | 'ENCODE_FOR_URI' '(' expression ')' | 'CONTAINS' '(' expression ',' expression ')' | 'STRSTARTS' '(' expression ',' expression ')' | 'STRENDS' '(' expression ',' expression ')' | 'STRBEFORE' '(' expression ',' expression ')' | 'STRAFTER' '(' expression ',' expression ')' | 'YEAR' '(' expression ')' | 'MONTH' '(' expression ')' | 'DAY' '(' expression ')' | 'HOURS' '(' expression ')' | 'MINUTES' '(' expression ')' | 'SECONDS' '(' expression ')' | 'TIMEZONE' '(' expression ')' | 'TZ' '(' expression ')' | 'NOW' NIL | 'UUID' NIL | 'STRUUID' NIL | 'MD5' '(' expression ')' | 'SHA1' '(' expression ')' | 'SHA256' '(' expression ')' | 'SHA384' '(' expression ')' | 'SHA512' '(' expression ')' | 'COALESCE' expressionList | 'IF' '(' expression ',' expression ',' expression ')' | 'STRLANG' '(' expression ',' expression ')' | 'STRDT' '(' expression ',' expression ')' | 'sameTerm' '(' expression ',' expression ')' | 'isIRI' '(' expression ')' | 'isURI' '(' expression ')' | 'isBLANK' '(' expression ')' | 'isLITERAL' '(' expression ')' | 'isNUMERIC' '(' expression ')' | regexExpression | existsFunc | notExistsFunc );
    public final builtInCall_return builtInCall() throws RecognitionException {
        builtInCall_return retval = new builtInCall_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal335=null;
        Token char_literal336=null;
        Token char_literal338=null;
        Token string_literal339=null;
        Token char_literal340=null;
        Token char_literal342=null;
        Token string_literal343=null;
        Token char_literal344=null;
        Token char_literal346=null;
        Token char_literal348=null;
        Token string_literal349=null;
        Token char_literal350=null;
        Token char_literal352=null;
        Token string_literal353=null;
        Token char_literal354=null;
        Token char_literal356=null;
        Token string_literal357=null;
        Token char_literal358=null;
        Token char_literal360=null;
        Token string_literal361=null;
        Token char_literal362=null;
        Token char_literal364=null;
        Token string_literal365=null;
        Token char_literal366=null;
        Token char_literal368=null;
        Token NIL369=null;
        Token string_literal370=null;
        Token NIL371=null;
        Token string_literal372=null;
        Token char_literal373=null;
        Token char_literal375=null;
        Token string_literal376=null;
        Token char_literal377=null;
        Token char_literal379=null;
        Token string_literal380=null;
        Token char_literal381=null;
        Token char_literal383=null;
        Token string_literal384=null;
        Token char_literal385=null;
        Token char_literal387=null;
        Token string_literal388=null;
        Token string_literal391=null;
        Token char_literal392=null;
        Token char_literal394=null;
        Token string_literal396=null;
        Token char_literal397=null;
        Token char_literal399=null;
        Token string_literal400=null;
        Token char_literal401=null;
        Token char_literal403=null;
        Token string_literal404=null;
        Token char_literal405=null;
        Token char_literal407=null;
        Token string_literal408=null;
        Token char_literal409=null;
        Token char_literal411=null;
        Token char_literal413=null;
        Token string_literal414=null;
        Token char_literal415=null;
        Token char_literal417=null;
        Token char_literal419=null;
        Token string_literal420=null;
        Token char_literal421=null;
        Token char_literal423=null;
        Token char_literal425=null;
        Token string_literal426=null;
        Token char_literal427=null;
        Token char_literal429=null;
        Token char_literal431=null;
        Token string_literal432=null;
        Token char_literal433=null;
        Token char_literal435=null;
        Token char_literal437=null;
        Token string_literal438=null;
        Token char_literal439=null;
        Token char_literal441=null;
        Token string_literal442=null;
        Token char_literal443=null;
        Token char_literal445=null;
        Token string_literal446=null;
        Token char_literal447=null;
        Token char_literal449=null;
        Token string_literal450=null;
        Token char_literal451=null;
        Token char_literal453=null;
        Token string_literal454=null;
        Token char_literal455=null;
        Token char_literal457=null;
        Token string_literal458=null;
        Token char_literal459=null;
        Token char_literal461=null;
        Token string_literal462=null;
        Token char_literal463=null;
        Token char_literal465=null;
        Token string_literal466=null;
        Token char_literal467=null;
        Token char_literal469=null;
        Token string_literal470=null;
        Token NIL471=null;
        Token string_literal472=null;
        Token NIL473=null;
        Token string_literal474=null;
        Token NIL475=null;
        Token string_literal476=null;
        Token char_literal477=null;
        Token char_literal479=null;
        Token string_literal480=null;
        Token char_literal481=null;
        Token char_literal483=null;
        Token string_literal484=null;
        Token char_literal485=null;
        Token char_literal487=null;
        Token string_literal488=null;
        Token char_literal489=null;
        Token char_literal491=null;
        Token string_literal492=null;
        Token char_literal493=null;
        Token char_literal495=null;
        Token string_literal496=null;
        Token string_literal498=null;
        Token char_literal499=null;
        Token char_literal501=null;
        Token char_literal503=null;
        Token char_literal505=null;
        Token string_literal506=null;
        Token char_literal507=null;
        Token char_literal509=null;
        Token char_literal511=null;
        Token string_literal512=null;
        Token char_literal513=null;
        Token char_literal515=null;
        Token char_literal517=null;
        Token string_literal518=null;
        Token char_literal519=null;
        Token char_literal521=null;
        Token char_literal523=null;
        Token string_literal524=null;
        Token char_literal525=null;
        Token char_literal527=null;
        Token string_literal528=null;
        Token char_literal529=null;
        Token char_literal531=null;
        Token string_literal532=null;
        Token char_literal533=null;
        Token char_literal535=null;
        Token string_literal536=null;
        Token char_literal537=null;
        Token char_literal539=null;
        Token string_literal540=null;
        Token char_literal541=null;
        Token char_literal543=null;
        aggregate_return aggregate334 = null;

        expression_return expression337 = null;

        expression_return expression341 = null;

        expression_return expression345 = null;

        expression_return expression347 = null;

        expression_return expression351 = null;

        var_return var355 = null;

        expression_return expression359 = null;

        expression_return expression363 = null;

        expression_return expression367 = null;

        expression_return expression374 = null;

        expression_return expression378 = null;

        expression_return expression382 = null;

        expression_return expression386 = null;

        expressionList_return expressionList389 = null;

        subStringExpression_return subStringExpression390 = null;

        expression_return expression393 = null;

        strReplaceExpression_return strReplaceExpression395 = null;

        expression_return expression398 = null;

        expression_return expression402 = null;

        expression_return expression406 = null;

        expression_return expression410 = null;

        expression_return expression412 = null;

        expression_return expression416 = null;

        expression_return expression418 = null;

        expression_return expression422 = null;

        expression_return expression424 = null;

        expression_return expression428 = null;

        expression_return expression430 = null;

        expression_return expression434 = null;

        expression_return expression436 = null;

        expression_return expression440 = null;

        expression_return expression444 = null;

        expression_return expression448 = null;

        expression_return expression452 = null;

        expression_return expression456 = null;

        expression_return expression460 = null;

        expression_return expression464 = null;

        expression_return expression468 = null;

        expression_return expression478 = null;

        expression_return expression482 = null;

        expression_return expression486 = null;

        expression_return expression490 = null;

        expression_return expression494 = null;

        expressionList_return expressionList497 = null;

        expression_return expression500 = null;

        expression_return expression502 = null;

        expression_return expression504 = null;

        expression_return expression508 = null;

        expression_return expression510 = null;

        expression_return expression514 = null;

        expression_return expression516 = null;

        expression_return expression520 = null;

        expression_return expression522 = null;

        expression_return expression526 = null;

        expression_return expression530 = null;

        expression_return expression534 = null;

        expression_return expression538 = null;

        expression_return expression542 = null;

        regexExpression_return regexExpression544 = null;

        existsFunc_return existsFunc545 = null;

        notExistsFunc_return notExistsFunc546 = null;


        Object string_literal335_tree=null;
        Object char_literal336_tree=null;
        Object char_literal338_tree=null;
        Object string_literal339_tree=null;
        Object char_literal340_tree=null;
        Object char_literal342_tree=null;
        Object string_literal343_tree=null;
        Object char_literal344_tree=null;
        Object char_literal346_tree=null;
        Object char_literal348_tree=null;
        Object string_literal349_tree=null;
        Object char_literal350_tree=null;
        Object char_literal352_tree=null;
        Object string_literal353_tree=null;
        Object char_literal354_tree=null;
        Object char_literal356_tree=null;
        Object string_literal357_tree=null;
        Object char_literal358_tree=null;
        Object char_literal360_tree=null;
        Object string_literal361_tree=null;
        Object char_literal362_tree=null;
        Object char_literal364_tree=null;
        Object string_literal365_tree=null;
        Object char_literal366_tree=null;
        Object char_literal368_tree=null;
        Object NIL369_tree=null;
        Object string_literal370_tree=null;
        Object NIL371_tree=null;
        Object string_literal372_tree=null;
        Object char_literal373_tree=null;
        Object char_literal375_tree=null;
        Object string_literal376_tree=null;
        Object char_literal377_tree=null;
        Object char_literal379_tree=null;
        Object string_literal380_tree=null;
        Object char_literal381_tree=null;
        Object char_literal383_tree=null;
        Object string_literal384_tree=null;
        Object char_literal385_tree=null;
        Object char_literal387_tree=null;
        Object string_literal388_tree=null;
        Object string_literal391_tree=null;
        Object char_literal392_tree=null;
        Object char_literal394_tree=null;
        Object string_literal396_tree=null;
        Object char_literal397_tree=null;
        Object char_literal399_tree=null;
        Object string_literal400_tree=null;
        Object char_literal401_tree=null;
        Object char_literal403_tree=null;
        Object string_literal404_tree=null;
        Object char_literal405_tree=null;
        Object char_literal407_tree=null;
        Object string_literal408_tree=null;
        Object char_literal409_tree=null;
        Object char_literal411_tree=null;
        Object char_literal413_tree=null;
        Object string_literal414_tree=null;
        Object char_literal415_tree=null;
        Object char_literal417_tree=null;
        Object char_literal419_tree=null;
        Object string_literal420_tree=null;
        Object char_literal421_tree=null;
        Object char_literal423_tree=null;
        Object char_literal425_tree=null;
        Object string_literal426_tree=null;
        Object char_literal427_tree=null;
        Object char_literal429_tree=null;
        Object char_literal431_tree=null;
        Object string_literal432_tree=null;
        Object char_literal433_tree=null;
        Object char_literal435_tree=null;
        Object char_literal437_tree=null;
        Object string_literal438_tree=null;
        Object char_literal439_tree=null;
        Object char_literal441_tree=null;
        Object string_literal442_tree=null;
        Object char_literal443_tree=null;
        Object char_literal445_tree=null;
        Object string_literal446_tree=null;
        Object char_literal447_tree=null;
        Object char_literal449_tree=null;
        Object string_literal450_tree=null;
        Object char_literal451_tree=null;
        Object char_literal453_tree=null;
        Object string_literal454_tree=null;
        Object char_literal455_tree=null;
        Object char_literal457_tree=null;
        Object string_literal458_tree=null;
        Object char_literal459_tree=null;
        Object char_literal461_tree=null;
        Object string_literal462_tree=null;
        Object char_literal463_tree=null;
        Object char_literal465_tree=null;
        Object string_literal466_tree=null;
        Object char_literal467_tree=null;
        Object char_literal469_tree=null;
        Object string_literal470_tree=null;
        Object NIL471_tree=null;
        Object string_literal472_tree=null;
        Object NIL473_tree=null;
        Object string_literal474_tree=null;
        Object NIL475_tree=null;
        Object string_literal476_tree=null;
        Object char_literal477_tree=null;
        Object char_literal479_tree=null;
        Object string_literal480_tree=null;
        Object char_literal481_tree=null;
        Object char_literal483_tree=null;
        Object string_literal484_tree=null;
        Object char_literal485_tree=null;
        Object char_literal487_tree=null;
        Object string_literal488_tree=null;
        Object char_literal489_tree=null;
        Object char_literal491_tree=null;
        Object string_literal492_tree=null;
        Object char_literal493_tree=null;
        Object char_literal495_tree=null;
        Object string_literal496_tree=null;
        Object string_literal498_tree=null;
        Object char_literal499_tree=null;
        Object char_literal501_tree=null;
        Object char_literal503_tree=null;
        Object char_literal505_tree=null;
        Object string_literal506_tree=null;
        Object char_literal507_tree=null;
        Object char_literal509_tree=null;
        Object char_literal511_tree=null;
        Object string_literal512_tree=null;
        Object char_literal513_tree=null;
        Object char_literal515_tree=null;
        Object char_literal517_tree=null;
        Object string_literal518_tree=null;
        Object char_literal519_tree=null;
        Object char_literal521_tree=null;
        Object char_literal523_tree=null;
        Object string_literal524_tree=null;
        Object char_literal525_tree=null;
        Object char_literal527_tree=null;
        Object string_literal528_tree=null;
        Object char_literal529_tree=null;
        Object char_literal531_tree=null;
        Object string_literal532_tree=null;
        Object char_literal533_tree=null;
        Object char_literal535_tree=null;
        Object string_literal536_tree=null;
        Object char_literal537_tree=null;
        Object char_literal539_tree=null;
        Object string_literal540_tree=null;
        Object char_literal541_tree=null;
        Object char_literal543_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:400:5: ( aggregate | 'STR' '(' expression ')' | 'LANG' '(' expression ')' | 'LANGMATCHES' '(' expression ',' expression ')' | 'DATATYPE' '(' expression ')' | 'BOUND' '(' var ')' | 'IRI' '(' expression ')' | 'URI' '(' expression ')' | 'BNODE' ( '(' expression ')' | NIL ) | 'RAND' NIL | 'ABS' '(' expression ')' | 'CEIL' '(' expression ')' | 'FLOOR' '(' expression ')' | 'ROUND' '(' expression ')' | 'CONCAT' expressionList | subStringExpression | 'STRLEN' '(' expression ')' | strReplaceExpression | 'UCASE' '(' expression ')' | 'LCASE' '(' expression ')' | 'ENCODE_FOR_URI' '(' expression ')' | 'CONTAINS' '(' expression ',' expression ')' | 'STRSTARTS' '(' expression ',' expression ')' | 'STRENDS' '(' expression ',' expression ')' | 'STRBEFORE' '(' expression ',' expression ')' | 'STRAFTER' '(' expression ',' expression ')' | 'YEAR' '(' expression ')' | 'MONTH' '(' expression ')' | 'DAY' '(' expression ')' | 'HOURS' '(' expression ')' | 'MINUTES' '(' expression ')' | 'SECONDS' '(' expression ')' | 'TIMEZONE' '(' expression ')' | 'TZ' '(' expression ')' | 'NOW' NIL | 'UUID' NIL | 'STRUUID' NIL | 'MD5' '(' expression ')' | 'SHA1' '(' expression ')' | 'SHA256' '(' expression ')' | 'SHA384' '(' expression ')' | 'SHA512' '(' expression ')' | 'COALESCE' expressionList | 'IF' '(' expression ',' expression ',' expression ')' | 'STRLANG' '(' expression ',' expression ')' | 'STRDT' '(' expression ',' expression ')' | 'sameTerm' '(' expression ',' expression ')' | 'isIRI' '(' expression ')' | 'isURI' '(' expression ')' | 'isBLANK' '(' expression ')' | 'isLITERAL' '(' expression ')' | 'isNUMERIC' '(' expression ')' | regexExpression | existsFunc | notExistsFunc )
            int alt101=55;
            switch ( input.LA(1) ) {
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
                {
                alt101=1;
                }
                break;
            case 92:
                {
                alt101=2;
                }
                break;
            case 93:
                {
                alt101=3;
                }
                break;
            case 94:
                {
                alt101=4;
                }
                break;
            case 95:
                {
                alt101=5;
                }
                break;
            case 96:
                {
                alt101=6;
                }
                break;
            case 97:
                {
                alt101=7;
                }
                break;
            case 98:
                {
                alt101=8;
                }
                break;
            case 99:
                {
                alt101=9;
                }
                break;
            case 100:
                {
                alt101=10;
                }
                break;
            case 101:
                {
                alt101=11;
                }
                break;
            case 102:
                {
                alt101=12;
                }
                break;
            case 103:
                {
                alt101=13;
                }
                break;
            case 104:
                {
                alt101=14;
                }
                break;
            case 105:
                {
                alt101=15;
                }
                break;
            case 142:
                {
                alt101=16;
                }
                break;
            case 106:
                {
                alt101=17;
                }
                break;
            case 143:
                {
                alt101=18;
                }
                break;
            case 107:
                {
                alt101=19;
                }
                break;
            case 108:
                {
                alt101=20;
                }
                break;
            case 109:
                {
                alt101=21;
                }
                break;
            case 110:
                {
                alt101=22;
                }
                break;
            case 111:
                {
                alt101=23;
                }
                break;
            case 112:
                {
                alt101=24;
                }
                break;
            case 113:
                {
                alt101=25;
                }
                break;
            case 114:
                {
                alt101=26;
                }
                break;
            case 115:
                {
                alt101=27;
                }
                break;
            case 116:
                {
                alt101=28;
                }
                break;
            case 117:
                {
                alt101=29;
                }
                break;
            case 118:
                {
                alt101=30;
                }
                break;
            case 119:
                {
                alt101=31;
                }
                break;
            case 120:
                {
                alt101=32;
                }
                break;
            case 121:
                {
                alt101=33;
                }
                break;
            case 122:
                {
                alt101=34;
                }
                break;
            case 123:
                {
                alt101=35;
                }
                break;
            case 124:
                {
                alt101=36;
                }
                break;
            case 125:
                {
                alt101=37;
                }
                break;
            case 126:
                {
                alt101=38;
                }
                break;
            case 127:
                {
                alt101=39;
                }
                break;
            case 128:
                {
                alt101=40;
                }
                break;
            case 129:
                {
                alt101=41;
                }
                break;
            case 130:
                {
                alt101=42;
                }
                break;
            case 131:
                {
                alt101=43;
                }
                break;
            case 132:
                {
                alt101=44;
                }
                break;
            case 133:
                {
                alt101=45;
                }
                break;
            case 134:
                {
                alt101=46;
                }
                break;
            case 135:
                {
                alt101=47;
                }
                break;
            case 136:
                {
                alt101=48;
                }
                break;
            case 137:
                {
                alt101=49;
                }
                break;
            case 138:
                {
                alt101=50;
                }
                break;
            case 139:
                {
                alt101=51;
                }
                break;
            case 140:
                {
                alt101=52;
                }
                break;
            case 141:
                {
                alt101=53;
                }
                break;
            case 144:
                {
                alt101=54;
                }
                break;
            case 145:
                {
                alt101=55;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("399:1: builtInCall : ( aggregate | 'STR' '(' expression ')' | 'LANG' '(' expression ')' | 'LANGMATCHES' '(' expression ',' expression ')' | 'DATATYPE' '(' expression ')' | 'BOUND' '(' var ')' | 'IRI' '(' expression ')' | 'URI' '(' expression ')' | 'BNODE' ( '(' expression ')' | NIL ) | 'RAND' NIL | 'ABS' '(' expression ')' | 'CEIL' '(' expression ')' | 'FLOOR' '(' expression ')' | 'ROUND' '(' expression ')' | 'CONCAT' expressionList | subStringExpression | 'STRLEN' '(' expression ')' | strReplaceExpression | 'UCASE' '(' expression ')' | 'LCASE' '(' expression ')' | 'ENCODE_FOR_URI' '(' expression ')' | 'CONTAINS' '(' expression ',' expression ')' | 'STRSTARTS' '(' expression ',' expression ')' | 'STRENDS' '(' expression ',' expression ')' | 'STRBEFORE' '(' expression ',' expression ')' | 'STRAFTER' '(' expression ',' expression ')' | 'YEAR' '(' expression ')' | 'MONTH' '(' expression ')' | 'DAY' '(' expression ')' | 'HOURS' '(' expression ')' | 'MINUTES' '(' expression ')' | 'SECONDS' '(' expression ')' | 'TIMEZONE' '(' expression ')' | 'TZ' '(' expression ')' | 'NOW' NIL | 'UUID' NIL | 'STRUUID' NIL | 'MD5' '(' expression ')' | 'SHA1' '(' expression ')' | 'SHA256' '(' expression ')' | 'SHA384' '(' expression ')' | 'SHA512' '(' expression ')' | 'COALESCE' expressionList | 'IF' '(' expression ',' expression ',' expression ')' | 'STRLANG' '(' expression ',' expression ')' | 'STRDT' '(' expression ',' expression ')' | 'sameTerm' '(' expression ',' expression ')' | 'isIRI' '(' expression ')' | 'isURI' '(' expression ')' | 'isBLANK' '(' expression ')' | 'isLITERAL' '(' expression ')' | 'isNUMERIC' '(' expression ')' | regexExpression | existsFunc | notExistsFunc );", 101, 0, input);

                throw nvae;
            }

            switch (alt101) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:400:7: aggregate
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_aggregate_in_builtInCall2375);
                    aggregate334=aggregate();
                    _fsp--;

                    adaptor.addChild(root_0, aggregate334.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:401:4: 'STR' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal335=(Token)input.LT(1);
                    match(input,92,FOLLOW_92_in_builtInCall2381); 
                    string_literal335_tree = (Object)adaptor.create(string_literal335);
                    adaptor.addChild(root_0, string_literal335_tree);

                    char_literal336=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2383); 
                    char_literal336_tree = (Object)adaptor.create(char_literal336);
                    adaptor.addChild(root_0, char_literal336_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2385);
                    expression337=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression337.getTree());
                    char_literal338=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2387); 
                    char_literal338_tree = (Object)adaptor.create(char_literal338);
                    adaptor.addChild(root_0, char_literal338_tree);


                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:402:4: 'LANG' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal339=(Token)input.LT(1);
                    match(input,93,FOLLOW_93_in_builtInCall2393); 
                    string_literal339_tree = (Object)adaptor.create(string_literal339);
                    adaptor.addChild(root_0, string_literal339_tree);

                    char_literal340=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2395); 
                    char_literal340_tree = (Object)adaptor.create(char_literal340);
                    adaptor.addChild(root_0, char_literal340_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2397);
                    expression341=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression341.getTree());
                    char_literal342=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2399); 
                    char_literal342_tree = (Object)adaptor.create(char_literal342);
                    adaptor.addChild(root_0, char_literal342_tree);


                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:403:4: 'LANGMATCHES' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal343=(Token)input.LT(1);
                    match(input,94,FOLLOW_94_in_builtInCall2405); 
                    string_literal343_tree = (Object)adaptor.create(string_literal343);
                    adaptor.addChild(root_0, string_literal343_tree);

                    char_literal344=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2407); 
                    char_literal344_tree = (Object)adaptor.create(char_literal344);
                    adaptor.addChild(root_0, char_literal344_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2409);
                    expression345=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression345.getTree());
                    char_literal346=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2411); 
                    char_literal346_tree = (Object)adaptor.create(char_literal346);
                    adaptor.addChild(root_0, char_literal346_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2413);
                    expression347=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression347.getTree());
                    char_literal348=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2415); 
                    char_literal348_tree = (Object)adaptor.create(char_literal348);
                    adaptor.addChild(root_0, char_literal348_tree);


                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:404:4: 'DATATYPE' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal349=(Token)input.LT(1);
                    match(input,95,FOLLOW_95_in_builtInCall2421); 
                    string_literal349_tree = (Object)adaptor.create(string_literal349);
                    adaptor.addChild(root_0, string_literal349_tree);

                    char_literal350=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2423); 
                    char_literal350_tree = (Object)adaptor.create(char_literal350);
                    adaptor.addChild(root_0, char_literal350_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2425);
                    expression351=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression351.getTree());
                    char_literal352=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2427); 
                    char_literal352_tree = (Object)adaptor.create(char_literal352);
                    adaptor.addChild(root_0, char_literal352_tree);


                    }
                    break;
                case 6 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:405:4: 'BOUND' '(' var ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal353=(Token)input.LT(1);
                    match(input,96,FOLLOW_96_in_builtInCall2433); 
                    string_literal353_tree = (Object)adaptor.create(string_literal353);
                    adaptor.addChild(root_0, string_literal353_tree);

                    char_literal354=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2435); 
                    char_literal354_tree = (Object)adaptor.create(char_literal354);
                    adaptor.addChild(root_0, char_literal354_tree);

                    pushFollow(FOLLOW_var_in_builtInCall2437);
                    var355=var();
                    _fsp--;

                    adaptor.addChild(root_0, var355.getTree());
                    char_literal356=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2439); 
                    char_literal356_tree = (Object)adaptor.create(char_literal356);
                    adaptor.addChild(root_0, char_literal356_tree);


                    }
                    break;
                case 7 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:406:4: 'IRI' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal357=(Token)input.LT(1);
                    match(input,97,FOLLOW_97_in_builtInCall2445); 
                    string_literal357_tree = (Object)adaptor.create(string_literal357);
                    adaptor.addChild(root_0, string_literal357_tree);

                    char_literal358=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2447); 
                    char_literal358_tree = (Object)adaptor.create(char_literal358);
                    adaptor.addChild(root_0, char_literal358_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2449);
                    expression359=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression359.getTree());
                    char_literal360=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2451); 
                    char_literal360_tree = (Object)adaptor.create(char_literal360);
                    adaptor.addChild(root_0, char_literal360_tree);


                    }
                    break;
                case 8 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:407:4: 'URI' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal361=(Token)input.LT(1);
                    match(input,98,FOLLOW_98_in_builtInCall2457); 
                    string_literal361_tree = (Object)adaptor.create(string_literal361);
                    adaptor.addChild(root_0, string_literal361_tree);

                    char_literal362=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2459); 
                    char_literal362_tree = (Object)adaptor.create(char_literal362);
                    adaptor.addChild(root_0, char_literal362_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2461);
                    expression363=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression363.getTree());
                    char_literal364=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2463); 
                    char_literal364_tree = (Object)adaptor.create(char_literal364);
                    adaptor.addChild(root_0, char_literal364_tree);


                    }
                    break;
                case 9 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:408:4: 'BNODE' ( '(' expression ')' | NIL )
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal365=(Token)input.LT(1);
                    match(input,99,FOLLOW_99_in_builtInCall2469); 
                    string_literal365_tree = (Object)adaptor.create(string_literal365);
                    adaptor.addChild(root_0, string_literal365_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:408:12: ( '(' expression ')' | NIL )
                    int alt100=2;
                    int LA100_0 = input.LA(1);

                    if ( (LA100_0==41) ) {
                        alt100=1;
                    }
                    else if ( (LA100_0==NIL) ) {
                        alt100=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("408:12: ( '(' expression ')' | NIL )", 100, 0, input);

                        throw nvae;
                    }
                    switch (alt100) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:408:14: '(' expression ')'
                            {
                            char_literal366=(Token)input.LT(1);
                            match(input,41,FOLLOW_41_in_builtInCall2473); 
                            char_literal366_tree = (Object)adaptor.create(char_literal366);
                            adaptor.addChild(root_0, char_literal366_tree);

                            pushFollow(FOLLOW_expression_in_builtInCall2475);
                            expression367=expression();
                            _fsp--;

                            adaptor.addChild(root_0, expression367.getTree());
                            char_literal368=(Token)input.LT(1);
                            match(input,43,FOLLOW_43_in_builtInCall2477); 
                            char_literal368_tree = (Object)adaptor.create(char_literal368);
                            adaptor.addChild(root_0, char_literal368_tree);


                            }
                            break;
                        case 2 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:408:35: NIL
                            {
                            NIL369=(Token)input.LT(1);
                            match(input,NIL,FOLLOW_NIL_in_builtInCall2481); 
                            NIL369_tree = (Object)adaptor.create(NIL369);
                            adaptor.addChild(root_0, NIL369_tree);


                            }
                            break;

                    }


                    }
                    break;
                case 10 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:409:4: 'RAND' NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal370=(Token)input.LT(1);
                    match(input,100,FOLLOW_100_in_builtInCall2489); 
                    string_literal370_tree = (Object)adaptor.create(string_literal370);
                    adaptor.addChild(root_0, string_literal370_tree);

                    NIL371=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_builtInCall2491); 
                    NIL371_tree = (Object)adaptor.create(NIL371);
                    adaptor.addChild(root_0, NIL371_tree);


                    }
                    break;
                case 11 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:410:4: 'ABS' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal372=(Token)input.LT(1);
                    match(input,101,FOLLOW_101_in_builtInCall2497); 
                    string_literal372_tree = (Object)adaptor.create(string_literal372);
                    adaptor.addChild(root_0, string_literal372_tree);

                    char_literal373=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2499); 
                    char_literal373_tree = (Object)adaptor.create(char_literal373);
                    adaptor.addChild(root_0, char_literal373_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2501);
                    expression374=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression374.getTree());
                    char_literal375=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2503); 
                    char_literal375_tree = (Object)adaptor.create(char_literal375);
                    adaptor.addChild(root_0, char_literal375_tree);


                    }
                    break;
                case 12 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:411:4: 'CEIL' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal376=(Token)input.LT(1);
                    match(input,102,FOLLOW_102_in_builtInCall2509); 
                    string_literal376_tree = (Object)adaptor.create(string_literal376);
                    adaptor.addChild(root_0, string_literal376_tree);

                    char_literal377=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2511); 
                    char_literal377_tree = (Object)adaptor.create(char_literal377);
                    adaptor.addChild(root_0, char_literal377_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2513);
                    expression378=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression378.getTree());
                    char_literal379=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2515); 
                    char_literal379_tree = (Object)adaptor.create(char_literal379);
                    adaptor.addChild(root_0, char_literal379_tree);


                    }
                    break;
                case 13 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:412:4: 'FLOOR' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal380=(Token)input.LT(1);
                    match(input,103,FOLLOW_103_in_builtInCall2521); 
                    string_literal380_tree = (Object)adaptor.create(string_literal380);
                    adaptor.addChild(root_0, string_literal380_tree);

                    char_literal381=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2523); 
                    char_literal381_tree = (Object)adaptor.create(char_literal381);
                    adaptor.addChild(root_0, char_literal381_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2525);
                    expression382=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression382.getTree());
                    char_literal383=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2527); 
                    char_literal383_tree = (Object)adaptor.create(char_literal383);
                    adaptor.addChild(root_0, char_literal383_tree);


                    }
                    break;
                case 14 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:413:4: 'ROUND' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal384=(Token)input.LT(1);
                    match(input,104,FOLLOW_104_in_builtInCall2533); 
                    string_literal384_tree = (Object)adaptor.create(string_literal384);
                    adaptor.addChild(root_0, string_literal384_tree);

                    char_literal385=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2535); 
                    char_literal385_tree = (Object)adaptor.create(char_literal385);
                    adaptor.addChild(root_0, char_literal385_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2537);
                    expression386=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression386.getTree());
                    char_literal387=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2539); 
                    char_literal387_tree = (Object)adaptor.create(char_literal387);
                    adaptor.addChild(root_0, char_literal387_tree);


                    }
                    break;
                case 15 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:414:4: 'CONCAT' expressionList
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal388=(Token)input.LT(1);
                    match(input,105,FOLLOW_105_in_builtInCall2545); 
                    string_literal388_tree = (Object)adaptor.create(string_literal388);
                    adaptor.addChild(root_0, string_literal388_tree);

                    pushFollow(FOLLOW_expressionList_in_builtInCall2547);
                    expressionList389=expressionList();
                    _fsp--;

                    adaptor.addChild(root_0, expressionList389.getTree());

                    }
                    break;
                case 16 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:415:4: subStringExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_subStringExpression_in_builtInCall2553);
                    subStringExpression390=subStringExpression();
                    _fsp--;

                    adaptor.addChild(root_0, subStringExpression390.getTree());

                    }
                    break;
                case 17 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:416:4: 'STRLEN' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal391=(Token)input.LT(1);
                    match(input,106,FOLLOW_106_in_builtInCall2559); 
                    string_literal391_tree = (Object)adaptor.create(string_literal391);
                    adaptor.addChild(root_0, string_literal391_tree);

                    char_literal392=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2561); 
                    char_literal392_tree = (Object)adaptor.create(char_literal392);
                    adaptor.addChild(root_0, char_literal392_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2563);
                    expression393=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression393.getTree());
                    char_literal394=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2565); 
                    char_literal394_tree = (Object)adaptor.create(char_literal394);
                    adaptor.addChild(root_0, char_literal394_tree);


                    }
                    break;
                case 18 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:417:4: strReplaceExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_strReplaceExpression_in_builtInCall2571);
                    strReplaceExpression395=strReplaceExpression();
                    _fsp--;

                    adaptor.addChild(root_0, strReplaceExpression395.getTree());

                    }
                    break;
                case 19 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:418:4: 'UCASE' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal396=(Token)input.LT(1);
                    match(input,107,FOLLOW_107_in_builtInCall2577); 
                    string_literal396_tree = (Object)adaptor.create(string_literal396);
                    adaptor.addChild(root_0, string_literal396_tree);

                    char_literal397=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2579); 
                    char_literal397_tree = (Object)adaptor.create(char_literal397);
                    adaptor.addChild(root_0, char_literal397_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2581);
                    expression398=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression398.getTree());
                    char_literal399=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2583); 
                    char_literal399_tree = (Object)adaptor.create(char_literal399);
                    adaptor.addChild(root_0, char_literal399_tree);


                    }
                    break;
                case 20 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:419:4: 'LCASE' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal400=(Token)input.LT(1);
                    match(input,108,FOLLOW_108_in_builtInCall2589); 
                    string_literal400_tree = (Object)adaptor.create(string_literal400);
                    adaptor.addChild(root_0, string_literal400_tree);

                    char_literal401=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2591); 
                    char_literal401_tree = (Object)adaptor.create(char_literal401);
                    adaptor.addChild(root_0, char_literal401_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2593);
                    expression402=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression402.getTree());
                    char_literal403=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2595); 
                    char_literal403_tree = (Object)adaptor.create(char_literal403);
                    adaptor.addChild(root_0, char_literal403_tree);


                    }
                    break;
                case 21 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:420:4: 'ENCODE_FOR_URI' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal404=(Token)input.LT(1);
                    match(input,109,FOLLOW_109_in_builtInCall2601); 
                    string_literal404_tree = (Object)adaptor.create(string_literal404);
                    adaptor.addChild(root_0, string_literal404_tree);

                    char_literal405=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2603); 
                    char_literal405_tree = (Object)adaptor.create(char_literal405);
                    adaptor.addChild(root_0, char_literal405_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2605);
                    expression406=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression406.getTree());
                    char_literal407=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2607); 
                    char_literal407_tree = (Object)adaptor.create(char_literal407);
                    adaptor.addChild(root_0, char_literal407_tree);


                    }
                    break;
                case 22 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:421:4: 'CONTAINS' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal408=(Token)input.LT(1);
                    match(input,110,FOLLOW_110_in_builtInCall2613); 
                    string_literal408_tree = (Object)adaptor.create(string_literal408);
                    adaptor.addChild(root_0, string_literal408_tree);

                    char_literal409=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2615); 
                    char_literal409_tree = (Object)adaptor.create(char_literal409);
                    adaptor.addChild(root_0, char_literal409_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2617);
                    expression410=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression410.getTree());
                    char_literal411=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2619); 
                    char_literal411_tree = (Object)adaptor.create(char_literal411);
                    adaptor.addChild(root_0, char_literal411_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2621);
                    expression412=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression412.getTree());
                    char_literal413=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2623); 
                    char_literal413_tree = (Object)adaptor.create(char_literal413);
                    adaptor.addChild(root_0, char_literal413_tree);


                    }
                    break;
                case 23 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:422:4: 'STRSTARTS' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal414=(Token)input.LT(1);
                    match(input,111,FOLLOW_111_in_builtInCall2629); 
                    string_literal414_tree = (Object)adaptor.create(string_literal414);
                    adaptor.addChild(root_0, string_literal414_tree);

                    char_literal415=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2631); 
                    char_literal415_tree = (Object)adaptor.create(char_literal415);
                    adaptor.addChild(root_0, char_literal415_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2633);
                    expression416=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression416.getTree());
                    char_literal417=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2635); 
                    char_literal417_tree = (Object)adaptor.create(char_literal417);
                    adaptor.addChild(root_0, char_literal417_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2637);
                    expression418=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression418.getTree());
                    char_literal419=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2639); 
                    char_literal419_tree = (Object)adaptor.create(char_literal419);
                    adaptor.addChild(root_0, char_literal419_tree);


                    }
                    break;
                case 24 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:423:4: 'STRENDS' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal420=(Token)input.LT(1);
                    match(input,112,FOLLOW_112_in_builtInCall2645); 
                    string_literal420_tree = (Object)adaptor.create(string_literal420);
                    adaptor.addChild(root_0, string_literal420_tree);

                    char_literal421=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2647); 
                    char_literal421_tree = (Object)adaptor.create(char_literal421);
                    adaptor.addChild(root_0, char_literal421_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2649);
                    expression422=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression422.getTree());
                    char_literal423=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2651); 
                    char_literal423_tree = (Object)adaptor.create(char_literal423);
                    adaptor.addChild(root_0, char_literal423_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2653);
                    expression424=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression424.getTree());
                    char_literal425=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2655); 
                    char_literal425_tree = (Object)adaptor.create(char_literal425);
                    adaptor.addChild(root_0, char_literal425_tree);


                    }
                    break;
                case 25 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:424:4: 'STRBEFORE' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal426=(Token)input.LT(1);
                    match(input,113,FOLLOW_113_in_builtInCall2661); 
                    string_literal426_tree = (Object)adaptor.create(string_literal426);
                    adaptor.addChild(root_0, string_literal426_tree);

                    char_literal427=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2663); 
                    char_literal427_tree = (Object)adaptor.create(char_literal427);
                    adaptor.addChild(root_0, char_literal427_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2665);
                    expression428=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression428.getTree());
                    char_literal429=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2667); 
                    char_literal429_tree = (Object)adaptor.create(char_literal429);
                    adaptor.addChild(root_0, char_literal429_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2669);
                    expression430=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression430.getTree());
                    char_literal431=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2671); 
                    char_literal431_tree = (Object)adaptor.create(char_literal431);
                    adaptor.addChild(root_0, char_literal431_tree);


                    }
                    break;
                case 26 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:425:4: 'STRAFTER' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal432=(Token)input.LT(1);
                    match(input,114,FOLLOW_114_in_builtInCall2677); 
                    string_literal432_tree = (Object)adaptor.create(string_literal432);
                    adaptor.addChild(root_0, string_literal432_tree);

                    char_literal433=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2679); 
                    char_literal433_tree = (Object)adaptor.create(char_literal433);
                    adaptor.addChild(root_0, char_literal433_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2681);
                    expression434=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression434.getTree());
                    char_literal435=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2683); 
                    char_literal435_tree = (Object)adaptor.create(char_literal435);
                    adaptor.addChild(root_0, char_literal435_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2685);
                    expression436=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression436.getTree());
                    char_literal437=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2687); 
                    char_literal437_tree = (Object)adaptor.create(char_literal437);
                    adaptor.addChild(root_0, char_literal437_tree);


                    }
                    break;
                case 27 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:426:4: 'YEAR' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal438=(Token)input.LT(1);
                    match(input,115,FOLLOW_115_in_builtInCall2693); 
                    string_literal438_tree = (Object)adaptor.create(string_literal438);
                    adaptor.addChild(root_0, string_literal438_tree);

                    char_literal439=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2695); 
                    char_literal439_tree = (Object)adaptor.create(char_literal439);
                    adaptor.addChild(root_0, char_literal439_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2697);
                    expression440=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression440.getTree());
                    char_literal441=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2699); 
                    char_literal441_tree = (Object)adaptor.create(char_literal441);
                    adaptor.addChild(root_0, char_literal441_tree);


                    }
                    break;
                case 28 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:427:4: 'MONTH' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal442=(Token)input.LT(1);
                    match(input,116,FOLLOW_116_in_builtInCall2705); 
                    string_literal442_tree = (Object)adaptor.create(string_literal442);
                    adaptor.addChild(root_0, string_literal442_tree);

                    char_literal443=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2707); 
                    char_literal443_tree = (Object)adaptor.create(char_literal443);
                    adaptor.addChild(root_0, char_literal443_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2709);
                    expression444=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression444.getTree());
                    char_literal445=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2711); 
                    char_literal445_tree = (Object)adaptor.create(char_literal445);
                    adaptor.addChild(root_0, char_literal445_tree);


                    }
                    break;
                case 29 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:428:4: 'DAY' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal446=(Token)input.LT(1);
                    match(input,117,FOLLOW_117_in_builtInCall2717); 
                    string_literal446_tree = (Object)adaptor.create(string_literal446);
                    adaptor.addChild(root_0, string_literal446_tree);

                    char_literal447=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2719); 
                    char_literal447_tree = (Object)adaptor.create(char_literal447);
                    adaptor.addChild(root_0, char_literal447_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2721);
                    expression448=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression448.getTree());
                    char_literal449=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2723); 
                    char_literal449_tree = (Object)adaptor.create(char_literal449);
                    adaptor.addChild(root_0, char_literal449_tree);


                    }
                    break;
                case 30 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:429:4: 'HOURS' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal450=(Token)input.LT(1);
                    match(input,118,FOLLOW_118_in_builtInCall2729); 
                    string_literal450_tree = (Object)adaptor.create(string_literal450);
                    adaptor.addChild(root_0, string_literal450_tree);

                    char_literal451=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2731); 
                    char_literal451_tree = (Object)adaptor.create(char_literal451);
                    adaptor.addChild(root_0, char_literal451_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2733);
                    expression452=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression452.getTree());
                    char_literal453=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2735); 
                    char_literal453_tree = (Object)adaptor.create(char_literal453);
                    adaptor.addChild(root_0, char_literal453_tree);


                    }
                    break;
                case 31 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:430:4: 'MINUTES' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal454=(Token)input.LT(1);
                    match(input,119,FOLLOW_119_in_builtInCall2741); 
                    string_literal454_tree = (Object)adaptor.create(string_literal454);
                    adaptor.addChild(root_0, string_literal454_tree);

                    char_literal455=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2743); 
                    char_literal455_tree = (Object)adaptor.create(char_literal455);
                    adaptor.addChild(root_0, char_literal455_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2745);
                    expression456=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression456.getTree());
                    char_literal457=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2747); 
                    char_literal457_tree = (Object)adaptor.create(char_literal457);
                    adaptor.addChild(root_0, char_literal457_tree);


                    }
                    break;
                case 32 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:431:4: 'SECONDS' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal458=(Token)input.LT(1);
                    match(input,120,FOLLOW_120_in_builtInCall2753); 
                    string_literal458_tree = (Object)adaptor.create(string_literal458);
                    adaptor.addChild(root_0, string_literal458_tree);

                    char_literal459=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2755); 
                    char_literal459_tree = (Object)adaptor.create(char_literal459);
                    adaptor.addChild(root_0, char_literal459_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2757);
                    expression460=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression460.getTree());
                    char_literal461=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2759); 
                    char_literal461_tree = (Object)adaptor.create(char_literal461);
                    adaptor.addChild(root_0, char_literal461_tree);


                    }
                    break;
                case 33 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:432:4: 'TIMEZONE' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal462=(Token)input.LT(1);
                    match(input,121,FOLLOW_121_in_builtInCall2765); 
                    string_literal462_tree = (Object)adaptor.create(string_literal462);
                    adaptor.addChild(root_0, string_literal462_tree);

                    char_literal463=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2767); 
                    char_literal463_tree = (Object)adaptor.create(char_literal463);
                    adaptor.addChild(root_0, char_literal463_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2769);
                    expression464=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression464.getTree());
                    char_literal465=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2771); 
                    char_literal465_tree = (Object)adaptor.create(char_literal465);
                    adaptor.addChild(root_0, char_literal465_tree);


                    }
                    break;
                case 34 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:433:4: 'TZ' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal466=(Token)input.LT(1);
                    match(input,122,FOLLOW_122_in_builtInCall2777); 
                    string_literal466_tree = (Object)adaptor.create(string_literal466);
                    adaptor.addChild(root_0, string_literal466_tree);

                    char_literal467=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2779); 
                    char_literal467_tree = (Object)adaptor.create(char_literal467);
                    adaptor.addChild(root_0, char_literal467_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2781);
                    expression468=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression468.getTree());
                    char_literal469=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2783); 
                    char_literal469_tree = (Object)adaptor.create(char_literal469);
                    adaptor.addChild(root_0, char_literal469_tree);


                    }
                    break;
                case 35 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:434:4: 'NOW' NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal470=(Token)input.LT(1);
                    match(input,123,FOLLOW_123_in_builtInCall2789); 
                    string_literal470_tree = (Object)adaptor.create(string_literal470);
                    adaptor.addChild(root_0, string_literal470_tree);

                    NIL471=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_builtInCall2791); 
                    NIL471_tree = (Object)adaptor.create(NIL471);
                    adaptor.addChild(root_0, NIL471_tree);


                    }
                    break;
                case 36 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:435:4: 'UUID' NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal472=(Token)input.LT(1);
                    match(input,124,FOLLOW_124_in_builtInCall2797); 
                    string_literal472_tree = (Object)adaptor.create(string_literal472);
                    adaptor.addChild(root_0, string_literal472_tree);

                    NIL473=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_builtInCall2799); 
                    NIL473_tree = (Object)adaptor.create(NIL473);
                    adaptor.addChild(root_0, NIL473_tree);


                    }
                    break;
                case 37 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:436:4: 'STRUUID' NIL
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal474=(Token)input.LT(1);
                    match(input,125,FOLLOW_125_in_builtInCall2805); 
                    string_literal474_tree = (Object)adaptor.create(string_literal474);
                    adaptor.addChild(root_0, string_literal474_tree);

                    NIL475=(Token)input.LT(1);
                    match(input,NIL,FOLLOW_NIL_in_builtInCall2807); 
                    NIL475_tree = (Object)adaptor.create(NIL475);
                    adaptor.addChild(root_0, NIL475_tree);


                    }
                    break;
                case 38 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:437:4: 'MD5' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal476=(Token)input.LT(1);
                    match(input,126,FOLLOW_126_in_builtInCall2813); 
                    string_literal476_tree = (Object)adaptor.create(string_literal476);
                    adaptor.addChild(root_0, string_literal476_tree);

                    char_literal477=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2815); 
                    char_literal477_tree = (Object)adaptor.create(char_literal477);
                    adaptor.addChild(root_0, char_literal477_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2817);
                    expression478=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression478.getTree());
                    char_literal479=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2819); 
                    char_literal479_tree = (Object)adaptor.create(char_literal479);
                    adaptor.addChild(root_0, char_literal479_tree);


                    }
                    break;
                case 39 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:438:4: 'SHA1' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal480=(Token)input.LT(1);
                    match(input,127,FOLLOW_127_in_builtInCall2825); 
                    string_literal480_tree = (Object)adaptor.create(string_literal480);
                    adaptor.addChild(root_0, string_literal480_tree);

                    char_literal481=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2827); 
                    char_literal481_tree = (Object)adaptor.create(char_literal481);
                    adaptor.addChild(root_0, char_literal481_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2829);
                    expression482=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression482.getTree());
                    char_literal483=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2831); 
                    char_literal483_tree = (Object)adaptor.create(char_literal483);
                    adaptor.addChild(root_0, char_literal483_tree);


                    }
                    break;
                case 40 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:439:4: 'SHA256' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal484=(Token)input.LT(1);
                    match(input,128,FOLLOW_128_in_builtInCall2837); 
                    string_literal484_tree = (Object)adaptor.create(string_literal484);
                    adaptor.addChild(root_0, string_literal484_tree);

                    char_literal485=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2839); 
                    char_literal485_tree = (Object)adaptor.create(char_literal485);
                    adaptor.addChild(root_0, char_literal485_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2841);
                    expression486=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression486.getTree());
                    char_literal487=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2843); 
                    char_literal487_tree = (Object)adaptor.create(char_literal487);
                    adaptor.addChild(root_0, char_literal487_tree);


                    }
                    break;
                case 41 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:440:4: 'SHA384' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal488=(Token)input.LT(1);
                    match(input,129,FOLLOW_129_in_builtInCall2849); 
                    string_literal488_tree = (Object)adaptor.create(string_literal488);
                    adaptor.addChild(root_0, string_literal488_tree);

                    char_literal489=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2851); 
                    char_literal489_tree = (Object)adaptor.create(char_literal489);
                    adaptor.addChild(root_0, char_literal489_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2853);
                    expression490=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression490.getTree());
                    char_literal491=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2855); 
                    char_literal491_tree = (Object)adaptor.create(char_literal491);
                    adaptor.addChild(root_0, char_literal491_tree);


                    }
                    break;
                case 42 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:441:4: 'SHA512' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal492=(Token)input.LT(1);
                    match(input,130,FOLLOW_130_in_builtInCall2861); 
                    string_literal492_tree = (Object)adaptor.create(string_literal492);
                    adaptor.addChild(root_0, string_literal492_tree);

                    char_literal493=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2863); 
                    char_literal493_tree = (Object)adaptor.create(char_literal493);
                    adaptor.addChild(root_0, char_literal493_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2865);
                    expression494=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression494.getTree());
                    char_literal495=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2867); 
                    char_literal495_tree = (Object)adaptor.create(char_literal495);
                    adaptor.addChild(root_0, char_literal495_tree);


                    }
                    break;
                case 43 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:442:4: 'COALESCE' expressionList
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal496=(Token)input.LT(1);
                    match(input,131,FOLLOW_131_in_builtInCall2873); 
                    string_literal496_tree = (Object)adaptor.create(string_literal496);
                    adaptor.addChild(root_0, string_literal496_tree);

                    pushFollow(FOLLOW_expressionList_in_builtInCall2875);
                    expressionList497=expressionList();
                    _fsp--;

                    adaptor.addChild(root_0, expressionList497.getTree());

                    }
                    break;
                case 44 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:443:4: 'IF' '(' expression ',' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal498=(Token)input.LT(1);
                    match(input,132,FOLLOW_132_in_builtInCall2881); 
                    string_literal498_tree = (Object)adaptor.create(string_literal498);
                    adaptor.addChild(root_0, string_literal498_tree);

                    char_literal499=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2883); 
                    char_literal499_tree = (Object)adaptor.create(char_literal499);
                    adaptor.addChild(root_0, char_literal499_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2885);
                    expression500=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression500.getTree());
                    char_literal501=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2887); 
                    char_literal501_tree = (Object)adaptor.create(char_literal501);
                    adaptor.addChild(root_0, char_literal501_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2889);
                    expression502=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression502.getTree());
                    char_literal503=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2891); 
                    char_literal503_tree = (Object)adaptor.create(char_literal503);
                    adaptor.addChild(root_0, char_literal503_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2893);
                    expression504=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression504.getTree());
                    char_literal505=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2895); 
                    char_literal505_tree = (Object)adaptor.create(char_literal505);
                    adaptor.addChild(root_0, char_literal505_tree);


                    }
                    break;
                case 45 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:444:4: 'STRLANG' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal506=(Token)input.LT(1);
                    match(input,133,FOLLOW_133_in_builtInCall2901); 
                    string_literal506_tree = (Object)adaptor.create(string_literal506);
                    adaptor.addChild(root_0, string_literal506_tree);

                    char_literal507=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2903); 
                    char_literal507_tree = (Object)adaptor.create(char_literal507);
                    adaptor.addChild(root_0, char_literal507_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2905);
                    expression508=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression508.getTree());
                    char_literal509=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2907); 
                    char_literal509_tree = (Object)adaptor.create(char_literal509);
                    adaptor.addChild(root_0, char_literal509_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2909);
                    expression510=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression510.getTree());
                    char_literal511=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2911); 
                    char_literal511_tree = (Object)adaptor.create(char_literal511);
                    adaptor.addChild(root_0, char_literal511_tree);


                    }
                    break;
                case 46 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:445:4: 'STRDT' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal512=(Token)input.LT(1);
                    match(input,134,FOLLOW_134_in_builtInCall2917); 
                    string_literal512_tree = (Object)adaptor.create(string_literal512);
                    adaptor.addChild(root_0, string_literal512_tree);

                    char_literal513=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2919); 
                    char_literal513_tree = (Object)adaptor.create(char_literal513);
                    adaptor.addChild(root_0, char_literal513_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2921);
                    expression514=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression514.getTree());
                    char_literal515=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2923); 
                    char_literal515_tree = (Object)adaptor.create(char_literal515);
                    adaptor.addChild(root_0, char_literal515_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2925);
                    expression516=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression516.getTree());
                    char_literal517=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2927); 
                    char_literal517_tree = (Object)adaptor.create(char_literal517);
                    adaptor.addChild(root_0, char_literal517_tree);


                    }
                    break;
                case 47 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:446:4: 'sameTerm' '(' expression ',' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal518=(Token)input.LT(1);
                    match(input,135,FOLLOW_135_in_builtInCall2933); 
                    string_literal518_tree = (Object)adaptor.create(string_literal518);
                    adaptor.addChild(root_0, string_literal518_tree);

                    char_literal519=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2935); 
                    char_literal519_tree = (Object)adaptor.create(char_literal519);
                    adaptor.addChild(root_0, char_literal519_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2937);
                    expression520=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression520.getTree());
                    char_literal521=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_builtInCall2939); 
                    char_literal521_tree = (Object)adaptor.create(char_literal521);
                    adaptor.addChild(root_0, char_literal521_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2941);
                    expression522=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression522.getTree());
                    char_literal523=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2943); 
                    char_literal523_tree = (Object)adaptor.create(char_literal523);
                    adaptor.addChild(root_0, char_literal523_tree);


                    }
                    break;
                case 48 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:447:4: 'isIRI' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal524=(Token)input.LT(1);
                    match(input,136,FOLLOW_136_in_builtInCall2949); 
                    string_literal524_tree = (Object)adaptor.create(string_literal524);
                    adaptor.addChild(root_0, string_literal524_tree);

                    char_literal525=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2951); 
                    char_literal525_tree = (Object)adaptor.create(char_literal525);
                    adaptor.addChild(root_0, char_literal525_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2953);
                    expression526=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression526.getTree());
                    char_literal527=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2955); 
                    char_literal527_tree = (Object)adaptor.create(char_literal527);
                    adaptor.addChild(root_0, char_literal527_tree);


                    }
                    break;
                case 49 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:448:4: 'isURI' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal528=(Token)input.LT(1);
                    match(input,137,FOLLOW_137_in_builtInCall2961); 
                    string_literal528_tree = (Object)adaptor.create(string_literal528);
                    adaptor.addChild(root_0, string_literal528_tree);

                    char_literal529=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2963); 
                    char_literal529_tree = (Object)adaptor.create(char_literal529);
                    adaptor.addChild(root_0, char_literal529_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2965);
                    expression530=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression530.getTree());
                    char_literal531=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2967); 
                    char_literal531_tree = (Object)adaptor.create(char_literal531);
                    adaptor.addChild(root_0, char_literal531_tree);


                    }
                    break;
                case 50 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:449:4: 'isBLANK' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal532=(Token)input.LT(1);
                    match(input,138,FOLLOW_138_in_builtInCall2973); 
                    string_literal532_tree = (Object)adaptor.create(string_literal532);
                    adaptor.addChild(root_0, string_literal532_tree);

                    char_literal533=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2975); 
                    char_literal533_tree = (Object)adaptor.create(char_literal533);
                    adaptor.addChild(root_0, char_literal533_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2977);
                    expression534=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression534.getTree());
                    char_literal535=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2979); 
                    char_literal535_tree = (Object)adaptor.create(char_literal535);
                    adaptor.addChild(root_0, char_literal535_tree);


                    }
                    break;
                case 51 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:450:4: 'isLITERAL' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal536=(Token)input.LT(1);
                    match(input,139,FOLLOW_139_in_builtInCall2985); 
                    string_literal536_tree = (Object)adaptor.create(string_literal536);
                    adaptor.addChild(root_0, string_literal536_tree);

                    char_literal537=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2987); 
                    char_literal537_tree = (Object)adaptor.create(char_literal537);
                    adaptor.addChild(root_0, char_literal537_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall2989);
                    expression538=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression538.getTree());
                    char_literal539=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall2991); 
                    char_literal539_tree = (Object)adaptor.create(char_literal539);
                    adaptor.addChild(root_0, char_literal539_tree);


                    }
                    break;
                case 52 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:451:4: 'isNUMERIC' '(' expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal540=(Token)input.LT(1);
                    match(input,140,FOLLOW_140_in_builtInCall2997); 
                    string_literal540_tree = (Object)adaptor.create(string_literal540);
                    adaptor.addChild(root_0, string_literal540_tree);

                    char_literal541=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_builtInCall2999); 
                    char_literal541_tree = (Object)adaptor.create(char_literal541);
                    adaptor.addChild(root_0, char_literal541_tree);

                    pushFollow(FOLLOW_expression_in_builtInCall3001);
                    expression542=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression542.getTree());
                    char_literal543=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_builtInCall3003); 
                    char_literal543_tree = (Object)adaptor.create(char_literal543);
                    adaptor.addChild(root_0, char_literal543_tree);


                    }
                    break;
                case 53 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:452:4: regexExpression
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_regexExpression_in_builtInCall3009);
                    regexExpression544=regexExpression();
                    _fsp--;

                    adaptor.addChild(root_0, regexExpression544.getTree());

                    }
                    break;
                case 54 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:453:4: existsFunc
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_existsFunc_in_builtInCall3015);
                    existsFunc545=existsFunc();
                    _fsp--;

                    adaptor.addChild(root_0, existsFunc545.getTree());

                    }
                    break;
                case 55 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:454:4: notExistsFunc
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_notExistsFunc_in_builtInCall3021);
                    notExistsFunc546=notExistsFunc();
                    _fsp--;

                    adaptor.addChild(root_0, notExistsFunc546.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end builtInCall

    public static class regexExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start regexExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:457:1: regexExpression : 'REGEX' '(' expression ',' expression ( ',' expression )? ')' ;
    public final regexExpression_return regexExpression() throws RecognitionException {
        regexExpression_return retval = new regexExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal547=null;
        Token char_literal548=null;
        Token char_literal550=null;
        Token char_literal552=null;
        Token char_literal554=null;
        expression_return expression549 = null;

        expression_return expression551 = null;

        expression_return expression553 = null;


        Object string_literal547_tree=null;
        Object char_literal548_tree=null;
        Object char_literal550_tree=null;
        Object char_literal552_tree=null;
        Object char_literal554_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:458:5: ( 'REGEX' '(' expression ',' expression ( ',' expression )? ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:458:7: 'REGEX' '(' expression ',' expression ( ',' expression )? ')'
            {
            root_0 = (Object)adaptor.nil();

            string_literal547=(Token)input.LT(1);
            match(input,141,FOLLOW_141_in_regexExpression3039); 
            string_literal547_tree = (Object)adaptor.create(string_literal547);
            adaptor.addChild(root_0, string_literal547_tree);

            char_literal548=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_regexExpression3041); 
            char_literal548_tree = (Object)adaptor.create(char_literal548);
            adaptor.addChild(root_0, char_literal548_tree);

            pushFollow(FOLLOW_expression_in_regexExpression3043);
            expression549=expression();
            _fsp--;

            adaptor.addChild(root_0, expression549.getTree());
            char_literal550=(Token)input.LT(1);
            match(input,72,FOLLOW_72_in_regexExpression3045); 
            char_literal550_tree = (Object)adaptor.create(char_literal550);
            adaptor.addChild(root_0, char_literal550_tree);

            pushFollow(FOLLOW_expression_in_regexExpression3047);
            expression551=expression();
            _fsp--;

            adaptor.addChild(root_0, expression551.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:458:45: ( ',' expression )?
            int alt102=2;
            int LA102_0 = input.LA(1);

            if ( (LA102_0==72) ) {
                alt102=1;
            }
            switch (alt102) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:458:47: ',' expression
                    {
                    char_literal552=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_regexExpression3051); 
                    char_literal552_tree = (Object)adaptor.create(char_literal552);
                    adaptor.addChild(root_0, char_literal552_tree);

                    pushFollow(FOLLOW_expression_in_regexExpression3053);
                    expression553=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression553.getTree());

                    }
                    break;

            }

            char_literal554=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_regexExpression3058); 
            char_literal554_tree = (Object)adaptor.create(char_literal554);
            adaptor.addChild(root_0, char_literal554_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end regexExpression

    public static class subStringExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start subStringExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:461:1: subStringExpression : 'SUBSTR' '(' expression ',' expression ( ',' expression )? ')' ;
    public final subStringExpression_return subStringExpression() throws RecognitionException {
        subStringExpression_return retval = new subStringExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal555=null;
        Token char_literal556=null;
        Token char_literal558=null;
        Token char_literal560=null;
        Token char_literal562=null;
        expression_return expression557 = null;

        expression_return expression559 = null;

        expression_return expression561 = null;


        Object string_literal555_tree=null;
        Object char_literal556_tree=null;
        Object char_literal558_tree=null;
        Object char_literal560_tree=null;
        Object char_literal562_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:462:5: ( 'SUBSTR' '(' expression ',' expression ( ',' expression )? ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:462:7: 'SUBSTR' '(' expression ',' expression ( ',' expression )? ')'
            {
            root_0 = (Object)adaptor.nil();

            string_literal555=(Token)input.LT(1);
            match(input,142,FOLLOW_142_in_subStringExpression3076); 
            string_literal555_tree = (Object)adaptor.create(string_literal555);
            adaptor.addChild(root_0, string_literal555_tree);

            char_literal556=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_subStringExpression3078); 
            char_literal556_tree = (Object)adaptor.create(char_literal556);
            adaptor.addChild(root_0, char_literal556_tree);

            pushFollow(FOLLOW_expression_in_subStringExpression3080);
            expression557=expression();
            _fsp--;

            adaptor.addChild(root_0, expression557.getTree());
            char_literal558=(Token)input.LT(1);
            match(input,72,FOLLOW_72_in_subStringExpression3082); 
            char_literal558_tree = (Object)adaptor.create(char_literal558);
            adaptor.addChild(root_0, char_literal558_tree);

            pushFollow(FOLLOW_expression_in_subStringExpression3084);
            expression559=expression();
            _fsp--;

            adaptor.addChild(root_0, expression559.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:462:46: ( ',' expression )?
            int alt103=2;
            int LA103_0 = input.LA(1);

            if ( (LA103_0==72) ) {
                alt103=1;
            }
            switch (alt103) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:462:48: ',' expression
                    {
                    char_literal560=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_subStringExpression3088); 
                    char_literal560_tree = (Object)adaptor.create(char_literal560);
                    adaptor.addChild(root_0, char_literal560_tree);

                    pushFollow(FOLLOW_expression_in_subStringExpression3090);
                    expression561=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression561.getTree());

                    }
                    break;

            }

            char_literal562=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_subStringExpression3095); 
            char_literal562_tree = (Object)adaptor.create(char_literal562);
            adaptor.addChild(root_0, char_literal562_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end subStringExpression

    public static class strReplaceExpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start strReplaceExpression
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:465:1: strReplaceExpression : 'REPLACE' '(' expression ',' expression ',' expression ( ',' expression )? ')' ;
    public final strReplaceExpression_return strReplaceExpression() throws RecognitionException {
        strReplaceExpression_return retval = new strReplaceExpression_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal563=null;
        Token char_literal564=null;
        Token char_literal566=null;
        Token char_literal568=null;
        Token char_literal570=null;
        Token char_literal572=null;
        expression_return expression565 = null;

        expression_return expression567 = null;

        expression_return expression569 = null;

        expression_return expression571 = null;


        Object string_literal563_tree=null;
        Object char_literal564_tree=null;
        Object char_literal566_tree=null;
        Object char_literal568_tree=null;
        Object char_literal570_tree=null;
        Object char_literal572_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:466:5: ( 'REPLACE' '(' expression ',' expression ',' expression ( ',' expression )? ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:466:7: 'REPLACE' '(' expression ',' expression ',' expression ( ',' expression )? ')'
            {
            root_0 = (Object)adaptor.nil();

            string_literal563=(Token)input.LT(1);
            match(input,143,FOLLOW_143_in_strReplaceExpression3110); 
            string_literal563_tree = (Object)adaptor.create(string_literal563);
            adaptor.addChild(root_0, string_literal563_tree);

            char_literal564=(Token)input.LT(1);
            match(input,41,FOLLOW_41_in_strReplaceExpression3112); 
            char_literal564_tree = (Object)adaptor.create(char_literal564);
            adaptor.addChild(root_0, char_literal564_tree);

            pushFollow(FOLLOW_expression_in_strReplaceExpression3114);
            expression565=expression();
            _fsp--;

            adaptor.addChild(root_0, expression565.getTree());
            char_literal566=(Token)input.LT(1);
            match(input,72,FOLLOW_72_in_strReplaceExpression3116); 
            char_literal566_tree = (Object)adaptor.create(char_literal566);
            adaptor.addChild(root_0, char_literal566_tree);

            pushFollow(FOLLOW_expression_in_strReplaceExpression3118);
            expression567=expression();
            _fsp--;

            adaptor.addChild(root_0, expression567.getTree());
            char_literal568=(Token)input.LT(1);
            match(input,72,FOLLOW_72_in_strReplaceExpression3120); 
            char_literal568_tree = (Object)adaptor.create(char_literal568);
            adaptor.addChild(root_0, char_literal568_tree);

            pushFollow(FOLLOW_expression_in_strReplaceExpression3122);
            expression569=expression();
            _fsp--;

            adaptor.addChild(root_0, expression569.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:466:62: ( ',' expression )?
            int alt104=2;
            int LA104_0 = input.LA(1);

            if ( (LA104_0==72) ) {
                alt104=1;
            }
            switch (alt104) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:466:64: ',' expression
                    {
                    char_literal570=(Token)input.LT(1);
                    match(input,72,FOLLOW_72_in_strReplaceExpression3126); 
                    char_literal570_tree = (Object)adaptor.create(char_literal570);
                    adaptor.addChild(root_0, char_literal570_tree);

                    pushFollow(FOLLOW_expression_in_strReplaceExpression3128);
                    expression571=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression571.getTree());

                    }
                    break;

            }

            char_literal572=(Token)input.LT(1);
            match(input,43,FOLLOW_43_in_strReplaceExpression3133); 
            char_literal572_tree = (Object)adaptor.create(char_literal572);
            adaptor.addChild(root_0, char_literal572_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end strReplaceExpression

    public static class existsFunc_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start existsFunc
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:469:1: existsFunc : 'EXISTS' groupGraphPattern ;
    public final existsFunc_return existsFunc() throws RecognitionException {
        existsFunc_return retval = new existsFunc_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal573=null;
        groupGraphPattern_return groupGraphPattern574 = null;


        Object string_literal573_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:470:5: ( 'EXISTS' groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:470:7: 'EXISTS' groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            string_literal573=(Token)input.LT(1);
            match(input,144,FOLLOW_144_in_existsFunc3148); 
            string_literal573_tree = (Object)adaptor.create(string_literal573);
            adaptor.addChild(root_0, string_literal573_tree);

            pushFollow(FOLLOW_groupGraphPattern_in_existsFunc3150);
            groupGraphPattern574=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern574.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end existsFunc

    public static class notExistsFunc_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start notExistsFunc
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:473:1: notExistsFunc : 'NOT' 'EXISTS' groupGraphPattern ;
    public final notExistsFunc_return notExistsFunc() throws RecognitionException {
        notExistsFunc_return retval = new notExistsFunc_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal575=null;
        Token string_literal576=null;
        groupGraphPattern_return groupGraphPattern577 = null;


        Object string_literal575_tree=null;
        Object string_literal576_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:474:5: ( 'NOT' 'EXISTS' groupGraphPattern )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:474:7: 'NOT' 'EXISTS' groupGraphPattern
            {
            root_0 = (Object)adaptor.nil();

            string_literal575=(Token)input.LT(1);
            match(input,145,FOLLOW_145_in_notExistsFunc3165); 
            string_literal575_tree = (Object)adaptor.create(string_literal575);
            adaptor.addChild(root_0, string_literal575_tree);

            string_literal576=(Token)input.LT(1);
            match(input,144,FOLLOW_144_in_notExistsFunc3167); 
            string_literal576_tree = (Object)adaptor.create(string_literal576);
            adaptor.addChild(root_0, string_literal576_tree);

            pushFollow(FOLLOW_groupGraphPattern_in_notExistsFunc3169);
            groupGraphPattern577=groupGraphPattern();
            _fsp--;

            adaptor.addChild(root_0, groupGraphPattern577.getTree());

            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end notExistsFunc

    public static class aggregate_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start aggregate
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:477:1: aggregate : ( 'COUNT' '(' ( 'DISTINCT' )? ( '*' | expression ) ')' | 'SUM' '(' ( 'DISTINCT' )? expression ')' | 'MIN' '(' ( 'DISTINCT' )? expression ')' | 'MAX' '(' ( 'DISTINCT' )? expression ')' | 'AVG' '(' ( 'DISTINCT' )? expression ')' | 'SAMPLE' '(' ( 'DISTINCT' )? expression ')' | 'GROUP_CONCAT' '(' ( 'DISTINCT' )? expression ( ';' 'SEPARATOR' '=' string )? ')' );
    public final aggregate_return aggregate() throws RecognitionException {
        aggregate_return retval = new aggregate_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal578=null;
        Token char_literal579=null;
        Token string_literal580=null;
        Token char_literal581=null;
        Token char_literal583=null;
        Token string_literal584=null;
        Token char_literal585=null;
        Token string_literal586=null;
        Token char_literal588=null;
        Token string_literal589=null;
        Token char_literal590=null;
        Token string_literal591=null;
        Token char_literal593=null;
        Token string_literal594=null;
        Token char_literal595=null;
        Token string_literal596=null;
        Token char_literal598=null;
        Token string_literal599=null;
        Token char_literal600=null;
        Token string_literal601=null;
        Token char_literal603=null;
        Token string_literal604=null;
        Token char_literal605=null;
        Token string_literal606=null;
        Token char_literal608=null;
        Token string_literal609=null;
        Token char_literal610=null;
        Token string_literal611=null;
        Token char_literal613=null;
        Token string_literal614=null;
        Token char_literal615=null;
        Token char_literal617=null;
        expression_return expression582 = null;

        expression_return expression587 = null;

        expression_return expression592 = null;

        expression_return expression597 = null;

        expression_return expression602 = null;

        expression_return expression607 = null;

        expression_return expression612 = null;

        string_return string616 = null;


        Object string_literal578_tree=null;
        Object char_literal579_tree=null;
        Object string_literal580_tree=null;
        Object char_literal581_tree=null;
        Object char_literal583_tree=null;
        Object string_literal584_tree=null;
        Object char_literal585_tree=null;
        Object string_literal586_tree=null;
        Object char_literal588_tree=null;
        Object string_literal589_tree=null;
        Object char_literal590_tree=null;
        Object string_literal591_tree=null;
        Object char_literal593_tree=null;
        Object string_literal594_tree=null;
        Object char_literal595_tree=null;
        Object string_literal596_tree=null;
        Object char_literal598_tree=null;
        Object string_literal599_tree=null;
        Object char_literal600_tree=null;
        Object string_literal601_tree=null;
        Object char_literal603_tree=null;
        Object string_literal604_tree=null;
        Object char_literal605_tree=null;
        Object string_literal606_tree=null;
        Object char_literal608_tree=null;
        Object string_literal609_tree=null;
        Object char_literal610_tree=null;
        Object string_literal611_tree=null;
        Object char_literal613_tree=null;
        Object string_literal614_tree=null;
        Object char_literal615_tree=null;
        Object char_literal617_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:5: ( 'COUNT' '(' ( 'DISTINCT' )? ( '*' | expression ) ')' | 'SUM' '(' ( 'DISTINCT' )? expression ')' | 'MIN' '(' ( 'DISTINCT' )? expression ')' | 'MAX' '(' ( 'DISTINCT' )? expression ')' | 'AVG' '(' ( 'DISTINCT' )? expression ')' | 'SAMPLE' '(' ( 'DISTINCT' )? expression ')' | 'GROUP_CONCAT' '(' ( 'DISTINCT' )? expression ( ';' 'SEPARATOR' '=' string )? ')' )
            int alt114=7;
            switch ( input.LA(1) ) {
            case 146:
                {
                alt114=1;
                }
                break;
            case 147:
                {
                alt114=2;
                }
                break;
            case 148:
                {
                alt114=3;
                }
                break;
            case 149:
                {
                alt114=4;
                }
                break;
            case 150:
                {
                alt114=5;
                }
                break;
            case 151:
                {
                alt114=6;
                }
                break;
            case 152:
                {
                alt114=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("477:1: aggregate : ( 'COUNT' '(' ( 'DISTINCT' )? ( '*' | expression ) ')' | 'SUM' '(' ( 'DISTINCT' )? expression ')' | 'MIN' '(' ( 'DISTINCT' )? expression ')' | 'MAX' '(' ( 'DISTINCT' )? expression ')' | 'AVG' '(' ( 'DISTINCT' )? expression ')' | 'SAMPLE' '(' ( 'DISTINCT' )? expression ')' | 'GROUP_CONCAT' '(' ( 'DISTINCT' )? expression ( ';' 'SEPARATOR' '=' string )? ')' );", 114, 0, input);

                throw nvae;
            }

            switch (alt114) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:7: 'COUNT' '(' ( 'DISTINCT' )? ( '*' | expression ) ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal578=(Token)input.LT(1);
                    match(input,146,FOLLOW_146_in_aggregate3184); 
                    string_literal578_tree = (Object)adaptor.create(string_literal578);
                    adaptor.addChild(root_0, string_literal578_tree);

                    char_literal579=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3186); 
                    char_literal579_tree = (Object)adaptor.create(char_literal579);
                    adaptor.addChild(root_0, char_literal579_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:19: ( 'DISTINCT' )?
                    int alt105=2;
                    int LA105_0 = input.LA(1);

                    if ( (LA105_0==39) ) {
                        alt105=1;
                    }
                    switch (alt105) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:19: 'DISTINCT'
                            {
                            string_literal580=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3188); 
                            string_literal580_tree = (Object)adaptor.create(string_literal580);
                            adaptor.addChild(root_0, string_literal580_tree);


                            }
                            break;

                    }

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:31: ( '*' | expression )
                    int alt106=2;
                    int LA106_0 = input.LA(1);

                    if ( (LA106_0==44) ) {
                        alt106=1;
                    }
                    else if ( ((LA106_0>=IRI_REF && LA106_0<=INTEGER)||(LA106_0>=VAR1 && LA106_0<=VAR2)||(LA106_0>=DECIMAL && LA106_0<=PNAME_LN)||LA106_0==41||(LA106_0>=79 && LA106_0<=80)||(LA106_0>=91 && LA106_0<=152)||(LA106_0>=155 && LA106_0<=156)) ) {
                        alt106=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("478:31: ( '*' | expression )", 106, 0, input);

                        throw nvae;
                    }
                    switch (alt106) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:33: '*'
                            {
                            char_literal581=(Token)input.LT(1);
                            match(input,44,FOLLOW_44_in_aggregate3193); 
                            char_literal581_tree = (Object)adaptor.create(char_literal581);
                            adaptor.addChild(root_0, char_literal581_tree);


                            }
                            break;
                        case 2 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:478:39: expression
                            {
                            pushFollow(FOLLOW_expression_in_aggregate3197);
                            expression582=expression();
                            _fsp--;

                            adaptor.addChild(root_0, expression582.getTree());

                            }
                            break;

                    }

                    char_literal583=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3201); 
                    char_literal583_tree = (Object)adaptor.create(char_literal583);
                    adaptor.addChild(root_0, char_literal583_tree);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:479:7: 'SUM' '(' ( 'DISTINCT' )? expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal584=(Token)input.LT(1);
                    match(input,147,FOLLOW_147_in_aggregate3210); 
                    string_literal584_tree = (Object)adaptor.create(string_literal584);
                    adaptor.addChild(root_0, string_literal584_tree);

                    char_literal585=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3212); 
                    char_literal585_tree = (Object)adaptor.create(char_literal585);
                    adaptor.addChild(root_0, char_literal585_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:479:17: ( 'DISTINCT' )?
                    int alt107=2;
                    int LA107_0 = input.LA(1);

                    if ( (LA107_0==39) ) {
                        alt107=1;
                    }
                    switch (alt107) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:479:17: 'DISTINCT'
                            {
                            string_literal586=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3214); 
                            string_literal586_tree = (Object)adaptor.create(string_literal586);
                            adaptor.addChild(root_0, string_literal586_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_aggregate3217);
                    expression587=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression587.getTree());
                    char_literal588=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3219); 
                    char_literal588_tree = (Object)adaptor.create(char_literal588);
                    adaptor.addChild(root_0, char_literal588_tree);


                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:480:7: 'MIN' '(' ( 'DISTINCT' )? expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal589=(Token)input.LT(1);
                    match(input,148,FOLLOW_148_in_aggregate3228); 
                    string_literal589_tree = (Object)adaptor.create(string_literal589);
                    adaptor.addChild(root_0, string_literal589_tree);

                    char_literal590=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3230); 
                    char_literal590_tree = (Object)adaptor.create(char_literal590);
                    adaptor.addChild(root_0, char_literal590_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:480:17: ( 'DISTINCT' )?
                    int alt108=2;
                    int LA108_0 = input.LA(1);

                    if ( (LA108_0==39) ) {
                        alt108=1;
                    }
                    switch (alt108) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:480:17: 'DISTINCT'
                            {
                            string_literal591=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3232); 
                            string_literal591_tree = (Object)adaptor.create(string_literal591);
                            adaptor.addChild(root_0, string_literal591_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_aggregate3235);
                    expression592=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression592.getTree());
                    char_literal593=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3237); 
                    char_literal593_tree = (Object)adaptor.create(char_literal593);
                    adaptor.addChild(root_0, char_literal593_tree);


                    }
                    break;
                case 4 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:481:7: 'MAX' '(' ( 'DISTINCT' )? expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal594=(Token)input.LT(1);
                    match(input,149,FOLLOW_149_in_aggregate3246); 
                    string_literal594_tree = (Object)adaptor.create(string_literal594);
                    adaptor.addChild(root_0, string_literal594_tree);

                    char_literal595=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3248); 
                    char_literal595_tree = (Object)adaptor.create(char_literal595);
                    adaptor.addChild(root_0, char_literal595_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:481:17: ( 'DISTINCT' )?
                    int alt109=2;
                    int LA109_0 = input.LA(1);

                    if ( (LA109_0==39) ) {
                        alt109=1;
                    }
                    switch (alt109) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:481:17: 'DISTINCT'
                            {
                            string_literal596=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3250); 
                            string_literal596_tree = (Object)adaptor.create(string_literal596);
                            adaptor.addChild(root_0, string_literal596_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_aggregate3253);
                    expression597=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression597.getTree());
                    char_literal598=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3255); 
                    char_literal598_tree = (Object)adaptor.create(char_literal598);
                    adaptor.addChild(root_0, char_literal598_tree);


                    }
                    break;
                case 5 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:482:7: 'AVG' '(' ( 'DISTINCT' )? expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal599=(Token)input.LT(1);
                    match(input,150,FOLLOW_150_in_aggregate3264); 
                    string_literal599_tree = (Object)adaptor.create(string_literal599);
                    adaptor.addChild(root_0, string_literal599_tree);

                    char_literal600=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3266); 
                    char_literal600_tree = (Object)adaptor.create(char_literal600);
                    adaptor.addChild(root_0, char_literal600_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:482:17: ( 'DISTINCT' )?
                    int alt110=2;
                    int LA110_0 = input.LA(1);

                    if ( (LA110_0==39) ) {
                        alt110=1;
                    }
                    switch (alt110) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:482:17: 'DISTINCT'
                            {
                            string_literal601=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3268); 
                            string_literal601_tree = (Object)adaptor.create(string_literal601);
                            adaptor.addChild(root_0, string_literal601_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_aggregate3271);
                    expression602=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression602.getTree());
                    char_literal603=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3273); 
                    char_literal603_tree = (Object)adaptor.create(char_literal603);
                    adaptor.addChild(root_0, char_literal603_tree);


                    }
                    break;
                case 6 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:483:7: 'SAMPLE' '(' ( 'DISTINCT' )? expression ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal604=(Token)input.LT(1);
                    match(input,151,FOLLOW_151_in_aggregate3282); 
                    string_literal604_tree = (Object)adaptor.create(string_literal604);
                    adaptor.addChild(root_0, string_literal604_tree);

                    char_literal605=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3284); 
                    char_literal605_tree = (Object)adaptor.create(char_literal605);
                    adaptor.addChild(root_0, char_literal605_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:483:20: ( 'DISTINCT' )?
                    int alt111=2;
                    int LA111_0 = input.LA(1);

                    if ( (LA111_0==39) ) {
                        alt111=1;
                    }
                    switch (alt111) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:483:20: 'DISTINCT'
                            {
                            string_literal606=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3286); 
                            string_literal606_tree = (Object)adaptor.create(string_literal606);
                            adaptor.addChild(root_0, string_literal606_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_aggregate3289);
                    expression607=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression607.getTree());
                    char_literal608=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3291); 
                    char_literal608_tree = (Object)adaptor.create(char_literal608);
                    adaptor.addChild(root_0, char_literal608_tree);


                    }
                    break;
                case 7 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:484:7: 'GROUP_CONCAT' '(' ( 'DISTINCT' )? expression ( ';' 'SEPARATOR' '=' string )? ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal609=(Token)input.LT(1);
                    match(input,152,FOLLOW_152_in_aggregate3300); 
                    string_literal609_tree = (Object)adaptor.create(string_literal609);
                    adaptor.addChild(root_0, string_literal609_tree);

                    char_literal610=(Token)input.LT(1);
                    match(input,41,FOLLOW_41_in_aggregate3302); 
                    char_literal610_tree = (Object)adaptor.create(char_literal610);
                    adaptor.addChild(root_0, char_literal610_tree);

                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:484:26: ( 'DISTINCT' )?
                    int alt112=2;
                    int LA112_0 = input.LA(1);

                    if ( (LA112_0==39) ) {
                        alt112=1;
                    }
                    switch (alt112) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:484:26: 'DISTINCT'
                            {
                            string_literal611=(Token)input.LT(1);
                            match(input,39,FOLLOW_39_in_aggregate3304); 
                            string_literal611_tree = (Object)adaptor.create(string_literal611);
                            adaptor.addChild(root_0, string_literal611_tree);


                            }
                            break;

                    }

                    pushFollow(FOLLOW_expression_in_aggregate3307);
                    expression612=expression();
                    _fsp--;

                    adaptor.addChild(root_0, expression612.getTree());
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:484:49: ( ';' 'SEPARATOR' '=' string )?
                    int alt113=2;
                    int LA113_0 = input.LA(1);

                    if ( (LA113_0==73) ) {
                        alt113=1;
                    }
                    switch (alt113) {
                        case 1 :
                            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:484:51: ';' 'SEPARATOR' '=' string
                            {
                            char_literal613=(Token)input.LT(1);
                            match(input,73,FOLLOW_73_in_aggregate3311); 
                            char_literal613_tree = (Object)adaptor.create(char_literal613);
                            adaptor.addChild(root_0, char_literal613_tree);

                            string_literal614=(Token)input.LT(1);
                            match(input,153,FOLLOW_153_in_aggregate3313); 
                            string_literal614_tree = (Object)adaptor.create(string_literal614);
                            adaptor.addChild(root_0, string_literal614_tree);

                            char_literal615=(Token)input.LT(1);
                            match(input,85,FOLLOW_85_in_aggregate3315); 
                            char_literal615_tree = (Object)adaptor.create(char_literal615);
                            adaptor.addChild(root_0, char_literal615_tree);

                            pushFollow(FOLLOW_string_in_aggregate3317);
                            string616=string();
                            _fsp--;

                            adaptor.addChild(root_0, string616.getTree());

                            }
                            break;

                    }

                    char_literal617=(Token)input.LT(1);
                    match(input,43,FOLLOW_43_in_aggregate3322); 
                    char_literal617_tree = (Object)adaptor.create(char_literal617);
                    adaptor.addChild(root_0, char_literal617_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end aggregate

    public static class iriRefOrFunction_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start iriRefOrFunction
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:487:1: iriRefOrFunction : iriRef ( argList )? ;
    public final iriRefOrFunction_return iriRefOrFunction() throws RecognitionException {
        iriRefOrFunction_return retval = new iriRefOrFunction_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        iriRef_return iriRef618 = null;

        argList_return argList619 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:488:5: ( iriRef ( argList )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:488:7: iriRef ( argList )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_iriRef_in_iriRefOrFunction3337);
            iriRef618=iriRef();
            _fsp--;

            adaptor.addChild(root_0, iriRef618.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:488:14: ( argList )?
            int alt115=2;
            int LA115_0 = input.LA(1);

            if ( (LA115_0==NIL||LA115_0==41) ) {
                alt115=1;
            }
            switch (alt115) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:488:14: argList
                    {
                    pushFollow(FOLLOW_argList_in_iriRefOrFunction3339);
                    argList619=argList();
                    _fsp--;

                    adaptor.addChild(root_0, argList619.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end iriRefOrFunction

    public static class rdfLiteral_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start rdfLiteral
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:491:1: rdfLiteral : string ( LANGTAG | ( '^^' iriRef ) )? ;
    public final rdfLiteral_return rdfLiteral() throws RecognitionException {
        rdfLiteral_return retval = new rdfLiteral_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token LANGTAG621=null;
        Token string_literal622=null;
        string_return string620 = null;

        iriRef_return iriRef623 = null;


        Object LANGTAG621_tree=null;
        Object string_literal622_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:5: ( string ( LANGTAG | ( '^^' iriRef ) )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:7: string ( LANGTAG | ( '^^' iriRef ) )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_string_in_rdfLiteral3358);
            string620=string();
            _fsp--;

            adaptor.addChild(root_0, string620.getTree());
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:14: ( LANGTAG | ( '^^' iriRef ) )?
            int alt116=3;
            int LA116_0 = input.LA(1);

            if ( (LA116_0==LANGTAG) ) {
                alt116=1;
            }
            else if ( (LA116_0==154) ) {
                alt116=2;
            }
            switch (alt116) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:16: LANGTAG
                    {
                    LANGTAG621=(Token)input.LT(1);
                    match(input,LANGTAG,FOLLOW_LANGTAG_in_rdfLiteral3362); 
                    LANGTAG621_tree = (Object)adaptor.create(LANGTAG621);
                    adaptor.addChild(root_0, LANGTAG621_tree);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:26: ( '^^' iriRef )
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:26: ( '^^' iriRef )
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:492:28: '^^' iriRef
                    {
                    string_literal622=(Token)input.LT(1);
                    match(input,154,FOLLOW_154_in_rdfLiteral3368); 
                    string_literal622_tree = (Object)adaptor.create(string_literal622);
                    adaptor.addChild(root_0, string_literal622_tree);

                    pushFollow(FOLLOW_iriRef_in_rdfLiteral3370);
                    iriRef623=iriRef();
                    _fsp--;

                    adaptor.addChild(root_0, iriRef623.getTree());

                    }


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end rdfLiteral

    public static class numericLiteral_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start numericLiteral
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:495:1: numericLiteral : ( numericLiteralUnsigned | numericLiteralPositive | numericLiteralNegative );
    public final numericLiteral_return numericLiteral() throws RecognitionException {
        numericLiteral_return retval = new numericLiteral_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        numericLiteralUnsigned_return numericLiteralUnsigned624 = null;

        numericLiteralPositive_return numericLiteralPositive625 = null;

        numericLiteralNegative_return numericLiteralNegative626 = null;



        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:496:5: ( numericLiteralUnsigned | numericLiteralPositive | numericLiteralNegative )
            int alt117=3;
            switch ( input.LA(1) ) {
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
                {
                alt117=1;
                }
                break;
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
                {
                alt117=2;
                }
                break;
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt117=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("495:1: numericLiteral : ( numericLiteralUnsigned | numericLiteralPositive | numericLiteralNegative );", 117, 0, input);

                throw nvae;
            }

            switch (alt117) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:496:7: numericLiteralUnsigned
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_numericLiteralUnsigned_in_numericLiteral3392);
                    numericLiteralUnsigned624=numericLiteralUnsigned();
                    _fsp--;

                    adaptor.addChild(root_0, numericLiteralUnsigned624.getTree());

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:496:32: numericLiteralPositive
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_numericLiteralPositive_in_numericLiteral3396);
                    numericLiteralPositive625=numericLiteralPositive();
                    _fsp--;

                    adaptor.addChild(root_0, numericLiteralPositive625.getTree());

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:496:57: numericLiteralNegative
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_numericLiteralNegative_in_numericLiteral3400);
                    numericLiteralNegative626=numericLiteralNegative();
                    _fsp--;

                    adaptor.addChild(root_0, numericLiteralNegative626.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end numericLiteral

    public static class numericLiteralUnsigned_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start numericLiteralUnsigned
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:499:1: numericLiteralUnsigned : ( INTEGER | DECIMAL | DOUBLE );
    public final numericLiteralUnsigned_return numericLiteralUnsigned() throws RecognitionException {
        numericLiteralUnsigned_return retval = new numericLiteralUnsigned_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set627=null;

        Object set627_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:500:5: ( INTEGER | DECIMAL | DOUBLE )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set627=(Token)input.LT(1);
            if ( input.LA(1)==INTEGER||(input.LA(1)>=DECIMAL && input.LA(1)<=DOUBLE) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set627));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_numericLiteralUnsigned0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end numericLiteralUnsigned

    public static class numericLiteralPositive_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start numericLiteralPositive
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:505:1: numericLiteralPositive : ( INTEGER_POSITIVE | DECIMAL_POSITIVE | DOUBLE_POSITIVE );
    public final numericLiteralPositive_return numericLiteralPositive() throws RecognitionException {
        numericLiteralPositive_return retval = new numericLiteralPositive_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set628=null;

        Object set628_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:506:5: ( INTEGER_POSITIVE | DECIMAL_POSITIVE | DOUBLE_POSITIVE )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set628=(Token)input.LT(1);
            if ( (input.LA(1)>=INTEGER_POSITIVE && input.LA(1)<=DOUBLE_POSITIVE) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set628));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_numericLiteralPositive0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end numericLiteralPositive

    public static class numericLiteralNegative_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start numericLiteralNegative
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:511:1: numericLiteralNegative : ( INTEGER_NEGATIVE | DECIMAL_NEGATIVE | DOUBLE_NEGATIVE );
    public final numericLiteralNegative_return numericLiteralNegative() throws RecognitionException {
        numericLiteralNegative_return retval = new numericLiteralNegative_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set629=null;

        Object set629_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:512:5: ( INTEGER_NEGATIVE | DECIMAL_NEGATIVE | DOUBLE_NEGATIVE )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set629=(Token)input.LT(1);
            if ( (input.LA(1)>=INTEGER_NEGATIVE && input.LA(1)<=DOUBLE_NEGATIVE) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set629));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_numericLiteralNegative0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end numericLiteralNegative

    public static class booleanLiteral_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start booleanLiteral
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:517:1: booleanLiteral : ( 'true' | 'false' );
    public final booleanLiteral_return booleanLiteral() throws RecognitionException {
        booleanLiteral_return retval = new booleanLiteral_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set630=null;

        Object set630_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:518:5: ( 'true' | 'false' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set630=(Token)input.LT(1);
            if ( (input.LA(1)>=155 && input.LA(1)<=156) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set630));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_booleanLiteral0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end booleanLiteral

    public static class string_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start string
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:522:1: string : ( STRING_LITERAL1 | STRING_LITERAL2 );
    public final string_return string() throws RecognitionException {
        string_return retval = new string_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set631=null;

        Object set631_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:523:5: ( STRING_LITERAL1 | STRING_LITERAL2 )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set631=(Token)input.LT(1);
            if ( (input.LA(1)>=STRING_LITERAL1 && input.LA(1)<=STRING_LITERAL2) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set631));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_string0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end string

    public static class iriRef_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start iriRef
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:528:1: iriRef : ( IRI_REF | prefixedName );
    public final iriRef_return iriRef() throws RecognitionException {
        iriRef_return retval = new iriRef_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IRI_REF632=null;
        prefixedName_return prefixedName633 = null;


        Object IRI_REF632_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:529:5: ( IRI_REF | prefixedName )
            int alt118=2;
            int LA118_0 = input.LA(1);

            if ( (LA118_0==IRI_REF) ) {
                alt118=1;
            }
            else if ( (LA118_0==PNAME_NS||LA118_0==PNAME_LN) ) {
                alt118=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("528:1: iriRef : ( IRI_REF | prefixedName );", 118, 0, input);

                throw nvae;
            }
            switch (alt118) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:529:7: IRI_REF
                    {
                    root_0 = (Object)adaptor.nil();

                    IRI_REF632=(Token)input.LT(1);
                    match(input,IRI_REF,FOLLOW_IRI_REF_in_iriRef3572); 
                    IRI_REF632_tree = (Object)adaptor.create(IRI_REF632);
                    adaptor.addChild(root_0, IRI_REF632_tree);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:530:7: prefixedName
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_prefixedName_in_iriRef3580);
                    prefixedName633=prefixedName();
                    _fsp--;

                    adaptor.addChild(root_0, prefixedName633.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end iriRef

    public static class prefixedName_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prefixedName
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:533:1: prefixedName : ( PNAME_LN | PNAME_NS );
    public final prefixedName_return prefixedName() throws RecognitionException {
        prefixedName_return retval = new prefixedName_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set634=null;

        Object set634_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:534:5: ( PNAME_LN | PNAME_NS )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set634=(Token)input.LT(1);
            if ( input.LA(1)==PNAME_NS||input.LA(1)==PNAME_LN ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set634));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_prefixedName0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end prefixedName

    public static class blankNode_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start blankNode
    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:538:1: blankNode : ( BLANK_NODE_LABEL | ANON );
    public final blankNode_return blankNode() throws RecognitionException {
        blankNode_return retval = new blankNode_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set635=null;

        Object set635_tree=null;

        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:539:5: ( BLANK_NODE_LABEL | ANON )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            root_0 = (Object)adaptor.nil();

            set635=(Token)input.LT(1);
            if ( (input.LA(1)>=BLANK_NODE_LABEL && input.LA(1)<=ANON) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set635));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_blankNode0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (Object)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
             finally {
        }
        return retval;
    }
    // $ANTLR end blankNode


 

    public static final BitSet FOLLOW_prologue_in_query38 = new BitSet(new long[]{0x0006204000000000L});
    public static final BitSet FOLLOW_selectQuery_in_query42 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_constructQuery_in_query46 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_describeQuery_in_query50 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_askQuery_in_query54 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_valuesClause_in_query58 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_baseDecl_in_prologue75 = new BitSet(new long[]{0x0000003000000002L});
    public static final BitSet FOLLOW_prefixDecl_in_prologue79 = new BitSet(new long[]{0x0000003000000002L});
    public static final BitSet FOLLOW_36_in_baseDecl97 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_IRI_REF_in_baseDecl99 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_prefixDecl114 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_PNAME_NS_in_prefixDecl116 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_IRI_REF_in_prefixDecl118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_selectClause_in_selectQuery136 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_datasetClause_in_selectQuery138 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_whereClause_in_selectQuery141 = new BitSet(new long[]{0x19A0000000000002L});
    public static final BitSet FOLLOW_solutionModifier_in_selectQuery143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_selectClause_in_subSelect161 = new BitSet(new long[]{0x0000C00000000000L});
    public static final BitSet FOLLOW_whereClause_in_subSelect163 = new BitSet(new long[]{0x39A0000000000002L});
    public static final BitSet FOLLOW_solutionModifier_in_subSelect165 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_valuesClause_in_subSelect167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_selectClause182 = new BitSet(new long[]{0x0000138000000300L});
    public static final BitSet FOLLOW_set_in_selectClause184 = new BitSet(new long[]{0x0000120000000300L});
    public static final BitSet FOLLOW_var_in_selectClause199 = new BitSet(new long[]{0x0000020000000302L});
    public static final BitSet FOLLOW_41_in_selectClause205 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_selectClause207 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_selectClause209 = new BitSet(new long[]{0x0000000000000300L});
    public static final BitSet FOLLOW_var_in_selectClause211 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_selectClause213 = new BitSet(new long[]{0x0000020000000302L});
    public static final BitSet FOLLOW_44_in_selectClause222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_constructQuery239 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_constructTemplate_in_constructQuery243 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_datasetClause_in_constructQuery245 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_whereClause_in_constructQuery248 = new BitSet(new long[]{0x19A0000000000002L});
    public static final BitSet FOLLOW_solutionModifier_in_constructQuery250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_datasetClause_in_constructQuery254 = new BitSet(new long[]{0x0008400000000000L});
    public static final BitSet FOLLOW_46_in_constructQuery257 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_constructQuery259 = new BitSet(new long[]{0x0001020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_triplesTemplate_in_constructQuery261 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_constructQuery264 = new BitSet(new long[]{0x19A0000000000002L});
    public static final BitSet FOLLOW_solutionModifier_in_constructQuery266 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_describeQuery283 = new BitSet(new long[]{0x0000100000200330L});
    public static final BitSet FOLLOW_varOrIRIref_in_describeQuery287 = new BitSet(new long[]{0x19A8C00000200332L});
    public static final BitSet FOLLOW_44_in_describeQuery292 = new BitSet(new long[]{0x19A8C00000000002L});
    public static final BitSet FOLLOW_datasetClause_in_describeQuery296 = new BitSet(new long[]{0x19A8C00000000002L});
    public static final BitSet FOLLOW_whereClause_in_describeQuery299 = new BitSet(new long[]{0x19A0000000000002L});
    public static final BitSet FOLLOW_solutionModifier_in_describeQuery302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_askQuery317 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_datasetClause_in_askQuery319 = new BitSet(new long[]{0x0008C00000000000L});
    public static final BitSet FOLLOW_whereClause_in_askQuery322 = new BitSet(new long[]{0x19A0000000000002L});
    public static final BitSet FOLLOW_solutionModifier_in_askQuery324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_datasetClause339 = new BitSet(new long[]{0x0010000000200030L});
    public static final BitSet FOLLOW_defaultGraphClause_in_datasetClause343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_namedGraphClause_in_datasetClause347 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_sourceSelector_in_defaultGraphClause364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_namedGraphClause382 = new BitSet(new long[]{0x0000000000200030L});
    public static final BitSet FOLLOW_sourceSelector_in_namedGraphClause384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_sourceSelector402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_whereClause420 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_whereClause423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_groupClause_in_solutionModifier438 = new BitSet(new long[]{0x1980000000000002L});
    public static final BitSet FOLLOW_havingClause_in_solutionModifier441 = new BitSet(new long[]{0x1900000000000002L});
    public static final BitSet FOLLOW_orderClause_in_solutionModifier444 = new BitSet(new long[]{0x1800000000000002L});
    public static final BitSet FOLLOW_limitOffsetClauses_in_solutionModifier447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_groupClause463 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_54_in_groupClause465 = new BitSet(new long[]{0x0000020000200330L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_groupCondition_in_groupClause467 = new BitSet(new long[]{0x0000020000200332L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_builtInCall_in_groupCondition483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_groupCondition487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_groupCondition491 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_groupCondition493 = new BitSet(new long[]{0x00000C0000000000L});
    public static final BitSet FOLLOW_42_in_groupCondition497 = new BitSet(new long[]{0x0000000000000300L});
    public static final BitSet FOLLOW_var_in_groupCondition499 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_groupCondition504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_groupCondition508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_havingClause523 = new BitSet(new long[]{0x0000020000200030L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_havingCondition_in_havingClause525 = new BitSet(new long[]{0x0000020000200032L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_constraint_in_havingCondition541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_orderClause556 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_54_in_orderClause558 = new BitSet(new long[]{0x0600020000200330L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_orderCondition_in_orderClause560 = new BitSet(new long[]{0x0600020000200332L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_set_in_orderCondition578 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_brackettedExpression_in_orderCondition588 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_constraint_in_orderCondition596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_orderCondition600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_limitClause_in_limitOffsetClauses614 = new BitSet(new long[]{0x1000000000000002L});
    public static final BitSet FOLLOW_offsetClause_in_limitOffsetClauses616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_offsetClause_in_limitOffsetClauses621 = new BitSet(new long[]{0x0800000000000002L});
    public static final BitSet FOLLOW_limitClause_in_limitOffsetClauses623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_limitClause639 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_limitClause641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_offsetClause656 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_offsetClause658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_valuesClause675 = new BitSet(new long[]{0x0000020000000380L});
    public static final BitSet FOLLOW_dataBlock_in_valuesClause677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesSameSubject_in_triplesTemplate695 = new BitSet(new long[]{0x4000000000000002L});
    public static final BitSet FOLLOW_62_in_triplesTemplate699 = new BitSet(new long[]{0x0000020000FFFBF2L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_triplesTemplate_in_triplesTemplate701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_groupGraphPattern720 = new BitSet(new long[]{0xA001824000FFFBF0L,0x00000000000200ABL,0x0000000018000000L});
    public static final BitSet FOLLOW_subSelect_in_groupGraphPattern724 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_groupGraphPatternSub_in_groupGraphPattern728 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_groupGraphPattern732 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesBlock_in_groupGraphPatternSub744 = new BitSet(new long[]{0xA000800000000002L,0x00000000000000ABL});
    public static final BitSet FOLLOW_graphPatternNotTriples_in_groupGraphPatternSub749 = new BitSet(new long[]{0xE000820000FFFBF2L,0x00000000000200ABL,0x0000000018000000L});
    public static final BitSet FOLLOW_62_in_groupGraphPatternSub751 = new BitSet(new long[]{0xA000820000FFFBF2L,0x00000000000200ABL,0x0000000018000000L});
    public static final BitSet FOLLOW_triplesBlock_in_groupGraphPatternSub754 = new BitSet(new long[]{0xA000800000000002L,0x00000000000000ABL});
    public static final BitSet FOLLOW_triplesSameSubjectPath_in_triplesBlock773 = new BitSet(new long[]{0x4000000000000002L});
    public static final BitSet FOLLOW_62_in_triplesBlock777 = new BitSet(new long[]{0x0000020000FFFBF2L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_triplesBlock_in_triplesBlock779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_groupOrUnionGraphPattern_in_graphPatternNotTriples795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_optionalGraphPattern_in_graphPatternNotTriples799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_minusGraphPattern_in_graphPatternNotTriples803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_graphGraphPattern_in_graphPatternNotTriples807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_serviceGraphPattern_in_graphPatternNotTriples811 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_filter_in_graphPatternNotTriples815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_bind_in_graphPatternNotTriples819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_inlineData_in_graphPatternNotTriples823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_63_in_optionalGraphPattern838 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_optionalGraphPattern840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_graphGraphPattern855 = new BitSet(new long[]{0x0000000000200330L});
    public static final BitSet FOLLOW_varOrIRIref_in_graphGraphPattern857 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_graphGraphPattern859 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_serviceGraphPattern871 = new BitSet(new long[]{0x0000000000200330L,0x0000000000000004L});
    public static final BitSet FOLLOW_66_in_serviceGraphPattern873 = new BitSet(new long[]{0x0000000000200330L});
    public static final BitSet FOLLOW_varOrIRIref_in_serviceGraphPattern876 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_serviceGraphPattern878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_bind893 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_bind895 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_bind897 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_bind899 = new BitSet(new long[]{0x0000000000000300L});
    public static final BitSet FOLLOW_var_in_bind901 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_bind903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_inlineData915 = new BitSet(new long[]{0x0000020000000380L});
    public static final BitSet FOLLOW_dataBlock_in_inlineData917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_inlineDataOneVar_in_dataBlock932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_inlineDataFull_in_dataBlock936 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_inlineDataOneVar951 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_inlineDataOneVar953 = new BitSet(new long[]{0x00010000003FF870L,0x0000000000000010L,0x0000000018000000L});
    public static final BitSet FOLLOW_dataBlockValue_in_inlineDataOneVar955 = new BitSet(new long[]{0x00010000003FF870L,0x0000000000000010L,0x0000000018000000L});
    public static final BitSet FOLLOW_48_in_inlineDataOneVar958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NIL_in_inlineDataFull972 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_41_in_inlineDataFull976 = new BitSet(new long[]{0x0000080000000300L});
    public static final BitSet FOLLOW_var_in_inlineDataFull978 = new BitSet(new long[]{0x0000080000000300L});
    public static final BitSet FOLLOW_43_in_inlineDataFull981 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_inlineDataFull985 = new BitSet(new long[]{0x0001020000000080L});
    public static final BitSet FOLLOW_41_in_inlineDataFull989 = new BitSet(new long[]{0x00000800003FF870L,0x0000000000000010L,0x0000000018000000L});
    public static final BitSet FOLLOW_dataBlockValue_in_inlineDataFull991 = new BitSet(new long[]{0x00000800003FF870L,0x0000000000000010L,0x0000000018000000L});
    public static final BitSet FOLLOW_43_in_inlineDataFull994 = new BitSet(new long[]{0x0001020000000080L});
    public static final BitSet FOLLOW_NIL_in_inlineDataFull998 = new BitSet(new long[]{0x0001020000000080L});
    public static final BitSet FOLLOW_48_in_inlineDataFull1003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_dataBlockValue1015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rdfLiteral_in_dataBlockValue1019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteral_in_dataBlockValue1023 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_booleanLiteral_in_dataBlockValue1027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_dataBlockValue1031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_69_in_minusGraphPattern1046 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_minusGraphPattern1048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern1063 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000040L});
    public static final BitSet FOLLOW_70_in_groupOrUnionGraphPattern1067 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern1069 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000040L});
    public static final BitSet FOLLOW_71_in_filter1087 = new BitSet(new long[]{0x0000020000200030L,0xFFFFFFFFF0000000L,0x0000000001FFFFFFL});
    public static final BitSet FOLLOW_constraint_in_filter1089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_brackettedExpression_in_constraint1101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_builtInCall_in_constraint1105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_constraint1109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_functionCall1124 = new BitSet(new long[]{0x0000020000000080L});
    public static final BitSet FOLLOW_argList_in_functionCall1126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NIL_in_argList1141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_argList1145 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_argList1147 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_argList1150 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_argList1154 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_argList1156 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_43_in_argList1161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NIL_in_expressionList1176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_expressionList1180 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_expressionList1182 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_expressionList1186 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_expressionList1188 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_43_in_expressionList1193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_constructTemplate1208 = new BitSet(new long[]{0x0001020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_constructTriples_in_constructTemplate1210 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_constructTemplate1213 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesSameSubject_in_constructTriples1228 = new BitSet(new long[]{0x4000000000000002L});
    public static final BitSet FOLLOW_62_in_constructTriples1232 = new BitSet(new long[]{0x0000020000FFFBF2L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_constructTriples_in_constructTriples1234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrTerm_in_triplesSameSubject1253 = new BitSet(new long[]{0x0000000000200330L,0x0000000000000400L});
    public static final BitSet FOLLOW_propertyListNotEmpty_in_triplesSameSubject1255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesNode_in_triplesSameSubject1259 = new BitSet(new long[]{0x0000000000200332L,0x0000000000000400L});
    public static final BitSet FOLLOW_propertyList_in_triplesSameSubject1261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_propertyListNotEmpty_in_propertyList1276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_verb_in_propertyListNotEmpty1289 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_objectList_in_propertyListNotEmpty1291 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});
    public static final BitSet FOLLOW_73_in_propertyListNotEmpty1295 = new BitSet(new long[]{0x0000000000200332L,0x0000000000000600L});
    public static final BitSet FOLLOW_verb_in_propertyListNotEmpty1299 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_objectList_in_propertyListNotEmpty1301 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});
    public static final BitSet FOLLOW_varOrIRIref_in_verb1319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_verb1323 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_object_in_objectList1335 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_objectList1339 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_object_in_objectList1341 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_graphNode_in_object1359 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrTerm_in_triplesSameSubjectPath1371 = new BitSet(new long[]{0x0000020000200330L,0x0000000000012400L});
    public static final BitSet FOLLOW_propertyListPathNotEmpty_in_triplesSameSubjectPath1373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesNodePath_in_triplesSameSubjectPath1377 = new BitSet(new long[]{0x0000020000200332L,0x0000000000012400L});
    public static final BitSet FOLLOW_propertyListPath_in_triplesSameSubjectPath1379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_propertyListPathNotEmpty_in_propertyListPath1391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_verbPath_in_propertyListPathNotEmpty1409 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_verbSimple_in_propertyListPathNotEmpty1413 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_objectListPath_in_propertyListPathNotEmpty1417 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});
    public static final BitSet FOLLOW_73_in_propertyListPathNotEmpty1421 = new BitSet(new long[]{0x0000020000200332L,0x0000000000012600L});
    public static final BitSet FOLLOW_verbPath_in_propertyListPathNotEmpty1427 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_verbSimple_in_propertyListPathNotEmpty1431 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_objectList_in_propertyListPathNotEmpty1435 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000200L});
    public static final BitSet FOLLOW_path_in_verbPath1456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_verbSimple1471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_objectPath_in_objectListPath1486 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_objectListPath1490 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_objectPath_in_objectListPath1492 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_graphNodePath_in_objectPath1510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pathAlternative_in_path1525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pathSequence_in_pathAlternative1540 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});
    public static final BitSet FOLLOW_75_in_pathAlternative1544 = new BitSet(new long[]{0x0000020000200030L,0x0000000000012400L});
    public static final BitSet FOLLOW_pathSequence_in_pathAlternative1546 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});
    public static final BitSet FOLLOW_pathEltOrInverse_in_pathSequence1563 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
    public static final BitSet FOLLOW_76_in_pathSequence1567 = new BitSet(new long[]{0x0000020000200030L,0x0000000000012400L});
    public static final BitSet FOLLOW_pathEltOrInverse_in_pathSequence1569 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
    public static final BitSet FOLLOW_pathPrimary_in_pathElt1587 = new BitSet(new long[]{0x0000100000000002L,0x000000000000C000L});
    public static final BitSet FOLLOW_pathMod_in_pathElt1589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pathElt_in_pathEltOrInverse1605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_pathEltOrInverse1609 = new BitSet(new long[]{0x0000020000200030L,0x0000000000010400L});
    public static final BitSet FOLLOW_pathElt_in_pathEltOrInverse1611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_pathMod0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_pathPrimary1649 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_pathPrimary1653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_80_in_pathPrimary1657 = new BitSet(new long[]{0x0000020000200030L,0x0000000000002400L});
    public static final BitSet FOLLOW_pathNegatedPropertySet_in_pathPrimary1659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_pathPrimary1663 = new BitSet(new long[]{0x0000020000200030L,0x0000000000012400L});
    public static final BitSet FOLLOW_path_in_pathPrimary1665 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_pathPrimary1667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pathOneInPropertySet_in_pathNegatedPropertySet1682 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_pathNegatedPropertySet1686 = new BitSet(new long[]{0x0000080000200030L,0x0000000000002400L});
    public static final BitSet FOLLOW_pathOneInPropertySet_in_pathNegatedPropertySet1690 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_75_in_pathNegatedPropertySet1694 = new BitSet(new long[]{0x0000000000200030L,0x0000000000002400L});
    public static final BitSet FOLLOW_pathOneInPropertySet_in_pathNegatedPropertySet1696 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_43_in_pathNegatedPropertySet1704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_pathOneInPropertySet1716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_pathOneInPropertySet1720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_pathOneInPropertySet1724 = new BitSet(new long[]{0x0000000000200030L,0x0000000000000400L});
    public static final BitSet FOLLOW_iriRef_in_pathOneInPropertySet1728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_pathOneInPropertySet1732 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_collection_in_triplesNode1749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_blankNodePropertyList_in_triplesNode1753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_81_in_blankNodePropertyList1768 = new BitSet(new long[]{0x0000000000200330L,0x0000000000000400L});
    public static final BitSet FOLLOW_propertyListNotEmpty_in_blankNodePropertyList1770 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
    public static final BitSet FOLLOW_82_in_blankNodePropertyList1772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_collectionPath_in_triplesNodePath1787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_blankNodePropertyListPath_in_triplesNodePath1791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_81_in_blankNodePropertyListPath1803 = new BitSet(new long[]{0x0000020000200330L,0x0000000000012400L});
    public static final BitSet FOLLOW_propertyListPathNotEmpty_in_blankNodePropertyListPath1805 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
    public static final BitSet FOLLOW_82_in_blankNodePropertyListPath1807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_collection1822 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_graphNode_in_collection1824 = new BitSet(new long[]{0x00000A0000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_43_in_collection1827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_collectionPath1842 = new BitSet(new long[]{0x0000020000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_graphNodePath_in_collectionPath1844 = new BitSet(new long[]{0x00000A0000FFFBF0L,0x0000000000020000L,0x0000000018000000L});
    public static final BitSet FOLLOW_43_in_collectionPath1847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrTerm_in_graphNode1862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesNode_in_graphNode1866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrTerm_in_graphNodePath1881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesNodePath_in_graphNodePath1885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_varOrTerm1900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_graphTerm_in_varOrTerm1908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_varOrIRIref1926 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_varOrIRIref1930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_var0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_graphTerm1974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rdfLiteral_in_graphTerm1982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteral_in_graphTerm1990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_booleanLiteral_in_graphTerm1998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_blankNode_in_graphTerm2006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NIL_in_graphTerm2014 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_conditionalOrExpression_in_expression2032 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_conditionalAndExpression_in_conditionalOrExpression2050 = new BitSet(new long[]{0x0000000000000002L,0x0000000000080000L});
    public static final BitSet FOLLOW_83_in_conditionalOrExpression2054 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_conditionalAndExpression_in_conditionalOrExpression2056 = new BitSet(new long[]{0x0000000000000002L,0x0000000000080000L});
    public static final BitSet FOLLOW_valueLogical_in_conditionalAndExpression2076 = new BitSet(new long[]{0x0000000000000002L,0x0000000000100000L});
    public static final BitSet FOLLOW_84_in_conditionalAndExpression2080 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_valueLogical_in_conditionalAndExpression2082 = new BitSet(new long[]{0x0000000000000002L,0x0000000000100000L});
    public static final BitSet FOLLOW_relationalExpression_in_valueLogical2102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2119 = new BitSet(new long[]{0x0000000000000002L,0x0000000007E00000L});
    public static final BitSet FOLLOW_85_in_relationalExpression2123 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_86_in_relationalExpression2129 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_87_in_relationalExpression2135 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_88_in_relationalExpression2141 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_89_in_relationalExpression2147 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_90_in_relationalExpression2153 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression2155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_additiveExpression_in_numericExpression2175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression2192 = new BitSet(new long[]{0x000000000007E002L,0x0000000008008000L});
    public static final BitSet FOLLOW_79_in_additiveExpression2196 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression2198 = new BitSet(new long[]{0x000000000007E002L,0x0000000008008000L});
    public static final BitSet FOLLOW_91_in_additiveExpression2202 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression2204 = new BitSet(new long[]{0x000000000007E002L,0x0000000008008000L});
    public static final BitSet FOLLOW_numericLiteralPositive_in_additiveExpression2208 = new BitSet(new long[]{0x000000000007E002L,0x0000000008008000L});
    public static final BitSet FOLLOW_numericLiteralNegative_in_additiveExpression2212 = new BitSet(new long[]{0x000000000007E002L,0x0000000008008000L});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression2232 = new BitSet(new long[]{0x0000100000000002L,0x0000000000001000L});
    public static final BitSet FOLLOW_44_in_multiplicativeExpression2236 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression2238 = new BitSet(new long[]{0x0000100000000002L,0x0000000000001000L});
    public static final BitSet FOLLOW_76_in_multiplicativeExpression2242 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression2244 = new BitSet(new long[]{0x0000100000000002L,0x0000000000001000L});
    public static final BitSet FOLLOW_80_in_unaryExpression2265 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF0000000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression2267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_79_in_unaryExpression2275 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF0000000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression2277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_91_in_unaryExpression2285 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF0000000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression2287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression2295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_brackettedExpression_in_primaryExpression2312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_builtInCall_in_primaryExpression2316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRefOrFunction_in_primaryExpression2320 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rdfLiteral_in_primaryExpression2324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteral_in_primaryExpression2328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_booleanLiteral_in_primaryExpression2332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_primaryExpression2336 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_brackettedExpression2353 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_brackettedExpression2355 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_brackettedExpression2357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_aggregate_in_builtInCall2375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_92_in_builtInCall2381 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2383 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2385 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_93_in_builtInCall2393 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2395 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2397 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_94_in_builtInCall2405 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2407 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2409 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2411 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2413 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_95_in_builtInCall2421 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2423 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2425 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_96_in_builtInCall2433 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2435 = new BitSet(new long[]{0x0000000000000300L});
    public static final BitSet FOLLOW_var_in_builtInCall2437 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_97_in_builtInCall2445 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2447 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2449 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2451 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_98_in_builtInCall2457 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2459 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2461 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_99_in_builtInCall2469 = new BitSet(new long[]{0x0000020000000080L});
    public static final BitSet FOLLOW_41_in_builtInCall2473 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2475 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NIL_in_builtInCall2481 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_100_in_builtInCall2489 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_NIL_in_builtInCall2491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_101_in_builtInCall2497 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2499 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2501 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_102_in_builtInCall2509 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2511 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2513 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_103_in_builtInCall2521 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2523 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2525 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_104_in_builtInCall2533 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2535 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2537 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_105_in_builtInCall2545 = new BitSet(new long[]{0x0000020000000080L});
    public static final BitSet FOLLOW_expressionList_in_builtInCall2547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_subStringExpression_in_builtInCall2553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_106_in_builtInCall2559 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2561 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2563 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_strReplaceExpression_in_builtInCall2571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_107_in_builtInCall2577 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2579 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2581 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_108_in_builtInCall2589 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2591 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2593 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_109_in_builtInCall2601 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2603 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2605 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_110_in_builtInCall2613 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2615 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2617 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2619 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2621 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_111_in_builtInCall2629 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2631 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2633 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2635 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2637 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_112_in_builtInCall2645 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2647 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2649 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2651 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2653 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_113_in_builtInCall2661 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2663 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2665 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2667 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2669 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2671 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_114_in_builtInCall2677 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2679 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2681 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2683 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2685 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_115_in_builtInCall2693 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2695 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2697 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_116_in_builtInCall2705 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2707 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2709 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_117_in_builtInCall2717 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2719 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2721 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_118_in_builtInCall2729 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2731 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2733 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_119_in_builtInCall2741 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2743 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2745 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_120_in_builtInCall2753 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2755 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2757 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_121_in_builtInCall2765 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2767 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2769 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_122_in_builtInCall2777 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2779 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2781 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_123_in_builtInCall2789 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_NIL_in_builtInCall2791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_124_in_builtInCall2797 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_NIL_in_builtInCall2799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_125_in_builtInCall2805 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_NIL_in_builtInCall2807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_126_in_builtInCall2813 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2815 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2817 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_127_in_builtInCall2825 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2827 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2829 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_128_in_builtInCall2837 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2839 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2841 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_129_in_builtInCall2849 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2851 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2853 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_130_in_builtInCall2861 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2863 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2865 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_131_in_builtInCall2873 = new BitSet(new long[]{0x0000020000000080L});
    public static final BitSet FOLLOW_expressionList_in_builtInCall2875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_132_in_builtInCall2881 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2883 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2885 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2887 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2889 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2891 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2893 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_133_in_builtInCall2901 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2903 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2905 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2907 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2909 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2911 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_134_in_builtInCall2917 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2919 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2921 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2923 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2925 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2927 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_135_in_builtInCall2933 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2935 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2937 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_builtInCall2939 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2941 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_136_in_builtInCall2949 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2951 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2953 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2955 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_137_in_builtInCall2961 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2963 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2965 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_138_in_builtInCall2973 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2975 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2977 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_139_in_builtInCall2985 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2987 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall2989 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall2991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_140_in_builtInCall2997 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_builtInCall2999 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_builtInCall3001 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_builtInCall3003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_regexExpression_in_builtInCall3009 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_existsFunc_in_builtInCall3015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_notExistsFunc_in_builtInCall3021 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_141_in_regexExpression3039 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_regexExpression3041 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_regexExpression3043 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_regexExpression3045 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_regexExpression3047 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_regexExpression3051 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_regexExpression3053 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_regexExpression3058 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_142_in_subStringExpression3076 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_subStringExpression3078 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_subStringExpression3080 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_subStringExpression3082 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_subStringExpression3084 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_subStringExpression3088 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_subStringExpression3090 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_subStringExpression3095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_143_in_strReplaceExpression3110 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_strReplaceExpression3112 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_strReplaceExpression3114 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_strReplaceExpression3116 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_strReplaceExpression3118 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_strReplaceExpression3120 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_strReplaceExpression3122 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_strReplaceExpression3126 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_strReplaceExpression3128 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_strReplaceExpression3133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_144_in_existsFunc3148 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_existsFunc3150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_145_in_notExistsFunc3165 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000010000L});
    public static final BitSet FOLLOW_144_in_notExistsFunc3167 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_notExistsFunc3169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_146_in_aggregate3184 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3186 = new BitSet(new long[]{0x00001280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3188 = new BitSet(new long[]{0x00001200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_44_in_aggregate3193 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_expression_in_aggregate3197 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_147_in_aggregate3210 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3212 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3214 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_aggregate3217 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3219 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_148_in_aggregate3228 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3230 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3232 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_aggregate3235 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_149_in_aggregate3246 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3248 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3250 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_aggregate3253 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_150_in_aggregate3264 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3266 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3268 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_aggregate3271 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_151_in_aggregate3282 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3284 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3286 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_aggregate3289 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_152_in_aggregate3300 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_aggregate3302 = new BitSet(new long[]{0x00000280003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_39_in_aggregate3304 = new BitSet(new long[]{0x00000200003FFB70L,0xFFFFFFFFF8018000L,0x0000000019FFFFFFL});
    public static final BitSet FOLLOW_expression_in_aggregate3307 = new BitSet(new long[]{0x0000080000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_73_in_aggregate3311 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_153_in_aggregate3313 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
    public static final BitSet FOLLOW_85_in_aggregate3315 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_string_in_aggregate3317 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_aggregate3322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_iriRefOrFunction3337 = new BitSet(new long[]{0x0000020000000082L});
    public static final BitSet FOLLOW_argList_in_iriRefOrFunction3339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_string_in_rdfLiteral3358 = new BitSet(new long[]{0x0000000000000402L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_LANGTAG_in_rdfLiteral3362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_154_in_rdfLiteral3368 = new BitSet(new long[]{0x0000000000200030L});
    public static final BitSet FOLLOW_iriRef_in_rdfLiteral3370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteralUnsigned_in_numericLiteral3392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteralPositive_in_numericLiteral3396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteralNegative_in_numericLiteral3400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_numericLiteralUnsigned0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_numericLiteralPositive0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_numericLiteralNegative0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_booleanLiteral0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_string0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IRI_REF_in_iriRef3572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_prefixedName_in_iriRef3580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_prefixedName0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_blankNode0 = new BitSet(new long[]{0x0000000000000002L});

}