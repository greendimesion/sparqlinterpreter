// $ANTLR 3.0.1 C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g 2013-10-25 12:51:01
package sparqltree;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class SparqlLexer extends Lexer {
    public static final int T114=114;
    public static final int T115=115;
    public static final int T116=116;
    public static final int T117=117;
    public static final int EXPONENT=30;
    public static final int T118=118;
    public static final int T119=119;
    public static final int PNAME_LN=21;
    public static final int EOF=-1;
    public static final int T120=120;
    public static final int T122=122;
    public static final int T121=121;
    public static final int T124=124;
    public static final int T123=123;
    public static final int VARNAME=27;
    public static final int T127=127;
    public static final int T128=128;
    public static final int T125=125;
    public static final int T126=126;
    public static final int T129=129;
    public static final int DOUBLE=12;
    public static final int PN_CHARS_U=35;
    public static final int T38=38;
    public static final int T37=37;
    public static final int T39=39;
    public static final int T131=131;
    public static final int T130=130;
    public static final int T36=36;
    public static final int T135=135;
    public static final int DOUBLE_POSITIVE=15;
    public static final int T134=134;
    public static final int T133=133;
    public static final int T132=132;
    public static final int BLANK_NODE_LABEL=22;
    public static final int T49=49;
    public static final int T48=48;
    public static final int T100=100;
    public static final int T43=43;
    public static final int T42=42;
    public static final int T102=102;
    public static final int T41=41;
    public static final int T101=101;
    public static final int T40=40;
    public static final int T47=47;
    public static final int T46=46;
    public static final int T45=45;
    public static final int T44=44;
    public static final int T109=109;
    public static final int T107=107;
    public static final int T108=108;
    public static final int T105=105;
    public static final int WS=34;
    public static final int T106=106;
    public static final int T103=103;
    public static final int NIL=7;
    public static final int T104=104;
    public static final int T50=50;
    public static final int INTEGER_POSITIVE=13;
    public static final int STRING_LITERAL2=20;
    public static final int STRING_LITERAL1=19;
    public static final int PN_CHARS=24;
    public static final int T59=59;
    public static final int DOUBLE_NEGATIVE=18;
    public static final int T113=113;
    public static final int T52=52;
    public static final int T112=112;
    public static final int T51=51;
    public static final int T111=111;
    public static final int T54=54;
    public static final int T110=110;
    public static final int T53=53;
    public static final int T56=56;
    public static final int T55=55;
    public static final int T58=58;
    public static final int T57=57;
    public static final int T75=75;
    public static final int T76=76;
    public static final int IRI_REF=4;
    public static final int T73=73;
    public static final int T74=74;
    public static final int T79=79;
    public static final int T77=77;
    public static final int T78=78;
    public static final int T72=72;
    public static final int T71=71;
    public static final int T70=70;
    public static final int T62=62;
    public static final int T63=63;
    public static final int T64=64;
    public static final int T65=65;
    public static final int T66=66;
    public static final int T67=67;
    public static final int T68=68;
    public static final int T69=69;
    public static final int DECIMAL_POSITIVE=14;
    public static final int DIGIT=29;
    public static final int T61=61;
    public static final int T60=60;
    public static final int INTEGER=6;
    public static final int T99=99;
    public static final int T97=97;
    public static final int T98=98;
    public static final int T95=95;
    public static final int T96=96;
    public static final int T137=137;
    public static final int T136=136;
    public static final int T139=139;
    public static final int INTEGER_NEGATIVE=16;
    public static final int T138=138;
    public static final int T143=143;
    public static final int PN_LOCAL=26;
    public static final int T144=144;
    public static final int PNAME_NS=5;
    public static final int T145=145;
    public static final int T146=146;
    public static final int T140=140;
    public static final int T141=141;
    public static final int T142=142;
    public static final int T94=94;
    public static final int Tokens=157;
    public static final int T93=93;
    public static final int T92=92;
    public static final int T91=91;
    public static final int T90=90;
    public static final int ECHAR=31;
    public static final int T88=88;
    public static final int T89=89;
    public static final int ANON=23;
    public static final int T84=84;
    public static final int T85=85;
    public static final int STRING_LITERAL_LONG2=33;
    public static final int PN_CHARS_BASE=28;
    public static final int T86=86;
    public static final int DECIMAL=11;
    public static final int T87=87;
    public static final int VAR1=8;
    public static final int VAR2=9;
    public static final int STRING_LITERAL_LONG1=32;
    public static final int DECIMAL_NEGATIVE=17;
    public static final int T149=149;
    public static final int PN_PREFIX=25;
    public static final int T148=148;
    public static final int T147=147;
    public static final int T156=156;
    public static final int T154=154;
    public static final int T155=155;
    public static final int T152=152;
    public static final int T153=153;
    public static final int T150=150;
    public static final int T151=151;
    public static final int T81=81;
    public static final int T80=80;
    public static final int T83=83;
    public static final int T82=82;
    public static final int LANGTAG=10;
    public SparqlLexer() {;} 
    public SparqlLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g"; }

    // $ANTLR start T36
    public final void mT36() throws RecognitionException {
        try {
            int _type = T36;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:3:5: ( 'BASE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:3:7: 'BASE'
            {
            match("BASE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T36

    // $ANTLR start T37
    public final void mT37() throws RecognitionException {
        try {
            int _type = T37;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:4:5: ( 'PREFIX' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:4:7: 'PREFIX'
            {
            match("PREFIX"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T37

    // $ANTLR start T38
    public final void mT38() throws RecognitionException {
        try {
            int _type = T38;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:5:5: ( 'SELECT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:5:7: 'SELECT'
            {
            match("SELECT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T38

    // $ANTLR start T39
    public final void mT39() throws RecognitionException {
        try {
            int _type = T39;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:6:5: ( 'DISTINCT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:6:7: 'DISTINCT'
            {
            match("DISTINCT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T39

    // $ANTLR start T40
    public final void mT40() throws RecognitionException {
        try {
            int _type = T40;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:7:5: ( 'REDUCED' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:7:7: 'REDUCED'
            {
            match("REDUCED"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T40

    // $ANTLR start T41
    public final void mT41() throws RecognitionException {
        try {
            int _type = T41;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:8:5: ( '(' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:8:7: '('
            {
            match('('); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T41

    // $ANTLR start T42
    public final void mT42() throws RecognitionException {
        try {
            int _type = T42;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:9:5: ( 'AS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:9:7: 'AS'
            {
            match("AS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T42

    // $ANTLR start T43
    public final void mT43() throws RecognitionException {
        try {
            int _type = T43;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:10:5: ( ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:10:7: ')'
            {
            match(')'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T43

    // $ANTLR start T44
    public final void mT44() throws RecognitionException {
        try {
            int _type = T44;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:11:5: ( '*' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:11:7: '*'
            {
            match('*'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T44

    // $ANTLR start T45
    public final void mT45() throws RecognitionException {
        try {
            int _type = T45;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:12:5: ( 'CONSTRUCT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:12:7: 'CONSTRUCT'
            {
            match("CONSTRUCT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T45

    // $ANTLR start T46
    public final void mT46() throws RecognitionException {
        try {
            int _type = T46;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:13:5: ( 'WHERE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:13:7: 'WHERE'
            {
            match("WHERE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T46

    // $ANTLR start T47
    public final void mT47() throws RecognitionException {
        try {
            int _type = T47;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:5: ( '{' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:14:7: '{'
            {
            match('{'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T47

    // $ANTLR start T48
    public final void mT48() throws RecognitionException {
        try {
            int _type = T48;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:15:5: ( '}' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:15:7: '}'
            {
            match('}'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T48

    // $ANTLR start T49
    public final void mT49() throws RecognitionException {
        try {
            int _type = T49;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:16:5: ( 'DESCRIBE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:16:7: 'DESCRIBE'
            {
            match("DESCRIBE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T49

    // $ANTLR start T50
    public final void mT50() throws RecognitionException {
        try {
            int _type = T50;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:17:5: ( 'ASK' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:17:7: 'ASK'
            {
            match("ASK"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T50

    // $ANTLR start T51
    public final void mT51() throws RecognitionException {
        try {
            int _type = T51;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:5: ( 'FROM' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:18:7: 'FROM'
            {
            match("FROM"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T51

    // $ANTLR start T52
    public final void mT52() throws RecognitionException {
        try {
            int _type = T52;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:19:5: ( 'NAMED' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:19:7: 'NAMED'
            {
            match("NAMED"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T52

    // $ANTLR start T53
    public final void mT53() throws RecognitionException {
        try {
            int _type = T53;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:20:5: ( 'GROUP' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:20:7: 'GROUP'
            {
            match("GROUP"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T53

    // $ANTLR start T54
    public final void mT54() throws RecognitionException {
        try {
            int _type = T54;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:21:5: ( 'BY' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:21:7: 'BY'
            {
            match("BY"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T54

    // $ANTLR start T55
    public final void mT55() throws RecognitionException {
        try {
            int _type = T55;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:22:5: ( 'HAVING' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:22:7: 'HAVING'
            {
            match("HAVING"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T55

    // $ANTLR start T56
    public final void mT56() throws RecognitionException {
        try {
            int _type = T56;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:23:5: ( 'ORDER' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:23:7: 'ORDER'
            {
            match("ORDER"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T56

    // $ANTLR start T57
    public final void mT57() throws RecognitionException {
        try {
            int _type = T57;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:24:5: ( 'ASC' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:24:7: 'ASC'
            {
            match("ASC"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T57

    // $ANTLR start T58
    public final void mT58() throws RecognitionException {
        try {
            int _type = T58;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:25:5: ( 'DESC' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:25:7: 'DESC'
            {
            match("DESC"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T58

    // $ANTLR start T59
    public final void mT59() throws RecognitionException {
        try {
            int _type = T59;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:26:5: ( 'LIMIT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:26:7: 'LIMIT'
            {
            match("LIMIT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T59

    // $ANTLR start T60
    public final void mT60() throws RecognitionException {
        try {
            int _type = T60;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:27:5: ( 'OFFSET' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:27:7: 'OFFSET'
            {
            match("OFFSET"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T60

    // $ANTLR start T61
    public final void mT61() throws RecognitionException {
        try {
            int _type = T61;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:28:5: ( 'VALUES' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:28:7: 'VALUES'
            {
            match("VALUES"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T61

    // $ANTLR start T62
    public final void mT62() throws RecognitionException {
        try {
            int _type = T62;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:29:5: ( '.' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:29:7: '.'
            {
            match('.'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T62

    // $ANTLR start T63
    public final void mT63() throws RecognitionException {
        try {
            int _type = T63;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:30:5: ( 'OPTIONAL' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:30:7: 'OPTIONAL'
            {
            match("OPTIONAL"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T63

    // $ANTLR start T64
    public final void mT64() throws RecognitionException {
        try {
            int _type = T64;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:31:5: ( 'GRAPH' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:31:7: 'GRAPH'
            {
            match("GRAPH"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T64

    // $ANTLR start T65
    public final void mT65() throws RecognitionException {
        try {
            int _type = T65;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:32:5: ( 'SERVICE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:32:7: 'SERVICE'
            {
            match("SERVICE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T65

    // $ANTLR start T66
    public final void mT66() throws RecognitionException {
        try {
            int _type = T66;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:33:5: ( 'SILENT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:33:7: 'SILENT'
            {
            match("SILENT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T66

    // $ANTLR start T67
    public final void mT67() throws RecognitionException {
        try {
            int _type = T67;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:34:5: ( 'BIND' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:34:7: 'BIND'
            {
            match("BIND"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T67

    // $ANTLR start T68
    public final void mT68() throws RecognitionException {
        try {
            int _type = T68;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:35:5: ( 'UNDEF' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:35:7: 'UNDEF'
            {
            match("UNDEF"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T68

    // $ANTLR start T69
    public final void mT69() throws RecognitionException {
        try {
            int _type = T69;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:36:5: ( 'MINUS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:36:7: 'MINUS'
            {
            match("MINUS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T69

    // $ANTLR start T70
    public final void mT70() throws RecognitionException {
        try {
            int _type = T70;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:37:5: ( 'UNION' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:37:7: 'UNION'
            {
            match("UNION"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T70

    // $ANTLR start T71
    public final void mT71() throws RecognitionException {
        try {
            int _type = T71;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:5: ( 'FILTER' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:38:7: 'FILTER'
            {
            match("FILTER"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T71

    // $ANTLR start T72
    public final void mT72() throws RecognitionException {
        try {
            int _type = T72;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:39:5: ( ',' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:39:7: ','
            {
            match(','); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T72

    // $ANTLR start T73
    public final void mT73() throws RecognitionException {
        try {
            int _type = T73;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:40:5: ( ';' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:40:7: ';'
            {
            match(';'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T73

    // $ANTLR start T74
    public final void mT74() throws RecognitionException {
        try {
            int _type = T74;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:41:5: ( 'a' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:41:7: 'a'
            {
            match('a'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T74

    // $ANTLR start T75
    public final void mT75() throws RecognitionException {
        try {
            int _type = T75;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:5: ( '|' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:42:7: '|'
            {
            match('|'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T75

    // $ANTLR start T76
    public final void mT76() throws RecognitionException {
        try {
            int _type = T76;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:43:5: ( '/' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:43:7: '/'
            {
            match('/'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T76

    // $ANTLR start T77
    public final void mT77() throws RecognitionException {
        try {
            int _type = T77;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:44:5: ( '^' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:44:7: '^'
            {
            match('^'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T77

    // $ANTLR start T78
    public final void mT78() throws RecognitionException {
        try {
            int _type = T78;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:45:5: ( '?' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:45:7: '?'
            {
            match('?'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T78

    // $ANTLR start T79
    public final void mT79() throws RecognitionException {
        try {
            int _type = T79;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:5: ( '+' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:46:7: '+'
            {
            match('+'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T79

    // $ANTLR start T80
    public final void mT80() throws RecognitionException {
        try {
            int _type = T80;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:47:5: ( '!' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:47:7: '!'
            {
            match('!'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T80

    // $ANTLR start T81
    public final void mT81() throws RecognitionException {
        try {
            int _type = T81;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:48:5: ( '[' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:48:7: '['
            {
            match('['); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T81

    // $ANTLR start T82
    public final void mT82() throws RecognitionException {
        try {
            int _type = T82;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:49:5: ( ']' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:49:7: ']'
            {
            match(']'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T82

    // $ANTLR start T83
    public final void mT83() throws RecognitionException {
        try {
            int _type = T83;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:50:5: ( '||' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:50:7: '||'
            {
            match("||"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T83

    // $ANTLR start T84
    public final void mT84() throws RecognitionException {
        try {
            int _type = T84;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:51:5: ( '&&' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:51:7: '&&'
            {
            match("&&"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T84

    // $ANTLR start T85
    public final void mT85() throws RecognitionException {
        try {
            int _type = T85;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:52:5: ( '=' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:52:7: '='
            {
            match('='); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T85

    // $ANTLR start T86
    public final void mT86() throws RecognitionException {
        try {
            int _type = T86;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:53:5: ( '!=' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:53:7: '!='
            {
            match("!="); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T86

    // $ANTLR start T87
    public final void mT87() throws RecognitionException {
        try {
            int _type = T87;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:5: ( '<' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:54:7: '<'
            {
            match('<'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T87

    // $ANTLR start T88
    public final void mT88() throws RecognitionException {
        try {
            int _type = T88;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:55:5: ( '>' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:55:7: '>'
            {
            match('>'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T88

    // $ANTLR start T89
    public final void mT89() throws RecognitionException {
        try {
            int _type = T89;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:56:5: ( '<=' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:56:7: '<='
            {
            match("<="); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T89

    // $ANTLR start T90
    public final void mT90() throws RecognitionException {
        try {
            int _type = T90;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:57:5: ( '>=' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:57:7: '>='
            {
            match(">="); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T90

    // $ANTLR start T91
    public final void mT91() throws RecognitionException {
        try {
            int _type = T91;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:58:5: ( '-' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:58:7: '-'
            {
            match('-'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T91

    // $ANTLR start T92
    public final void mT92() throws RecognitionException {
        try {
            int _type = T92;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:59:5: ( 'STR' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:59:7: 'STR'
            {
            match("STR"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T92

    // $ANTLR start T93
    public final void mT93() throws RecognitionException {
        try {
            int _type = T93;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:60:5: ( 'LANG' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:60:7: 'LANG'
            {
            match("LANG"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T93

    // $ANTLR start T94
    public final void mT94() throws RecognitionException {
        try {
            int _type = T94;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:61:5: ( 'LANGMATCHES' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:61:7: 'LANGMATCHES'
            {
            match("LANGMATCHES"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T94

    // $ANTLR start T95
    public final void mT95() throws RecognitionException {
        try {
            int _type = T95;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:62:5: ( 'DATATYPE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:62:7: 'DATATYPE'
            {
            match("DATATYPE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T95

    // $ANTLR start T96
    public final void mT96() throws RecognitionException {
        try {
            int _type = T96;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:63:5: ( 'BOUND' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:63:7: 'BOUND'
            {
            match("BOUND"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T96

    // $ANTLR start T97
    public final void mT97() throws RecognitionException {
        try {
            int _type = T97;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:64:5: ( 'IRI' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:64:7: 'IRI'
            {
            match("IRI"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T97

    // $ANTLR start T98
    public final void mT98() throws RecognitionException {
        try {
            int _type = T98;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:65:5: ( 'URI' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:65:7: 'URI'
            {
            match("URI"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T98

    // $ANTLR start T99
    public final void mT99() throws RecognitionException {
        try {
            int _type = T99;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:66:5: ( 'BNODE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:66:7: 'BNODE'
            {
            match("BNODE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T99

    // $ANTLR start T100
    public final void mT100() throws RecognitionException {
        try {
            int _type = T100;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:67:6: ( 'RAND' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:67:8: 'RAND'
            {
            match("RAND"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T100

    // $ANTLR start T101
    public final void mT101() throws RecognitionException {
        try {
            int _type = T101;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:68:6: ( 'ABS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:68:8: 'ABS'
            {
            match("ABS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T101

    // $ANTLR start T102
    public final void mT102() throws RecognitionException {
        try {
            int _type = T102;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:69:6: ( 'CEIL' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:69:8: 'CEIL'
            {
            match("CEIL"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T102

    // $ANTLR start T103
    public final void mT103() throws RecognitionException {
        try {
            int _type = T103;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:70:6: ( 'FLOOR' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:70:8: 'FLOOR'
            {
            match("FLOOR"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T103

    // $ANTLR start T104
    public final void mT104() throws RecognitionException {
        try {
            int _type = T104;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:71:6: ( 'ROUND' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:71:8: 'ROUND'
            {
            match("ROUND"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T104

    // $ANTLR start T105
    public final void mT105() throws RecognitionException {
        try {
            int _type = T105;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:72:6: ( 'CONCAT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:72:8: 'CONCAT'
            {
            match("CONCAT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T105

    // $ANTLR start T106
    public final void mT106() throws RecognitionException {
        try {
            int _type = T106;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:73:6: ( 'STRLEN' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:73:8: 'STRLEN'
            {
            match("STRLEN"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T106

    // $ANTLR start T107
    public final void mT107() throws RecognitionException {
        try {
            int _type = T107;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:6: ( 'UCASE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:74:8: 'UCASE'
            {
            match("UCASE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T107

    // $ANTLR start T108
    public final void mT108() throws RecognitionException {
        try {
            int _type = T108;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:75:6: ( 'LCASE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:75:8: 'LCASE'
            {
            match("LCASE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T108

    // $ANTLR start T109
    public final void mT109() throws RecognitionException {
        try {
            int _type = T109;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:76:6: ( 'ENCODE_FOR_URI' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:76:8: 'ENCODE_FOR_URI'
            {
            match("ENCODE_FOR_URI"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T109

    // $ANTLR start T110
    public final void mT110() throws RecognitionException {
        try {
            int _type = T110;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:77:6: ( 'CONTAINS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:77:8: 'CONTAINS'
            {
            match("CONTAINS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T110

    // $ANTLR start T111
    public final void mT111() throws RecognitionException {
        try {
            int _type = T111;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:78:6: ( 'STRSTARTS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:78:8: 'STRSTARTS'
            {
            match("STRSTARTS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T111

    // $ANTLR start T112
    public final void mT112() throws RecognitionException {
        try {
            int _type = T112;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:79:6: ( 'STRENDS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:79:8: 'STRENDS'
            {
            match("STRENDS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T112

    // $ANTLR start T113
    public final void mT113() throws RecognitionException {
        try {
            int _type = T113;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:80:6: ( 'STRBEFORE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:80:8: 'STRBEFORE'
            {
            match("STRBEFORE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T113

    // $ANTLR start T114
    public final void mT114() throws RecognitionException {
        try {
            int _type = T114;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:81:6: ( 'STRAFTER' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:81:8: 'STRAFTER'
            {
            match("STRAFTER"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T114

    // $ANTLR start T115
    public final void mT115() throws RecognitionException {
        try {
            int _type = T115;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:6: ( 'YEAR' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:82:8: 'YEAR'
            {
            match("YEAR"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T115

    // $ANTLR start T116
    public final void mT116() throws RecognitionException {
        try {
            int _type = T116;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:83:6: ( 'MONTH' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:83:8: 'MONTH'
            {
            match("MONTH"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T116

    // $ANTLR start T117
    public final void mT117() throws RecognitionException {
        try {
            int _type = T117;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:84:6: ( 'DAY' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:84:8: 'DAY'
            {
            match("DAY"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T117

    // $ANTLR start T118
    public final void mT118() throws RecognitionException {
        try {
            int _type = T118;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:85:6: ( 'HOURS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:85:8: 'HOURS'
            {
            match("HOURS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T118

    // $ANTLR start T119
    public final void mT119() throws RecognitionException {
        try {
            int _type = T119;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:86:6: ( 'MINUTES' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:86:8: 'MINUTES'
            {
            match("MINUTES"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T119

    // $ANTLR start T120
    public final void mT120() throws RecognitionException {
        try {
            int _type = T120;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:87:6: ( 'SECONDS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:87:8: 'SECONDS'
            {
            match("SECONDS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T120

    // $ANTLR start T121
    public final void mT121() throws RecognitionException {
        try {
            int _type = T121;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:88:6: ( 'TIMEZONE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:88:8: 'TIMEZONE'
            {
            match("TIMEZONE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T121

    // $ANTLR start T122
    public final void mT122() throws RecognitionException {
        try {
            int _type = T122;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:89:6: ( 'TZ' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:89:8: 'TZ'
            {
            match("TZ"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T122

    // $ANTLR start T123
    public final void mT123() throws RecognitionException {
        try {
            int _type = T123;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:90:6: ( 'NOW' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:90:8: 'NOW'
            {
            match("NOW"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T123

    // $ANTLR start T124
    public final void mT124() throws RecognitionException {
        try {
            int _type = T124;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:91:6: ( 'UUID' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:91:8: 'UUID'
            {
            match("UUID"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T124

    // $ANTLR start T125
    public final void mT125() throws RecognitionException {
        try {
            int _type = T125;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:92:6: ( 'STRUUID' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:92:8: 'STRUUID'
            {
            match("STRUUID"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T125

    // $ANTLR start T126
    public final void mT126() throws RecognitionException {
        try {
            int _type = T126;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:93:6: ( 'MD5' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:93:8: 'MD5'
            {
            match("MD5"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T126

    // $ANTLR start T127
    public final void mT127() throws RecognitionException {
        try {
            int _type = T127;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:94:6: ( 'SHA1' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:94:8: 'SHA1'
            {
            match("SHA1"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T127

    // $ANTLR start T128
    public final void mT128() throws RecognitionException {
        try {
            int _type = T128;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:95:6: ( 'SHA256' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:95:8: 'SHA256'
            {
            match("SHA256"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T128

    // $ANTLR start T129
    public final void mT129() throws RecognitionException {
        try {
            int _type = T129;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:96:6: ( 'SHA384' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:96:8: 'SHA384'
            {
            match("SHA384"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T129

    // $ANTLR start T130
    public final void mT130() throws RecognitionException {
        try {
            int _type = T130;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:97:6: ( 'SHA512' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:97:8: 'SHA512'
            {
            match("SHA512"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T130

    // $ANTLR start T131
    public final void mT131() throws RecognitionException {
        try {
            int _type = T131;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:6: ( 'COALESCE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:98:8: 'COALESCE'
            {
            match("COALESCE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T131

    // $ANTLR start T132
    public final void mT132() throws RecognitionException {
        try {
            int _type = T132;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:99:6: ( 'IF' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:99:8: 'IF'
            {
            match("IF"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T132

    // $ANTLR start T133
    public final void mT133() throws RecognitionException {
        try {
            int _type = T133;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:100:6: ( 'STRLANG' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:100:8: 'STRLANG'
            {
            match("STRLANG"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T133

    // $ANTLR start T134
    public final void mT134() throws RecognitionException {
        try {
            int _type = T134;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:101:6: ( 'STRDT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:101:8: 'STRDT'
            {
            match("STRDT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T134

    // $ANTLR start T135
    public final void mT135() throws RecognitionException {
        try {
            int _type = T135;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:6: ( 'sameTerm' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:102:8: 'sameTerm'
            {
            match("sameTerm"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T135

    // $ANTLR start T136
    public final void mT136() throws RecognitionException {
        try {
            int _type = T136;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:103:6: ( 'isIRI' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:103:8: 'isIRI'
            {
            match("isIRI"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T136

    // $ANTLR start T137
    public final void mT137() throws RecognitionException {
        try {
            int _type = T137;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:104:6: ( 'isURI' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:104:8: 'isURI'
            {
            match("isURI"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T137

    // $ANTLR start T138
    public final void mT138() throws RecognitionException {
        try {
            int _type = T138;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:105:6: ( 'isBLANK' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:105:8: 'isBLANK'
            {
            match("isBLANK"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T138

    // $ANTLR start T139
    public final void mT139() throws RecognitionException {
        try {
            int _type = T139;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:106:6: ( 'isLITERAL' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:106:8: 'isLITERAL'
            {
            match("isLITERAL"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T139

    // $ANTLR start T140
    public final void mT140() throws RecognitionException {
        try {
            int _type = T140;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:107:6: ( 'isNUMERIC' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:107:8: 'isNUMERIC'
            {
            match("isNUMERIC"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T140

    // $ANTLR start T141
    public final void mT141() throws RecognitionException {
        try {
            int _type = T141;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:108:6: ( 'REGEX' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:108:8: 'REGEX'
            {
            match("REGEX"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T141

    // $ANTLR start T142
    public final void mT142() throws RecognitionException {
        try {
            int _type = T142;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:109:6: ( 'SUBSTR' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:109:8: 'SUBSTR'
            {
            match("SUBSTR"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T142

    // $ANTLR start T143
    public final void mT143() throws RecognitionException {
        try {
            int _type = T143;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:110:6: ( 'REPLACE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:110:8: 'REPLACE'
            {
            match("REPLACE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T143

    // $ANTLR start T144
    public final void mT144() throws RecognitionException {
        try {
            int _type = T144;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:111:6: ( 'EXISTS' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:111:8: 'EXISTS'
            {
            match("EXISTS"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T144

    // $ANTLR start T145
    public final void mT145() throws RecognitionException {
        try {
            int _type = T145;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:112:6: ( 'NOT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:112:8: 'NOT'
            {
            match("NOT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T145

    // $ANTLR start T146
    public final void mT146() throws RecognitionException {
        try {
            int _type = T146;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:113:6: ( 'COUNT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:113:8: 'COUNT'
            {
            match("COUNT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T146

    // $ANTLR start T147
    public final void mT147() throws RecognitionException {
        try {
            int _type = T147;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:114:6: ( 'SUM' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:114:8: 'SUM'
            {
            match("SUM"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T147

    // $ANTLR start T148
    public final void mT148() throws RecognitionException {
        try {
            int _type = T148;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:115:6: ( 'MIN' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:115:8: 'MIN'
            {
            match("MIN"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T148

    // $ANTLR start T149
    public final void mT149() throws RecognitionException {
        try {
            int _type = T149;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:116:6: ( 'MAX' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:116:8: 'MAX'
            {
            match("MAX"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T149

    // $ANTLR start T150
    public final void mT150() throws RecognitionException {
        try {
            int _type = T150;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:117:6: ( 'AVG' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:117:8: 'AVG'
            {
            match("AVG"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T150

    // $ANTLR start T151
    public final void mT151() throws RecognitionException {
        try {
            int _type = T151;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:6: ( 'SAMPLE' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:118:8: 'SAMPLE'
            {
            match("SAMPLE"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T151

    // $ANTLR start T152
    public final void mT152() throws RecognitionException {
        try {
            int _type = T152;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:119:6: ( 'GROUP_CONCAT' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:119:8: 'GROUP_CONCAT'
            {
            match("GROUP_CONCAT"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T152

    // $ANTLR start T153
    public final void mT153() throws RecognitionException {
        try {
            int _type = T153;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:120:6: ( 'SEPARATOR' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:120:8: 'SEPARATOR'
            {
            match("SEPARATOR"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T153

    // $ANTLR start T154
    public final void mT154() throws RecognitionException {
        try {
            int _type = T154;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:121:6: ( '^^' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:121:8: '^^'
            {
            match("^^"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T154

    // $ANTLR start T155
    public final void mT155() throws RecognitionException {
        try {
            int _type = T155;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:6: ( 'true' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:122:8: 'true'
            {
            match("true"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T155

    // $ANTLR start T156
    public final void mT156() throws RecognitionException {
        try {
            int _type = T156;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:123:6: ( 'false' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:123:8: 'false'
            {
            match("false"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T156

    // $ANTLR start IRI_REF
    public final void mIRI_REF() throws RecognitionException {
        try {
            int _type = IRI_REF;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:5: ( '<' ( options {greedy=false; } : ~ ( '<' | '>' | '\"' | '{' | '}' | '|' | '^' | '\\\\' | '`' ) | ( PN_CHARS ) )* '>' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:7: '<' ( options {greedy=false; } : ~ ( '<' | '>' | '\"' | '{' | '}' | '|' | '^' | '\\\\' | '`' ) | ( PN_CHARS ) )* '>'
            {
            match('<'); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:11: ( options {greedy=false; } : ~ ( '<' | '>' | '\"' | '{' | '}' | '|' | '^' | '\\\\' | '`' ) | ( PN_CHARS ) )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='>') ) {
                    alt1=3;
                }
                else if ( ((LA1_0>='\u0000' && LA1_0<='!')||(LA1_0>='#' && LA1_0<=';')||LA1_0=='='||(LA1_0>='?' && LA1_0<='[')||LA1_0==']'||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')||(LA1_0>='~' && LA1_0<='\uFFFE')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:39: ~ ( '<' | '>' | '\"' | '{' | '}' | '|' | '^' | '\\\\' | '`' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<=';')||input.LA(1)=='='||(input.LA(1)>='?' && input.LA(1)<='[')||input.LA(1)==']'||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='~' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:97: ( PN_CHARS )
            	    {
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:97: ( PN_CHARS )
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:548:98: PN_CHARS
            	    {
            	    mPN_CHARS(); 

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('>'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end IRI_REF

    // $ANTLR start PNAME_NS
    public final void mPNAME_NS() throws RecognitionException {
        try {
            int _type = PNAME_NS;
            Token p=null;

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:552:5: ( (p= PN_PREFIX )? ':' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:552:7: (p= PN_PREFIX )? ':'
            {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:552:8: (p= PN_PREFIX )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>='A' && LA2_0<='Z')||(LA2_0>='a' && LA2_0<='z')||(LA2_0>='\u00C0' && LA2_0<='\u00D6')||(LA2_0>='\u00D8' && LA2_0<='\u00F6')||(LA2_0>='\u00F8' && LA2_0<='\u02FF')||(LA2_0>='\u0370' && LA2_0<='\u037D')||(LA2_0>='\u037F' && LA2_0<='\u1FFF')||(LA2_0>='\u200C' && LA2_0<='\u200D')||(LA2_0>='\u2070' && LA2_0<='\u218F')||(LA2_0>='\u2C00' && LA2_0<='\u2FEF')||(LA2_0>='\u3001' && LA2_0<='\uD7FF')||(LA2_0>='\uF900' && LA2_0<='\uFDCF')||(LA2_0>='\uFDF0' && LA2_0<='\uFFFD')) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:552:8: p= PN_PREFIX
                    {
                    int pStart1064 = getCharIndex();
                    mPN_PREFIX(); 
                    p = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, pStart1064, getCharIndex()-1);

                    }
                    break;

            }

            match(':'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PNAME_NS

    // $ANTLR start PNAME_LN
    public final void mPNAME_LN() throws RecognitionException {
        try {
            int _type = PNAME_LN;
            Token n=null;
            Token l=null;

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:556:5: (n= PNAME_NS l= PN_LOCAL )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:556:7: n= PNAME_NS l= PN_LOCAL
            {
            int nStart1087 = getCharIndex();
            mPNAME_NS(); 
            n = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, nStart1087, getCharIndex()-1);
            int lStart1091 = getCharIndex();
            mPN_LOCAL(); 
            l = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, lStart1091, getCharIndex()-1);

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PNAME_LN

    // $ANTLR start BLANK_NODE_LABEL
    public final void mBLANK_NODE_LABEL() throws RecognitionException {
        try {
            int _type = BLANK_NODE_LABEL;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:560:5: ( '_:' PN_LOCAL )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:560:7: '_:' PN_LOCAL
            {
            match("_:"); 

            mPN_LOCAL(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end BLANK_NODE_LABEL

    // $ANTLR start VAR1
    public final void mVAR1() throws RecognitionException {
        try {
            int _type = VAR1;
            Token v=null;

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:564:5: ( '?' v= VARNAME )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:564:7: '?' v= VARNAME
            {
            match('?'); 
            int vStart1133 = getCharIndex();
            mVARNAME(); 
            v = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, vStart1133, getCharIndex()-1);
             setText(v.getText()); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VAR1

    // $ANTLR start VAR2
    public final void mVAR2() throws RecognitionException {
        try {
            int _type = VAR2;
            Token v=null;

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:568:5: ( '$' v= VARNAME )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:568:7: '$' v= VARNAME
            {
            match('$'); 
            int vStart1157 = getCharIndex();
            mVARNAME(); 
            v = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, vStart1157, getCharIndex()-1);
             setText(v.getText()); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VAR2

    // $ANTLR start LANGTAG
    public final void mLANGTAG() throws RecognitionException {
        try {
            int _type = LANGTAG;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:5: ( '@' ( PN_CHARS_BASE )+ ( '-' ( PN_CHARS_BASE DIGIT )+ )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:7: '@' ( PN_CHARS_BASE )+ ( '-' ( PN_CHARS_BASE DIGIT )+ )*
            {
            match('@'); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:11: ( PN_CHARS_BASE )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='A' && LA3_0<='Z')||(LA3_0>='a' && LA3_0<='z')||(LA3_0>='\u00C0' && LA3_0<='\u00D6')||(LA3_0>='\u00D8' && LA3_0<='\u00F6')||(LA3_0>='\u00F8' && LA3_0<='\u02FF')||(LA3_0>='\u0370' && LA3_0<='\u037D')||(LA3_0>='\u037F' && LA3_0<='\u1FFF')||(LA3_0>='\u200C' && LA3_0<='\u200D')||(LA3_0>='\u2070' && LA3_0<='\u218F')||(LA3_0>='\u2C00' && LA3_0<='\u2FEF')||(LA3_0>='\u3001' && LA3_0<='\uD7FF')||(LA3_0>='\uF900' && LA3_0<='\uFDCF')||(LA3_0>='\uFDF0' && LA3_0<='\uFFFD')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:11: PN_CHARS_BASE
            	    {
            	    mPN_CHARS_BASE(); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:26: ( '-' ( PN_CHARS_BASE DIGIT )+ )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='-') ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:27: '-' ( PN_CHARS_BASE DIGIT )+
            	    {
            	    match('-'); 
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:31: ( PN_CHARS_BASE DIGIT )+
            	    int cnt4=0;
            	    loop4:
            	    do {
            	        int alt4=2;
            	        int LA4_0 = input.LA(1);

            	        if ( ((LA4_0>='A' && LA4_0<='Z')||(LA4_0>='a' && LA4_0<='z')||(LA4_0>='\u00C0' && LA4_0<='\u00D6')||(LA4_0>='\u00D8' && LA4_0<='\u00F6')||(LA4_0>='\u00F8' && LA4_0<='\u02FF')||(LA4_0>='\u0370' && LA4_0<='\u037D')||(LA4_0>='\u037F' && LA4_0<='\u1FFF')||(LA4_0>='\u200C' && LA4_0<='\u200D')||(LA4_0>='\u2070' && LA4_0<='\u218F')||(LA4_0>='\u2C00' && LA4_0<='\u2FEF')||(LA4_0>='\u3001' && LA4_0<='\uD7FF')||(LA4_0>='\uF900' && LA4_0<='\uFDCF')||(LA4_0>='\uFDF0' && LA4_0<='\uFFFD')) ) {
            	            alt4=1;
            	        }


            	        switch (alt4) {
            	    	case 1 :
            	    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:572:32: PN_CHARS_BASE DIGIT
            	    	    {
            	    	    mPN_CHARS_BASE(); 
            	    	    mDIGIT(); 

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt4 >= 1 ) break loop4;
            	                EarlyExitException eee =
            	                    new EarlyExitException(4, input);
            	                throw eee;
            	        }
            	        cnt4++;
            	    } while (true);


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end LANGTAG

    // $ANTLR start INTEGER
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:576:5: ( ( DIGIT )+ )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:576:7: ( DIGIT )+
            {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:576:7: ( DIGIT )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:576:7: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end INTEGER

    // $ANTLR start DECIMAL
    public final void mDECIMAL() throws RecognitionException {
        try {
            int _type = DECIMAL;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:580:5: ( ( DIGIT )+ '.' ( DIGIT )* | '.' ( DIGIT )+ )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( ((LA10_0>='0' && LA10_0<='9')) ) {
                alt10=1;
            }
            else if ( (LA10_0=='.') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("579:1: DECIMAL : ( ( DIGIT )+ '.' ( DIGIT )* | '.' ( DIGIT )+ );", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:580:7: ( DIGIT )+ '.' ( DIGIT )*
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:580:7: ( DIGIT )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:580:7: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);

                    match('.'); 
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:580:18: ( DIGIT )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:580:18: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:581:7: '.' ( DIGIT )+
                    {
                    match('.'); 
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:581:11: ( DIGIT )+
                    int cnt9=0;
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( ((LA9_0>='0' && LA9_0<='9')) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:581:11: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt9 >= 1 ) break loop9;
                                EarlyExitException eee =
                                    new EarlyExitException(9, input);
                                throw eee;
                        }
                        cnt9++;
                    } while (true);


                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DECIMAL

    // $ANTLR start DOUBLE
    public final void mDOUBLE() throws RecognitionException {
        try {
            int _type = DOUBLE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:585:5: ( ( DIGIT )+ '.' ( DIGIT )* EXPONENT | '.' ( DIGIT )+ EXPONENT | ( DIGIT )+ EXPONENT )
            int alt15=3;
            alt15 = dfa15.predict(input);
            switch (alt15) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:585:7: ( DIGIT )+ '.' ( DIGIT )* EXPONENT
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:585:7: ( DIGIT )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( ((LA11_0>='0' && LA11_0<='9')) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:585:7: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);

                    match('.'); 
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:585:18: ( DIGIT )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0>='0' && LA12_0<='9')) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:585:18: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    mEXPONENT(); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:586:7: '.' ( DIGIT )+ EXPONENT
                    {
                    match('.'); 
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:586:11: ( DIGIT )+
                    int cnt13=0;
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( ((LA13_0>='0' && LA13_0<='9')) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:586:11: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt13 >= 1 ) break loop13;
                                EarlyExitException eee =
                                    new EarlyExitException(13, input);
                                throw eee;
                        }
                        cnt13++;
                    } while (true);

                    mEXPONENT(); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:587:7: ( DIGIT )+ EXPONENT
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:587:7: ( DIGIT )+
                    int cnt14=0;
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( ((LA14_0>='0' && LA14_0<='9')) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:587:7: DIGIT
                    	    {
                    	    mDIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt14 >= 1 ) break loop14;
                                EarlyExitException eee =
                                    new EarlyExitException(14, input);
                                throw eee;
                        }
                        cnt14++;
                    } while (true);

                    mEXPONENT(); 

                    }
                    break;

            }
            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DOUBLE

    // $ANTLR start INTEGER_POSITIVE
    public final void mINTEGER_POSITIVE() throws RecognitionException {
        try {
            int _type = INTEGER_POSITIVE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:591:5: ( '+' INTEGER )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:591:7: '+' INTEGER
            {
            match('+'); 
            mINTEGER(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end INTEGER_POSITIVE

    // $ANTLR start DECIMAL_POSITIVE
    public final void mDECIMAL_POSITIVE() throws RecognitionException {
        try {
            int _type = DECIMAL_POSITIVE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:595:5: ( '+' DECIMAL )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:595:7: '+' DECIMAL
            {
            match('+'); 
            mDECIMAL(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DECIMAL_POSITIVE

    // $ANTLR start DOUBLE_POSITIVE
    public final void mDOUBLE_POSITIVE() throws RecognitionException {
        try {
            int _type = DOUBLE_POSITIVE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:599:5: ( '+' DOUBLE )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:599:7: '+' DOUBLE
            {
            match('+'); 
            mDOUBLE(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DOUBLE_POSITIVE

    // $ANTLR start INTEGER_NEGATIVE
    public final void mINTEGER_NEGATIVE() throws RecognitionException {
        try {
            int _type = INTEGER_NEGATIVE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:603:5: ( '-' INTEGER )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:603:7: '-' INTEGER
            {
            match('-'); 
            mINTEGER(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end INTEGER_NEGATIVE

    // $ANTLR start DECIMAL_NEGATIVE
    public final void mDECIMAL_NEGATIVE() throws RecognitionException {
        try {
            int _type = DECIMAL_NEGATIVE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:607:5: ( '-' DECIMAL )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:607:7: '-' DECIMAL
            {
            match('-'); 
            mDECIMAL(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DECIMAL_NEGATIVE

    // $ANTLR start DOUBLE_NEGATIVE
    public final void mDOUBLE_NEGATIVE() throws RecognitionException {
        try {
            int _type = DOUBLE_NEGATIVE;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:611:5: ( '-' DOUBLE )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:611:7: '-' DOUBLE
            {
            match('-'); 
            mDOUBLE(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end DOUBLE_NEGATIVE

    // $ANTLR start EXPONENT
    public final void mEXPONENT() throws RecognitionException {
        try {
            int _type = EXPONENT;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:615:5: ( ( 'e' | 'E' ) ( '+' | '-' )? ( DIGIT )+ )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:615:7: ( 'e' | 'E' ) ( '+' | '-' )? ( DIGIT )+
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:615:17: ( '+' | '-' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0=='+'||LA16_0=='-') ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse =
                            new MismatchedSetException(null,input);
                        recover(mse);    throw mse;
                    }


                    }
                    break;

            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:615:28: ( DIGIT )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>='0' && LA17_0<='9')) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:615:28: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end EXPONENT

    // $ANTLR start STRING_LITERAL1
    public final void mSTRING_LITERAL1() throws RecognitionException {
        try {
            int _type = STRING_LITERAL1;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:619:5: ( '\\'' ( options {greedy=false; } : ~ ( '\\u0027' | '\\u005C' | '\\u000A' | '\\u000D' ) | ECHAR )* '\\'' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:619:7: '\\'' ( options {greedy=false; } : ~ ( '\\u0027' | '\\u005C' | '\\u000A' | '\\u000D' ) | ECHAR )* '\\''
            {
            match('\''); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:619:12: ( options {greedy=false; } : ~ ( '\\u0027' | '\\u005C' | '\\u000A' | '\\u000D' ) | ECHAR )*
            loop18:
            do {
                int alt18=3;
                int LA18_0 = input.LA(1);

                if ( (LA18_0=='\'') ) {
                    alt18=3;
                }
                else if ( ((LA18_0>='\u0000' && LA18_0<='\t')||(LA18_0>='\u000B' && LA18_0<='\f')||(LA18_0>='\u000E' && LA18_0<='&')||(LA18_0>='(' && LA18_0<='[')||(LA18_0>=']' && LA18_0<='\uFFFE')) ) {
                    alt18=1;
                }
                else if ( (LA18_0=='\\') ) {
                    alt18=2;
                }


                switch (alt18) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:619:40: ~ ( '\\u0027' | '\\u005C' | '\\u000A' | '\\u000D' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:619:87: ECHAR
            	    {
            	    mECHAR(); 

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            match('\''); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING_LITERAL1

    // $ANTLR start STRING_LITERAL2
    public final void mSTRING_LITERAL2() throws RecognitionException {
        try {
            int _type = STRING_LITERAL2;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:623:5: ( '\"' ( options {greedy=false; } : ~ ( '\\u0022' | '\\u005C' | '\\u000A' | '\\u000D' ) | ECHAR )* '\"' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:623:7: '\"' ( options {greedy=false; } : ~ ( '\\u0022' | '\\u005C' | '\\u000A' | '\\u000D' ) | ECHAR )* '\"'
            {
            match('\"'); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:623:12: ( options {greedy=false; } : ~ ( '\\u0022' | '\\u005C' | '\\u000A' | '\\u000D' ) | ECHAR )*
            loop19:
            do {
                int alt19=3;
                int LA19_0 = input.LA(1);

                if ( (LA19_0=='\"') ) {
                    alt19=3;
                }
                else if ( ((LA19_0>='\u0000' && LA19_0<='\t')||(LA19_0>='\u000B' && LA19_0<='\f')||(LA19_0>='\u000E' && LA19_0<='!')||(LA19_0>='#' && LA19_0<='[')||(LA19_0>=']' && LA19_0<='\uFFFE')) ) {
                    alt19=1;
                }
                else if ( (LA19_0=='\\') ) {
                    alt19=2;
                }


                switch (alt19) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:623:40: ~ ( '\\u0022' | '\\u005C' | '\\u000A' | '\\u000D' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:623:87: ECHAR
            	    {
            	    mECHAR(); 

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            match('\"'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING_LITERAL2

    // $ANTLR start STRING_LITERAL_LONG1
    public final void mSTRING_LITERAL_LONG1() throws RecognitionException {
        try {
            int _type = STRING_LITERAL_LONG1;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:5: ( '\\'\\'\\'' ( options {greedy=false; } : ( '\\'' | '\\'\\'' )? (~ ( '\\'' | '\\\\' ) | ECHAR ) )* '\\'\\'\\'' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:7: '\\'\\'\\'' ( options {greedy=false; } : ( '\\'' | '\\'\\'' )? (~ ( '\\'' | '\\\\' ) | ECHAR ) )* '\\'\\'\\''
            {
            match("\'\'\'"); 

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:16: ( options {greedy=false; } : ( '\\'' | '\\'\\'' )? (~ ( '\\'' | '\\\\' ) | ECHAR ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0=='\'') ) {
                    int LA22_1 = input.LA(2);

                    if ( (LA22_1=='\'') ) {
                        int LA22_3 = input.LA(3);

                        if ( (LA22_3=='\'') ) {
                            alt22=2;
                        }
                        else if ( ((LA22_3>='\u0000' && LA22_3<='&')||(LA22_3>='(' && LA22_3<='\uFFFE')) ) {
                            alt22=1;
                        }


                    }
                    else if ( ((LA22_1>='\u0000' && LA22_1<='&')||(LA22_1>='(' && LA22_1<='\uFFFE')) ) {
                        alt22=1;
                    }


                }
                else if ( ((LA22_0>='\u0000' && LA22_0<='&')||(LA22_0>='(' && LA22_0<='\uFFFE')) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:44: ( '\\'' | '\\'\\'' )? (~ ( '\\'' | '\\\\' ) | ECHAR )
            	    {
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:44: ( '\\'' | '\\'\\'' )?
            	    int alt20=3;
            	    int LA20_0 = input.LA(1);

            	    if ( (LA20_0=='\'') ) {
            	        int LA20_1 = input.LA(2);

            	        if ( (LA20_1=='\'') ) {
            	            alt20=2;
            	        }
            	        else if ( ((LA20_1>='\u0000' && LA20_1<='&')||(LA20_1>='(' && LA20_1<='\uFFFE')) ) {
            	            alt20=1;
            	        }
            	    }
            	    switch (alt20) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:46: '\\''
            	            {
            	            match('\''); 

            	            }
            	            break;
            	        case 2 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:53: '\\'\\''
            	            {
            	            match("\'\'"); 


            	            }
            	            break;

            	    }

            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:63: (~ ( '\\'' | '\\\\' ) | ECHAR )
            	    int alt21=2;
            	    int LA21_0 = input.LA(1);

            	    if ( ((LA21_0>='\u0000' && LA21_0<='&')||(LA21_0>='(' && LA21_0<='[')||(LA21_0>=']' && LA21_0<='\uFFFE')) ) {
            	        alt21=1;
            	    }
            	    else if ( (LA21_0=='\\') ) {
            	        alt21=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("627:63: (~ ( '\\'' | '\\\\' ) | ECHAR )", 21, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt21) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:64: ~ ( '\\'' | '\\\\' )
            	            {
            	            if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	                input.consume();

            	            }
            	            else {
            	                MismatchedSetException mse =
            	                    new MismatchedSetException(null,input);
            	                recover(mse);    throw mse;
            	            }


            	            }
            	            break;
            	        case 2 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:627:81: ECHAR
            	            {
            	            mECHAR(); 

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            match("\'\'\'"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING_LITERAL_LONG1

    // $ANTLR start STRING_LITERAL_LONG2
    public final void mSTRING_LITERAL_LONG2() throws RecognitionException {
        try {
            int _type = STRING_LITERAL_LONG2;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:5: ( '\"\"\"' ( options {greedy=false; } : ( '\"' | '\"\"' )? (~ ( '\\'' | '\\\\' ) | ECHAR ) )* '\"\"\"' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:7: '\"\"\"' ( options {greedy=false; } : ( '\"' | '\"\"' )? (~ ( '\\'' | '\\\\' ) | ECHAR ) )* '\"\"\"'
            {
            match("\"\"\""); 

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:13: ( options {greedy=false; } : ( '\"' | '\"\"' )? (~ ( '\\'' | '\\\\' ) | ECHAR ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0=='\"') ) {
                    int LA25_1 = input.LA(2);

                    if ( (LA25_1=='\"') ) {
                        int LA25_3 = input.LA(3);

                        if ( (LA25_3=='\"') ) {
                            alt25=2;
                        }
                        else if ( ((LA25_3>='\u0000' && LA25_3<='!')||(LA25_3>='#' && LA25_3<='&')||(LA25_3>='(' && LA25_3<='\uFFFE')) ) {
                            alt25=1;
                        }


                    }
                    else if ( ((LA25_1>='\u0000' && LA25_1<='!')||(LA25_1>='#' && LA25_1<='&')||(LA25_1>='(' && LA25_1<='\uFFFE')) ) {
                        alt25=1;
                    }


                }
                else if ( ((LA25_0>='\u0000' && LA25_0<='!')||(LA25_0>='#' && LA25_0<='&')||(LA25_0>='(' && LA25_0<='\uFFFE')) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:41: ( '\"' | '\"\"' )? (~ ( '\\'' | '\\\\' ) | ECHAR )
            	    {
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:41: ( '\"' | '\"\"' )?
            	    int alt23=3;
            	    int LA23_0 = input.LA(1);

            	    if ( (LA23_0=='\"') ) {
            	        int LA23_1 = input.LA(2);

            	        if ( ((LA23_1>='\u0000' && LA23_1<='&')||(LA23_1>='(' && LA23_1<='\uFFFE')) ) {
            	            alt23=1;
            	        }
            	    }
            	    switch (alt23) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:43: '\"'
            	            {
            	            match('\"'); 

            	            }
            	            break;
            	        case 2 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:49: '\"\"'
            	            {
            	            match("\"\""); 


            	            }
            	            break;

            	    }

            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:57: (~ ( '\\'' | '\\\\' ) | ECHAR )
            	    int alt24=2;
            	    int LA24_0 = input.LA(1);

            	    if ( ((LA24_0>='\u0000' && LA24_0<='&')||(LA24_0>='(' && LA24_0<='[')||(LA24_0>=']' && LA24_0<='\uFFFE')) ) {
            	        alt24=1;
            	    }
            	    else if ( (LA24_0=='\\') ) {
            	        alt24=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("631:57: (~ ( '\\'' | '\\\\' ) | ECHAR )", 24, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt24) {
            	        case 1 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:59: ~ ( '\\'' | '\\\\' )
            	            {
            	            if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	                input.consume();

            	            }
            	            else {
            	                MismatchedSetException mse =
            	                    new MismatchedSetException(null,input);
            	                recover(mse);    throw mse;
            	            }


            	            }
            	            break;
            	        case 2 :
            	            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:631:76: ECHAR
            	            {
            	            mECHAR(); 

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            match("\"\"\""); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING_LITERAL_LONG2

    // $ANTLR start ECHAR
    public final void mECHAR() throws RecognitionException {
        try {
            int _type = ECHAR;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:635:5: ( '\\\\' ( 't' | 'b' | 'n' | 'r' | 'f' | '\"' | '\\'' ) )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:635:7: '\\\\' ( 't' | 'b' | 'n' | 'r' | 'f' | '\"' | '\\'' )
            {
            match('\\'); 
            if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ECHAR

    // $ANTLR start NIL
    public final void mNIL() throws RecognitionException {
        try {
            int _type = NIL;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:639:5: ( '(' ( WS )* ')' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:639:7: '(' ( WS )* ')'
            {
            match('('); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:639:11: ( WS )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>='\t' && LA26_0<='\n')||LA26_0=='\r'||LA26_0==' ') ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:639:11: WS
            	    {
            	    mWS(); 

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

            match(')'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NIL

    // $ANTLR start ANON
    public final void mANON() throws RecognitionException {
        try {
            int _type = ANON;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:643:5: ( '[' ( WS )* ']' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:643:7: '[' ( WS )* ']'
            {
            match('['); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:643:11: ( WS )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( ((LA27_0>='\t' && LA27_0<='\n')||LA27_0=='\r'||LA27_0==' ') ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:643:11: WS
            	    {
            	    mWS(); 

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            match(']'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ANON

    // $ANTLR start PN_CHARS_U
    public final void mPN_CHARS_U() throws RecognitionException {
        try {
            int _type = PN_CHARS_U;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:647:5: ( PN_CHARS_BASE | '_' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PN_CHARS_U

    // $ANTLR start VARNAME
    public final void mVARNAME() throws RecognitionException {
        try {
            int _type = VARNAME;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:5: ( ( PN_CHARS_U | DIGIT ) ( PN_CHARS_U | DIGIT | '\\u00B7' | ( '\\u0300' .. '\\u036F' ) | ( '\\u203F' .. '\\u2040' ) )* )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:7: ( PN_CHARS_U | DIGIT ) ( PN_CHARS_U | DIGIT | '\\u00B7' | ( '\\u0300' .. '\\u036F' ) | ( '\\u203F' .. '\\u2040' ) )*
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:30: ( PN_CHARS_U | DIGIT | '\\u00B7' | ( '\\u0300' .. '\\u036F' ) | ( '\\u203F' .. '\\u2040' ) )*
            loop28:
            do {
                int alt28=6;
                int LA28_0 = input.LA(1);

                if ( ((LA28_0>='A' && LA28_0<='Z')||LA28_0=='_'||(LA28_0>='a' && LA28_0<='z')||(LA28_0>='\u00C0' && LA28_0<='\u00D6')||(LA28_0>='\u00D8' && LA28_0<='\u00F6')||(LA28_0>='\u00F8' && LA28_0<='\u02FF')||(LA28_0>='\u0370' && LA28_0<='\u037D')||(LA28_0>='\u037F' && LA28_0<='\u1FFF')||(LA28_0>='\u200C' && LA28_0<='\u200D')||(LA28_0>='\u2070' && LA28_0<='\u218F')||(LA28_0>='\u2C00' && LA28_0<='\u2FEF')||(LA28_0>='\u3001' && LA28_0<='\uD7FF')||(LA28_0>='\uF900' && LA28_0<='\uFDCF')||(LA28_0>='\uFDF0' && LA28_0<='\uFFFD')) ) {
                    alt28=1;
                }
                else if ( ((LA28_0>='0' && LA28_0<='9')) ) {
                    alt28=2;
                }
                else if ( (LA28_0=='\u00B7') ) {
                    alt28=3;
                }
                else if ( ((LA28_0>='\u0300' && LA28_0<='\u036F')) ) {
                    alt28=4;
                }
                else if ( ((LA28_0>='\u203F' && LA28_0<='\u2040')) ) {
                    alt28=5;
                }


                switch (alt28) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:32: PN_CHARS_U
            	    {
            	    mPN_CHARS_U(); 

            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:45: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;
            	case 3 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:53: '\\u00B7'
            	    {
            	    match('\u00B7'); 

            	    }
            	    break;
            	case 4 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:64: ( '\\u0300' .. '\\u036F' )
            	    {
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:64: ( '\\u0300' .. '\\u036F' )
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:65: '\\u0300' .. '\\u036F'
            	    {
            	    matchRange('\u0300','\u036F'); 

            	    }


            	    }
            	    break;
            	case 5 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:87: ( '\\u203F' .. '\\u2040' )
            	    {
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:87: ( '\\u203F' .. '\\u2040' )
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:651:88: '\\u203F' .. '\\u2040'
            	    {
            	    matchRange('\u203F','\u2040'); 

            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VARNAME

    // $ANTLR start PN_CHARS
    public final void mPN_CHARS() throws RecognitionException {
        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:656:5: ( PN_CHARS_U | '-' | DIGIT )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            if ( input.LA(1)=='-'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end PN_CHARS

    // $ANTLR start PN_PREFIX
    public final void mPN_PREFIX() throws RecognitionException {
        try {
            int _type = PN_PREFIX;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:665:5: ( PN_CHARS_BASE ( ( PN_CHARS | '.' )* PN_CHARS )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:665:7: PN_CHARS_BASE ( ( PN_CHARS | '.' )* PN_CHARS )?
            {
            mPN_CHARS_BASE(); 
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:665:21: ( ( PN_CHARS | '.' )* PN_CHARS )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>='-' && LA30_0<='.')||(LA30_0>='0' && LA30_0<='9')||(LA30_0>='A' && LA30_0<='Z')||LA30_0=='_'||(LA30_0>='a' && LA30_0<='z')||(LA30_0>='\u00C0' && LA30_0<='\u00D6')||(LA30_0>='\u00D8' && LA30_0<='\u00F6')||(LA30_0>='\u00F8' && LA30_0<='\u02FF')||(LA30_0>='\u0370' && LA30_0<='\u037D')||(LA30_0>='\u037F' && LA30_0<='\u1FFF')||(LA30_0>='\u200C' && LA30_0<='\u200D')||(LA30_0>='\u2070' && LA30_0<='\u218F')||(LA30_0>='\u2C00' && LA30_0<='\u2FEF')||(LA30_0>='\u3001' && LA30_0<='\uD7FF')||(LA30_0>='\uF900' && LA30_0<='\uFDCF')||(LA30_0>='\uFDF0' && LA30_0<='\uFFFD')) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:665:22: ( PN_CHARS | '.' )* PN_CHARS
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:665:22: ( PN_CHARS | '.' )*
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0=='-'||(LA29_0>='0' && LA29_0<='9')||(LA29_0>='A' && LA29_0<='Z')||LA29_0=='_'||(LA29_0>='a' && LA29_0<='z')||(LA29_0>='\u00C0' && LA29_0<='\u00D6')||(LA29_0>='\u00D8' && LA29_0<='\u00F6')||(LA29_0>='\u00F8' && LA29_0<='\u02FF')||(LA29_0>='\u0370' && LA29_0<='\u037D')||(LA29_0>='\u037F' && LA29_0<='\u1FFF')||(LA29_0>='\u200C' && LA29_0<='\u200D')||(LA29_0>='\u2070' && LA29_0<='\u218F')||(LA29_0>='\u2C00' && LA29_0<='\u2FEF')||(LA29_0>='\u3001' && LA29_0<='\uD7FF')||(LA29_0>='\uF900' && LA29_0<='\uFDCF')||(LA29_0>='\uFDF0' && LA29_0<='\uFFFD')) ) {
                            int LA29_1 = input.LA(2);

                            if ( ((LA29_1>='-' && LA29_1<='.')||(LA29_1>='0' && LA29_1<='9')||(LA29_1>='A' && LA29_1<='Z')||LA29_1=='_'||(LA29_1>='a' && LA29_1<='z')||(LA29_1>='\u00C0' && LA29_1<='\u00D6')||(LA29_1>='\u00D8' && LA29_1<='\u00F6')||(LA29_1>='\u00F8' && LA29_1<='\u02FF')||(LA29_1>='\u0370' && LA29_1<='\u037D')||(LA29_1>='\u037F' && LA29_1<='\u1FFF')||(LA29_1>='\u200C' && LA29_1<='\u200D')||(LA29_1>='\u2070' && LA29_1<='\u218F')||(LA29_1>='\u2C00' && LA29_1<='\u2FEF')||(LA29_1>='\u3001' && LA29_1<='\uD7FF')||(LA29_1>='\uF900' && LA29_1<='\uFDCF')||(LA29_1>='\uFDF0' && LA29_1<='\uFFFD')) ) {
                                alt29=1;
                            }


                        }
                        else if ( (LA29_0=='.') ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
                    	    {
                    	    if ( (input.LA(1)>='-' && input.LA(1)<='.')||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse =
                    	            new MismatchedSetException(null,input);
                    	        recover(mse);    throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop29;
                        }
                    } while (true);

                    mPN_CHARS(); 

                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PN_PREFIX

    // $ANTLR start PN_LOCAL
    public final void mPN_LOCAL() throws RecognitionException {
        try {
            int _type = PN_LOCAL;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:669:5: ( ( PN_CHARS_U | DIGIT ) ( ( PN_CHARS | '.' )* PN_CHARS )? )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:669:7: ( PN_CHARS_U | DIGIT ) ( ( PN_CHARS | '.' )* PN_CHARS )?
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:669:30: ( ( PN_CHARS | '.' )* PN_CHARS )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( ((LA32_0>='-' && LA32_0<='.')||(LA32_0>='0' && LA32_0<='9')||(LA32_0>='A' && LA32_0<='Z')||LA32_0=='_'||(LA32_0>='a' && LA32_0<='z')||(LA32_0>='\u00C0' && LA32_0<='\u00D6')||(LA32_0>='\u00D8' && LA32_0<='\u00F6')||(LA32_0>='\u00F8' && LA32_0<='\u02FF')||(LA32_0>='\u0370' && LA32_0<='\u037D')||(LA32_0>='\u037F' && LA32_0<='\u1FFF')||(LA32_0>='\u200C' && LA32_0<='\u200D')||(LA32_0>='\u2070' && LA32_0<='\u218F')||(LA32_0>='\u2C00' && LA32_0<='\u2FEF')||(LA32_0>='\u3001' && LA32_0<='\uD7FF')||(LA32_0>='\uF900' && LA32_0<='\uFDCF')||(LA32_0>='\uFDF0' && LA32_0<='\uFFFD')) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:669:31: ( PN_CHARS | '.' )* PN_CHARS
                    {
                    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:669:31: ( PN_CHARS | '.' )*
                    loop31:
                    do {
                        int alt31=2;
                        int LA31_0 = input.LA(1);

                        if ( (LA31_0=='-'||(LA31_0>='0' && LA31_0<='9')||(LA31_0>='A' && LA31_0<='Z')||LA31_0=='_'||(LA31_0>='a' && LA31_0<='z')||(LA31_0>='\u00C0' && LA31_0<='\u00D6')||(LA31_0>='\u00D8' && LA31_0<='\u00F6')||(LA31_0>='\u00F8' && LA31_0<='\u02FF')||(LA31_0>='\u0370' && LA31_0<='\u037D')||(LA31_0>='\u037F' && LA31_0<='\u1FFF')||(LA31_0>='\u200C' && LA31_0<='\u200D')||(LA31_0>='\u2070' && LA31_0<='\u218F')||(LA31_0>='\u2C00' && LA31_0<='\u2FEF')||(LA31_0>='\u3001' && LA31_0<='\uD7FF')||(LA31_0>='\uF900' && LA31_0<='\uFDCF')||(LA31_0>='\uFDF0' && LA31_0<='\uFFFD')) ) {
                            int LA31_1 = input.LA(2);

                            if ( ((LA31_1>='-' && LA31_1<='.')||(LA31_1>='0' && LA31_1<='9')||(LA31_1>='A' && LA31_1<='Z')||LA31_1=='_'||(LA31_1>='a' && LA31_1<='z')||(LA31_1>='\u00C0' && LA31_1<='\u00D6')||(LA31_1>='\u00D8' && LA31_1<='\u00F6')||(LA31_1>='\u00F8' && LA31_1<='\u02FF')||(LA31_1>='\u0370' && LA31_1<='\u037D')||(LA31_1>='\u037F' && LA31_1<='\u1FFF')||(LA31_1>='\u200C' && LA31_1<='\u200D')||(LA31_1>='\u2070' && LA31_1<='\u218F')||(LA31_1>='\u2C00' && LA31_1<='\u2FEF')||(LA31_1>='\u3001' && LA31_1<='\uD7FF')||(LA31_1>='\uF900' && LA31_1<='\uFDCF')||(LA31_1>='\uFDF0' && LA31_1<='\uFFFD')) ) {
                                alt31=1;
                            }


                        }
                        else if ( (LA31_0=='.') ) {
                            alt31=1;
                        }


                        switch (alt31) {
                    	case 1 :
                    	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
                    	    {
                    	    if ( (input.LA(1)>='-' && input.LA(1)<='.')||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse =
                    	            new MismatchedSetException(null,input);
                    	        recover(mse);    throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop31;
                        }
                    } while (true);

                    mPN_CHARS(); 

                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end PN_LOCAL

    // $ANTLR start PN_CHARS_BASE
    public final void mPN_CHARS_BASE() throws RecognitionException {
        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:674:5: ( 'A' .. 'Z' | 'a' .. 'z' | '\\u00C0' .. '\\u00D6' | '\\u00D8' .. '\\u00F6' | '\\u00F8' .. '\\u02FF' | '\\u0370' .. '\\u037D' | '\\u037F' .. '\\u1FFF' | '\\u200C' .. '\\u200D' | '\\u2070' .. '\\u218F' | '\\u2C00' .. '\\u2FEF' | '\\u3001' .. '\\uD7FF' | '\\uF900' .. '\\uFDCF' | '\\uFDF0' .. '\\uFFFD' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z')||(input.LA(1)>='\u00C0' && input.LA(1)<='\u00D6')||(input.LA(1)>='\u00D8' && input.LA(1)<='\u00F6')||(input.LA(1)>='\u00F8' && input.LA(1)<='\u02FF')||(input.LA(1)>='\u0370' && input.LA(1)<='\u037D')||(input.LA(1)>='\u037F' && input.LA(1)<='\u1FFF')||(input.LA(1)>='\u200C' && input.LA(1)<='\u200D')||(input.LA(1)>='\u2070' && input.LA(1)<='\u218F')||(input.LA(1)>='\u2C00' && input.LA(1)<='\u2FEF')||(input.LA(1)>='\u3001' && input.LA(1)<='\uD7FF')||(input.LA(1)>='\uF900' && input.LA(1)<='\uFDCF')||(input.LA(1)>='\uFDF0' && input.LA(1)<='\uFFFD') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

        }
        finally {
        }
    }
    // $ANTLR end PN_CHARS_BASE

    // $ANTLR start DIGIT
    public final void mDIGIT() throws RecognitionException {
        try {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:691:5: ( '0' .. '9' )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:691:7: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end DIGIT

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:695:5: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:695:7: ( ' ' | '\\t' | '\\n' | '\\r' )+
            {
            // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:695:7: ( ' ' | '\\t' | '\\n' | '\\r' )+
            int cnt33=0;
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( ((LA33_0>='\t' && LA33_0<='\n')||LA33_0=='\r'||LA33_0==' ') ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt33 >= 1 ) break loop33;
                        EarlyExitException eee =
                            new EarlyExitException(33, input);
                        throw eee;
                }
                cnt33++;
            } while (true);

             channel=HIDDEN; 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:8: ( T36 | T37 | T38 | T39 | T40 | T41 | T42 | T43 | T44 | T45 | T46 | T47 | T48 | T49 | T50 | T51 | T52 | T53 | T54 | T55 | T56 | T57 | T58 | T59 | T60 | T61 | T62 | T63 | T64 | T65 | T66 | T67 | T68 | T69 | T70 | T71 | T72 | T73 | T74 | T75 | T76 | T77 | T78 | T79 | T80 | T81 | T82 | T83 | T84 | T85 | T86 | T87 | T88 | T89 | T90 | T91 | T92 | T93 | T94 | T95 | T96 | T97 | T98 | T99 | T100 | T101 | T102 | T103 | T104 | T105 | T106 | T107 | T108 | T109 | T110 | T111 | T112 | T113 | T114 | T115 | T116 | T117 | T118 | T119 | T120 | T121 | T122 | T123 | T124 | T125 | T126 | T127 | T128 | T129 | T130 | T131 | T132 | T133 | T134 | T135 | T136 | T137 | T138 | T139 | T140 | T141 | T142 | T143 | T144 | T145 | T146 | T147 | T148 | T149 | T150 | T151 | T152 | T153 | T154 | T155 | T156 | IRI_REF | PNAME_NS | PNAME_LN | BLANK_NODE_LABEL | VAR1 | VAR2 | LANGTAG | INTEGER | DECIMAL | DOUBLE | INTEGER_POSITIVE | DECIMAL_POSITIVE | DOUBLE_POSITIVE | INTEGER_NEGATIVE | DECIMAL_NEGATIVE | DOUBLE_NEGATIVE | EXPONENT | STRING_LITERAL1 | STRING_LITERAL2 | STRING_LITERAL_LONG1 | STRING_LITERAL_LONG2 | ECHAR | NIL | ANON | PN_CHARS_U | VARNAME | PN_PREFIX | PN_LOCAL | WS )
        int alt34=150;
        alt34 = dfa34.predict(input);
        switch (alt34) {
            case 1 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:10: T36
                {
                mT36(); 

                }
                break;
            case 2 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:14: T37
                {
                mT37(); 

                }
                break;
            case 3 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:18: T38
                {
                mT38(); 

                }
                break;
            case 4 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:22: T39
                {
                mT39(); 

                }
                break;
            case 5 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:26: T40
                {
                mT40(); 

                }
                break;
            case 6 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:30: T41
                {
                mT41(); 

                }
                break;
            case 7 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:34: T42
                {
                mT42(); 

                }
                break;
            case 8 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:38: T43
                {
                mT43(); 

                }
                break;
            case 9 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:42: T44
                {
                mT44(); 

                }
                break;
            case 10 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:46: T45
                {
                mT45(); 

                }
                break;
            case 11 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:50: T46
                {
                mT46(); 

                }
                break;
            case 12 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:54: T47
                {
                mT47(); 

                }
                break;
            case 13 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:58: T48
                {
                mT48(); 

                }
                break;
            case 14 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:62: T49
                {
                mT49(); 

                }
                break;
            case 15 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:66: T50
                {
                mT50(); 

                }
                break;
            case 16 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:70: T51
                {
                mT51(); 

                }
                break;
            case 17 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:74: T52
                {
                mT52(); 

                }
                break;
            case 18 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:78: T53
                {
                mT53(); 

                }
                break;
            case 19 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:82: T54
                {
                mT54(); 

                }
                break;
            case 20 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:86: T55
                {
                mT55(); 

                }
                break;
            case 21 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:90: T56
                {
                mT56(); 

                }
                break;
            case 22 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:94: T57
                {
                mT57(); 

                }
                break;
            case 23 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:98: T58
                {
                mT58(); 

                }
                break;
            case 24 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:102: T59
                {
                mT59(); 

                }
                break;
            case 25 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:106: T60
                {
                mT60(); 

                }
                break;
            case 26 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:110: T61
                {
                mT61(); 

                }
                break;
            case 27 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:114: T62
                {
                mT62(); 

                }
                break;
            case 28 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:118: T63
                {
                mT63(); 

                }
                break;
            case 29 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:122: T64
                {
                mT64(); 

                }
                break;
            case 30 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:126: T65
                {
                mT65(); 

                }
                break;
            case 31 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:130: T66
                {
                mT66(); 

                }
                break;
            case 32 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:134: T67
                {
                mT67(); 

                }
                break;
            case 33 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:138: T68
                {
                mT68(); 

                }
                break;
            case 34 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:142: T69
                {
                mT69(); 

                }
                break;
            case 35 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:146: T70
                {
                mT70(); 

                }
                break;
            case 36 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:150: T71
                {
                mT71(); 

                }
                break;
            case 37 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:154: T72
                {
                mT72(); 

                }
                break;
            case 38 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:158: T73
                {
                mT73(); 

                }
                break;
            case 39 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:162: T74
                {
                mT74(); 

                }
                break;
            case 40 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:166: T75
                {
                mT75(); 

                }
                break;
            case 41 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:170: T76
                {
                mT76(); 

                }
                break;
            case 42 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:174: T77
                {
                mT77(); 

                }
                break;
            case 43 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:178: T78
                {
                mT78(); 

                }
                break;
            case 44 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:182: T79
                {
                mT79(); 

                }
                break;
            case 45 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:186: T80
                {
                mT80(); 

                }
                break;
            case 46 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:190: T81
                {
                mT81(); 

                }
                break;
            case 47 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:194: T82
                {
                mT82(); 

                }
                break;
            case 48 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:198: T83
                {
                mT83(); 

                }
                break;
            case 49 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:202: T84
                {
                mT84(); 

                }
                break;
            case 50 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:206: T85
                {
                mT85(); 

                }
                break;
            case 51 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:210: T86
                {
                mT86(); 

                }
                break;
            case 52 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:214: T87
                {
                mT87(); 

                }
                break;
            case 53 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:218: T88
                {
                mT88(); 

                }
                break;
            case 54 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:222: T89
                {
                mT89(); 

                }
                break;
            case 55 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:226: T90
                {
                mT90(); 

                }
                break;
            case 56 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:230: T91
                {
                mT91(); 

                }
                break;
            case 57 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:234: T92
                {
                mT92(); 

                }
                break;
            case 58 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:238: T93
                {
                mT93(); 

                }
                break;
            case 59 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:242: T94
                {
                mT94(); 

                }
                break;
            case 60 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:246: T95
                {
                mT95(); 

                }
                break;
            case 61 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:250: T96
                {
                mT96(); 

                }
                break;
            case 62 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:254: T97
                {
                mT97(); 

                }
                break;
            case 63 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:258: T98
                {
                mT98(); 

                }
                break;
            case 64 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:262: T99
                {
                mT99(); 

                }
                break;
            case 65 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:266: T100
                {
                mT100(); 

                }
                break;
            case 66 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:271: T101
                {
                mT101(); 

                }
                break;
            case 67 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:276: T102
                {
                mT102(); 

                }
                break;
            case 68 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:281: T103
                {
                mT103(); 

                }
                break;
            case 69 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:286: T104
                {
                mT104(); 

                }
                break;
            case 70 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:291: T105
                {
                mT105(); 

                }
                break;
            case 71 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:296: T106
                {
                mT106(); 

                }
                break;
            case 72 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:301: T107
                {
                mT107(); 

                }
                break;
            case 73 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:306: T108
                {
                mT108(); 

                }
                break;
            case 74 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:311: T109
                {
                mT109(); 

                }
                break;
            case 75 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:316: T110
                {
                mT110(); 

                }
                break;
            case 76 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:321: T111
                {
                mT111(); 

                }
                break;
            case 77 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:326: T112
                {
                mT112(); 

                }
                break;
            case 78 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:331: T113
                {
                mT113(); 

                }
                break;
            case 79 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:336: T114
                {
                mT114(); 

                }
                break;
            case 80 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:341: T115
                {
                mT115(); 

                }
                break;
            case 81 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:346: T116
                {
                mT116(); 

                }
                break;
            case 82 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:351: T117
                {
                mT117(); 

                }
                break;
            case 83 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:356: T118
                {
                mT118(); 

                }
                break;
            case 84 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:361: T119
                {
                mT119(); 

                }
                break;
            case 85 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:366: T120
                {
                mT120(); 

                }
                break;
            case 86 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:371: T121
                {
                mT121(); 

                }
                break;
            case 87 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:376: T122
                {
                mT122(); 

                }
                break;
            case 88 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:381: T123
                {
                mT123(); 

                }
                break;
            case 89 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:386: T124
                {
                mT124(); 

                }
                break;
            case 90 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:391: T125
                {
                mT125(); 

                }
                break;
            case 91 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:396: T126
                {
                mT126(); 

                }
                break;
            case 92 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:401: T127
                {
                mT127(); 

                }
                break;
            case 93 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:406: T128
                {
                mT128(); 

                }
                break;
            case 94 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:411: T129
                {
                mT129(); 

                }
                break;
            case 95 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:416: T130
                {
                mT130(); 

                }
                break;
            case 96 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:421: T131
                {
                mT131(); 

                }
                break;
            case 97 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:426: T132
                {
                mT132(); 

                }
                break;
            case 98 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:431: T133
                {
                mT133(); 

                }
                break;
            case 99 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:436: T134
                {
                mT134(); 

                }
                break;
            case 100 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:441: T135
                {
                mT135(); 

                }
                break;
            case 101 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:446: T136
                {
                mT136(); 

                }
                break;
            case 102 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:451: T137
                {
                mT137(); 

                }
                break;
            case 103 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:456: T138
                {
                mT138(); 

                }
                break;
            case 104 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:461: T139
                {
                mT139(); 

                }
                break;
            case 105 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:466: T140
                {
                mT140(); 

                }
                break;
            case 106 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:471: T141
                {
                mT141(); 

                }
                break;
            case 107 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:476: T142
                {
                mT142(); 

                }
                break;
            case 108 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:481: T143
                {
                mT143(); 

                }
                break;
            case 109 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:486: T144
                {
                mT144(); 

                }
                break;
            case 110 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:491: T145
                {
                mT145(); 

                }
                break;
            case 111 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:496: T146
                {
                mT146(); 

                }
                break;
            case 112 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:501: T147
                {
                mT147(); 

                }
                break;
            case 113 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:506: T148
                {
                mT148(); 

                }
                break;
            case 114 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:511: T149
                {
                mT149(); 

                }
                break;
            case 115 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:516: T150
                {
                mT150(); 

                }
                break;
            case 116 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:521: T151
                {
                mT151(); 

                }
                break;
            case 117 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:526: T152
                {
                mT152(); 

                }
                break;
            case 118 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:531: T153
                {
                mT153(); 

                }
                break;
            case 119 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:536: T154
                {
                mT154(); 

                }
                break;
            case 120 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:541: T155
                {
                mT155(); 

                }
                break;
            case 121 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:546: T156
                {
                mT156(); 

                }
                break;
            case 122 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:551: IRI_REF
                {
                mIRI_REF(); 

                }
                break;
            case 123 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:559: PNAME_NS
                {
                mPNAME_NS(); 

                }
                break;
            case 124 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:568: PNAME_LN
                {
                mPNAME_LN(); 

                }
                break;
            case 125 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:577: BLANK_NODE_LABEL
                {
                mBLANK_NODE_LABEL(); 

                }
                break;
            case 126 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:594: VAR1
                {
                mVAR1(); 

                }
                break;
            case 127 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:599: VAR2
                {
                mVAR2(); 

                }
                break;
            case 128 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:604: LANGTAG
                {
                mLANGTAG(); 

                }
                break;
            case 129 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:612: INTEGER
                {
                mINTEGER(); 

                }
                break;
            case 130 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:620: DECIMAL
                {
                mDECIMAL(); 

                }
                break;
            case 131 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:628: DOUBLE
                {
                mDOUBLE(); 

                }
                break;
            case 132 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:635: INTEGER_POSITIVE
                {
                mINTEGER_POSITIVE(); 

                }
                break;
            case 133 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:652: DECIMAL_POSITIVE
                {
                mDECIMAL_POSITIVE(); 

                }
                break;
            case 134 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:669: DOUBLE_POSITIVE
                {
                mDOUBLE_POSITIVE(); 

                }
                break;
            case 135 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:685: INTEGER_NEGATIVE
                {
                mINTEGER_NEGATIVE(); 

                }
                break;
            case 136 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:702: DECIMAL_NEGATIVE
                {
                mDECIMAL_NEGATIVE(); 

                }
                break;
            case 137 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:719: DOUBLE_NEGATIVE
                {
                mDOUBLE_NEGATIVE(); 

                }
                break;
            case 138 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:735: EXPONENT
                {
                mEXPONENT(); 

                }
                break;
            case 139 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:744: STRING_LITERAL1
                {
                mSTRING_LITERAL1(); 

                }
                break;
            case 140 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:760: STRING_LITERAL2
                {
                mSTRING_LITERAL2(); 

                }
                break;
            case 141 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:776: STRING_LITERAL_LONG1
                {
                mSTRING_LITERAL_LONG1(); 

                }
                break;
            case 142 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:797: STRING_LITERAL_LONG2
                {
                mSTRING_LITERAL_LONG2(); 

                }
                break;
            case 143 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:818: ECHAR
                {
                mECHAR(); 

                }
                break;
            case 144 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:824: NIL
                {
                mNIL(); 

                }
                break;
            case 145 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:828: ANON
                {
                mANON(); 

                }
                break;
            case 146 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:833: PN_CHARS_U
                {
                mPN_CHARS_U(); 

                }
                break;
            case 147 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:844: VARNAME
                {
                mVARNAME(); 

                }
                break;
            case 148 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:852: PN_PREFIX
                {
                mPN_PREFIX(); 

                }
                break;
            case 149 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:862: PN_LOCAL
                {
                mPN_LOCAL(); 

                }
                break;
            case 150 :
                // C:\\Users\\UserTripleStore\\Documents\\NetBeansProjects\\JavaApplication12\\src\\main\\grammar\\Sparql.g:1:871: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA15 dfa15 = new DFA15(this);
    protected DFA34 dfa34 = new DFA34(this);
    static final String DFA15_eotS =
        "\5\uffff";
    static final String DFA15_eofS =
        "\5\uffff";
    static final String DFA15_minS =
        "\2\56\3\uffff";
    static final String DFA15_maxS =
        "\1\71\1\145\3\uffff";
    static final String DFA15_acceptS =
        "\2\uffff\1\2\1\3\1\1";
    static final String DFA15_specialS =
        "\5\uffff}>";
    static final String[] DFA15_transitionS = {
            "\1\2\1\uffff\12\1",
            "\1\4\1\uffff\12\1\13\uffff\1\3\37\uffff\1\3",
            "",
            "",
            ""
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "584:1: DOUBLE : ( ( DIGIT )+ '.' ( DIGIT )* EXPONENT | '.' ( DIGIT )+ EXPONENT | ( DIGIT )+ EXPONENT );";
        }
    }
    static final String DFA34_eotS =
        "\1\uffff\5\100\1\124\1\100\2\uffff\2\100\2\uffff\7\100\1\153\2\100"+
        "\2\uffff\1\164\1\166\1\uffff\1\170\1\172\1\175\1\177\1\u0080\3\uffff"+
        "\1\u0083\1\u0086\1\u0087\11\100\1\u0098\1\100\2\uffff\1\u009e\1"+
        "\100\4\uffff\4\105\1\u00aa\1\uffff\1\105\1\uffff\1\105\1\u00ab\1"+
        "\uffff\15\105\2\uffff\1\105\1\u00c3\23\105\1\u00db\1\uffff\10\105"+
        "\7\uffff\1\u00e6\6\uffff\1\u00ea\5\uffff\1\u00eb\1\uffff\1\105\1"+
        "\u00f0\2\105\1\u00ab\1\u0090\1\uffff\1\105\1\u00f5\5\105\3\uffff"+
        "\1\105\1\uffff\1\105\1\uffff\1\u00db\1\u009e\1\105\1\u00a3\1\uffff"+
        "\1\u00a5\1\uffff\4\105\2\uffff\1\105\1\u0111\6\105\1\u0118\2\105"+
        "\1\u011e\10\105\1\u0127\1\u0128\1\u0129\1\uffff\1\u012a\10\105\1"+
        "\u0135\1\u0136\14\105\2\uffff\2\105\1\u0145\2\105\1\u0149\1\105"+
        "\1\u014b\1\u014c\2\uffff\2\u014e\2\uffff\1\u0150\1\uffff\1\u0150"+
        "\1\u0151\1\uffff\2\105\1\u0090\1\105\1\uffff\11\105\1\u00db\2\u009c"+
        "\1\u00dc\2\uffff\1\105\1\u0162\1\105\1\u0164\10\105\1\uffff\6\105"+
        "\1\uffff\3\105\1\u0177\1\105\1\uffff\1\105\1\u017b\1\105\1\u017d"+
        "\4\105\4\uffff\5\105\1\u0187\3\105\1\u018b\2\uffff\10\105\1\u0195"+
        "\5\105\1\uffff\1\105\1\u019c\1\105\1\uffff\1\105\2\uffff\1\u014e"+
        "\1\uffff\1\u0150\2\uffff\2\105\1\u01a2\7\105\1\u01aa\1\105\1\u009c"+
        "\2\u00dc\1\u01ac\1\uffff\1\u01ad\1\uffff\4\105\1\u01b2\15\105\1"+
        "\uffff\3\105\1\uffff\1\105\1\uffff\1\u01c4\2\105\1\u01c7\3\105\1"+
        "\u01cb\1\105\1\uffff\1\u01cd\1\105\1\u01cf\1\uffff\1\u01d0\1\u01d1"+
        "\1\u01d3\1\u01d4\3\105\1\u01d8\1\105\1\uffff\1\u01da\1\u01db\1\105"+
        "\1\u01dd\1\u01de\1\u01df\1\uffff\1\u01e0\1\105\1\u01e2\2\105\1\uffff"+
        "\3\105\1\u01e8\1\u01e9\2\105\1\uffff\1\u01ec\2\uffff\1\u01ed\3\105"+
        "\1\uffff\1\105\1\u01f2\2\105\1\u01f5\1\u01f6\3\105\1\u01fa\1\u01fb"+
        "\1\u01fc\1\u01fd\1\u01fe\3\105\1\uffff\2\105\1\uffff\2\105\1\u0206"+
        "\1\uffff\1\105\1\uffff\1\u0208\3\uffff\1\105\2\uffff\1\u020a\1\105"+
        "\1\u020c\1\uffff\1\105\2\uffff\1\u020e\4\uffff\1\105\1\uffff\1\105"+
        "\1\u0211\3\105\2\uffff\2\105\2\uffff\1\u0217\1\105\1\u0219\1\u021a"+
        "\1\uffff\2\105\2\uffff\1\u021d\1\u021e\1\105\5\uffff\3\105\1\u0223"+
        "\1\u0224\2\105\1\uffff\1\105\1\uffff\1\105\1\uffff\1\105\1\uffff"+
        "\1\105\1\uffff\1\u022b\1\105\1\uffff\2\105\1\u022f\2\105\1\uffff"+
        "\1\105\2\uffff\1\105\1\u0234\2\uffff\1\105\1\u0236\1\u0237\1\u0238"+
        "\2\uffff\1\u0239\1\105\1\u023b\1\105\1\u023d\1\105\1\uffff\1\105"+
        "\1\u0240\1\u0241\1\uffff\2\105\1\u0244\1\u0245\1\uffff\1\u0246\4"+
        "\uffff\1\u0247\1\uffff\1\105\1\uffff\2\105\2\uffff\1\u024b\1\u024c"+
        "\4\uffff\3\105\2\uffff\1\105\1\u0251\1\105\1\u0253\1\uffff\1\105"+
        "\1\uffff\1\105\1\u0256\1\uffff";
    static final String DFA34_eofS =
        "\u0257\uffff";
    static final String DFA34_minS =
        "\1\11\5\55\1\11\1\55\2\uffff\2\55\2\uffff\7\55\1\60\2\55\2\uffff"+
        "\1\55\1\174\1\uffff\1\136\1\60\1\56\1\75\1\11\3\uffff\1\0\1\75\1"+
        "\56\1\55\1\53\6\55\1\53\1\60\1\55\2\uffff\2\55\2\0\2\uffff\5\55"+
        "\1\uffff\4\55\1\uffff\15\55\2\uffff\25\55\1\60\1\uffff\10\55\7\uffff"+
        "\1\56\1\60\5\uffff\1\0\5\uffff\1\56\1\60\6\55\1\uffff\7\55\3\uffff"+
        "\1\55\1\uffff\1\55\1\uffff\2\55\1\53\1\47\1\uffff\1\42\1\uffff\4"+
        "\55\2\uffff\27\55\1\uffff\27\55\2\uffff\11\55\2\uffff\2\60\2\uffff"+
        "\1\60\1\uffff\1\60\1\55\1\uffff\4\55\1\uffff\12\55\1\53\1\60\1\55"+
        "\2\uffff\14\55\1\uffff\6\55\1\uffff\5\55\1\uffff\10\55\4\uffff\12"+
        "\55\2\uffff\16\55\1\uffff\3\55\1\uffff\1\55\2\uffff\1\60\1\uffff"+
        "\1\60\2\uffff\14\55\1\60\3\55\1\uffff\1\55\1\uffff\22\55\1\uffff"+
        "\3\55\1\uffff\1\55\1\uffff\11\55\1\uffff\3\55\1\uffff\11\55\1\uffff"+
        "\6\55\1\uffff\5\55\1\uffff\7\55\1\uffff\1\55\2\uffff\4\55\1\uffff"+
        "\21\55\1\uffff\2\55\1\uffff\3\55\1\uffff\1\55\1\uffff\1\55\3\uffff"+
        "\1\55\2\uffff\3\55\1\uffff\1\55\2\uffff\1\55\4\uffff\1\55\1\uffff"+
        "\5\55\2\uffff\2\55\2\uffff\4\55\1\uffff\2\55\2\uffff\3\55\5\uffff"+
        "\7\55\1\uffff\1\55\1\uffff\1\55\1\uffff\1\55\1\uffff\1\55\1\uffff"+
        "\2\55\1\uffff\5\55\1\uffff\1\55\2\uffff\2\55\2\uffff\4\55\2\uffff"+
        "\6\55\1\uffff\3\55\1\uffff\4\55\1\uffff\1\55\4\uffff\1\55\1\uffff"+
        "\1\55\1\uffff\2\55\2\uffff\2\55\4\uffff\3\55\2\uffff\4\55\1\uffff"+
        "\1\55\1\uffff\2\55\1\uffff";
    static final String DFA34_maxS =
        "\6\ufffd\1\51\1\ufffd\2\uffff\2\ufffd\2\uffff\7\ufffd\1\71\2\ufffd"+
        "\2\uffff\1\ufffd\1\174\1\uffff\1\136\1\ufffd\1\71\1\75\1\135\3\uffff"+
        "\1\ufffe\1\75\1\71\13\ufffd\2\uffff\2\ufffd\2\ufffe\2\uffff\5\ufffd"+
        "\1\uffff\4\ufffd\1\uffff\15\ufffd\2\uffff\25\ufffd\1\145\1\uffff"+
        "\10\ufffd\7\uffff\1\145\1\71\5\uffff\1\ufffe\5\uffff\1\145\1\71"+
        "\6\ufffd\1\uffff\7\ufffd\3\uffff\1\ufffd\1\uffff\1\ufffd\1\uffff"+
        "\3\ufffd\1\47\1\uffff\1\42\1\uffff\4\ufffd\2\uffff\27\ufffd\1\uffff"+
        "\27\ufffd\2\uffff\11\ufffd\2\uffff\2\145\2\uffff\1\145\1\uffff\1"+
        "\145\1\ufffd\1\uffff\4\ufffd\1\uffff\12\ufffd\2\71\1\ufffd\2\uffff"+
        "\14\ufffd\1\uffff\6\ufffd\1\uffff\5\ufffd\1\uffff\10\ufffd\4\uffff"+
        "\12\ufffd\2\uffff\16\ufffd\1\uffff\3\ufffd\1\uffff\1\ufffd\2\uffff"+
        "\1\145\1\uffff\1\145\2\uffff\14\ufffd\1\71\3\ufffd\1\uffff\1\ufffd"+
        "\1\uffff\22\ufffd\1\uffff\3\ufffd\1\uffff\1\ufffd\1\uffff\11\ufffd"+
        "\1\uffff\3\ufffd\1\uffff\11\ufffd\1\uffff\6\ufffd\1\uffff\5\ufffd"+
        "\1\uffff\7\ufffd\1\uffff\1\ufffd\2\uffff\4\ufffd\1\uffff\21\ufffd"+
        "\1\uffff\2\ufffd\1\uffff\3\ufffd\1\uffff\1\ufffd\1\uffff\1\ufffd"+
        "\3\uffff\1\ufffd\2\uffff\3\ufffd\1\uffff\1\ufffd\2\uffff\1\ufffd"+
        "\4\uffff\1\ufffd\1\uffff\5\ufffd\2\uffff\2\ufffd\2\uffff\4\ufffd"+
        "\1\uffff\2\ufffd\2\uffff\3\ufffd\5\uffff\7\ufffd\1\uffff\1\ufffd"+
        "\1\uffff\1\ufffd\1\uffff\1\ufffd\1\uffff\1\ufffd\1\uffff\2\ufffd"+
        "\1\uffff\5\ufffd\1\uffff\1\ufffd\2\uffff\2\ufffd\2\uffff\4\ufffd"+
        "\2\uffff\6\ufffd\1\uffff\3\ufffd\1\uffff\4\ufffd\1\uffff\1\ufffd"+
        "\4\uffff\1\ufffd\1\uffff\1\ufffd\1\uffff\2\ufffd\2\uffff\2\ufffd"+
        "\4\uffff\3\ufffd\2\uffff\4\ufffd\1\uffff\1\ufffd\1\uffff\2\ufffd"+
        "\1\uffff";
    static final String DFA34_acceptS =
        "\10\uffff\1\10\1\11\2\uffff\1\14\1\15\12\uffff\1\45\1\46\2\uffff"+
        "\1\51\5\uffff\1\57\1\61\1\62\16\uffff\1\177\1\u0080\4\uffff\1\u008f"+
        "\1\u0096\5\uffff\1\u0092\4\uffff\1\u0093\15\uffff\1\u0090\1\6\26"+
        "\uffff\1\33\10\uffff\1\47\1\60\1\50\1\167\1\52\1\176\1\53\2\uffff"+
        "\1\54\1\63\1\55\1\56\1\u0091\1\uffff\1\64\1\172\1\67\1\65\1\70\10"+
        "\uffff\1\u008a\7\uffff\1\173\1\174\1\175\1\uffff\1\u0095\1\uffff"+
        "\1\u0081\4\uffff\1\u008b\1\uffff\1\u008c\4\uffff\1\23\1\u0094\27"+
        "\uffff\1\7\27\uffff\1\u0082\1\u0083\11\uffff\1\u0084\1\u0086\2\uffff"+
        "\1\66\1\u0087\1\uffff\1\u0089\2\uffff\1\141\4\uffff\1\127\15\uffff"+
        "\1\u008d\1\u008e\14\uffff\1\71\6\uffff\1\160\5\uffff\1\122\10\uffff"+
        "\1\102\1\17\1\26\1\163\12\uffff\1\130\1\156\16\uffff\1\77\3\uffff"+
        "\1\161\1\uffff\1\133\1\162\1\uffff\1\u0085\1\uffff\1\u0088\1\76"+
        "\20\uffff\1\40\1\uffff\1\1\22\uffff\1\134\3\uffff\1\27\1\uffff\1"+
        "\101\11\uffff\1\103\3\uffff\1\20\11\uffff\1\72\6\uffff\1\131\5\uffff"+
        "\1\120\7\uffff\1\170\1\uffff\1\75\1\100\4\uffff\1\143\21\uffff\1"+
        "\105\2\uffff\1\152\3\uffff\1\157\1\uffff\1\13\1\uffff\1\104\1\21"+
        "\1\35\1\uffff\1\22\1\123\3\uffff\1\25\1\uffff\1\111\1\30\1\uffff"+
        "\1\41\1\43\1\110\1\42\1\uffff\1\121\5\uffff\1\146\1\145\2\uffff"+
        "\1\171\1\2\4\uffff\1\107\2\uffff\1\37\1\3\3\uffff\1\153\1\137\1"+
        "\135\1\136\1\164\7\uffff\1\106\1\uffff\1\44\1\uffff\1\24\1\uffff"+
        "\1\31\1\uffff\1\32\2\uffff\1\155\5\uffff\1\132\1\uffff\1\115\1\142"+
        "\2\uffff\1\36\1\125\4\uffff\1\154\1\5\6\uffff\1\124\3\uffff\1\147"+
        "\4\uffff\1\117\1\uffff\1\74\1\16\1\4\1\113\1\uffff\1\140\1\uffff"+
        "\1\34\2\uffff\1\126\1\144\2\uffff\1\114\1\116\1\166\1\12\3\uffff"+
        "\1\151\1\150\4\uffff\1\73\1\uffff\1\165\2\uffff\1\112";
    static final String DFA34_specialS =
        "\u0257\uffff}>";
    static final String[] DFA34_transitionS = {
            "\2\72\2\uffff\1\72\22\uffff\1\72\1\40\1\70\1\uffff\1\63\1\uffff"+
            "\1\43\1\67\1\6\1\10\1\11\1\37\1\30\1\47\1\25\1\34\12\65\1\61"+
            "\1\31\1\45\1\44\1\46\1\36\1\64\1\7\1\1\1\12\1\4\1\51\1\16\1"+
            "\20\1\21\1\50\2\66\1\23\1\27\1\17\1\22\1\2\1\66\1\5\1\3\1\53"+
            "\1\26\1\24\1\13\1\66\1\52\1\66\1\41\1\71\1\42\1\35\1\62\1\uffff"+
            "\1\32\3\66\1\60\1\57\2\66\1\55\11\66\1\54\1\56\6\66\1\14\1\33"+
            "\1\15\102\uffff\27\66\1\uffff\37\66\1\uffff\u0208\66\160\uffff"+
            "\16\66\1\uffff\u1c81\66\14\uffff\2\66\142\uffff\u0120\66\u0a70"+
            "\uffff\u03f0\66\21\uffff\ua7ff\66\u2100\uffff\u04d0\66\40\uffff"+
            "\u020e\66",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\76\7\101\1\74\4\101"+
            "\1\75\1\73\11\101\1\77\1\101\4\uffff\1\101\1\uffff\32\101\74"+
            "\uffff\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101"+
            "\160\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2"+
            "\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\106\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\114\3\101\1\111\2"+
            "\101\1\113\1\110\12\101\1\107\1\112\5\101\4\uffff\1\101\1\uffff"+
            "\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61"+
            "\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff"+
            "\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\115\3\101\1\116\3"+
            "\101\1\117\21\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105"+
            "\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16"+
            "\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\120\3\101\1\122\11"+
            "\101\1\121\13\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105"+
            "\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16"+
            "\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\2\123\2\uffff\1\123\22\uffff\1\123\10\uffff\1\123",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\101\1\125\20\101"+
            "\1\126\2\101\1\127\4\101\4\uffff\1\101\1\uffff\32\101\74\uffff"+
            "\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160"+
            "\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105"+
            "\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\131\11\101"+
            "\1\130\13\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10"+
            "\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101"+
            "\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120"+
            "\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\7\101\1\132\22\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\133\2\101"+
            "\1\134\5\101\1\135\10\101\4\uffff\1\101\1\uffff\32\101\74\uffff"+
            "\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160"+
            "\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105"+
            "\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\137\15\101\1\136"+
            "\13\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff"+
            "\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff"+
            "\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101"+
            "\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\140\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\142\15\101\1\141"+
            "\13\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff"+
            "\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff"+
            "\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101"+
            "\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\144\11\101"+
            "\1\143\1\101\1\145\10\101\4\uffff\1\101\1\uffff\32\101\74\uffff"+
            "\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160"+
            "\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105"+
            "\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\146\1\101\1\147\5"+
            "\101\1\150\21\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105"+
            "\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16"+
            "\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\151\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff"+
            "\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14"+
            "\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0"+
            "\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e"+
            "\101",
            "\12\152",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\156\12\101"+
            "\1\154\3\101\1\155\2\101\1\157\5\101\4\uffff\1\101\1\uffff\32"+
            "\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff"+
            "\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\163\2\101\1\162\4"+
            "\101\1\160\5\101\1\161\13\101\4\uffff\1\101\1\uffff\32\101\74"+
            "\uffff\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101"+
            "\160\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2"+
            "\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\165",
            "",
            "\1\167",
            "\12\171\7\uffff\32\171\4\uffff\1\171\1\uffff\32\171\105\uffff"+
            "\27\171\1\uffff\37\171\1\uffff\u0208\171\160\uffff\16\171\1"+
            "\uffff\u1c81\171\14\uffff\2\171\142\uffff\u0120\171\u0a70\uffff"+
            "\u03f0\171\21\uffff\ua7ff\171\u2100\uffff\u04d0\171\40\uffff"+
            "\u020e\171",
            "\1\174\1\uffff\12\173",
            "\1\176",
            "\2\u0081\2\uffff\1\u0081\22\uffff\1\u0081\74\uffff\1\u0081",
            "",
            "",
            "",
            "\42\u0084\1\uffff\31\u0084\1\uffff\1\u0082\36\u0084\1\uffff"+
            "\1\u0084\1\uffff\1\u0084\1\uffff\32\u0084\3\uffff\uff81\u0084",
            "\1\u0085",
            "\1\u0089\1\uffff\12\u0088",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u008b\13\101"+
            "\1\u008a\10\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105"+
            "\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16"+
            "\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\u0090\1\uffff\1\u008e\1\102\1\uffff\12\u008f\1\61\6\uffff"+
            "\15\101\1\u008c\11\101\1\u008d\2\101\4\uffff\1\101\1\uffff\32"+
            "\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\105\16\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff"+
            "\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0091\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u0093\20\101"+
            "\1\u0092\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff"+
            "\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff"+
            "\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101"+
            "\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\1\u0094\31\101\74\uffff\1\105\10\uffff\27\101\1\uffff"+
            "\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14"+
            "\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0"+
            "\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e"+
            "\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\22\101\1\u0095\7\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\21\101\1\u0096\10\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\1\u0097\31\101\74\uffff\1\105\10\uffff\27\101\1\uffff"+
            "\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14"+
            "\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0"+
            "\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e"+
            "\101",
            "\1\u0090\1\uffff\1\u008e\1\102\1\uffff\12\u008f\1\61\6\uffff"+
            "\32\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff"+
            "\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff"+
            "\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101"+
            "\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "\12\u0099\7\uffff\32\u0099\4\uffff\1\u0099\1\uffff\32\u0099"+
            "\105\uffff\27\u0099\1\uffff\37\u0099\1\uffff\u0208\u0099\160"+
            "\uffff\16\u0099\1\uffff\u1c81\u0099\14\uffff\2\u0099\142\uffff"+
            "\u0120\u0099\u0a70\uffff\u03f0\u0099\21\uffff\ua7ff\u0099\u2100"+
            "\uffff\u04d0\u0099\40\uffff\u020e\u0099",
            "\2\u009c\1\uffff\12\u009d\1\u009a\6\uffff\32\u009b\4\uffff\1"+
            "\u009b\1\uffff\32\u009b\74\uffff\1\105\10\uffff\27\u009b\1\uffff"+
            "\37\u009b\1\uffff\u0208\u009b\160\105\16\u009b\1\uffff\u1c81"+
            "\u009b\14\uffff\2\u009b\61\uffff\2\105\57\uffff\u0120\u009b"+
            "\u0a70\uffff\u03f0\u009b\21\uffff\ua7ff\u009b\u2100\uffff\u04d0"+
            "\u009b\40\uffff\u020e\u009b",
            "",
            "",
            "\1\u009c\1\u009f\1\uffff\12\u00a0\7\uffff\4\u009b\1\u00a1\25"+
            "\u009b\4\uffff\1\u009b\1\uffff\4\u009b\1\u00a1\25\u009b\74\uffff"+
            "\1\105\10\uffff\27\u009b\1\uffff\37\u009b\1\uffff\u0208\u009b"+
            "\160\105\16\u009b\1\uffff\u1c81\u009b\14\uffff\2\u009b\61\uffff"+
            "\2\105\57\uffff\u0120\u009b\u0a70\uffff\u03f0\u009b\21\uffff"+
            "\ua7ff\u009b\u2100\uffff\u04d0\u009b\40\uffff\u020e\u009b",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\12\u00a3\1\uffff\2\u00a3\1\uffff\31\u00a3\1\u00a2\uffd7\u00a3",
            "\12\u00a5\1\uffff\2\u00a5\1\uffff\24\u00a5\1\u00a4\uffdc\u00a5",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u00a6\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u00a7\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u00a8\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u00a9\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\104\7\uffff\32\104\4\uffff\1\104\1\uffff"+
            "\32\104\105\uffff\27\104\1\uffff\37\104\1\uffff\u0208\104\160"+
            "\uffff\16\104\1\uffff\u1c81\104\14\uffff\2\104\142\uffff\u0120"+
            "\104\u0a70\uffff\u03f0\104\21\uffff\ua7ff\104\u2100\uffff\u04d0"+
            "\104\40\uffff\u020e\104",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\104\1\61\6\uffff\32\104\4\uffff\1\104"+
            "\1\uffff\32\104\105\uffff\27\104\1\uffff\37\104\1\uffff\u0208"+
            "\104\160\uffff\16\104\1\uffff\u1c81\104\14\uffff\2\104\142\uffff"+
            "\u0120\104\u0a70\uffff\u03f0\104\21\uffff\ua7ff\104\u2100\uffff"+
            "\u04d0\104\40\uffff\u020e\104",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u00ac\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u00ad\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u00ae\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u00b1\10\101"+
            "\1\u00af\3\101\1\u00b2\1\101\1\u00b0\10\101\4\uffff\1\101\1"+
            "\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\101\1\u00b3\12\101"+
            "\1\u00b4\15\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u00b5\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u00b6\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u00b8\4\101"+
            "\1\u00b7\1\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u00b9\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u00ba\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u00bb\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u00bc\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u00be\2\101"+
            "\1\u00bf\10\101\1\u00bd\12\101\4\uffff\1\101\1\uffff\32\101"+
            "\105\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\uffff"+
            "\16\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff\u0120\101"+
            "\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u00c0\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u00c2\7\101"+
            "\1\u00c1\17\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105"+
            "\10\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16"+
            "\101\1\uffff\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\6\101\1\u00c4\23\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u00c7\14\101\1\u00c5"+
            "\6\101\1\u00c6\5\101\4\uffff\1\101\1\uffff\32\101\105\uffff"+
            "\27\101\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16\101\1"+
            "\uffff\u1c81\101\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff"+
            "\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff"+
            "\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u00c8\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u00c9\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u00ca\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u00cb\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u00cc\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u00ce\2\101"+
            "\1\u00cd\3\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u00cf\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u00d0\15\101\1\u00d1"+
            "\13\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff"+
            "\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101"+
            "\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21"+
            "\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u00d2\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\25\101\1\u00d3\4\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u00d4\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u00d5\24\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u00d6\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u00d7\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u00d8\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u00d9\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u00da\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\12\152\13\uffff\1\u00dc\37\uffff\1\u00dc",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u00dd\4\101"+
            "\1\u00de\21\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u00df\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u00e0\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u00e1\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u00e2\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u00e3\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\5\103\1\u00e4\4\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\27\101\1\u00e5\2\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00e8\1\uffff\12\173\13\uffff\1\u00e7\37\uffff\1\u00e7",
            "\12\u00e9",
            "",
            "",
            "",
            "",
            "",
            "\42\u0084\1\uffff\31\u0084\1\uffff\37\u0084\1\uffff\1\u0084"+
            "\1\uffff\1\u0084\1\uffff\32\u0084\3\uffff\uff81\u0084",
            "",
            "",
            "",
            "",
            "",
            "\1\u00ec\1\uffff\12\u0088\13\uffff\1\u00ed\37\uffff\1\u00ed",
            "\12\u00ee",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u00ef\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u00f1\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u00f2\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\u00f3\1\61\6\uffff\32\104\4\uffff\1"+
            "\104\1\uffff\32\104\105\uffff\27\104\1\uffff\37\104\1\uffff"+
            "\u0208\104\160\uffff\16\104\1\uffff\u1c81\104\14\uffff\2\104"+
            "\142\uffff\u0120\104\u0a70\uffff\u03f0\104\21\uffff\ua7ff\104"+
            "\u2100\uffff\u04d0\104\40\uffff\u020e\104",
            "\1\104\1\102\1\uffff\12\u008f\1\61\6\uffff\32\101\4\uffff\1"+
            "\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37"+
            "\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u00f4\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u00f6\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\14\101\1\u00f7\15\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\101\1\u00f8\6\101"+
            "\1\u00fa\2\101\1\u00fc\1\101\1\u00fb\6\101\1\u00f9\5\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\24\101\1\u00fd\5\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\13\101\1\u00fe\16\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "\2\u009c\1\uffff\12\u009d\7\uffff\32\u009b\4\uffff\1\u009b\1"+
            "\uffff\32\u009b\105\uffff\27\u009b\1\uffff\37\u009b\1\uffff"+
            "\u0208\u009b\160\uffff\16\u009b\1\uffff\u1c81\u009b\14\uffff"+
            "\2\u009b\142\uffff\u0120\u009b\u0a70\uffff\u03f0\u009b\21\uffff"+
            "\ua7ff\u009b\u2100\uffff\u04d0\u009b\40\uffff\u020e\u009b",
            "",
            "\2\u009c\1\uffff\12\u009d\7\uffff\32\u009b\4\uffff\1\u009b\1"+
            "\uffff\32\u009b\105\uffff\27\u009b\1\uffff\37\u009b\1\uffff"+
            "\u0208\u009b\160\uffff\16\u009b\1\uffff\u1c81\u009b\14\uffff"+
            "\2\u009b\142\uffff\u0120\u009b\u0a70\uffff\u03f0\u009b\21\uffff"+
            "\ua7ff\u009b\u2100\uffff\u04d0\u009b\40\uffff\u020e\u009b",
            "",
            "\2\u009c\1\uffff\12\u00ff\7\uffff\4\u009c\1\u0100\25\u009c\4"+
            "\uffff\1\u009c\1\uffff\4\u009c\1\u0100\25\u009c\105\uffff\27"+
            "\u009c\1\uffff\37\u009c\1\uffff\u0208\u009c\160\uffff\16\u009c"+
            "\1\uffff\u1c81\u009c\14\uffff\2\u009c\142\uffff\u0120\u009c"+
            "\u0a70\uffff\u03f0\u009c\21\uffff\ua7ff\u009c\u2100\uffff\u04d0"+
            "\u009c\40\uffff\u020e\u009c",
            "\1\u009c\1\u009f\1\uffff\12\u00a0\7\uffff\4\u009b\1\u00a1\25"+
            "\u009b\4\uffff\1\u009b\1\uffff\4\u009b\1\u00a1\25\u009b\74\uffff"+
            "\1\105\10\uffff\27\u009b\1\uffff\37\u009b\1\uffff\u0208\u009b"+
            "\160\105\16\u009b\1\uffff\u1c81\u009b\14\uffff\2\u009b\61\uffff"+
            "\2\105\57\uffff\u0120\u009b\u0a70\uffff\u03f0\u009b\21\uffff"+
            "\ua7ff\u009b\u2100\uffff\u04d0\u009b\40\uffff\u020e\u009b",
            "\1\u00dc\1\uffff\1\u0101\1\u009c\1\uffff\12\u0102\7\uffff\32"+
            "\u009b\4\uffff\1\u009b\1\uffff\32\u009b\105\uffff\27\u009b\1"+
            "\uffff\37\u009b\1\uffff\u0208\u009b\160\uffff\16\u009b\1\uffff"+
            "\u1c81\u009b\14\uffff\2\u009b\142\uffff\u0120\u009b\u0a70\uffff"+
            "\u03f0\u009b\21\uffff\ua7ff\u009b\u2100\uffff\u04d0\u009b\40"+
            "\uffff\u020e\u009b",
            "\1\u0103",
            "",
            "\1\u0104",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0105\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u0106\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u0107\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0108\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u0109\24\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u0110\1\u010f\1\101"+
            "\1\u010d\1\u010c\6\101\1\u010e\6\101\1\u010b\1\101\1\u010a\5"+
            "\101\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27"+
            "\101\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff"+
            "\u1c81\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101"+
            "\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0"+
            "\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0112\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0113\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\25\101\1\u0114\4\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u0115\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u0116\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u0117\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\1\103\1\u011c\1\u011a\1\u011b\1\103\1\u0119"+
            "\4\103\1\61\6\uffff\32\101\4\uffff\1\101\1\uffff\32\101\105"+
            "\uffff\27\101\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16"+
            "\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\17\101\1\u011d\12\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u011f\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0120\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0121\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u0122\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0123\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u0124\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0125\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0126\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u012d\17\101"+
            "\1\u012c\1\u012b\6\101\4\uffff\1\101\1\uffff\32\101\105\uffff"+
            "\27\101\1\uffff\37\101\1\uffff\u0208\101\160\uffff\16\101\1"+
            "\uffff\u1c81\101\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff"+
            "\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff"+
            "\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u012e\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u012f\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u0130\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0131\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0132\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u0133\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u0134\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0137\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\17\101\1\u0138\12\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0139\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u013a\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u013b\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u013c\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u013d\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u013e\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\6\101\1\u013f\23\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u0140\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u0141\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0142\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0143\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u0144\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u0146\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u0147\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0148\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u014a\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\12\u014d\13\uffff\1\u00e7\37\uffff\1\u00e7",
            "\12\u00e9\13\uffff\1\u00e7\37\uffff\1\u00e7",
            "",
            "",
            "\12\u014f\13\uffff\1\u00ed\37\uffff\1\u00ed",
            "",
            "\12\u00ee\13\uffff\1\u00ed\37\uffff\1\u00ed",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u0152\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u0153\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\u00f3\1\61\6\uffff\32\104\4\uffff\1"+
            "\104\1\uffff\32\104\105\uffff\27\104\1\uffff\37\104\1\uffff"+
            "\u0208\104\160\uffff\16\104\1\uffff\u1c81\104\14\uffff\2\104"+
            "\142\uffff\u0120\104\u0a70\uffff\u03f0\104\21\uffff\ua7ff\104"+
            "\u2100\uffff\u04d0\104\40\uffff\u020e\104",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0154\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0155\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\4\101\1\u0156\25\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u0157\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0158\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0159\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u015a\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u015b\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\4\101\1\u015c\25\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\22\101\1\u015d\7\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\2\u009c\1\uffff\12\u00ff\7\uffff\4\u009c\1\u0100\25\u009c\4"+
            "\uffff\1\u009c\1\uffff\4\u009c\1\u0100\25\u009c\105\uffff\27"+
            "\u009c\1\uffff\37\u009c\1\uffff\u0208\u009c\160\uffff\16\u009c"+
            "\1\uffff\u1c81\u009c\14\uffff\2\u009c\142\uffff\u0120\u009c"+
            "\u0a70\uffff\u03f0\u009c\21\uffff\ua7ff\u009c\u2100\uffff\u04d0"+
            "\u009c\40\uffff\u020e\u009c",
            "\1\u00dc\1\uffff\1\u015e\2\uffff\12\u015f",
            "\12\u0160",
            "\2\u009c\1\uffff\12\u0102\7\uffff\32\u009b\4\uffff\1\u009b\1"+
            "\uffff\32\u009b\74\uffff\1\105\10\uffff\27\u009b\1\uffff\37"+
            "\u009b\1\uffff\u0208\u009b\160\105\16\u009b\1\uffff\u1c81\u009b"+
            "\14\uffff\2\u009b\61\uffff\2\105\57\uffff\u0120\u009b\u0a70"+
            "\uffff\u03f0\u009b\21\uffff\ua7ff\u009b\u2100\uffff\u04d0\u009b"+
            "\40\uffff\u020e\u009b",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u0161\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0163\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u0165\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0166\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0167\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0168\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0169\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u016a\3\101\1\u016b"+
            "\25\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff"+
            "\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101"+
            "\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21"+
            "\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u016c\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u016d\24\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u016e\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u016f\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u0170\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0171\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0172\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0173\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\1\103\1\u0174\10\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\5\103\1\u0175\4\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\10\103\1\u0176\1\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u0178\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0179\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u017a\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u017c\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u017e\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u017f\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0180\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\27\101\1\u0181\2\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u0182\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0183\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u0184\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0185\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0186\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0188\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0189\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u018a\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u018c\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\7\101\1\u018d\22\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\17\101\1\u018e\12\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u018f\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0190\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u0191\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0192\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0193\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u0194\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101"+
            "\1\uffff\37\101\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81"+
            "\101\14\uffff\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70"+
            "\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40"+
            "\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0196\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0197\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0198\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u0199\24\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u019a\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u019b\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u019d\1\u019e"+
            "\6\101\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff"+
            "\37\101\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101"+
            "\14\uffff\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21"+
            "\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\7\101\1\u019f\22\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\12\u014d\13\uffff\1\u00e7\37\uffff\1\u00e7",
            "",
            "\12\u014f\13\uffff\1\u00ed\37\uffff\1\u00ed",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u01a0\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01a1\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\31\101\1\u01a3\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01a4\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u01a5\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u01a6\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u01a7\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\14\101\1\u01a8\15\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01a9\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\4\101\1\u01ab\25\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\12\u015f",
            "\2\u009c\1\uffff\12\u015f\7\uffff\32\u009c\4\uffff\1\u009c\1"+
            "\uffff\32\u009c\105\uffff\27\u009c\1\uffff\37\u009c\1\uffff"+
            "\u0208\u009c\160\uffff\16\u009c\1\uffff\u1c81\u009c\14\uffff"+
            "\2\u009c\142\uffff\u0120\u009c\u0a70\uffff\u03f0\u009c\21\uffff"+
            "\ua7ff\u009c\u2100\uffff\u04d0\u009c\40\uffff\u020e\u009c",
            "\2\u009c\1\uffff\12\u0160\7\uffff\32\u009c\4\uffff\1\u009c\1"+
            "\uffff\32\u009c\105\uffff\27\u009c\1\uffff\37\u009c\1\uffff"+
            "\u0208\u009c\160\uffff\16\u009c\1\uffff\u1c81\u009c\14\uffff"+
            "\2\u009c\142\uffff\u0120\u009c\u0a70\uffff\u03f0\u009c\21\uffff"+
            "\ua7ff\u009c\u2100\uffff\u04d0\u009c\40\uffff\u020e\u009c",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\27\101\1\u01ae\2\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u01af\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u01b0\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u01b1\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u01b3\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u01b4\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u01b5\24\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01b6\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01b7\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01b8\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u01b9\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u01ba\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u01bb\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u01bc\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\2\103\1\u01bd\7\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\6\103\1\u01be\3\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\4\103\1\u01bf\5\103\1\61\6\uffff\32\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01c0\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\30\101\1\u01c1\1\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u01c2\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u01c3\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u01c5\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01c6\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u01c8\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u01c9\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01ca\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u01cc\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u01ce\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\u01d2"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\6\101\1\u01d5\23\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u01d6\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01d7\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u01d9\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u01dc\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01e1\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01e3\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u01e4\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u01e5\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\4\101\1\u01e6\25\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u01e7\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01ea\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01eb\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u01ee\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u01ef\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u01f0\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\6\101\1\u01f1\23\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u01f3\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01f4\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u01f7\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u01f8\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u01f9\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\17\101\1\u01ff\12\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\101\1\u0200\30\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0201\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0202\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\3\101\1\u0203\26\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0204\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0205\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0207\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0209\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u020b\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u020d\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u020f\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\u0210"+
            "\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u0212\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\21\101\1\u0213\10\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\12\101\1\u0214\17\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0215\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0216\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0218\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u021b\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u021c\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u021f\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0220\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0221\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0222\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u0225\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0226\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0227\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u0228\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u0229\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u022a\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\5\101\1\u022c\24\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u022d\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\14\101\1\u022e\15\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u0230\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u0231\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u0232\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0233\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0235\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u023a\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\15\101\1\u023c\14\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\7\101\1\u023e\22\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\16\101\1\u023f\13\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0242\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\13\101\1\u0243\16\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\2\101\1\u0248\27\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\4\101\1\u0249\25\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u024a\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\1\u024d\31\101\4\uffff"+
            "\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff"+
            "\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101"+
            "\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101"+
            "\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\22\101\1\u024e\7\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\u024f"+
            "\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101\1\uffff\u0208"+
            "\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff\2\101\142\uffff"+
            "\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff\101\u2100\uffff"+
            "\u04d0\101\40\uffff\u020e\101",
            "",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\23\101\1\u0250\6\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\24\101\1\u0252\5\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\21\101\1\u0254\10\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\10\101\1\u0255\21\101"+
            "\4\uffff\1\101\1\uffff\32\101\105\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\uffff\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\142\uffff\u0120\101\u0a70\uffff\u03f0\101\21\uffff\ua7ff"+
            "\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            "\1\104\1\102\1\uffff\12\103\1\61\6\uffff\32\101\4\uffff\1\101"+
            "\1\uffff\32\101\74\uffff\1\105\10\uffff\27\101\1\uffff\37\101"+
            "\1\uffff\u0208\101\160\105\16\101\1\uffff\u1c81\101\14\uffff"+
            "\2\101\61\uffff\2\105\57\uffff\u0120\101\u0a70\uffff\u03f0\101"+
            "\21\uffff\ua7ff\101\u2100\uffff\u04d0\101\40\uffff\u020e\101",
            ""
    };

    static final short[] DFA34_eot = DFA.unpackEncodedString(DFA34_eotS);
    static final short[] DFA34_eof = DFA.unpackEncodedString(DFA34_eofS);
    static final char[] DFA34_min = DFA.unpackEncodedStringToUnsignedChars(DFA34_minS);
    static final char[] DFA34_max = DFA.unpackEncodedStringToUnsignedChars(DFA34_maxS);
    static final short[] DFA34_accept = DFA.unpackEncodedString(DFA34_acceptS);
    static final short[] DFA34_special = DFA.unpackEncodedString(DFA34_specialS);
    static final short[][] DFA34_transition;

    static {
        int numStates = DFA34_transitionS.length;
        DFA34_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA34_transition[i] = DFA.unpackEncodedString(DFA34_transitionS[i]);
        }
    }

    class DFA34 extends DFA {

        public DFA34(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 34;
            this.eot = DFA34_eot;
            this.eof = DFA34_eof;
            this.min = DFA34_min;
            this.max = DFA34_max;
            this.accept = DFA34_accept;
            this.special = DFA34_special;
            this.transition = DFA34_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T36 | T37 | T38 | T39 | T40 | T41 | T42 | T43 | T44 | T45 | T46 | T47 | T48 | T49 | T50 | T51 | T52 | T53 | T54 | T55 | T56 | T57 | T58 | T59 | T60 | T61 | T62 | T63 | T64 | T65 | T66 | T67 | T68 | T69 | T70 | T71 | T72 | T73 | T74 | T75 | T76 | T77 | T78 | T79 | T80 | T81 | T82 | T83 | T84 | T85 | T86 | T87 | T88 | T89 | T90 | T91 | T92 | T93 | T94 | T95 | T96 | T97 | T98 | T99 | T100 | T101 | T102 | T103 | T104 | T105 | T106 | T107 | T108 | T109 | T110 | T111 | T112 | T113 | T114 | T115 | T116 | T117 | T118 | T119 | T120 | T121 | T122 | T123 | T124 | T125 | T126 | T127 | T128 | T129 | T130 | T131 | T132 | T133 | T134 | T135 | T136 | T137 | T138 | T139 | T140 | T141 | T142 | T143 | T144 | T145 | T146 | T147 | T148 | T149 | T150 | T151 | T152 | T153 | T154 | T155 | T156 | IRI_REF | PNAME_NS | PNAME_LN | BLANK_NODE_LABEL | VAR1 | VAR2 | LANGTAG | INTEGER | DECIMAL | DOUBLE | INTEGER_POSITIVE | DECIMAL_POSITIVE | DOUBLE_POSITIVE | INTEGER_NEGATIVE | DECIMAL_NEGATIVE | DOUBLE_NEGATIVE | EXPONENT | STRING_LITERAL1 | STRING_LITERAL2 | STRING_LITERAL_LONG1 | STRING_LITERAL_LONG2 | ECHAR | NIL | ANON | PN_CHARS_U | VARNAME | PN_PREFIX | PN_LOCAL | WS );";
        }
    }
 

}