package sparqltree;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;

public class SparqlSemanticTree {

    CommonTree semanticTree;
    String[] tokenNames;
    int index;

    public SparqlSemanticTree(String query) {
        try {
            SparqlLexer sparqlLexer = new SparqlLexer(new ANTLRStringStream(query));
            CommonTokenStream tokenStream = new CommonTokenStream(sparqlLexer);
            SparqlParser sparqlParser = new SparqlParser(tokenStream);
            SparqlParser.query_return queryResult = sparqlParser.query();
            this.semanticTree = (CommonTree) queryResult.tree;
            this.index = semanticTree.getChildCount();
            this.tokenNames = sparqlParser.getTokenNames(); 
            System.out.println(this.semanticTree.toStringTree());
//            for (int i = 0; i < tokenNames.length; i++) {
//                String string = tokenNames[i];
//                System.out.println(string);                
//            }
            
        } catch (Exception exception) {
            System.out.println(exception);
        }
    }
    
    public String getValue(int index){
        return (index < this.index) ? semanticTree.getChild(index).getText() : null;
    }

    public boolean isTripletMember(int index){
        return isTokenTripletMember(semanticTree.getChild(index).getType());
    }
    
    private boolean isTokenTripletMember(int tokenIndex) {
        if((tokenNames[tokenIndex].contains("BLANK_NODE_LABEL")) 
                || (tokenNames[tokenIndex].contains("IRI_REF")) 
                || (tokenNames[tokenIndex].contains("PNAME_LN"))) return true;
        return false;
    }
    
    public boolean isVariable(int index){
        return isTokenVariable(semanticTree.getChild(index).getType());
    }
    
    private boolean isTokenVariable(int tokenIndex){
        return (tokenNames[tokenIndex].contains("VAR"));
    }
    
    public boolean isFunction(int index){
        return isTokenFunction(semanticTree.getChild(index).getType());
    }
    
    public int size(){
        return this.index;
    }
    
    public String getTokenType(int index){
        return tokenNames[semanticTree.getChild(index).getType()];
    }

    private boolean isTokenFunction(int tokenIndex) {
        if((tokenNames[tokenIndex].contains("OPTIONAL")) 
                || (tokenNames[tokenIndex].contains("MINUS")) 
                || (tokenNames[tokenIndex].contains("FILTER"))) return true;
        return false;
    }


}
