package results;

import org.junit.Test;
import static org.junit.Assert.*;
import triple.ItemList;

public class ResultsTableTest {
    
    @Test
    public void createVariablesIntoResultsTable(){
        ResultsTable resultsTable = new ResultsTable();
        resultsTable.addVariable("var1", ResultType.SUBJECT);
        resultsTable.addVariable("var2", ResultType.OBJECT);
        assertEquals(resultsTable.getType("var2"), ResultType.OBJECT);
    }
    
    @Test
    public void setAndGetResultSet(){
        ResultsTable resultsTable = new ResultsTable();
        resultsTable.addVariable("var1", ResultType.OBJECT);
        ItemList<Long> resultSet = new ItemList<>();
        resultSet.add((long)1);
        resultSet.add((long)2);
        resultSet.add((long)3);
        resultsTable.setResults("var1", resultSet);
        resultsTable.addVariable("var2", ResultType.SUBJECT);
        resultsTable.addVariable("var3", ResultType.PREDICATE);
        assertEquals(3, resultsTable.getResults("var1").size());
        assertTrue(3 == resultsTable.getResults("var1").get(2));
    }
    
}
