package sparqlinterpreter;

import org.junit.Test;

public class InterpreterTest {

    @Test
    public void simplePatternDetection(){
        Interpreter interpreter = new Interpreter();
        interpreter.executeQuery("SELECT ?title WHERE {_:a :book ?title}");
    }
    
    @Test
    public void multiplePatternDetection(){
        Interpreter interpreter = new Interpreter();
        interpreter.executeQuery("SELECT ?title WHERE {_:a :book ?title. _:a :book :author1}");
    }
    
    @Test
    public void functionDetection(){
        Interpreter interpreter = new Interpreter();
        interpreter.executeQuery("SELECT ?name {?person rdf:type  foaf:Person . FILTER NOT EXISTS { ?person foaf:name ?name }}");        
    }
    
}
