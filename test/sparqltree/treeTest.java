package sparqltree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class treeTest {

    @Test
    public void getTokenValue() {
        String query = "SELECT ?title WHERE {:popo :book ?title}";
        SparqlSemanticTree semanticTree = new SparqlSemanticTree(query);
        assertEquals("SELECT", semanticTree.getValue(0));
    }

    @Test
    public void askForVariable() {
        String query = "SELECT ?title WHERE {?title :popo :book}";
        SparqlSemanticTree semanticTree = new SparqlSemanticTree(query);
        assertFalse(semanticTree.isVariable(0));
        assertTrue(semanticTree.isVariable(1));
    }

    @Test
    public void askForTripletMember() {
        String query = "SELECT ?title WHERE {<http://example/s> <http://example/p> ?title}";
        SparqlSemanticTree semanticTree = new SparqlSemanticTree(query);
        assertTrue(semanticTree.isTripletMember(4));
        assertFalse(semanticTree.isTripletMember(6));
    }
    
    @Test
    public void askForFunctionFilter() {
        String query = "SELECT ?name {?person rdf:type  foaf:Person . FILTER NOT EXISTS { ?person foaf:name ?name }}";
        SparqlSemanticTree semanticTree = new SparqlSemanticTree(query);  
        assertEquals("FILTER", semanticTree.getValue(7));
        assertTrue(semanticTree.isFunction(7));
    }

}
